<?php 

    include_once 'include/config.php';

    include_once 'include/admin-functions.php';

    $admin = new AdminFunctions();

    $count = $_POST['count'];

?>
<tr>

    <td>

        <?php echo $count+1;?>

    </td>

  
    <td>

        <select class="form-control form-control-sm select2 component_id"
            name="component_id[<?php echo $count; ?>]" onchange="currentStock(this)" required>

        </select>

    </td>

    <td>

        <input type="text" name="current_stock[<?php echo $count;?>]"
            class="form-control form-control-sm current_stock boxSize" required readonly>

    </td>

    <td>

        <input type="number" name="stock_adjust_plus[<?php echo $count;?>]"
            class="form-control form-control-sm stock_adjust_plus boxSize" value="0" required>

    </td>

    <td>

        <input type="number" name="stock_adjust_minus[<?php echo $count;?>]"
            class="form-control form-control-sm stock_adjust_minus boxSize" value="0" onchange="checkQty(this)" required>

    </td>

</tr>

            
 <script>

    $(document).ready(function () {

        $('.select2').select2();
		componentName($('#finishGoodItemName'));
        //currentStock($('.component_id'))
        //checkQty($('.component_id'));

    });


   
 </script>