<?php
    ob_start();
    $id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueBoqById($_GET['id']);
    $detailsData = $admin->getUniqueBoqRawmaterialById($id);
   $detailsDatas = $admin->getUniqueBoqHardwareById($id);
   
   $itemName = $admin->getUniqueItemById($data['item_name']);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title>Invoice</title>
      <link rel="stylesheet" href="assets/css/bootstrap.min.css">

   </head>

   <style>
   th,td {
    border: 1px solid #000;
    text-align: center;
    padding: 8px;
    font-size:10px;
    }
    /* tr{
        background:#2980b9;
    } */
   </style>
   <body style="padding:0; margin:0">

       <table>
           <tr >
               <th style="border:1px solid #000;">COMPONENT:</th>
               <td style="border:1px solid #000; text-align:center;"><?php echo $itemName['item_name'];?></td>
               <td style="border:1px solid white;"></td>
               <th style="border:1px solid #000; border-left:1px solid #000;">UNQ. COMP. CODE:</th>
               <td style="border:1px solid #000;  text-align:center;"><?php echo $data['unq_com_code']?></td>
               <td style="border:1px solid white;"></td>
               <th style="border:1px solid #000;">COATED / UNCATED:</th>
               <td style="border:1px solid #000; text-align:center;"><?php echo $data['coated']?></td>
           </tr>
           <tr>
               <th style="border:1px solid #000;">FINISH WEIGHT NETT:</th>
               <td style="border:1px solid #000; text-align:center;"><?php echo $data['finish_weight_net']?></td>
               <td style="border:1px solid white;"></td>
               <th style="border:1px solid #000;">PRODUCT SELLING RATE W/O TAX:</th>
               <td style="border:1px solid #000; text-align:center;"><?php echo $data['product_sale_rate']?></td>
               <td style="border:1px solid white;"></td>
               <th style="border:1px solid #000;"></th>
               <td style="border:1px solid #000; text-align:center;"></td>
           </tr>
       </table>
       <br>
       <br>

<table>
   <thead style="border:1px solid #000;">
      <tr style="background-color:#0984e3;color:#fff; font-weight:bold;">
         <th>SR</th>
         <th>PRODUCT</th>
         <th>QTY</th>
         <th width="10%">COMPONENT</th>
         <!-- <th>QTY</th> -->
         <th>SIZE</th>
         <th>THK</th>
         <th>TYPE OF</th>
         <th>LENGTH</th>
         <th>QTY</th>
         <th>QTY</th>
         <th>UNIT</th>
         <th>PURCHASE</th>
         <th>RATE</th>
         <th>REMARK</th>
      </tr>
      <tr style="background-color:#FFFF00;color:#0000FF;">
         <th></th>
         <th></th>
         <th>STOCK NORMS</th>
         <th width="10%">REQD. FOR ASSY</th>
         <!-- <th>REQD. OF COMP ASSY</th> -->
         <th>BLANK SIZE</th>
         <th></th>
         <th>MATL TYPE</th>
         <th>OF RM</th>
         <th>PRODUCE FROM RM</th>
         <th>REQD OF RM</th>
         <th></th>
         <th>FROM</th>
         <th>LAST PURCHASE RATE</th>
         <th></th>
      </tr>
      <tr style="background-color:#FFFF00;color:#0000FF;">
         <th></th>
         <th></th>
         <th>(NOS)</th>
         <th width="10%">(NAME)</th>
         <!-- <th>(NOS)</th> -->
         <th>(mm)</th>
         <th>(mm)</th>
         <th></th>
         <th></th>
         <th>(nos)</th>
         <th>(nos)</th>
         <th></th>
         <th>VENDORE NAME NOS.</th>
         <th>COMPONENT</th>
         <th></th>
      </tr>
   </thead>
   <tr>
      <td>1</td>
      <td><?php echo $itemName['item_name'];?></td>
      <td><?php echo $data['stock_norms_qty']?></td>
      <td width="10%"></td>
      <td></td>
      <!-- <td></td> -->
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
   </tr>
   <?php
      while($row = $admin->fetch($detailsData)) {
      
      ?>
   <tr>
      <td></td>
      <td></td>
      <td></td>
      <td width="10%"><?php echo $row['component_name'];?></td>
      <!-- <td><?php echo $row['qty'];?></td> -->
      <td><?php echo $row['size'];?></td>
      <td><?php echo $row['thikness'];?></td>
      <td><?php echo $row['mate_type'];?></td>
      <td><?php echo $row['lenght'];?></td>
      <td><?php echo $row['produce_qty'];?></td>
      <td><?php echo $row['reqd_qty'];?></td>
      <td><?php echo $row['unit'];?></td>
      <td><?php echo $row['Purchase_from'];?></td>
      <td><?php echo $row['Purchase_rate'];?></td>
      <td><?php echo $row['remark'];?></td>
   </tr>
   <?php
      }
      ?>
</table>
       <br><br>
       <!-- <br pagebreak="true"/> -->

       <table>
   <thead style="border:1px solid #000;">
      <tr style="background-color:#0984e3;color:#fff; font-weight:bold;">
         <th>SR</th>
         <th>COMPT</th>
         <th>OPEN NO.</th>
         <th>OPEN</th>
         <th>TOOL SETUP TIME</th>
         <th>CYCLE TIME</th>
         <th>M/C REQD</th>
         <th>MAN REQD</th>
         <th>TOOL</th>
         <th>QUALITY GAUGE</th>
         <th>POKAYOKE</th>
         <th colspan="2">GRINDING FRQUENCY OF TOOLS</th>
         <th>REMARK</th>
      </tr>
      <tr style="background-color:#FFFF00;color:#0000FF;">
         <th></th>
         <th></th>
         <th></th>
         <th></th>
         <th>(SEC)</th>
         <th>(SEC)</th>
         <th></th>
         <th>(nos)</th>
         <th>NUMBER</th>
         <th>(NOS)</th>
         <th>(NOS)</th>
         <th>LAST DONE DATE</th>
         <th>NEXT DUE DATE</th>
         <th></th>
      </tr>
   </thead>
   <?php
      $srno=1;
      while($row = $admin->fetch($detailsDatas)) {
      
      ?>
   <tr>
      <td><?php echo $srno;?></td>
      <td><?php echo $row['component'];?></td>
      <td><?php echo $row['open_no'];?></td>
      <td><?php echo $row['open'];?></td>
      <td><?php echo $row['tool_setup_time'];?></td>
      <td><?php echo $row['cycle_time'];?></td>
      <td><?php echo $row['mc_req'];?></td>
      <td><?php echo $row['man_req'];?></td>
      <td><?php echo $row['tool'];?></td>
      <td><?php echo $row['quality_gauge'];?></td>
      <td><?php echo $row['pokayoke'];?></td>
      <td><?php if($row['last_done_date']=!'0000-00-00'){ echo $row['last_done_date'];}?></td>
      <td><?php if($row['next_due_date']=!'0000-00-00'){ echo $row['next_due_date'];}?></td>
      <td><?php echo $row['remark'];?></td>
   </tr>
   <?php
      $srno++;
      }
      
      ?>
</table>

   </body>
</html>
<?php 
	$invoiceMsg = ob_get_contents();
	ob_end_clean();
?>