<?php
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$pageName = "BOM Master";
$parentPageURL = 'bom-master.php';
$pageURL = 'bom-master-add.php';

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}


$unit  = $admin-> getAllUnit();

// $rawMaterialItem1 = $admin-> getAllrawMaterialItem($_GET['id']);

// $hardwareItem1 = $admin->getAllHardwareItem();


include_once 'csrf.class.php';
$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$itemName = $admin->getAllItemFinishGood($loggedInUserDetailsArr['branch_id']);




if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addBoqMaster($_POST, $_FILES, $loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$parentPageURL."?registersuccess");
	}
}

// print_r();

if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueBoqById($id);
	$boqComponent   = $admin->getUniqueBoqComponentById($id);
    $boqRawmaterial = $admin->getUniqueBoqRawmaterialById($id);
	$boqHardware    = $admin->getUniqueBoqHardwareById($id);


}

// print_r($detailsData);

if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateBoqMaster($_POST,$_FILES,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$parentPageURL."?updatesuccess&edit&id=".$id);
		exit();
	}
}
//    print_r($_POST);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">

	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->
	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->
	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->
	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css" rel="stylesheet">
	
	<!-- Crop Image css -->
	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>
		em{
			color:red;
		}
		.add-more {
			margin-top: 27px;
		}
		.remove-row {
			/* margin-top: 27px; */
			margin-top: 0px;

		}
		.group{
			width:35px!important;
			height: 40px!important;
		}

        .form-control{
            border-bottom: 1px solid blue;
            height:25px!important;
        }

		.boxSize{
            width:200px!important;
        }

        label{
            font-size:11px;
			color: #eb4d4b;
    		/* font-weight: 500; */
			/* text-align: center; */

        }
		.card-title {
			color: #2980b9;
			font-size: 20px;
			font-weight: bold;
		}

        .select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

		.nav-tabs .nav-link.active {
			color: #2980b9;
			background-color: #fff;
			border-color: #2980b9 #2980b9 #fff;
			/* font-weight:bold; */
			/* font-size:18px; */
		}

		.nav-tabs {
			border-bottom: 1px solid #2980b9;
		}

		/* .tableFixHead          { overflow-y: auto; height: 500px; }
		.tableFixHead thead th { position: sticky; top: 0; }

		/* Just common table stuff. Really. */
		/* .tableFixHead table  { border-collapse: collapse; width: 100%; }
		.tableFixHead th, td { padding: 8px 16px; }
		.tableFixHead th     { background:#2980b9; color:#fff; } */

		/* .parent {
			text-align: center;
		} */

	</style>
</head>
<body>
	<!-- Main Wrapper -->
	<div class="main-wrapper">

		<!-- Header -->
		<?php include("include/header.php"); ?>
		<!-- /Header -->

		<!-- Sidebar -->
		<?php include("include/sidebar.php"); ?>
		<!-- /Sidebar -->
		
		<!-- Page Wrapper -->
		<div class="page-wrapper">

			<!-- Page Content -->
			<div class="content container-fluid">

				<!-- Page Header -->
				<div class="page-header">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="page-title"><?php echo $pageName; ?></h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="javascript:;">Master</a></li>
								<li class="breadcrumb-item"><a href="<?php echo $parentPageURL; ?>"> Bom Master</a></li>
								<li class="breadcrumb-item active">
									<?php if(isset($_GET['edit'])) {
										echo 'Edit '.$pageName;
									} else {
										echo 'Add New '.$pageName;
									}
									?>
								</li>
							</ul>
						</div>
						<div class="col-auto float-right ml-auto">
							<a href="<?php echo $parentPageURL; ?>" class="btn add-btn"><i class="fa fa-arrow-left"></i> Back to <?php echo $pageName; ?></a>
						</div>
					</div>
				</div>
				<!-- /Page Header -->

				<?php if(isset($_GET['registersuccess'])){ ?>
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.
					</div><br/>
				<?php } ?>

				<?php if(isset($_GET['registerfail'])){ ?>
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.
					</div><br/>
				<?php } ?>

				<?php if(isset($_GET['updatesuccess'])){ ?>
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.
					</div><br/>
				<?php } ?>

				<?php if(isset($_GET['updatefail'])){ ?>
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.
					</div>
				<?php } ?>
				
				<div class="row">

					<div class="col-lg-12">

						<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

							<div class="card">
								<!-- <div class="card-header">
									<h4 class="card-title mb-0"><?php echo $pageName; ?> Info</h4>
								</div> -->
								<div class="card-body" style="padding-top: 10px;padding-bottom: 0px;">

									<div class="row ">

										<div class="col-sm-5">

											<div class="form-group row">

												<label for="item_name" class="col-sm-4 col-form-label">Iten Name<em>*</em></label>

												<div class="col-sm-8">

													<select class="form-control form-control-sm select2" name="item_name" id="itemNames" onchange="itemName(this)">

														<option value="">Select Item Name</option>
														<?php while ($row = $admin->fetch($itemName)) { ?>
															<option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) && $data['item_name'] == $row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?></option>
														<?php } ?>

													</select>
												</div>

											</div>

											<div class="form-group row">

												<label for="drawing_no" style="margin-right: -8px;" class="col-sm-4 col-form-label">Drawing No<em>*</em></label>

												<div class="col-sm-3">

													<input type="text" style="margin-left: 8px;" name="drawing_no" id="drawing_no" value="<?php if(isset($_GET['edit'])) { echo $data['drawing_no']; } ?>" class="form-control form-control-sm">

												</div>

												<div class="col-sm-5" style="margin-left: -17px;">

													<!-- <input type="file" id="myFile" onchange="photoupload(this)" name="drawing_photo" class="form-control-sm" required> -->
													<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['drawing_photo'])){ echo "required"; } }else{ echo "required"; }?> name="drawing_photo" id="0" data-image-index="0" />

												</div>

											</div>

											<div class="form-group row">

												<label for="finish_weight_net" class="col-sm-4 col-form-label">Finish Wt. Net<em>*</em></label>

												<div class="col-sm-8">

													<input type="text" name="finish_weight_net" id="finish_weight_net" value="<?php if(isset($_GET['edit'])) { echo $data['finish_weight_net']; } ?>" class="form-control form-control-sm">

												</div>

											</div>
										
										</div>


										<div class="col-sm-4">

											<div class="form-group row">

												<label for="unq_com_code" class="col-sm-4 col-form-label">Unq. code <em>*</em></label>

												<div class="col-sm-8">

													<input type="text" name="unq_com_code" id="unq_com_code" value="<?php if(isset($_GET['edit'])) { echo $data['unq_com_code']; } ?>" class="form-control form-control-sm">

												</div>

											</div>

											<div class="form-group row">

												<label for="drawing_no" class="col-sm-4 col-form-label">Product Photo<em>*</em></label>

												<div class="col-sm-8">

													<!-- <input type="file" id="product_photo"  data-image-index="1"  onchange="photoupload(this)" name="product_photo" class="form-control-sm" required> -->
													<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['product_photo'])){ echo "required"; } }else{ echo "required"; }?> name="product_photo" id="100" data-image-index="100" />

												</div>

											</div>

											<div class="form-group row">

												<label for="product_sale_rate" class="col-sm-4 col-form-label">Prod. Sel. Rate<em>*</em></label>

												<div class="col-sm-8">

													<input type="text" name="product_sale_rate" id="product_sale_rate" value="<?php if(isset($_GET['edit'])) { echo $data['product_sale_rate']; } ?>" class="form-control form-control-sm">

												</div>

											</div>
										
										</div>


										<div class="col-sm-3">

											<div class="form-group row">

												<label for="coated" class="col-sm-4 col-form-label">Coating<em>*</em></label>

												<div class="col-sm-8">

													<select class="form-control form-control-sm" name="coated" id="coated">

														<option value="Coated" <?php if(isset($_GET['edit']) and $data['coated']=='Coated') { echo 'selected'; } ?>>Coated</option>

														<option value="Uncoated" <?php if(isset($_GET['edit']) and $data['coated']=='Uncoated') { echo 'selected'; } ?>>Uncoated</option>

													</select>

												</div>

											</div>

											<div class="form-group row">

												<label for="stock_norms_qty" class="col-sm-4 col-form-label">Norm qty<em>*</em></label>

												<div class="col-sm-8">

													<input type="number" name="stock_norms_qty" id="stock_norms_qty" value="<?php if(isset($_GET['edit'])) { echo $data['stock_norms_qty']; } ?>" class="form-control form-control-sm">

												</div>

											</div>	

											<div class="form-group row">


												<?php if(isset($_GET['edit'])){ 
													$drawing_photo = explode(".", $data['drawing_photo']);
													$drawing_photo=$drawing_photo[0].'_crop.'.substr($data['drawing_photo'], strpos($data['drawing_photo'], ".") + 1);
											
													$product_photo = explode(".", $data['product_photo']);
													$product_photo=$product_photo[0].'_crop.'.substr($data['product_photo'], strpos($data['product_photo'], ".") + 1);
												?>


													<a class="btn btn-info btn-sm" target="_blank_" href="boqimage/<?php echo $drawing_photo;?>">Open Drawing Img</a>

													<a class="btn btn-success btn-sm" target="_blank_" href="boqimage/<?php echo $product_photo;?>">Open Product Photo</a>


												<?php }  ?>

											</div>	

										
										</div>
									
									</div>

								
								</div>

								 <!-- Nav pills -->
								<div class="card-body">

								 	<ul class="nav nav-tabs">

									 	<li class="nav-item">

								 			<a class="nav-link active" data-toggle="tab" href="#home">Components</a>

								 		</li>

								 		<li class="nav-item">

								 			<a class="nav-link" data-toggle="tab" href="#menu1">Raw Material</a>

								 		</li>

								 		<li class="nav-item">

								 			<a class="nav-link" data-toggle="tab" href="#menu2">Process Flow</a>

								 		</li>

										

								 	</ul>

								 	<!-- Tab panes -->
								 	<div class="tab-content">

										<div id="home" class="tab-pane active"><br>

											<div class="table-responsive">

    											<table class="table table-bordered" id="components">

													<thead style="background: #2980b9;color: #fff;">

														<tr>

															<th>#</th>
															<th>Component</th>
															<th>Drawing No</th>
															<th>Photo</th>
															<th>Qty Per Piece</th>
															<th>Net Weight</th>
															<th>Category</th>
															<th>Remark</th>
															<th>Action</th>


														</tr>

													</thead>

														<?php if(isset($_GET['edit'])) { 

															$i = 0; 
															$y = 1;

															while($i < 20) {

																$row = $admin->fetch($boqComponent)
														?>

																<tbody>

																	<tr>

																		<td>

																			<?php echo $y;?>

																			<input type="hidden" value="<?php echo $row['id']; ?>" name="componentNameid[<?php echo $i;?>]" class="form-control form-control-sm">

																		</td>


																		<td>

																			<input type="text" name="components[<?php echo $i;?>]" value="<?php echo $row['component']; ?>" class="form-control form-control-sm component boxSize" onkeyup="componentName(this)" required>

																		</td>

																		<td>

																			<select class="form-control form-control-sm select2 boxSize component_drawing_no" name="component_drawing_no[<?php echo $i;?>]" data-no="<?php echo $i;?>" onchange="componentphoto(this)" required>
																			<option value="">Select</option>
																				<?php
																				    $drawingNo = $admin-> getAllDrawingComponentMaster($data['item_name']);

																				while ($row1 = $admin->fetch($drawingNo)) { ?>
																						<option value="<?php echo $row1['component_drawing_no']; ?>" <?php if($row1['component_drawing_no'] == $row['component_drawing_no']) { echo 'selected'; } ?>><?php echo $row1['component_drawing_no']; ?></option>
																				<?php } ?>

																			</select>

																		</td>

																		<td class="componentPhoto<?php echo $i;?>">

																			<!-- <input type="file" id="photo" name="photo[<?php echo $x;?>]" onchange="photoupload(this)" class="form-control-sm" required> -->
																			<!-- <input type="file" class="form-control-sm component_photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['component_photo'])){ echo "required"; } }else{ echo "required"; }?> name="component_photo[<?php echo $x;?>]" id="<?php echo $x;?>" data-image-index="<?php echo $x;?>" /> -->

																		</td>

																		

																		<td>

																			<input type="text" name="qty_per_piece[<?php echo $i;?>]" value="<?php echo $row['qty_per_piece']; ?>" class="form-control form-control-sm qty_per_piece boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="net_weight[<?php echo $i;?>]" value="<?php echo $row['net_weight']; ?>" class="form-control form-control-sm net_weight boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="category[<?php echo $i;?>]" value="<?php echo $row['category']; ?>" class="form-control form-control-sm category boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="component_remarks[<?php echo $i;?>]" value="<?php echo $row['component_remarks']; ?>" class="form-control form-control-sm component_remarks boxSize" required>

																		</td>

																	
																		<td>

																			<button class="btn btn-sm btn-danger remover" onclick="remove1(this)">Remove</buuton>

																		</td>	

																	</tr>

																<tbody>	

														        <input type="hidden" name="details_id[<?php echo $i; ?>]" value="<?php echo $row['id']; ?>">


													        <?php $y++; $i++;} ?>


												        <?php } else { ?>

												

															<?php 

																$y=1;
																for ($x=0; $x < 20; $x++) { 

																
															?>
												
																<tbody>

																	<tr>

																		<td>

																			<?php echo $y;?>

																		</td>

																		<td>

																			<input type="text" name="components[<?php echo $x;?>]" class="form-control form-control-sm component boxSize" onkeyup="componentName(this)" required>

																		</td>

																		<td>

																			<!-- <input type="text" name="drawing_no[<?php echo $x;?>]" class="form-control form-control-sm drawing_no boxSize" required> -->

																			<select class="form-control form-control-sm select2 boxSize component_drawing_no" name="component_drawing_no[<?php echo $x;?>]" data-no="<?php echo $x;?>" onchange="componentphoto(this)" required>
																		

																			</select>

																		</td>

																		<td class="componentPhoto<?php echo $x;?>">

																			<!-- <input type="file" id="photo" name="photo[<?php echo $x;?>]" onchange="photoupload(this)" class="form-control-sm" required> -->
																			<!-- <input type="file" class="form-control-sm component_photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['component_photo'])){ echo "required"; } }else{ echo "required"; }?> name="component_photo[<?php echo $x;?>]" id="<?php echo $x;?>" data-image-index="<?php echo $x;?>" /> -->

																		</td>

																		

																		<td>

																			<input type="text" name="qty_per_piece[<?php echo $x;?>]" class="form-control form-control-sm qty_per_piece boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="net_weight[<?php echo $x;?>]" class="form-control form-control-sm net_weight boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="category[<?php echo $x;?>]" class="form-control form-control-sm category boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="component_remarks[<?php echo $x;?>]" class="form-control form-control-sm component_remarks boxSize" required>

																		</td>

																	
																		<td>

																			<button class="btn btn-sm btn-danger remover" onclick="remove1(this)">Remove</buuton>

																		</td>	

																	</tr>

																</tbody>
																

															<?php $y++;}  ?>

												        <?php } ?>

												</table>
														
									        </div>

								 		</div>

								 		<div id="menu1" class="tab-pane fade"><br>

											<div class="table-responsive">

    											<table class="table table-bordered" id="rawMaterial">

													<thead style="background: #2980b9;color: #fff;">

														<tr>

															<th>#</th>
															<th>Component Name</th>
															<th>Photo</th>
															<!-- <th>Req. of Comp. Assy</th> -->
															<th>Size</th>
															<th>Thikness</th>
															<th>Mate. Type</th>
															<th>Lenght</th>
															<th>Produce Qty R.M</th>
															<th>Reqd. OF RM</th>
															<th>Unit</th>
															<th>Purchase From</th>
															<th>Last Purchase Rate</th>
															<th>Remark</th>
															<th>Action</th>


														</tr>

														<tr>

															<th></th>
															<th></th>
															<th></th>
															<!-- <th>(nos)</th> -->
															<th>(mm)</th>
															<th>(mm)</th>
															<th></th>
															<th>OF RM</th>
															<th>(nos)</th>
															<th>(nos)</th>
															<th></th>
															<th>Vendore Name Nos</th>
															<th>Component</th>
															<th></th>
															<th></th>

														</tr>

													</thead>

														<?php if(isset($_GET['edit'])) { 

															$i = 0; 
															$y = 1;

															while($i < 20) {

																$row = $admin->fetch($boqRawmaterial);
																$componentName = $admin->getComponentHardwareName($_GET['id']);

														?>

																<tbody>

																	<tr>

																		<td>

																			<?php echo $y;?>

																			<input type="hidden" value="<?php echo $row['id']; ?>" name="component_id[<?php echo $i;?>]" class="form-control form-control-sm">


																		</td>


																		<td>

																			<!-- <input type="text" value="<?php echo $row['component_name']; ?>" name="component_name[<?php echo $i;?>]" onkeyup="componentName(this)" class="form-control form-control-sm component_name boxSize" required> -->

																			<select name="component_name[<?php echo $i;?>]" class="form-control form-control-sm select2 component_name boxSize" required>
																			<option value="">Select</option>

																				<?php while ($row1 = $admin->fetch($componentName)) { ?>
																					<option value="<?php echo $row1['component']; ?>" <?php if($row1['component'] == $row['component_name']) { echo 'selected'; } ?>><?php echo $row1['component']; ?></option>
																				<?php } ?>
																			
																			</select>


																		</td>

																		<td>

																			<input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['photo'])){ echo "required"; } }else{ echo "required"; }?> name="photo[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																			<?php
																				if(!empty($row['photo'])){ 
																			?>
																				<a target="_blank_" href="<?php echo $row['photo'];?>" class="btn btn-info btn-sm">View Photo</a>
																			<?php
																				}
																			?>

																		</td>

																		<!-- <td>

																			<input type="text" name="qty[<?php echo $i;?>]" value="<?php echo $row['qty']; ?>" class="form-control form-control-sm qty boxSize" required>

																		</td> -->

																		<td>

																			<input type="text" name="size[<?php echo $i;?>]" value="<?php echo $row['size']; ?>" class="form-control form-control-sm size boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="thikness[<?php echo $i;?>]" value="<?php echo $row['thikness']; ?>" class="form-control form-control-sm thikness boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="mate_type[<?php echo $i;?>]" value="<?php echo $row['mate_type']; ?>" class="form-control form-control-sm mate_type boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="lenght[<?php echo $i;?>]" value="<?php echo $row['lenght']; ?>" class="form-control form-control-sm lenght" required>

																		</td>

																		<td>

																			<input type="text" name="produce_qty[<?php echo $i;?>]" value="<?php echo $row['produce_qty']; ?>" onkeyup="hardwareReqQty(this)" class="form-control form-control-sm produce_qty boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="reqd_qty[<?php echo $i;?>]" value="<?php echo $row['reqd_qty']; ?>" class="form-control form-control-sm reqd_qty boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="unit[<?php echo $i;?>]" value="<?php echo $row['unit']; ?>" class="form-control form-control-sm unit boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="Purchase_from[<?php echo $i;?>]" value="<?php echo $row['Purchase_from']; ?>" class="form-control form-control-sm Purchase_from boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="Purchase_rate[<?php echo $i;?>]" value="<?php echo $row['Purchase_rate']; ?>" class="form-control form-control-sm Purchase_rate boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="remark[<?php echo $i;?>]" value="<?php echo $row['remark']; ?>" class="form-control form-control-sm remark boxSize" required>

																		</td>

																		<td>

																			<button class="btn btn-sm btn-danger remover" onclick="remover(this)">Remove</buuton>

																		</td>

																	</tr>

																<tbody>	

														        <input type="hidden" name="details_id[<?php echo $i; ?>]" value="<?php echo $row['id']; ?>">


													        <?php $y++; $i++;} ?>


												        <?php } else { ?>

												

															<?php 

																$y=1;
																for ($x=0; $x < 20; $x++) { 

																
															?>
												
																<tbody>

																	<tr>

																		<td>

																			<?php echo $y;?>

																		</td>

																		<td>


																			<select name="component_name[<?php echo $x;?>]" class="form-control form-control-sm select2 component_name boxSize" required>
																			
																			</select>


																		</td>

																		<td>

																			<input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['photo'])){ echo "required"; } }else{ echo "required"; }?> name="photo[<?php echo $x;?>]" id="<?php echo $x;?>" data-image-index="<?php echo $x;?>" />

																		</td>

																		<!-- <td>

																			<input type="text" name="qty[<?php echo $x;?>]" class="form-control form-control-sm qty boxSize" required>

																		</td> -->

																		<td>

																			<input type="text" name="size[<?php echo $x;?>]" class="form-control form-control-sm size boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="thikness[<?php echo $x;?>]" class="form-control form-control-sm thikness boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="mate_type[<?php echo $x;?>]" class="form-control form-control-sm mate_type boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="lenght[<?php echo $x;?>]" class="form-control form-control-sm lenght boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="produce_qty[<?php echo $x;?>]" class="form-control form-control-sm produce_qty boxSize" onkeyup="hardwareReqQty(this)" required>

																		</td>	

																		<td>

																			<input type="text" name="reqd_qty[<?php echo $x;?>]" class="form-control form-control-sm reqd_qty boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="unit[<?php echo $x;?>]" class="form-control form-control-sm unit boxSize" required>

																		</td>	

																		<td>

																			<input type="text" name="Purchase_from[<?php echo $x;?>]" class="form-control form-control-sm Purchase_from boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="Purchase_rate[<?php echo $x;?>]" class="form-control form-control-sm Purchase_rate boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="remark[<?php echo $x;?>]" class="form-control form-control-sm remark boxSize" required>

																		</td>

																		<td>

																			<button class="btn btn-sm btn-danger remover" onclick="remover(this)">Remove</buuton>

																		</td>	

																	</tr>

																</tbody>
																

															<?php $y++;}  ?>

												        <?php } ?>

												</table>
														
									        </div>

								 		</div>

								 		<div id="menu2" class="tab-pane fade">

										 <div class="form-actions text-left">

											<button type="button" id="add-more" class="btn btn-warning add-more"><i class="fa fa-plus"></i> Add More Component</button>

										</div><br>

											<div class="table-responsive">

												<table class="table table-bordered" id="processFlow">

													<thead style="background: #2980b9;color: #fff;">

														<tr>

															<th>#</th>
															<th>Component</th>
															<th>Open No</th>
															<th>Open</th>
															<th>Video</th>
															<th>Check Sheet</th>
															<th>Tool Setup Time</th>
															<th>Cycle Time</th>
															<th>M/C Req.</th>
															<th>Man Req.</th>
															<th>Tool</th>
															<th>Check Sheet</th>
															<th>Quality Gauge</th>
															<th>Check Sheet</th>
															<th>Pokayoke</th>
															<th>Photo</th>
															<th colspan="2">Grinding Frq. Of Tools</th>
															<th>Remark</th>
															<th>Action</th>


														</tr>

														<tr>

															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th>Process Stage Wise Recording</th>
															<th>Process Wise</th>
															<th>(Sec)</th>
															<th>(Sec)</th>
															<th></th>
															<th>(Nos)</th>
															<th>Number</th>
															<th>Tool</th>
															<th>(Nos)</th>
															<th>Jigs</th>
															<th>(Nos)</th>
															<th>Pokayoke</th>
															<th>Last Done Date</th>
															<th>Next Due Date</th>
															<th></th>
															<th></th>

														</tr>

													</thead>



													<?php if(isset($_GET['edit'])) {

														$i = 0;
														$y = 1;


														while($row = $admin->fetch($boqHardware)) {

														$componentName = $admin->getcomponentName($_GET['id']);

															
													?>
													<tbody>

														<tr>
															<td>

																<?php echo $y;?>

																<input type="hidden" value="<?php echo $row['id']; ?>" name="hardware_component_id[<?php echo $i;?>]" class="form-control form-control-sm">


															</td>

															<td>

																<select name="component[<?php echo $i;?>]" class="form-control form-control-sm select2 component boxSize" required>

																	<?php while ($row1 = $admin->fetch($componentName)) { ?>
																		<option value="<?php echo $row1['component_name']; ?>" <?php if($row1['component_name'] == $row['component']) { echo 'selected'; } ?>><?php echo $row1['component_name']; ?></option>
																	<?php } ?>
																
																</select>


															</td>

															<td>

																<input type="text" name="open_no[<?php echo $i;?>]" value="<?php echo $row['open_no']; ?>" class="form-control form-control-sm open_no boxSize" onkeyup="openNo(this)" required>

															</td>

															<td>

																<input type="text" name="open[<?php echo $i;?>]" value="<?php echo $row['open']; ?>" class="form-control form-control-sm open boxSize" required>

															</td>

															<td>

																<!-- <input type="file" onchange="videoupload(this)" name="video[<?php echo $i;?>]"  class="form-control-sm" required> -->
																<input type="file" class="form-control-sm video" onchange="videoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['video'])){ echo "required"; } }else{ echo "required"; }?> name="video[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																<?php
																	if(!empty($row['video'])){ 
																?>
																	<a target="_blank_" href="<?php echo $row['video'];?>" class="btn btn-info btn-sm">View Video</a>
																<?php
																	}
																?>

															</td>

															<td>

																<input type="file" class="form-control-sm check_sheet" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['check_sheet'])){ echo "required"; } }else{ echo "required"; }?> name="check_sheet[<?php echo $i;?>]" id="check_sheet<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																<?php
																	if(!empty($row['check_sheet'])){ 
																?>
																	<a target="_blank_" href="<?php echo $row['check_sheet'];?>" class="btn btn-info btn-sm">View Photo</a>
																	<a href="<?php echo $row['check_sheet'];?>" class="btn btn-info btn-sm" download>Download Check Sheet</a>
																<?php
																	}
																?>
															</td>

															<td>

																<input type="text" name="tool_setup_time[<?php echo $i;?>]" value="<?php echo $row['tool_setup_time']; ?>" class="form-control form-control-sm tool_setup_time boxSize" required>

															</td>

															<td>

																<input type="text" name="cycle_time[<?php echo $i;?>]" value="<?php echo $row['cycle_time']; ?>" class="form-control form-control-sm cycle_time boxSize" required>

															</td>

															<td>

																<select class="form-control form-control-sm select2 boxSize" name="mc_req[<?php echo $i;?>]">

																	<option value="">Select Machine Name</option>
																	<?php $machineName = $admin->getAllMachineName(); while ($row1 = $admin->fetch($machineName)) { ?>
																		<option value="<?php echo $row1['id']; ?>" <?php if($row1['id'] == $row['mc_req']) { echo 'selected'; } ?>><?php echo $row1['machine']; ?></option>
																	<?php } ?>

																</select>

															</td>

															<td>

																<input type="text" name="man_req[<?php echo $i;?>]" value="<?php echo $row['man_req']; ?>" class="form-control form-control-sm man_req boxSize" required>

															</td>

															<td>

																<!-- <input type="text" name="tool[<?php echo $i;?>]" value="<?php echo $row['tool']; ?>" class="form-control form-control-sm tool boxSize" required> -->
																<select class="form-control form-control-sm select2 boxSize tool" name="tool[<?php echo $i;?>]" data-no="<?php echo $i;?>" onchange="checkSheetToolPhoto(this)" required>
																	<option value="">Select Tool No</option>

																	<?php $toolName = $admin->getAllTool(); while ($row2 = $admin->fetch($toolName)) { ?>
																		<option value="<?php echo $row2['id']; ?>" <?php if($row2['id'] == $row['tool']) { echo 'selected'; } ?>><?php echo $row2['tool_number']; ?></option>
																	<?php } ?>

																</select>

															</td>	

															<td class="check_sheet_tool<?php echo $i;?>">


															</td>

															<td>

																<select class="form-control form-control-sm select2 boxSize quality_gauge" name="quality_gauge[<?php echo $i;?>]" data-no="<?php echo $i;?>" onchange="checkSheetJigsPhoto(this)" required>
																	<option value="">Select Tool No</option>

																	<?php $InspectionGaugesComponent = $admin->getAllInspectionGaugesComponent(); while ($row2 = $admin->fetch($InspectionGaugesComponent)) { ?>
																		<option value="<?php echo $row2['id']; ?>" <?php if($row2['id'] == $row['quality_gauge']) { echo 'selected'; } ?>><?php echo $row2['gauges_number']; ?></option>
																	<?php } ?>

																</select>

															</td>	

															<td class="check_sheet_jigs<?php echo $i;?>">
														

															</td>

															<td>

																<input type="text" name="pokayoke[<?php echo $i;?>]" value="<?php echo $row['pokayoke']; ?>" class="form-control form-control-sm pokayoke boxSize" required>

															</td>	

															<td>


																<input type="file" class="form-control-sm pokayoke_photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['pokayoke_photo'])){ echo "required"; } }else{ echo "required"; }?> name="pokayoke_photo[<?php echo $i;?>]" id="pokayoke_photo<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																<?php
																	if(!empty($row['pokayoke_photo'])){ 
																?>
																	<a target="_blank_" href="<?php echo $row['pokayoke_photo'];?>" class="btn btn-info btn-sm">View Photo</a>
																<?php
																	}
																?>

															</td>

															<td>

																<input type="date" name="last_done_date[<?php echo $i;?>]" value="<?php echo $row['last_done_date']; ?>" class="form-control form-control-sm last_done_date boxSize">

															</td>	

															<td>

																<input type="date" name="next_due_date[<?php echo $i;?>]" value="<?php echo $row['next_due_date']; ?>" class="form-control form-control-sm next_due_date boxSize" >

															</td>

															<td>

																<input type="text" name="remark[<?php echo $i;?>]" value="<?php echo $row['remark']; ?>" class="form-control form-control-sm remark boxSize" required>

															</td>	

															<td>

																<button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

															</td>

														

															<input type="hidden" name="details_id[<?php echo $i; ?>]" value="<?php echo $row['id']; ?>">

														</tr>
													</tbody>

														<?php $y++; $i++; } ?>



													<?php } else { ?>



										
															<?php 

																$y=1;
															
																for ($x=0; $x < 20; $x++) { 
																
															?>
												
																<tbody>

																	<tr>

																		<td>

																			<?php echo $y;?>

																		</td>

																		<td>

																			<select name="component[<?php echo $x;?>]" class="form-control form-control-sm select2 component boxSize" required>
																			
																			</select>

																		</td>

																		<td>

																			<input type="text" name="open_no[<?php echo $x;?>]" class="form-control form-control-sm open_no boxSize" onkeyup="openNo(this)" required>

																		</td>

																		<td>

																			<input type="text" name="open[<?php echo $x;?>]" class="form-control form-control-sm open boxSize" required>

																		</td>

																		<td>

																			<input type="file" onchange="videoupload(this)" name="video[<?php echo $x;?>]"  class="form-control-sm" required>

																		</td>

																		<td>

																			<input type="file" class="form-control-sm check_sheet" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['check_sheet'])){ echo "required"; } }else{ echo "required"; }?> name="check_sheet[<?php echo $x;?>]" id="check_sheet<?php echo $x;?>" data-image-index="<?php echo $x;?>" />

																		</td>

																		<td>

																			<input type="text" name="tool_setup_time[<?php echo $x;?>]" class="form-control form-control-sm tool_setup_time boxSize" required>

																		</td>

																		<td>

																			<input type="text" name="cycle_time[<?php echo $x;?>]" class="form-control form-control-sm cycle_time boxSize" required>

																		</td>

																		<td>

																			<select class="form-control form-control-sm select2 boxSize" name="mc_req[<?php echo $x;?>]">

																				<option value="">Select Machine Name</option>
																				<?php $machineName = $admin->getAllMachineName(); while ($row = $admin->fetch($machineName)) { ?>
																					<option value="<?php echo $row['id']; ?>" ><?php echo $row['machine']; ?></option>
																				<?php } ?>

																			</select>

																		</td>

																		<td>

																			<input type="text" name="man_req[<?php echo $x;?>]" class="form-control form-control-sm man_req boxSize" required>

																		</td>

																		<td>

																			<select class="form-control form-control-sm select2 boxSize tool" name="tool[<?php echo $x;?>]"  data-no="<?php echo $x;?>" onchange="checkSheetToolPhoto(this)" required>

																			</select>

																		</td>	

																		<td class="check_sheet_tool<?php echo $x;?>">


																		</td>

																		<td>

																			<select class="form-control form-control-sm select2 boxSize quality_gauge" name="quality_gauge[<?php echo $x;?>]" data-no="<?php echo $x;?>" onchange="checkSheetJigsPhoto(this)" required>

																			</select>

																		</td>	

																		<td class="check_sheet_jigs<?php echo $x;?>">


																		</td>

																		<td>

																			<input type="text" name="pokayoke[<?php echo $x;?>]" class="form-control form-control-sm pokayoke boxSize" required>

																		</td>	

																		<td>


																			<input type="file" class="form-control-sm pokayoke_photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['pokayoke_photo'])){ echo "required"; } }else{ echo "required"; }?> name="pokayoke_photo[<?php echo $x;?>]" id="pokayoke_photo<?php echo $x;?>" data-image-index="<?php echo $x;?>" />


																		</td>

																		<td>

																			<input type="date" name="last_done_date[<?php echo $x;?>]" class="form-control form-control-sm last_done_date boxSize">

																		</td>	

																		<td>

																			<input type="date" name="next_due_date[<?php echo $x;?>]" class="form-control form-control-sm next_due_date boxSize" >

																		</td>

																		<td>

																			<input type="text" name="remark[<?php echo $x;?>]" class="form-control form-control-sm remark boxSize" required>

																		</td>	

																		<td>

																			<button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

																		</td>	

																	</tr>

																	</tbody>

															    <?php $y++;} ?>	


													<?php } ?>


												</table>

											</div>

								 		</div>
								 		
								 	</div>

								</div>

							</div> 

							<div class="form-actions text-right">

								<input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

								<?php if(isset($_GET['edit'])){ ?>

									<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

									<button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button>

								<?php } else { ?>

									<button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

								<?php } ?>

							</div>

						</form>

					</div>

				</div>

			</div>
			<!-- /Page Content -->

		</div>
		<!-- /Page Wrapper -->

	</div>
	<!-- /Main Wrapper -->

	<!-- jQuery -->
	<script src="assets/js/jquery-3.2.1.min.js"></script>

	<!-- Bootstrap Core JS -->
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- Slimscroll JS -->
	<script src="assets/js/jquery.slimscroll.min.js"></script>

	<!-- Custom JS -->
	<script src="assets/js/app.js"></script>

	<!-- Validate JS -->
	<script src="assets/js/jquery.validate.js"></script>
	<script src="assets/js/additional-methods.js"></script>

	<!-- Crop Image js -->
	<script src="assets/js/crop-image/cropper.min.js"></script>
	<script src="assets/js/crop-image/image-crop-app.js"></script>
	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>


	<!-- CK Editor -->
	<script type="text/javascript" src="assets/js/editor/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/js/editor/ckeditor/adapters/jquery.js"></script>
	<script type="text/javascript" src="assets/js/editor/ckfinder/ckfinder.js"></script>

	<script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2();
			$('.select3').select2();
			
			<?php if(isset($_GET['edit'])){ ?>
			 $('.component_drawing_no').change();
			 $('.tool').change();
			 $('.quality_gauge').change();                   
			 <?php  }?>
                       
			 

        });

		$(document).ready(function() {

			$("#form").validate({
				ignore: [],
				debug: false,
				rules: {
					item_name: {
						required: true,
					},
					unq_com_code:{
						required: true,
					},
					drawing_no: {
						required: true,
					},
					stock_norms_qty:{
						required: true,
					},
					finish_weight_net: {
						required: true,
					},
					product_sale_rate:{
						required: true,
					},
					// component_name: {
					// 	required: true,
					// },
					department: {
						number: true,
						remote:{
							url: "check_display_order.php",
							type: "post",
							data: {
								<?php if(isset($_GET['edit']) && !empty($data['display_order'])){ ?>
									id: function() {
										return "<?php echo $data['id']; ?>";
									},
								<?php } ?>
								table: "root_city_master"
							}
						},
					}
				},
				messages: {
					
				}
			});
			$.validator.addMethod('filesize', function (value, element, param) {
				return this.optional(element) || (element.files[0].size <= param)
			}, 'File size must be less than 2 MB');
			$.validator.addMethod("url", function(value, element) {
				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
			}, "Please enter a valid link address.");
		});
	</script>
	<script>
		$(document).ready(function() {
			$('input[name=thumbnail_image]').change(function(){
				loadImagePreview(this, (542 / 384));
			});

			//  itemDetails($('#item_group'));

			// componentName($('.component_name'));
			// openNo($('.open_no'));

		});

		$(function() {

			var callToEnhanceValidate=function(){
				$(".details_title").each(function(){
					$(this).rules('remove');
					$(this).rules('add', {
						required: true,
					});
				});
			}
			

            $(".add-more").on("click", function(){
				
                var count = $('#processFlow > tbody > tr').length;
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxaComponent.php',
                    success: function (services_clone) {

                        $("#processFlow tr:last").after(services_clone);

                    }
                });
			
            
				
				// callToEnhanceValidate();

			});


			$(".remove-row").on("click", function() {
				let data_count=$(this).attr('data-count');
				$("#form_group"+data_count+"").remove();

			});
            $(".remove-rows").on("click", function() {
				let data_count=$(this).attr('data-count');
				$("#form_groups"+data_count+"").remove();

			});
			callToEnhanceValidate();
		});



		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));

				
			}
			
		}

    


		
		function remover(e) {

			$(e).parent().parent().remove();

		}
		function remove(e) {

			$(e).parent().parent().remove();

		}

		function remove1(e) {

			$(e).parent().parent().remove();

		}

	
		function componentName(e) {
			var arr = [];
			var arr1 = [];

			$('#rawMaterial > tbody > tr').each(function () {
				arr.push({
					'srno': $(this).find('td:eq(0)').text().trim(),
					'value': $(this).find('.component_name').val()
				});
			})

			$('#processFlow > tbody > tr').each(function () {
				arr1.push({
					'srno': $(this).find('td:eq(0)').text().trim(),
					'value': $(this).find('.component').val()
				});
			})

			$('.component_name').find('option').remove();

			$('.component').find('option').remove();

			let str = '<option value=""> Select Component Name</option>';

			$('.component').each(function () {

				let componentName = $(this).val();

				if (componentName != '' && componentName != null) {
					str += '<option value="' + componentName + '">' + componentName + '</option>';

				}

			});


			$('.component_name').html(str);

			$('.component').html(str);

			for (var i in arr) {
				$('#rawMaterial > tbody > tr').each(function () {
					var srno = $(this).find('td:eq(0)').text().trim();
					if (arr[i].srno == srno) {
						$(this).find('.component_name').val(arr[i].value);
					}
				})
			}

			for (var x in arr1) {
				$('#processFlow > tbody > tr').each(function () {
					var srno = $(this).find('td:eq(0)').text().trim();
					if (arr1[x].srno == srno) {
						$(this).find('.component').val(arr1[x].value);
					}
				})
			}

		}

		function openNo(e) {

			let openNo     = $(e).val();
			let counter    = 0;
			let component1 = $(e).parent().parent().find('.component').val();
			
			$('#processFlow > tbody > tr').each(function(){

				let component = $(this).find('.component').val();

				let open_no   = $(this).find('.open_no').val();

				if (component == component1 && openNo == open_no && counter==1) {

					alert('Componet And Open No Are Same Pls Change');
					$(e).val("");

				} else {

					if (component == component1 && openNo == open_no && counter==0) {

						counter++;

					}

				}

			});
			
		}


		// $('#myFile').bind('change', function() {

		// //this.files[0].size gets the size of your file.
		// alert(this.files[0].size);


		// });

		function photoupload(e){

			let photoSize = e.files[0].size;
			if (photoSize > 5000000) {
				alert('Photo Size Not Greater Than 5 mb ');

				$(e).val("");

			}
		}

		function videoupload(e){

		let videoSize = e.files[0].size;
			if (videoSize > 5000000) {
				
				alert('Video Size Not Greater Than 5 MB ');

				$(e).val("");

			}
		}

		

        

        function rawMaterialReqQty(e) {

            let produceQty   = parseFloat($(e).val());
            let dataCount    = $(e).attr('data-count');
            let itemQty = parseFloat($('#itemQty').val());

            let total = 0;

            if (!isNaN(produceQty) && !isNaN(itemQty)) {

                total = itemQty / produceQty;

                $('#required_qty'+dataCount+'').val(total.toFixed(2));
            }
    
        }

          function hardwareReqQty(e) {

            let produceQty   = parseFloat($(e).val());
            let itemQty = parseFloat($('#stock_norms_qty').val());

            let total = 0;

            if (!isNaN(produceQty) && !isNaN(itemQty)) {

                total = itemQty / produceQty;

				$(e).parent().parent().find('.reqd_qty').val(total.toFixed(3));

            }
    
        }

		$(document).ready(function () {

			$('input[name="drawing_photo"]').change(function () {

				loadImagePreview(this, (800 / 800));

			});
			$('input[name="product_photo"]').change(function () {

				loadImagePreview(this, (800 / 800));

			});
			
			// $('.photo').change(function () {

			// 	loadImagePreview(this, (800 / 800));

			// });

		});

		// itemName($('#itemNames'));


		function itemName(e) {

			let itemName = $(e).val();

			if (itemName != '') {

				$.ajax({

					type: "POST",
					data: 'itemName=' + itemName,
					url: 'getAjaxDrawingNoBom.php',
					cache: false,
					success: function (res) {

						$('#components > tbody > tr').each(function () {
							$(this).find('.component_drawing_no').html(res);

						});

						$.ajax({

							type: "POST",
							data: 'itemName=' + itemName,
							url: 'getAjaxToolBom.php',
							cache: false,
							success: function (res1) {
								$('#processFlow > tbody > tr').each(function () {
									$(this).find('.tool').html(res1);

								});

								$.ajax({

									type: "POST",
									data: 'itemName=' + itemName,
									url: 'getAjaxGaugesBom.php',
									cache: false,
									success: function (res2) {
										$('#processFlow > tbody > tr').each(function () {
											$(this).find('.quality_gauge ').html(res2);

										});
									}

								});

							}

						});

					}

				})

			}
		}


		function componentphoto(e) {

			let componentphoto = $(e).val();
			let dataNo = $(e).attr('data-no');


			if (componentphoto != '') {

				$.ajax({

					type: "POST",
					data: 'componentphoto=' + componentphoto,
					url: 'getAjaxcomponentphotoBom.php',
					cache: false,
					success: function (res) {

						$(e).parent().parent().find('.componentPhoto' + dataNo).html(res);


					}

				});

			} else {
				$(e).parent().parent().find('.componentPhoto' + dataNo).html("");
			}

		}

		function checkSheetToolPhoto(e) {

			let checkSheetToolPhoto = $(e).val();

			let dataNo = $(e).attr('data-no');


			if (checkSheetToolPhoto != '') {

				$.ajax({

					type: "POST",
					data: 'checkSheetToolPhoto=' + checkSheetToolPhoto,
					url: 'getAjaxCheckSheetToolPhotoBom.php',
					cache: false,
					success: function (res) {

						$(e).parent().parent().find('.check_sheet_tool' + dataNo).html(res);


					}

				});

			}

		}

		function checkSheetJigsPhoto(e) {

			let checkSheetJigsPhoto = $(e).val();

			let dataNo = $(e).attr('data-no');

			if (checkSheetJigsPhoto != '') {

				$.ajax({

					type: "POST",
					data: 'checkSheetJigsPhoto=' + checkSheetJigsPhoto,
					url: 'getAjaxCheckSheetJigsPhotoBom.php',
					cache: false,
					success: function (res) {

						$(e).parent().parent().find('.check_sheet_jigs' + dataNo).html(res);

					}

				});

			}

		}

	

	</script>
	</html>