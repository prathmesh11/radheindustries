<?php 
	
	include_once 'include/config.php';
	
	include_once 'include/admin-functions.php';
	
	$admin = new AdminFunctions();

// if (isset($_POST['itemName'])) {
    
//     $toolNo = $admin-> getAllToolComponentMaster($_POST);
    
// }


if(!$loggedInUserDetailsArr = $admin->sessionExists()){

	header("location: admin-login.php");

	exit();

}

if (isset($_POST['itemName'])) {

	$itemId = $_POST['itemName'];
	
	$drawingNo = $admin-> getAllDrawingComponentMaster($itemId,$loggedInUserDetailsArr['branch_id']);
	
}

?>

<?php 

    $x = 0;

    $i = 1;


    while ($row = $admin->fetch($drawingNo)) { 

        $itemName = $admin-> getUniqueItemNameById($row['component_name'],$loggedInUserDetailsArr['branch_id']);


?>

    <tr>

        <td>

            <?php echo $i;?>

        </td>

		<td>

			<input type="hidden" name="component_name[<?php echo $x;?>]" value ="<?php echo $row['component_name'];?>" class="form-control form-control-sm component_name boxSize" required readonly>

			<input type="text" class="form-control form-control-sm component boxSize" value ="<?php echo $itemName;?>" required readonly>

		</td>

		<td>

			<input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['photo'])){ echo "required"; } }else{ echo "required"; }?> name="photo[<?php echo $x;?>]" id="<?php echo $x;?>" data-image-index="<?php echo $x;?>" />

		</td>

		<!-- <td>

			<input type="text" name="qty[<?php echo $x;?>]" class="form-control form-control-sm qty boxSize" required>

		</td> -->

		<td>

			<input type="text" name="size[<?php echo $x;?>]" class="form-control form-control-sm size boxSize" required>

		</td>

		<td>

			<input type="text" name="thikness[<?php echo $x;?>]" class="form-control form-control-sm thikness boxSize" required>

		</td>

		<td>

			<input type="text" name="mate_type[<?php echo $x;?>]" class="form-control form-control-sm mate_type boxSize" required>

		</td>	

		<td>

			<input type="text" name="lenght[<?php echo $x;?>]" class="form-control form-control-sm lenght boxSize" required>

		</td>

		<td>

			<input type="text" name="produce_qty[<?php echo $x;?>]" class="form-control form-control-sm produce_qty boxSize" onkeyup="hardwareReqQty(this)" required>

		</td>	

		<td>

			<input type="text" name="reqd_qty[<?php echo $x;?>]" class="form-control form-control-sm reqd_qty boxSize" required>

		</td>

		<td>

			<input type="text" name="unit[<?php echo $x;?>]" class="form-control form-control-sm unit boxSize" required>

		</td>	

		<td>

			<input type="text" name="Purchase_from[<?php echo $x;?>]" class="form-control form-control-sm Purchase_from boxSize" required>

		</td>

		<td>

			<input type="text" name="Purchase_rate[<?php echo $x;?>]" class="form-control form-control-sm Purchase_rate boxSize" required>

		</td>

		<td>

			<input type="text" name="remark[<?php echo $x;?>]" class="form-control form-control-sm remark boxSize" required>

		</td>

		<td>

			<button class="btn btn-sm btn-danger remover" onclick="remover(this)">Remove</buuton>

		</td>	


	</tr>

    
<?php $x++;$i++; } ?>

<script>

	$('.select2').select2();

	function remove(e) {

		$(e).parent().parent().remove();

	}

	function photoupload(e) {

		let photoSize = e.files[0].size;
		if (photoSize > 5000000) {
			alert('Photo Size Not Greater Than 5 mb ');

			$(e).val("");

		}
	}

</script>