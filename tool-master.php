<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName = "Tool Master";
$pageURL = 'tool-master.php';
$deleteURL = 'tool-master.php';
$tableName = 'tool_master';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


$results = $admin->query("SELECT * FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND branch_id = '".$loggedInUserDetailsArr['branch_id']."' ");

$itemName = $admin->getAllItemFinishGood($loggedInUserDetailsArr['branch_id']);

if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET deleted_by = '".$loggedInUserDetailsArr['id']."', deleted_time='".CURRENTMILLIS."' where id = '".$id."' AND branch_id = '".$loggedInUserDetailsArr['branch_id']."' ");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_GET['activate']) && !empty($_GET['activate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['activate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '1', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."' AND branch_id = '".$loggedInUserDetailsArr['branch_id']."' ");
	header("location:".$pageURL."?activatesuccess");
	exit();
}

if(isset($_GET['deactivate']) && !empty($_GET['deactivate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['deactivate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '0', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."' AND branch_id = '".$loggedInUserDetailsArr['branch_id']."' ");
	header("location:".$pageURL."?deactivatesuccess");
	exit();
}

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addToolMaster($_POST, $_FILES, $loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
        header("location:".$parentPageURL."?registersuccess");
        exit();

	}
}


if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueToolById($id);
    $detailsData = $admin->getUniqueToolComponentById($id);

}

// print_r($detailsData);

if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateToolMaster($_POST,$_FILES,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$parentPageURL."?updatesuccess");
		exit();
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.boxSize{
            border-bottom: 1px solid blue;
            height:25px!important;
			width:200px!important;

        }
        label{
            font-size:11px;
        }
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

		.tableFixHead          { overflow-y: auto; height: 600px; }
		.tableFixHead thead th { position: sticky; top: 0; }

		/* Just common table stuff. Really. */
		.tableFixHead table  { border-collapse: collapse; width: 100%; }
		.tableFixHead th, td { padding: 8px 16px; }
		.tableFixHead th     { background:#2980b9; color:#fff; }

	</style>

</head>

<body>



	<div class='loading_wrapper' style="display: none;">

		<div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">

		<!-- Header -->

		<?php include("include/header.php"); ?>

		<!-- /Header -->



		<!-- Sidebar -->

		<?php include("include/sidebar.php"); ?>

		<!-- /Sidebar -->



		<!-- Page Wrapper -->

		<div class="page-wrapper">



			<!-- Page Content -->

			<div class="content container-fluid">



				<!-- Page Header -->

				<div class="page-header">

					<div class="row align-items-center">

						<div class="col">

							<h3 class="page-title"><?php echo $pageName; ?></h3>

							<ul class="breadcrumb">

								<li class="breadcrumb-item">Master</li>


								<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

									<li class="breadcrumb-item"><?php echo $pageName; ?></li>

									<li class="breadcrumb-item active">

										<?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

									</li>

								<?php } else { ?>

									<li class="breadcrumb-item active"><?php echo $pageName; ?></li>

								<?php } ?>

							</ul>

						</div>

						<div class="col-auto float-right ml-auto">

							<a href="<?php echo $pageURL; ?>?add" class="btn add-btn"><i class="fa fa-plus"></i> Add <?php echo $pageName; ?></a>

						</div>

					</div>

				</div>

				<!-- /Page Header -->



				<?php if(isset($_GET['registersuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['registerfail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatefail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

					</div>

				<?php } ?>



				<?php if(isset($_GET['deletesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<h4 class="card-title mb-0"><?php if(isset($_GET['edit'])) {

										echo 'Edit '.$pageName;

									} else {

										echo 'Add New '.$pageName;

									}

									?></h4>

								</div>

								
								<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

                            
                                    <div class="card-body" style="padding-top: 10px;padding-bottom: 0px;">

                                        <div class="row ">

                                            <div class="col-sm-4">

                                                <div class="form-group row">

                                                    <label for="product_name" class="col-sm-4 col-form-label">Product Name<em>*</em></label>

                                                    <div class="col-sm-8">

														<select class="form-control form-control-sm select2" name="product_name" id="finishGoodId" onchange="componentName()">
															
															<option value="">Select Item Name</option>
															
															<?php while ($row = $admin->fetch($itemName)) { ?>
															
																<option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) && $data['product_name'] == $row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?></option>
															
															<?php } ?>
														
														</select>

                                                    </div>

                                                </div>
                                            
                                            </div>


                                            <div class="col-sm-2">

                                                <div class="form-group row">

                                                    <label for="sub_sr" class="col-sm-5 col-form-label">Sub Sr<em>*</em></label>

                                                    <div class="col-sm-7">

                                                        <input type="text" name="sub_sr" id="sub_sr" value="<?php if(isset($_GET['edit'])) { echo $data['sub_sr']; } ?>" class="form-control form-control-sm">

                                                    </div>

                                                </div>
                                            
                                            </div>

                                            <div class="col-sm-4">

                                                <div class="form-group row">

                                                    <label for="component_name" class="col-sm-4 col-form-label">Component<em>*</em></label>

                                                    <div class="col-sm-8">
														
														<select class="form-control form-control-sm select2" name="component_name" id="ComponentName">
															
															<option value="">Select Component Name</option>

															<?php
																if (isset($_GET['edit'])) {

																	$allComponentName = $admin-> getAllcompoentName($data['product_name'],$loggedInUserDetailsArr['branch_id']);

																	while ($rows = $admin->fetch($allComponentName)) {
														
															?>
																<option value="<?php echo $rows['id']; ?>" <?php if(isset($_GET['edit']) && $data['component_name'] == $rows['id']) { echo 'selected'; } ?>><?php echo $rows['item_name']; ?></option>

															<?php } } ?>
														
														</select>

                                                    </div>

                                                </div>
                                            
                                            </div>


                                            <div class="col-sm-2">
    	
											    <button type="button" id="add-more" class="btn btn-warning add-more btn-sm"><i class="fa fa-plus"></i> Add More </button>
                                            
                                            </div>
                                        
                                        </div>

                                    </div>


                                    <div class="card-body">

                                        <div class="table-responsive tableFixHead">

                                            <table class="table table-bordered" id="components">

                                                <thead style="background: #2980b9;color: #fff;">

                                                    <tr>

                                                        <th>#</th>
                                                        <th>PROCESS</th>
                                                        <th>TOOL</th>
                                                        <th>TOOL NUMBER</th>
                                                        <th>PHOTO</th>
                                                        <th>TOOL CHK LIST</th>
                                                        <th>TOOL GRINDING FREQUENCY</th>
                                                        <th>LAST GRINDING DATE</th>
                                                        <th>NEXT DUE DATE</th>
                                                        <th>Action</th>

                                                    </tr>

                                                </thead>

                                                <?php if(isset($_GET['edit'])) { $i = 0; $y = 1;

													while($row = $admin->fetch($detailsData)) {

												?>

                                                    <tbody>

                                                        <tr>

                                                            <td>

                                                                <?php echo $y;?>

                                                                <input type="hidden" value="<?php echo $row['id']; ?>" name="tool_component_id[<?php echo $i;?>]" class="form-control form-control-sm boxSize">

                                                            </td>

                                                            <td>

                                                                <input type="text" name="process_name[<?php echo $i;?>]" value="<?php echo $row['process_name']; ?>" class="form-control form-control-sm process_name boxSize" required>

                                                            </td>

                                                            <td>

                                                                <input type="text" name="tool_name[<?php echo $i;?>]" value="<?php echo $row['tool_name']; ?>" class="form-control form-control-sm tool_name boxSize" required>

                                                            </td>

                                                            <td>

                                                                <input type="text" name="tool_number[<?php echo $i;?>]" value="<?php echo $row['tool_number']; ?>" class="form-control form-control-sm tool_number boxSize" required>

                                                            </td>

                                                            <td>

																<input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['photo'])){ } }?> name="photo[<?php echo $i;?>]" id="photo<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																<?php
																	if(!empty($row['photo'])){ 
																?>
																	<a target="_blank_" href="/toolmasterimage/<?php echo $data['id'].'/'.$row['photo'];?>"  class="btn btn-info btn-sm">View Photo</a>
																	<a href="/toolmasterimage/<?php echo $data['id'].'/'.$row['photo'];?>" class="btn btn-info btn-sm" download>Download Check Sheet</a>
																<?php
																	}
																?>



															</td>

                                                            <td>

																<input type="file" class="form-control-sm check_list" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['check_list'])){  }}?> name="check_list[<?php echo $i;?>]" id="check_list<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																<?php
																	if(!empty($row['check_list'])){ 
																?>

																	<a target="_blank_" href="/toolmasterimage/<?php echo $data['id'].'/'.$row['check_list'];?>"  class="btn btn-info btn-sm">View Photo</a>
																	<a href="/toolmasterimage/<?php echo $data['id'].'/'.$row['check_list'];?>" class="btn btn-info btn-sm" download>Download Check Sheet</a>

																<?php
																	}
																?>

															</td>

                                                            <td>

                                                                <input type="text" name="tool_grinding_frequency[<?php echo $i;?>]" value="<?php echo $row['tool_grinding_frequency']; ?>" class="form-control form-control-sm tool_grinding_frequency">

                                                            </td>

                                                            <td>

                                                                <input type="date" name="last_grinding_date[<?php echo $i;?>]" value="<?php echo $row['last_grinding_date']; ?>" class="form-control form-control-sm last_grinding_date">

                                                            </td>

                                                            <td>

                                                                <input type="date" name="next_due_date[<?php echo $i;?>]" value="<?php echo $row['next_due_date']; ?>" class="form-control form-control-sm next_due_date">

                                                            </td>

                                                            <td>

                                                                <button class="btn btn-sm btn-danger remover" data-id = "<?php echo $row['id'];?>"  onclick="remove(this)">Remove</buuton>

                                                            </td>
                                                            
                                                        </tr>

                                                    </tbody>

                                                <?php $y++; $i++; }  } else { ?>

                                                    <?php $y=1;for ($x=0; $x < 10; $x++) { ?>

                                                        <tbody>

                                                            <tr>

                                                                <td>

                                                                    <?php echo $y;?>

                                                                </td>

                                                                <td>

                                                                    <input type="text" name="process_name[<?php echo $x;?>]" class="form-control form-control-sm process_name boxSize" required>

                                                                </td>

                                                                <td>

                                                                    <input type="text" name="tool_name[<?php echo $x;?>]" class="form-control form-control-sm tool_name boxSize" required>

                                                                </td>

                                                                <td>

                                                                    <input type="text" name="tool_number[<?php echo $x;?>]" class="form-control form-control-sm tool_number boxSize" required>

                                                                </td>

                                                                <td>

                                                                    <input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['photo'])){ echo "required"; } }?> name="photo[<?php echo $x;?>]" id="<?php echo $x;?>" data-image-index="<?php echo $x;?>" />

                                                                </td>

                                                                <td>

                                                                    <input type="file" class="form-control-sm check_list" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['check_list'])){ echo "required"; } }?> name="check_list[<?php echo $x;?>]" id="<?php echo $x;?>" data-image-index="<?php echo $x;?>" />

                                                                </td>

                                                                <td>

                                                                    <input type="text" name="tool_grinding_frequency[<?php echo $x;?>]" class="form-control form-control-sm tool_grinding_frequency boxSize">

                                                                </td>

                                                                <td>

                                                                    <input type="date" name="last_grinding_date[<?php echo $x;?>]" class="form-control form-control-sm last_grinding_date boxSize">

                                                                </td>

                                                                <td>

                                                                    <input type="date" name="next_due_date[<?php echo $x;?>]" class="form-control form-control-sm next_due_date boxSize" >

                                                                </td>

                                                                <td>

																	<button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

																</td>

                                                            </tr>

                                                        </tbody>    


                                                    <?php $y++;} ?>


                                                <?php } ?>


                                            </table>

                                        </div>

                                        <div class="form-actions text-right">

                                            <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

                                            <?php if(isset($_GET['edit'])){ ?>

                                                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

                                                <button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button>

                                            <?php } else { ?>

                                                <button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

                                            <?php } ?>

                                        </div>

                                    </div>

								</form>

                            </div>

                        </div>

                    </div>

                <?php } ?>



				<div class="row">

					<div class="col-md-12">

						<table class="table table-striped custom-table mb-0 datatable datatable-selectable-data">
							<thead>
								<tr>
									<th width="20px">#</th>
									<th>Product Name</th>
                                    <th>Sub Sr No</th>
                                    <th>Component</th>
									<th width="100px">Active</th>
									<th width="10px">Actions</th>
								</tr>
							</thead>
							<tbody>

								<?php

								$x = 1;

								while($row = $admin->fetch($results)){ 

									$product_name = $admin->getUniqueItemById($row['product_name']);

									?>

									<tr>

										<td><?php echo $x++ ?></td>
                                        <td> <?php echo $product_name['item_name']; ?> </td>
                                        <td> <?php echo $row['sub_sr']; ?> </td>
                                        <td> <?php echo $admin->getUniqueItemById($row['component_name'])['item_name']; ?> </td>
										<td>
											<div class="badge badge-<?php echo $row['active'] == '1'?'success':'danger'; ?> ml-2"><?php echo $row['active'] == '1'?'Yes':'No'; ?></div>
										</td>
										<td class="text-right">

											<div class="dropdown dropdown-action">

												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>

												<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(103px, 32px, 0px);">

													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?edit&id=<?php echo $row['id']; ?>" title="Edit">   <i class="fa fa-pencil"></i> Edit</a>

													<?php if($row['active'] == '0') { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?activate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to activate?');" title="Activate"> <i class="fa fa-thumbs-up"></i> Activate </a>

													<?php } else { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?deactivate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to deactivate?');" title="Deactivate"> <i class="fa fa-thumbs-down"></i> Deactivate </a>

													<?php } ?>

											
													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?delId=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to delete?');" title="Delete"> <i class="fa fa-trash-o"></i> Delete </a>

												</div>

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>



			</div>

			<!-- /Page Content -->

		</div>

		<!-- /Page Wrapper -->

	</div>

	<!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					product_name: {
						required: true,
					},
                    sub_sr: {
						required: true,
					},
                    component_name: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function(){

				if($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

			$('.select2').select2();


			$(".add-more").on("click", function(){
                
				var count = $('#components > tbody > tr').length;
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxAddTool.php',
                    success: function (services_clone) {

                        $("#components tr:last").after(services_clone);

                    }
                });

			});

		});


		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));
				
			}
			
		}

        function photoupload(e) {

            let photoSize = e.files[0].size;
            if (photoSize > 5000000) {
				
                alert('Photo Size Not Greater Than 5 mb ');

                $(e).val("");

            }
        }


		function remove(e) {

			$(e).parent().parent().remove();

		}

		function componentName() {

			var finishGoodId = $('#finishGoodId').val();

			if (finishGoodId != '') {

				$.ajax({

					type: 'POST',
					
					data: 'itemId='+finishGoodId,
					
					url: 'getAjaxComponentItem.php',
					
					success: function (res) {

						$('#ComponentName').html(res);

					}
				
				});
				
			} else {

				$('#ComponentName').html('<option value="">Select Component Name</option>');

			}
			
		}
				
	</script>

</body>

</html>