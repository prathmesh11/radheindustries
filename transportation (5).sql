-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2021 at 03:09 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `transportation`
--

-- --------------------------------------------------------

--
-- Table structure for table `tr_admin`
--

CREATE TABLE `tr_admin` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` text,
  `role` varchar(50) DEFAULT NULL,
  `branch_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `last_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_admin`
--

INSERT INTO `tr_admin` (`id`, `full_name`, `username`, `email`, `password`, `role`, `branch_id`, `created`, `last_modified`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '$2y$10$7xX6moMly2Uzh/Ahb3kTmOhq5XU3pF/GYOXn7sLb5PxwCd0rGA5RG', 'admin', 1, '2020-09-15 11:03:18', '2020-10-13 12:01:39'),
(2, 'Admin Pune', 'Admin Pune', 'adminpune@gmail.com', '$2y$10$7xX6moMly2Uzh/Ahb3kTmOhq5XU3pF/GYOXn7sLb5PxwCd0rGA5RG', 'admin', 2, '2020-10-13 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tr_boq_component_master`
--

CREATE TABLE `tr_boq_component_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `boq_id` int(11) NOT NULL,
  `item_name` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `component` varchar(500) NOT NULL,
  `component_drawing_no` varchar(200) NOT NULL,
  `component_photo` text NOT NULL,
  `qty_per_piece` double(11,2) NOT NULL,
  `net_weight` double(11,2) NOT NULL,
  `category` varchar(200) NOT NULL,
  `component_remarks` varchar(200) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_boq_component_master`
--

INSERT INTO `tr_boq_component_master` (`id`, `branch_id`, `boq_id`, `item_name`, `component_id`, `component`, `component_drawing_no`, `component_photo`, `qty_per_piece`, `net_weight`, `category`, `component_remarks`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 1, 2, 'ROD', '1', '', 1.00, 1.00, '1', '1', 1, 1602226277324, 1, 1602742155152, 0, 0),
(2, 1, 1, 1, 3, 'TRAY', '1', '', 2.00, 2.00, '2', '2', 1, 1602226277324, 1, 1602742155152, 0, 0),
(3, 1, 1, 1, 4, 'MOUCE TRAY', '2', '', 3.00, 3.00, '3', '3', 1, 1602226277324, 1, 1602742155152, 0, 0),
(4, 1, 1, 1, 5, 'HARDWARE', '2', '', 4.00, 4.00, '4', '4', 1, 1602226277324, 1, 1602742155152, 0, 0),
(5, 1, 1, 1, 6, 'PLASTIC BAG', '3', '', 5.00, 5.00, '5', '5', 1, 1602226277324, 1, 1602742155152, 0, 0),
(6, 1, 1, 1, 7, 'SLIDER', '3', '', 6.00, 6.00, '6', '6', 1, 1602226277324, 1, 1602742155152, 0, 0),
(7, 1, 1, 1, 8, 'MS NUT BUSH', '1', '', 7.00, 7.00, '7', '7', 1, 1602226277324, 1, 1602742155152, 0, 0),
(8, 1, 1, 1, 9, 'RUBBUR BUSH', '2', '', 8.00, 8.00, '8', '8', 1, 1602226277324, 1, 1602742155152, 0, 0),
(9, 1, 1, 1, 10, 'SLIDER CAP', '3', '', 9.00, 9.00, '9', '9', 1, 1602226277324, 1, 1602742155152, 0, 0),
(10, 1, 1, 1, 11, 'REVIT', '1', '', 10.00, 10.00, '10', '10', 1, 1602226277324, 1, 1602742155152, 0, 0),
(11, 1, 1, 1, 12, 'LACTITE SF 770', '2', '', 11.00, 11.00, '11', '11', 1, 1602226277324, 1, 1602742155152, 0, 0),
(12, 1, 1, 1, 13, 'LACTITE HUTT (GLOW)', '3', '', 12.00, 12.00, '12', '12', 1, 1602226277324, 1, 1602742155152, 0, 0),
(13, 1, 1, 1, 14, 'PLASTIC BAG(HW)', '1', '', 13.00, 13.00, '13', '13', 1, 1602226277324, 1, 1602742155152, 0, 0),
(14, 1, 1, 1, 15, 'L-BRACKET', '2', '', 14.00, 14.00, '14', '14', 1, 1602226277324, 1, 1602742155152, 0, 0),
(15, 1, 1, 1, 16, 'SCREW PACKET', '3', '', 15.00, 15.00, '15', '15', 1, 1602226277324, 1, 1602742155152, 0, 0),
(16, 1, 1, 1, 17, 'CARTON', '1', '', 16.00, 16.00, '16', '16', 1, 1602226277324, 1, 1602742155152, 0, 0),
(17, 1, 1, 1, 18, 'Vijaya', '3', '', 17.00, 17.00, '17', '17', 1, 1602231712947, 1, 1602742155152, 0, 0),
(18, 1, 1, 1, 19, 'prathmesh', '1', '', 18.00, 18.00, '18', '18', 1, 1602233506981, 1, 1602742155152, 0, 0),
(19, 1, 1, 1, 20, 'Karekars', '2', '', 19.00, 19.00, '19', '19', 1, 1602234043255, 1, 1602742155152, 0, 0),
(28, 1, 7, 1, 22, 'SUNNY', '1', '', 1.00, 1.00, '1', '1', 1, 1602748380864, 1, 1602749749127, 0, 0),
(29, 1, 7, 1, 28, 'Pratik', '2', '', 2.00, 2.00, '2', '2', 1, 1602748380864, 1, 1602749749127, 0, 0),
(33, 1, 7, 1, 32, 'HardCore', '3', '', 3.00, 3.00, '3', '3', 1, 1602749749127, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_boq_hardware_master`
--

CREATE TABLE `tr_boq_hardware_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `boq_id` int(11) NOT NULL,
  `component` varchar(500) NOT NULL,
  `open_no` int(11) NOT NULL,
  `open` varchar(500) NOT NULL,
  `video` text NOT NULL,
  `check_sheet` text NOT NULL,
  `tool_setup_time` varchar(100) NOT NULL,
  `cycle_time` varchar(100) NOT NULL,
  `mc_req` varchar(100) NOT NULL,
  `man_req` varchar(100) NOT NULL,
  `tool` varchar(100) NOT NULL,
  `check_sheet_tool` text NOT NULL,
  `quality_gauge` varchar(200) NOT NULL,
  `check_sheet_jigs` text NOT NULL,
  `pokayoke` varchar(100) NOT NULL,
  `pokayoke_photo` text NOT NULL,
  `last_done_date` date NOT NULL,
  `next_due_date` date NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_boq_hardware_master`
--

INSERT INTO `tr_boq_hardware_master` (`id`, `branch_id`, `boq_id`, `component`, `open_no`, `open`, `video`, `check_sheet`, `tool_setup_time`, `cycle_time`, `mc_req`, `man_req`, `tool`, `check_sheet_tool`, `quality_gauge`, `check_sheet_jigs`, `pokayoke`, `pokayoke_photo`, `last_done_date`, `next_due_date`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 'ROD', 1, '1', 'boqvideo/0_1_1602226277324.mp4', 'boqimage/0_check_sheet_1_1602226277324.jpg', '1', '1', '1', '1', '1', '', '1', '', '1', 'boqimage/0_pokayoke_photo_1_1602226277324.jpg', '2020-10-01', '2020-10-01', '1', 1, 1602226277324, 1, 1602239134169, 0, 0),
(2, 1, 1, 'TRAY', 2, '2', 'boqvideo/1_1_1602226277324.mp4', 'boqimage/1_check_sheet_1_1602226277324.jpg', '2', '2', '2', '2', '2', '', '2', '', '2', 'boqimage/1_pokayoke_photo_1_1602226277324.jpg', '2020-10-02', '2020-10-02', '2', 1, 1602226277324, 1, 1602239134169, 0, 0),
(3, 1, 1, 'MOUCE TRAY', 3, '3', 'boqvideo/2_1_1602226277324.mp4', 'boqimage/2_check_sheet_1_1602226277324.jpg', '3', '3', '4', '3', '2', '', '3', '', '3', 'boqimage/2_pokayoke_photo_1_1602226277324.jpg', '2020-10-03', '2020-10-03', '3', 1, 1602226277324, 1, 1602239134169, 0, 0),
(4, 1, 1, 'HARDWARE', 4, '4', 'boqvideo/3_1_1602226277324.mp4', 'boqimage/3_check_sheet_1_1602226277324.jpg', '4', '4', '1', '4', '1', '', '1', '', '4', 'boqimage/3_pokayoke_photo_1_1602226277324.jpg', '2020-10-04', '2020-10-04', '4', 1, 1602226277324, 1, 1602239134169, 0, 0),
(5, 1, 1, 'PLASTIC BAG', 5, '5', 'boqvideo/4_1_1602226277324.mp4', 'boqimage/4_check_sheet_1_1602226277324.jpg', '5', '5', '2', '5', '2', '', '2', '', '5', 'boqimage/4_pokayoke_photo_1_1602226277324.jpg', '2020-10-05', '2020-10-05', '5', 1, 1602226277324, 1, 1602239134169, 0, 0),
(6, 1, 1, 'SLIDER', 6, '6', 'boqvideo/5_1_1602226277324.mp4', 'boqimage/5_check_sheet_1_1602226277324.jpg', '6', '6', '4', '6', '1', '', '3', '', '6', 'boqimage/5_pokayoke_photo_1_1602226277324.jpg', '2020-10-06', '2020-10-06', '6', 1, 1602226277324, 1, 1602239134169, 0, 0),
(7, 1, 1, 'MS NUT BUSH', 7, '7', 'boqvideo/6_1_1602226277324.mp4', 'boqimage/6_check_sheet_1_1602226277324.jpg', '7', '7', '1', '7', '2', '', '1', '', '7', 'boqimage/6_pokayoke_photo_1_1602226277324.jpg', '2020-10-07', '2020-10-07', '7', 1, 1602226277324, 1, 1602239134169, 0, 0),
(8, 1, 1, 'RUBBUR BUSH', 8, '8', 'boqvideo/7_1_1602226277324.mp4', 'boqimage/7_check_sheet_1_1602226277324.jpg', '8', '8', '2', '8', '1', '', '2', '', '8', 'boqimage/7_pokayoke_photo_1_1602226277324.jpg', '2020-10-08', '2020-10-08', '8', 1, 1602226277324, 1, 1602239134169, 0, 0),
(9, 1, 1, 'SLIDER CAP', 9, '9', 'boqvideo/8_1_1602226277324.mp4', 'boqimage/8_check_sheet_1_1602226277324.jpg', '9', '9', '4', '9', '2', '', '3', '', '9', 'boqimage/8_pokayoke_photo_1_1602226277324.jpg', '2020-10-09', '2020-10-09', '9', 1, 1602226277324, 1, 1602239134169, 0, 0),
(10, 1, 1, 'REVIT', 10, '10', 'boqvideo/9_1_1602226277324.mp4', 'boqimage/9_check_sheet_1_1602226277324.jpg', '10', '10', '1', '10', '1', '', '1', '', '10', 'boqimage/9_pokayoke_photo_1_1602226277324.jpg', '2020-10-10', '2020-10-10', '10', 1, 1602226277324, 1, 1602239134169, 0, 0),
(11, 1, 1, 'LACTITE SF 770', 11, '11', 'boqvideo/10_1_1602226277324.mp4', 'boqimage/10_check_sheet_1_1602226277324.jpg', '11', '11', '2', '11', '2', '', '2', '', '11', 'boqimage/10_pokayoke_photo_1_1602226277324.jpg', '2020-10-11', '2020-10-11', '11', 1, 1602226277324, 1, 1602239134169, 0, 0),
(12, 1, 1, 'LACTITE HUTT (GLOW)', 12, '12', 'boqvideo/11_1_1602226277324.mp4', 'boqimage/11_check_sheet_1_1602226277324.jpg', '12', '12', '4', '12', '1', '', '3', '', '12', 'boqimage/11_pokayoke_photo_1_1602226277324.jpg', '2020-10-12', '2020-10-12', '12', 1, 1602226277324, 1, 1602239134169, 0, 0),
(13, 1, 1, 'PLASTIC BAG(HW)', 13, '13', 'boqvideo/12_1_1602226277324.mp4', 'boqimage/12_check_sheet_1_1602226277324.jpg', '13', '13', '1', '13', '2', '', '1', '', '13', 'boqimage/12_pokayoke_photo_1_1602226277324.jpg', '2020-10-13', '2020-10-13', '13', 1, 1602226277324, 1, 1602239134169, 0, 0),
(14, 1, 1, 'L-BRACKET', 14, '14', 'boqvideo/13_1_1602226277324.mp4', 'boqimage/13_check_sheet_1_1602226277324.jpg', '14', '14', '2', '14', '1', '', '2', '', '14', 'boqimage/13_pokayoke_photo_1_1602226277324.jpg', '2020-10-14', '2020-10-14', '14', 1, 1602226277324, 1, 1602239134169, 0, 0),
(15, 1, 1, 'SCREW PACKET', 15, '15', 'boqvideo/14_1_1602226277324.mp4', 'boqimage/14_check_sheet_1_1602226277324.jpg', '15', '15', '4', '15', '2', '', '3', '', '15', 'boqimage/14_pokayoke_photo_1_1602226277324.jpg', '2020-10-15', '2020-10-15', '15', 1, 1602226277324, 1, 1602239134169, 0, 0),
(16, 1, 1, 'CARTON', 16, '16', 'boqvideo/15_1_1602226277324.mp4', 'boqimage/15_check_sheet_1_1602226277324.jpg', '16', '16', '1', '16', '1', '', '1', '', '16', 'boqimage/15_pokayoke_photo_1_1602226277324.jpg', '2020-10-16', '2020-10-16', '16', 1, 1602226277324, 1, 1602239134169, 0, 0),
(24, 1, 7, 'SUNNY', 1, '1', 'boqvideo/0_7_1602748380864.mp4', 'boqimage/0_check_sheet_7_1602748380864.jpg', '1', '1', '1', '1', '1', '', '1', '', '1', 'boqimage/0_pokayoke_photo_7_1602748380864.jpg', '2020-10-01', '2020-10-30', '1', 1, 1602748380864, 1, 1602749749127, 0, 0),
(25, 1, 7, 'Pratik', 2, '2', 'boqvideo/1_7_1602748380864.mp4', 'boqimage/1_check_sheet_7_1602748380864.jpg', '2', '2', '2', '2', '2', '', '2', '', '2', 'boqimage/1_pokayoke_photo_7_1602748380864.jpg', '2020-10-01', '2020-10-31', '2', 1, 1602748380864, 1, 1602749749127, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_boq_master`
--

CREATE TABLE `tr_boq_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `unq_com_code` varchar(500) NOT NULL,
  `coated` varchar(100) NOT NULL,
  `drawing_no` varchar(50) NOT NULL,
  `drawing_photo` text NOT NULL,
  `product_photo` text NOT NULL,
  `stock_norms_qty` varchar(50) NOT NULL,
  `finish_weight_net` varchar(100) NOT NULL,
  `product_sale_rate` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_boq_master`
--

INSERT INTO `tr_boq_master` (`id`, `branch_id`, `item_name`, `unq_com_code`, `coated`, `drawing_no`, `drawing_photo`, `product_photo`, `stock_norms_qty`, `finish_weight_net`, `product_sale_rate`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, '1', '1', 'Coated', '1', 'c1-1602226277-1.jpg', 'c2-1602226278-1.jpg', '100', '100', '100', 1, 1602226277324, 1, 1602742155152, 0, 0),
(7, 1, '1', '1', 'Coated', '1', '81e9pznuawl._ss500_-1602748380-1.jpg', '81e9pznuawl._ss500_-1602748381-1.jpg', '1', '1', '1', 1, 1602748380864, 1, 1602749749127, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_boq_rawmaterial_master`
--

CREATE TABLE `tr_boq_rawmaterial_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `boq_id` int(11) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `photo` text NOT NULL,
  `qty` varchar(100) NOT NULL,
  `size` varchar(200) NOT NULL,
  `thikness` varchar(200) NOT NULL,
  `mate_type` varchar(500) NOT NULL,
  `lenght` varchar(200) NOT NULL,
  `produce_qty` varchar(200) NOT NULL,
  `reqd_qty` varchar(100) NOT NULL,
  `unit` varchar(100) NOT NULL,
  `Purchase_from` varchar(200) NOT NULL,
  `Purchase_rate` varchar(200) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_boq_rawmaterial_master`
--

INSERT INTO `tr_boq_rawmaterial_master` (`id`, `branch_id`, `boq_id`, `component_name`, `photo`, `qty`, `size`, `thikness`, `mate_type`, `lenght`, `produce_qty`, `reqd_qty`, `unit`, `Purchase_from`, `Purchase_rate`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 'ROD', 'boqimage/0_ROD_1_1602226277324.jpg', '', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', 1, 1602226277324, 1, 1602239134169, 0, 0),
(2, 1, 1, 'TRAY', 'boqimage/1_TRAY_1_1602226277324.jpg', '', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', 1, 1602226277324, 1, 1602239134169, 0, 0),
(3, 1, 1, 'MOUCE TRAY', 'boqimage/2_MOUCE TRAY_1_1602226277324.jpg', '', '3', '3', '3', '3', '3', '3', '3', '3', '3', '3', 1, 1602226277324, 1, 1602239134169, 0, 0),
(4, 1, 1, 'HARDWARE', 'boqimage/3_HARDWARE_1_1602226277324.jpg', '', '4', '4', '4', '4', '4', '4', '4', '4', '4', '4', 1, 1602226277324, 1, 1602239134169, 0, 0),
(5, 1, 1, 'PLASTIC BAG', 'boqimage/4_PLASTIC BAG_1_1602226277324.jpg', '', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', 1, 1602226277324, 1, 1602239134169, 0, 0),
(6, 1, 1, 'SLIDER', 'boqimage/5_SLIDER_1_1602226277324.jpg', '', '6', '6', '6', '6', '6', '6', '6', '6', '6', '6', 1, 1602226277324, 1, 1602239134169, 0, 0),
(7, 1, 1, 'MS NUT BUSH', 'boqimage/6_MS NUT BUSH_1_1602226277324.jpg', '', '7', '7', '7', '7', '7', '7', '7', '7', '7', '7', 1, 1602226277324, 1, 1602239134169, 0, 0),
(8, 1, 1, 'RUBBUR BUSH', 'boqimage/7_RUBBUR BUSH_1_1602226277324.jpg', '', '8', '8', '8', '8', '8', '8', '8', '8', '8', '8', 1, 1602226277324, 1, 1602239134169, 0, 0),
(9, 1, 1, 'SLIDER CAP', 'boqimage/8_SLIDER CAP_1_1602226277324.jpg', '', '9', '9', '9', '9', '9', '9', '9', '9', '9', '9', 1, 1602226277324, 1, 1602239134169, 0, 0),
(10, 1, 1, 'REVIT', 'boqimage/9_REVIT_1_1602226277324.jpg', '', '10', '10', '10', '10', '10', '10', '10', '10', '10', '10', 1, 1602226277324, 1, 1602239134169, 0, 0),
(11, 1, 1, 'LACTITE SF 770', 'boqimage/10_LACTITE SF 770_1_1602226277324.jpg', '', '11', '11', '11', '11', '11', '11', '11', '11', '11', '11', 1, 1602226277324, 1, 1602239134169, 0, 0),
(12, 1, 1, 'LACTITE HUTT (GLOW)', 'boqimage/11_LACTITE HUTT (GLOW)_1_1602226277324.jpg', '', '12', '12', '12', '12', '12', '12', '12', '12', '12', '12', 1, 1602226277324, 1, 1602239134169, 0, 0),
(13, 1, 1, 'PLASTIC BAG(HW)', 'boqimage/12_PLASTIC BAG(HW)_1_1602226277324.jpg', '', '13', '13', '13', '13', '13', '13', '13', '13', '13', '13', 1, 1602226277324, 1, 1602239134169, 0, 0),
(14, 1, 1, 'L-BRACKET', 'boqimage/13_L-BRACKET_1_1602226277324.jpg', '', '14', '14', '14', '14', '14', '14', '14', '14', '14', '14', 1, 1602226277324, 1, 1602239134169, 0, 0),
(15, 1, 1, 'SCREW PACKET', 'boqimage/14_SCREW PACKET_1_1602226277324.jpg', '', '15', '15', '15', '15', '15', '15', '15', '15', '15', '15', 1, 1602226277324, 1, 1602239134169, 0, 0),
(16, 1, 1, 'CARTON', 'boqimage/15_CARTON_1_1602226277324.jpg', '', '16', '16', '16', '16', '16', '16', '16', '16', '16', '16', 1, 1602226277324, 1, 1602239134169, 0, 0),
(24, 1, 7, 'SUNNY', 'boqimage/0_SUNNY_7_1602748380864.jpg', '', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', 1, 1602748380864, 1, 1602749749127, 0, 0),
(25, 1, 7, 'Pratik', 'boqimage/1_Pratik_7_1602748380864.jpg', '', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', 1, 1602748380864, 1, 1602749749127, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_branch_master`
--

CREATE TABLE `tr_branch_master` (
  `id` int(11) NOT NULL,
  `branch_name` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_branch_master`
--

INSERT INTO `tr_branch_master` (`id`, `branch_name`, `created_by`, `created_time`) VALUES
(1, 'Virar', 1, 1601644857800),
(2, 'Pune', 1, 1601644857800);

-- --------------------------------------------------------

--
-- Table structure for table `tr_contact_person_master`
--

CREATE TABLE `tr_contact_person_master` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `person_name` varchar(500) NOT NULL,
  `department` varchar(500) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `landline_no` varchar(100) NOT NULL,
  `email` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_time` bigint(17) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `updated_time` bigint(17) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `deleted_time` bigint(17) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_contact_person_master`
--

INSERT INTO `tr_contact_person_master` (`id`, `customer_id`, `person_name`, `department`, `mobile`, `landline_no`, `email`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(15, 1, 'Mr. Milind Dere', 'Purchase', '9967833175', '0', 'milind.dere@doverfs.com', 0, 1600490400665, 0, 0, 0, 0),
(16, 1, 'Prathmesh', 'It', '9004348261', '0', 'p@gmail.com', 0, 1600490400665, 0, 0, 0, 0),
(17, 1, 'Vijaya Karekar', 'Sales', '9920121080', '0', 'v@gmail.com', 0, 1600490400665, 0, 0, 0, 0),
(20, 2, 'Person Name 1', 'Department 1', 'Mobile 1', 'Landline No 1', 'email@email.com', 0, 1600523100768, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_countrymaster`
--

CREATE TABLE `tr_countrymaster` (
  `id` int(11) NOT NULL,
  `countrycode` varchar(10) NOT NULL,
  `countryname` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_countrymaster`
--

INSERT INTO `tr_countrymaster` (`id`, `countrycode`, `countryname`) VALUES
(247, 'AF', 'Afghanistan'),
(248, 'AL', 'Albania'),
(249, 'DZ', 'Algeria'),
(250, 'DS', 'American Samoa'),
(251, 'AD', 'Andorra'),
(252, 'AO', 'Angola'),
(253, 'AI', 'Anguilla'),
(254, 'AQ', 'Antarctica'),
(255, 'AG', 'Antigua and Barbuda'),
(256, 'AR', 'Argentina'),
(257, 'AM', 'Armenia'),
(258, 'AW', 'Aruba'),
(259, 'AU', 'Australia'),
(260, 'AT', 'Austria'),
(261, 'AZ', 'Azerbaijan'),
(262, 'BS', 'Bahamas'),
(263, 'BH', 'Bahrain'),
(264, 'BD', 'Bangladesh'),
(265, 'BB', 'Barbados'),
(266, 'BY', 'Belarus'),
(267, 'BE', 'Belgium'),
(268, 'BZ', 'Belize'),
(269, 'BJ', 'Benin'),
(270, 'BM', 'Bermuda'),
(271, 'BT', 'Bhutan'),
(272, 'BO', 'Bolivia'),
(273, 'BA', 'Bosnia and Herzegovina'),
(274, 'BW', 'Botswana'),
(275, 'BV', 'Bouvet Island'),
(276, 'BR', 'Brazil'),
(277, 'IO', 'British Indian Ocean Territory'),
(278, 'BN', 'Brunei Darussalam'),
(279, 'BG', 'Bulgaria'),
(280, 'BF', 'Burkina Faso'),
(281, 'BI', 'Burundi'),
(282, 'KH', 'Cambodia'),
(283, 'CM', 'Cameroon'),
(284, 'CA', 'Canada'),
(285, 'CV', 'Cape Verde'),
(286, 'KY', 'Cayman Islands'),
(287, 'CF', 'Central African Republic'),
(288, 'TD', 'Chad'),
(289, 'CL', 'Chile'),
(290, 'CN', 'China'),
(291, 'CX', 'Christmas Island'),
(292, 'CC', 'Cocos (Keeling) Islands'),
(293, 'CO', 'Colombia'),
(294, 'KM', 'Comoros'),
(295, 'CG', 'Congo'),
(296, 'CK', 'Cook Islands'),
(297, 'CR', 'Costa Rica'),
(298, 'HR', 'Croatia (Hrvatska)'),
(299, 'CU', 'Cuba'),
(300, 'CY', 'Cyprus'),
(301, 'CZ', 'Czech Republic'),
(302, 'DK', 'Denmark'),
(303, 'DJ', 'Djibouti'),
(304, 'DM', 'Dominica'),
(305, 'DO', 'Dominican Republic'),
(306, 'TP', 'East Timor'),
(307, 'EC', 'Ecuador'),
(308, 'EG', 'Egypt'),
(309, 'SV', 'El Salvador'),
(310, 'GQ', 'Equatorial Guinea'),
(311, 'ER', 'Eritrea'),
(312, 'EE', 'Estonia'),
(313, 'ET', 'Ethiopia'),
(314, 'FK', 'Falkland Islands (Malvinas)'),
(315, 'FO', 'Faroe Islands'),
(316, 'FJ', 'Fiji'),
(317, 'FI', 'Finland'),
(318, 'FR', 'France'),
(319, 'FX', 'France, Metropolitan'),
(320, 'GF', 'French Guiana'),
(321, 'PF', 'French Polynesia'),
(322, 'TF', 'French Southern Territories'),
(323, 'GA', 'Gabon'),
(324, 'GM', 'Gambia'),
(325, 'GE', 'Georgia'),
(326, 'DE', 'Germany'),
(327, 'GH', 'Ghana'),
(328, 'GI', 'Gibraltar'),
(329, 'GK', 'Guernsey'),
(330, 'GR', 'Greece'),
(331, 'GL', 'Greenland'),
(332, 'GD', 'Grenada'),
(333, 'GP', 'Guadeloupe'),
(334, 'GU', 'Guam'),
(335, 'GT', 'Guatemala'),
(336, 'GN', 'Guinea'),
(337, 'GW', 'Guinea-Bissau'),
(338, 'GY', 'Guyana'),
(339, 'HT', 'Haiti'),
(340, 'HM', 'Heard and Mc Donald Islands'),
(341, 'HN', 'Honduras'),
(342, 'HK', 'Hong Kong'),
(343, 'HU', 'Hungary'),
(344, 'IS', 'Iceland'),
(345, 'IN', 'India'),
(346, 'IM', 'Isle of Man'),
(347, 'ID', 'Indonesia'),
(348, 'IR', 'Iran (Islamic Republic of)'),
(349, 'IQ', 'Iraq'),
(350, 'IE', 'Ireland'),
(351, 'IL', 'Israel'),
(352, 'IT', 'Italy'),
(353, 'CI', 'Ivory Coast'),
(354, 'JE', 'Jersey'),
(355, 'JM', 'Jamaica'),
(356, 'JP', 'Japan'),
(357, 'JO', 'Jordan'),
(358, 'KZ', 'Kazakhstan'),
(359, 'KE', 'Kenya'),
(360, 'KI', 'Kiribati'),
(361, 'KP', 'Korea, Democratic People\'s Republic of'),
(362, 'KR', 'Korea, Republic of'),
(363, 'XK', 'Kosovo'),
(364, 'KW', 'Kuwait'),
(365, 'KG', 'Kyrgyzstan'),
(366, 'LA', 'Lao People\'s Democratic Republic'),
(367, 'LV', 'Latvia'),
(368, 'LB', 'Lebanon'),
(369, 'LS', 'Lesotho'),
(370, 'LR', 'Liberia'),
(371, 'LY', 'Libyan Arab Jamahiriya'),
(372, 'LI', 'Liechtenstein'),
(373, 'LT', 'Lithuania'),
(374, 'LU', 'Luxembourg'),
(375, 'MO', 'Macau'),
(376, 'MK', 'Macedonia'),
(377, 'MG', 'Madagascar'),
(378, 'MW', 'Malawi'),
(379, 'MY', 'Malaysia'),
(380, 'MV', 'Maldives'),
(381, 'ML', 'Mali'),
(382, 'MT', 'Malta'),
(383, 'MH', 'Marshall Islands'),
(384, 'MQ', 'Martinique'),
(385, 'MR', 'Mauritania'),
(386, 'MU', 'Mauritius'),
(387, 'TY', 'Mayotte'),
(388, 'MX', 'Mexico'),
(389, 'FM', 'Micronesia, Federated States of'),
(390, 'MD', 'Moldova, Republic of'),
(391, 'MC', 'Monaco'),
(392, 'MN', 'Mongolia'),
(393, 'ME', 'Montenegro'),
(394, 'MS', 'Montserrat'),
(395, 'MA', 'Morocco'),
(396, 'MZ', 'Mozambique'),
(397, 'MM', 'Myanmar'),
(398, 'NA', 'Namibia'),
(399, 'NR', 'Nauru'),
(400, 'NP', 'Nepal'),
(401, 'NL', 'Netherlands'),
(402, 'AN', 'Netherlands Antilles'),
(403, 'NC', 'New Caledonia'),
(404, 'NZ', 'New Zealand'),
(405, 'NI', 'Nicaragua'),
(406, 'NE', 'Niger'),
(407, 'NG', 'Nigeria'),
(408, 'NU', 'Niue'),
(409, 'NF', 'Norfolk Island'),
(410, 'MP', 'Northern Mariana Islands'),
(411, 'NO', 'Norway'),
(412, 'OM', 'Oman'),
(413, 'PK', 'Pakistan'),
(414, 'PW', 'Palau'),
(415, 'PS', 'Palestine'),
(416, 'PA', 'Panama'),
(417, 'PG', 'Papua New Guinea'),
(418, 'PY', 'Paraguay'),
(419, 'PE', 'Peru'),
(420, 'PH', 'Philippines'),
(421, 'PN', 'Pitcairn'),
(422, 'PL', 'Poland'),
(423, 'PT', 'Portugal'),
(424, 'PR', 'Puerto Rico'),
(425, 'QA', 'Qatar'),
(426, 'RE', 'Reunion'),
(427, 'RO', 'Romania'),
(428, 'RU', 'Russian Federation'),
(429, 'RW', 'Rwanda'),
(430, 'KN', 'Saint Kitts and Nevis'),
(431, 'LC', 'Saint Lucia'),
(432, 'VC', 'Saint Vincent and the Grenadines'),
(433, 'WS', 'Samoa'),
(434, 'SM', 'San Marino'),
(435, 'ST', 'Sao Tome and Principe'),
(436, 'SA', 'Saudi Arabia'),
(437, 'SN', 'Senegal'),
(438, 'RS', 'Serbia'),
(439, 'SC', 'Seychelles'),
(440, 'SL', 'Sierra Leone'),
(441, 'SG', 'Singapore'),
(442, 'SK', 'Slovakia'),
(443, 'SI', 'Slovenia'),
(444, 'SB', 'Solomon Islands'),
(445, 'SO', 'Somalia'),
(446, 'ZA', 'South Africa'),
(447, 'GS', 'South Georgia South Sandwich Islands'),
(448, 'SS', 'South Sudan'),
(449, 'ES', 'Spain'),
(450, 'LK', 'Sri Lanka'),
(451, 'SH', 'St. Helena'),
(452, 'PM', 'St. Pierre and Miquelon'),
(453, 'SD', 'Sudan'),
(454, 'SR', 'Suriname'),
(455, 'SJ', 'Svalbard and Jan Mayen Islands'),
(456, 'SZ', 'Swaziland'),
(457, 'SE', 'Sweden'),
(458, 'CH', 'Switzerland'),
(459, 'SY', 'Syrian Arab Republic'),
(460, 'TW', 'Taiwan'),
(461, 'TJ', 'Tajikistan'),
(462, 'TZ', 'Tanzania, United Republic of'),
(463, 'TH', 'Thailand'),
(464, 'TG', 'Togo'),
(465, 'TK', 'Tokelau'),
(466, 'TO', 'Tonga'),
(467, 'TT', 'Trinidad and Tobago'),
(468, 'TN', 'Tunisia'),
(469, 'TR', 'Turkey'),
(470, 'TM', 'Turkmenistan'),
(471, 'TC', 'Turks and Caicos Islands'),
(472, 'TV', 'Tuvalu'),
(473, 'UG', 'Uganda'),
(474, 'UA', 'Ukraine'),
(475, 'AE', 'United Arab Emirates'),
(476, 'GB', 'United Kingdom'),
(477, 'US', 'United States'),
(478, 'UM', 'United States minor outlying islands'),
(479, 'UY', 'Uruguay'),
(480, 'UZ', 'Uzbekistan'),
(481, 'VU', 'Vanuatu'),
(482, 'VA', 'Vatican City State'),
(483, 'VE', 'Venezuela'),
(484, 'VN', 'Vietnam'),
(485, 'VG', 'Virgin Islands (British)'),
(486, 'VI', 'Virgin Islands (U.S.)'),
(487, 'WF', 'Wallis and Futuna Islands'),
(488, 'EH', 'Western Sahara'),
(489, 'YE', 'Yemen'),
(490, 'ZR', 'Zaire'),
(491, 'ZM', 'Zambia'),
(492, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tr_customer_master`
--

CREATE TABLE `tr_customer_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `customer_short_name` varchar(500) NOT NULL,
  `vendor_code` varchar(100) NOT NULL,
  `company_address` text NOT NULL,
  `contact_person` varchar(500) NOT NULL,
  `contact_no` varchar(20) NOT NULL,
  `gst_no` varchar(100) NOT NULL,
  `tax_structure` varchar(500) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `active` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_customer_master`
--

INSERT INTO `tr_customer_master` (`id`, `branch_id`, `customer_name`, `customer_short_name`, `vendor_code`, `company_address`, `contact_person`, `contact_no`, `gst_no`, `tax_structure`, `remark`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 'GODREJ & BOYCE MFG. COM. LTD (SHIRVAL)', 'GB SHRV', '1234', 'Bhayanders', 'pr', '9920121080', 'ijijkmk', '123456789', 'pp', 0, 1, 1600998768972, 1, 1602134398664, 0, 0),
(2, 1, 'GODREJ & BOYCE MFG. COM. LTD (VIKHROLI)', 'GB VIKH', '12', 'hjk', 'nm', '', '52', '4152', 'm', 0, 1, 1600998768972, 0, 0, 0, 0),
(3, 1, 'GODREJ & BOYCE MFG. COM. LTD (BHIVANDI)', 'GB BHIVND', '542', 'vgbh', 'bhn', '', '78452', '4512', 'bn', 0, 1, 1600998768972, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_drawing_component_master`
--

CREATE TABLE `tr_drawing_component_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `drawing_id` int(11) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `component_drawing_no` varchar(100) NOT NULL,
  `drawing_photo` text NOT NULL,
  `rev_chk_frequency` varchar(100) NOT NULL,
  `last_check_date` date NOT NULL,
  `next_due_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_drawing_component_master`
--

INSERT INTO `tr_drawing_component_master` (`id`, `branch_id`, `drawing_id`, `component_name`, `component_drawing_no`, `drawing_photo`, `rev_chk_frequency`, `last_check_date`, `next_due_date`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, 1, 'ASSY', '1', 'drawingmasterimage/ASSY_1_1601024551746.png', '1 Month', '2020-09-01', '2020-09-30', 1, 1601024551746, 1, 1601623898557, 0, 0, ''),
(2, 1, 1, 'ROD', '2', 'drawingmasterimage/ROD_1_1601027879936.png', '2 Month', '2020-09-15', '2020-09-25', 1, 1601027879936, 1, 1601623898557, 0, 0, ''),
(3, 1, 1, 'TRAY', '3', 'drawingmasterimage/TRAY_1_1601028008130.png', '3 Month', '2020-09-01', '2020-09-30', 1, 1601028008130, 1, 1601623898557, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_drawing_master`
--

CREATE TABLE `tr_drawing_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `drawing_no` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_drawing_master`
--

INSERT INTO `tr_drawing_master` (`id`, `branch_id`, `product_name`, `drawing_no`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, '1', '1234', 1, 1601024551746, 1, 1601623898557, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_fixture_component`
--

CREATE TABLE `tr_fixture_component` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `fixture_id` int(11) NOT NULL,
  `process_name` varchar(500) NOT NULL,
  `fixture` varchar(500) NOT NULL,
  `fixture_number` varchar(200) NOT NULL,
  `photo` text NOT NULL,
  `fixture_check_list` text NOT NULL,
  `calibration_frequency` varchar(200) NOT NULL,
  `last_calibration_date` date NOT NULL,
  `next_calibration_date` date NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_fixture_component`
--

INSERT INTO `tr_fixture_component` (`id`, `branch_id`, `fixture_id`, `process_name`, `fixture`, `fixture_number`, `photo`, `fixture_check_list`, `calibration_frequency`, `last_calibration_date`, `next_calibration_date`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 'SHEARING', 'NO TOOL REQD', '1234', 'fixturemasterimage/photo_0__1601056066781.jpg', 'fixturemasterimage/FixtureList0__1601056066781.jpg', '1', '2020-09-01', '2020-09-10', '1', 1, 1601055550994, 1, 1601625315032, 0, 0),
(2, 1, 1, 'CORNER PUNCHING', 'CRN PUNCHING TOOL', 'CRN-001', 'fixturemasterimage/photo_1__1601056039325.jpg', 'fixturemasterimage/FixtureList1__1601056039325.jpg', '2', '2020-09-10', '2020-09-20', '2', 1, 1601056039325, 1, 1601625315032, 0, 0),
(3, 1, 1, 'HOLE PUNCHING', 'HOLE PUNCH TOOL', 'HP-001', 'fixturemasterimage/photo_2__1601056039325.jpg', 'fixturemasterimage/FixtureList2__1601056039325.jpg', '3', '2020-09-20', '2020-09-30', '3', 1, 1601056039325, 1, 1601625315032, 0, 0),
(4, 1, 2, 'prathmesh', '1', '1', 'fixturemasterimage/photo_0_2_1601972563970.png', 'fixturemasterimage/FixtureList_0_2_1601968888587.png', '1', '2020-10-06', '2020-10-06', '1', 1, 1601968888587, 1, 1601972563970, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_fixture_master`
--

CREATE TABLE `tr_fixture_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `sub_sr` varchar(100) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_fixture_master`
--

INSERT INTO `tr_fixture_master` (`id`, `branch_id`, `product_name`, `sub_sr`, `component_name`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, '1', '1', 'Tray', 1, 1601055441503, 1, 1601625315032, 0, 0, ''),
(2, 1, '2', '1', 'p', 1, 1601968888587, 1, 1601972563970, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_inspection_gauges_component`
--

CREATE TABLE `tr_inspection_gauges_component` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `inspect_gauges_id` int(11) NOT NULL,
  `process_name` varchar(500) NOT NULL,
  `inspect_gauges` varchar(500) NOT NULL,
  `gauges_number` varchar(200) NOT NULL,
  `photo` text NOT NULL,
  `gauges_check_list` text NOT NULL,
  `calibration_frequency` varchar(200) NOT NULL,
  `last_calibration_date` date NOT NULL,
  `next_calibration_date` date NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_inspection_gauges_component`
--

INSERT INTO `tr_inspection_gauges_component` (`id`, `branch_id`, `inspect_gauges_id`, `process_name`, `inspect_gauges`, `gauges_number`, `photo`, `gauges_check_list`, `calibration_frequency`, `last_calibration_date`, `next_calibration_date`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 'SHEARING', 'NO TOOL REQD', '123', 'inspectmasterimage/photo_1_1601051512214.jpg', 'inspectmasterimage/Checklist_1_1601051512214.jpg', '1', '2020-09-01', '2020-09-10', '1', 1, 1601051512214, 1, 1601625148136, 0, 0),
(2, 1, 1, 'CORNER PUNCHING', 'CRN PUNCHING TOOL', 'CRN-001', 'inspectmasterimage/photo_1_1_1601052398732.jpg', 'inspectmasterimage/Checklist_1_1_1601052398732.jpg', '2', '2020-09-10', '2020-09-20', '2', 1, 1601052398732, 1, 1601625148136, 0, 0),
(3, 1, 1, 'HOLE PUNCHING', 'HOLE PUNCH TOOL', 'HP-001', 'inspectmasterimage/photo_2_1_1601052398732.jpg', 'inspectmasterimage/Checklist_2_1_1601052398732.jpg', '3', '2020-09-20', '2020-09-30', '3', 1, 1601052398732, 1, 1601625148136, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_inspection_gauges_master`
--

CREATE TABLE `tr_inspection_gauges_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `sub_sr` varchar(100) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` int(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` int(17) NOT NULL,
  `active` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_inspection_gauges_master`
--

INSERT INTO `tr_inspection_gauges_master` (`id`, `branch_id`, `product_name`, `sub_sr`, `component_name`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, '1', '1', 'Tray', 1, 1601051512214, 1, 2147483647, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_item_component_opening_balance`
--

CREATE TABLE `tr_item_component_opening_balance` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `opening_balace_id` int(11) NOT NULL,
  `item_name` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `opening_balance` double(11,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_item_component_opening_balance`
--

INSERT INTO `tr_item_component_opening_balance` (`id`, `branch_id`, `opening_balace_id`, `item_name`, `component_id`, `component_name`, `opening_balance`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 1, 2, 'ROD', 100.00, 1, 1602490703865, 0, 0, 0, 0),
(2, 1, 1, 1, 3, 'TRAY', 200.00, 1, 1602490703865, 0, 0, 0, 0),
(3, 1, 1, 1, 4, 'MOUCE TRAY', 300.00, 1, 1602490703865, 0, 0, 0, 0),
(4, 1, 1, 1, 5, 'HARDWARE', 400.00, 1, 1602490703865, 0, 0, 0, 0),
(5, 1, 1, 1, 6, 'PLASTIC BAG', 500.00, 1, 1602490703865, 0, 0, 0, 0),
(6, 1, 2, 1, 2, 'ROD', 0.00, 1, 1602503818087, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_item_group_master`
--

CREATE TABLE `tr_item_group_master` (
  `id` bigint(20) NOT NULL,
  `mainGroupCode` varchar(50) DEFAULT NULL,
  `mainGroupName` varchar(40) DEFAULT NULL,
  `groupCode` varchar(50) DEFAULT NULL,
  `groupName` varchar(40) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `createdby` varchar(50) DEFAULT '0',
  `status` varchar(50) DEFAULT '0',
  `approvedby` varchar(50) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_item_group_master`
--

INSERT INTO `tr_item_group_master` (`id`, `mainGroupCode`, `mainGroupName`, `groupCode`, `groupName`, `title`, `createdby`, `status`, `approvedby`) VALUES
(1, 'MG', 'MAIN GROUP', 'MG', 'MAIN GROUP', 'MAIN GROUP', '0', '0', '0'),
(2, 'MG', 'MAIN GROUP', '2', 'RAW MATERIAL', 'RAW MATERIAL', '1', '0', '0'),
(3, 'MG', 'MAIN GROUP', '3', 'HARDWARE', 'HARDWARE', '1', '0', '0'),
(5, '2', 'RAW MATERIAL', '4', 'HR', 'HR', '1', '0', '0'),
(6, '2', 'RAW MATERIAL', '6', 'CRCA', 'CRCA', '1', '0', '0'),
(7, '3', 'HARDWARE', '7', 'SS', 'SS', '1', '0', '0'),
(8, '3', 'HARDWARE', '8', 'MS', 'MS', '1', '0', '0'),
(9, '7', 'SS', '9', 'WASHER', 'WASHER', '1', '0', '0'),
(10, '7', 'SS', '10', 'RIVET', 'RIVET', '1', '0', '0'),
(11, '8', 'MS', '11', 'WASHER', 'WASHER', '1', '0', '0'),
(12, '8', 'MS', '12', 'REVERT', 'REVERT', '1', '0', '0'),
(13, '9', 'WASHER', '13', 'P', 'P', '1', '0', '0'),
(14, '13', 'P', '14', 'R', 'R', '1', '0', '0'),
(15, '14', 'R', '15', 'A', 'A', '1', '0', '0'),
(16, 'MG', 'MAIN GROUP', '16', 'FINISH GOOD', 'FINISH GOOD', '1', '0', '0'),
(18, '16', 'FINISH GOOD', '18', 'CPU', 'CPU', '1', '0', '0'),
(19, '16', 'FINISH GOOD', '19', 'BOOKCASE', 'BOOKCASE', '1', '0', '0'),
(20, '16', 'FINISH GOOD', '20', 'LINEA BKT', 'LINEA BKT', '1', '0', '0'),
(21, '16', 'FINISH GOOD', '21', 'WT', 'WT', '1', '0', '0'),
(22, '2', 'RAW MATERIAL', '22', 'BOUGHT OUT', 'BOUGHT OUT', '1', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_item_master`
--

CREATE TABLE `tr_item_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `main_group` varchar(100) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `description` varchar(500) NOT NULL,
  `stock_ind` varchar(20) NOT NULL,
  `unit` varchar(20) NOT NULL,
  `class` varchar(10) NOT NULL,
  `max_level` double(11,2) NOT NULL,
  `min_level` double(11,2) NOT NULL,
  `reorder_level` double(11,2) NOT NULL,
  `reorder_qty` double(11,2) NOT NULL,
  `hsn_code` int(11) NOT NULL,
  `gst_rate` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_item_master`
--

INSERT INTO `tr_item_master` (`id`, `branch_id`, `main_group`, `component_name`, `item_name`, `description`, `stock_ind`, `unit`, `class`, `max_level`, `min_level`, `reorder_level`, `reorder_qty`, `hsn_code`, `gst_rate`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, 'Finish Good', '', 'KBPT', 'KBPT', 'stock', '28', 'A', 10.00, 5.00, 5.00, 10.00, 123, 18, 1, 1601617488032, 1, 1601622911576, 0, 0, ''),
(2, 1, 'Component', '1', 'ROD', 'ROD', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(3, 1, 'Component', '1', 'TRAY', 'TRAY', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(4, 1, 'Component', '1', 'MOUCE TRAY', 'MOUCE TRAY', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(5, 1, 'Component', '1', 'HARDWARE', 'HARDWARE', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(6, 1, 'Component', '1', 'PLASTIC BAG', 'PLASTIC BAG', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(7, 1, 'Component', '1', 'SLIDER', 'SLIDER', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(8, 1, 'Component', '1', 'MS NUT BUSH', 'MS NUT BUSH', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(9, 1, 'Component', '1', 'RUBBUR BUSH', 'RUBBUR BUSH', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(10, 1, 'Component', '1', 'SLIDER CAP', 'SLIDER CAP', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(11, 1, 'Component', '1', 'REVIT', 'REVIT', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(12, 1, 'Component', '1', 'LACTITE SF 770', 'LACTITE SF 770', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(13, 1, 'Component', '1', 'LACTITE HUTT (GLOW)', 'LACTITE HUTT (GLOW)', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(14, 1, 'Component', '1', 'PLASTIC BAG(HW)', 'PLASTIC BAG(HW)', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(15, 1, 'Component', '1', 'L-BRACKET', 'L-BRACKET', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(16, 1, 'Component', '1', 'SCREW PACKET', 'SCREW PACKET', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(17, 1, 'Component', '1', 'CARTON', 'CARTON', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(18, 1, 'Component', '1', 'Vijaya', 'Vijaya', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(19, 1, 'Component', '1', 'prathmesh', 'prathmesh', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602233687295, 1, 1602742155152, 0, 0, ''),
(20, 1, 'Component', '1', 'Karekars', 'Karekar', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602234043255, 1, 1602742155152, 0, 0, ''),
(22, 1, 'Component', '1', 'SUNNY', 'SUnny', 'stock', '28', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602249200444, 1, 1602749749127, 0, 0, ''),
(28, 1, 'Component', '1', 'Pratik', 'Pratik', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602748380864, 1, 1602749749127, 0, 0, ''),
(32, 1, 'Component', '1', 'HardCore', 'HardCore', 'stock', '', 'C', 0.00, 0.00, 0.00, 0.00, 0, 0, 1, 1602749749127, 0, 0, 0, 0, ''),
(33, 1, 'Finish Good', '', 'KBPT WITH MOUSE', '350 mm', 'stock', '28', 'C', 10.00, 5.00, 10.00, 10.00, 123, 18, 1, 1617343988649, 0, 0, 0, 0, ''),
(34, 1, 'Component', '33', 'TRAY', '320*578*0.8', 'stock', '28', 'A', 10.00, 5.00, 10.00, 5.00, 1234, 18, 1, 1617344337570, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_item_opening_balance`
--

CREATE TABLE `tr_item_opening_balance` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `finish_good_item_name` int(11) NOT NULL,
  `opening_balance_date` date NOT NULL,
  `opening_balance_timestamp_date` bigint(17) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_item_opening_balance`
--

INSERT INTO `tr_item_opening_balance` (`id`, `branch_id`, `finish_good_item_name`, `opening_balance_date`, `opening_balance_timestamp_date`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, '2020-10-01', 1601490600000, 1, 1602490703865, 0, 0, 0, 0),
(2, 1, 1, '0000-00-00', 0, 1, 1602503818087, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_ledger_master`
--

CREATE TABLE `tr_ledger_master` (
  `id` int(11) NOT NULL,
  `partyname` varchar(200) DEFAULT NULL,
  `partytype` int(11) NOT NULL DEFAULT '0',
  `partyaddress` text,
  `account_name` varchar(150) DEFAULT NULL,
  `mobileno` varchar(100) DEFAULT NULL,
  `gstno` varchar(20) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT '0',
  `created_time` bigint(17) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `updated_time` bigint(17) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `deleted_time` bigint(17) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_ledger_master`
--

INSERT INTO `tr_ledger_master` (`id`, `partyname`, `partytype`, `partyaddress`, `account_name`, `mobileno`, `gstno`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 'party name 1', 2, 'address 1', 'account name 1', 'mobile no 1', 'gst mo 1', 0, 1, 1600163049154, 1, 1600234761134, 0, 0),
(2, 'asdfgb', 2, 'wsedf', '1', 'sedrfg', 'sdf', 1, 1, 1600163194962, 1, 1600164685591, 1, 1600164724773),
(3, '', 0, '', '', '', '', 0, 1, 1600411938677, 0, 0, 0, 0),
(4, '', 0, '', '', '', '', 0, 1, 1600411945265, 0, 0, 0, 0),
(5, '', 0, '', '', '', '', 0, 1, 1600412004684, 0, 0, 0, 0),
(6, '', 0, '', '', '', '', 0, 1, 1600412015139, 0, 0, 0, 0),
(7, '', 0, '', '', '', '', 0, 1, 1600412037253, 0, 0, 0, 0),
(8, '', 0, '', '', '', '', 0, 1, 1600412047358, 0, 0, 0, 0),
(9, '', 0, '', '', '', '', 0, 1, 1600412153524, 0, 0, 0, 0),
(10, '', 0, '', '', '', '', 0, 1, 1600412362373, 0, 0, 0, 0),
(11, '', 0, '', '', '', '', 0, 1, 1600412401729, 0, 0, 0, 0),
(12, '', 0, '', '', '', '', 0, 1, 1600412440435, 0, 0, 0, 0),
(13, '', 0, '', '', '', '', 0, 1, 1600412510196, 0, 0, 0, 0),
(14, '', 0, '', '', '', '', 0, 1, 1600412524328, 0, 0, 0, 0),
(15, '', 0, '', '', '', '', 0, 1, 1600412528732, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_location_master`
--

CREATE TABLE `tr_location_master` (
  `id` int(11) NOT NULL,
  `location_name` varchar(150) DEFAULT NULL,
  `location_address` text,
  `mobileno` varchar(100) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT '0',
  `created_time` bigint(17) NOT NULL DEFAULT '0',
  `updated_by` int(11) DEFAULT '0',
  `updated_time` bigint(17) NOT NULL DEFAULT '0',
  `deleted_by` int(11) NOT NULL DEFAULT '0',
  `deleted_time` bigint(17) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tr_location_master`
--

INSERT INTO `tr_location_master` (`id`, `location_name`, `location_address`, `mobileno`, `active`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 'Vasai Road', 'Near Railway Station', '9975701724', 1, 1, 1600160205058, 1, 1600160276700, 0, 0),
(2, 'mumbai', 'sdfvb', '9876543212', 1, 1, 1600162881817, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_machine_component`
--

CREATE TABLE `tr_machine_component` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL,
  `machine` varchar(500) NOT NULL,
  `size` varchar(100) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `machine_number` varchar(100) NOT NULL,
  `sop` varchar(200) NOT NULL,
  `machine_photo` text NOT NULL,
  `check_sheet_maintainance` varchar(500) NOT NULL,
  `maintenance_frequency` varchar(500) NOT NULL,
  `last_maintenance_date` date NOT NULL,
  `next_maintenance_date` date NOT NULL,
  `special_remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_machine_component`
--

INSERT INTO `tr_machine_component` (`id`, `branch_id`, `machine_id`, `machine`, `size`, `capacity`, `machine_number`, `sop`, `machine_photo`, `check_sheet_maintainance`, `maintenance_frequency`, `last_maintenance_date`, `next_maintenance_date`, `special_remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 'SHEARING', '1', '1', '1', '1', 'machinemasterimage/Machinephoto_0_1_1601062233946.png', '1', '1', '0000-00-00', '0000-00-00', '', 1, 1601062233946, 0, 0, 0, 0),
(2, 1, 1, 'ROTARY CUTTURE', '2', '2', '2', '2', 'machinemasterimage/Machinephoto_1_1_1601062233946.png', '2', '2', '0000-00-00', '0000-00-00', '', 1, 1601062233946, 0, 0, 0, 0),
(4, 1, 1, 'hbnjm', '3', '3', '3', '3', 'machinemasterimage/Machinephoto_2_1_1601064567482.jpg', '3', '3', '0000-00-00', '0000-00-00', '', 1, 1601064567482, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_machine_master`
--

CREATE TABLE `tr_machine_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `department` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_machine_master`
--

INSERT INTO `tr_machine_master` (`id`, `branch_id`, `department`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, 'SHEARING / CURRING', 1, 1601062233946, 1, 1601064567482, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_man_master`
--

CREATE TABLE `tr_man_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `emp_name` varchar(500) DEFAULT NULL,
  `emp_photo` text,
  `gender` varchar(20) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `contact_no` varchar(20) DEFAULT NULL,
  `email_id` varchar(100) DEFAULT NULL,
  `experience` varchar(50) NOT NULL,
  `qualification` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `pAddress` text NOT NULL,
  `language` varchar(100) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `emergency_name` varchar(200) NOT NULL,
  `emergency_contact` varchar(20) NOT NULL,
  `relation` varchar(100) NOT NULL,
  `machine_id` varchar(20) NOT NULL,
  `reference_by` varchar(200) NOT NULL,
  `category` varchar(100) NOT NULL,
  `department` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `salary_per` varchar(20) NOT NULL,
  `payment` varchar(20) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `branch_name` varchar(100) NOT NULL,
  `ifsc_code` varchar(50) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `smoking` varchar(10) NOT NULL,
  `tobacco` varchar(10) NOT NULL,
  `liquor` varchar(10) NOT NULL,
  `basic` double(11,2) NOT NULL,
  `da` double(11,2) NOT NULL,
  `hra` double(11,2) NOT NULL,
  `travelling` double(11,2) NOT NULL,
  `medical` double(11,2) NOT NULL,
  `conveyance` double(11,2) NOT NULL,
  `education` double(11,2) NOT NULL,
  `cca` double(11,2) NOT NULL,
  `total_salary` double(11,2) NOT NULL,
  `mobile` double(11,2) NOT NULL,
  `uniform` double(11,2) NOT NULL,
  `internet` double(11,2) NOT NULL,
  `employee_esic` double(11,2) NOT NULL,
  `employer_esic` double(11,2) NOT NULL,
  `training_date` date NOT NULL,
  `joining_date` date NOT NULL,
  `probation_period` varchar(100) NOT NULL,
  `permanent_date` date NOT NULL,
  `leaving_date` date NOT NULL,
  `additional_charges` double(11,2) NOT NULL,
  `additional_deduction` double(11,2) NOT NULL,
  `pf_wages` double(11,2) NOT NULL,
  `employee_pf` double(11,2) NOT NULL,
  `employer_pf` double(11,2) NOT NULL,
  `bonus` double(11,2) NOT NULL,
  `ctc` double(11,2) NOT NULL,
  `working_hours` double(11,2) NOT NULL,
  `professional_tax` varchar(20) NOT NULL,
  `over_time` varchar(20) NOT NULL,
  `sunday_payment` varchar(20) NOT NULL,
  `dress_allowed` varchar(20) NOT NULL,
  `shoes_allowed` varchar(20) NOT NULL,
  `lockers_allowed` varchar(20) NOT NULL,
  `esic_no` varchar(50) NOT NULL,
  `pf_no` varchar(50) NOT NULL,
  `it_deduction` varchar(50) NOT NULL,
  `weekly_Off` varchar(50) NOT NULL,
  `aadhar_no` varchar(100) NOT NULL,
  `aadhar_photo` text NOT NULL,
  `pan_no` varchar(100) NOT NULL,
  `pan_photo` text NOT NULL,
  `driving_licence` varchar(100) NOT NULL,
  `driving_licence_photo` text NOT NULL,
  `passport_no` varchar(100) NOT NULL,
  `passport_photo` text NOT NULL,
  `Other_document` varchar(100) NOT NULL,
  `Other_document_photo` text NOT NULL,
  `created_by` varchar(120) DEFAULT NULL,
  `created_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_man_master`
--

INSERT INTO `tr_man_master` (`id`, `branch_id`, `emp_name`, `emp_photo`, `gender`, `dob`, `contact_no`, `email_id`, `experience`, `qualification`, `address`, `pAddress`, `language`, `blood_group`, `emergency_name`, `emergency_contact`, `relation`, `machine_id`, `reference_by`, `category`, `department`, `designation`, `salary_per`, `payment`, `bank_name`, `branch_name`, `ifsc_code`, `account_number`, `smoking`, `tobacco`, `liquor`, `basic`, `da`, `hra`, `travelling`, `medical`, `conveyance`, `education`, `cca`, `total_salary`, `mobile`, `uniform`, `internet`, `employee_esic`, `employer_esic`, `training_date`, `joining_date`, `probation_period`, `permanent_date`, `leaving_date`, `additional_charges`, `additional_deduction`, `pf_wages`, `employee_pf`, `employer_pf`, `bonus`, `ctc`, `working_hours`, `professional_tax`, `over_time`, `sunday_payment`, `dress_allowed`, `shoes_allowed`, `lockers_allowed`, `esic_no`, `pf_no`, `it_deduction`, `weekly_Off`, `aadhar_no`, `aadhar_photo`, `pan_no`, `pan_photo`, `driving_licence`, `driving_licence_photo`, `passport_no`, `passport_photo`, `Other_document`, `Other_document_photo`, `created_by`, `created_time`, `deleted_by`, `deleted_time`, `updated_by`, `updated_time`, `active`) VALUES
(1, 1, 'Prathmesh Karekar', 'manimage/Prathmesh Karekar_1603098333353.jpg', 'Male', '1991-12-11', '9920121080', 'prathmeshkarekar11@gmail.com', '2 Years', 'BCA', 'Bhayander', 'Bhayander', 'Marathi,Hindi,English', 'AB+', 'Vijaya', '9004348261', 'Mother', '123', 'NA', 'SKILLED', 'SALES', 'MANAGER', 'MONTHLY', 'CASH', 'Kotak', 'Bhayander', '123', '1718', 'No', 'No', 'No', 1.00, 2.00, 3.00, 4.00, 5.00, 6.00, 7.00, 8.00, 36.00, 9.00, 1.00, 2.00, 3.00, 4.00, '2020-10-01', '2020-10-19', 'NA', '2020-10-01', '2020-10-31', 0.00, 0.00, 10.00, 1.00, 2.00, 3.00, 603.72, 9.00, 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', '1', '1', '3', 'SUNDAY', '1', 'manimage/Prathmesh Karekar_1_1603094419612.png', '2', 'manimage/Prathmesh Karekar_2_1603094419612.png', '3', 'manimage/Prathmesh Karekar_3_1603094419612.png', '4', 'manimage/Prathmesh Karekar_4_1603094419612.png', '5', 'manimage/Prathmesh Karekar_5_1603098333353.jpg', '1', 1603094419612, 0, 0, 1, 1603098333353, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_material_in`
--

CREATE TABLE `tr_material_in` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `sr_no` int(11) NOT NULL,
  `finish_good_item_name` int(11) NOT NULL,
  `entry_date` date NOT NULL,
  `supplier_name` varchar(500) NOT NULL,
  `document_no` varchar(100) NOT NULL,
  `document_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_material_in`
--

INSERT INTO `tr_material_in` (`id`, `branch_id`, `sr_no`, `finish_good_item_name`, `entry_date`, `supplier_name`, `document_no`, `document_date`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1001, 0, '2020-10-01', 'Prathmesh', '123', '2020-10-01', 1, 1602493174071, 0, 0, 0, 0),
(2, 1, 1002, 0, '2020-10-02', 'ABC', '1234', '2020-10-02', 1, 1602493682434, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_material_in_component`
--

CREATE TABLE `tr_material_in_component` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `material_in_id` int(11) NOT NULL,
  `material_in_sr_no` int(11) NOT NULL,
  `item_name` int(11) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `material_in_qty` double(11,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_material_in_component`
--

INSERT INTO `tr_material_in_component` (`id`, `branch_id`, `material_in_id`, `material_in_sr_no`, `item_name`, `component_name`, `material_in_qty`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 1001, 1, '2', 100.00, 1, 1602493174071, 0, 0, 0, 0),
(2, 1, 1, 1001, 1, '3', 100.00, 1, 1602493174071, 0, 0, 0, 0),
(3, 1, 1, 1001, 1, '4', 100.00, 1, 1602493174071, 0, 0, 0, 0),
(4, 1, 1, 1001, 1, '5', 100.00, 1, 1602493174071, 0, 0, 0, 0),
(5, 1, 1, 1001, 1, '6', 100.00, 1, 1602493174071, 0, 0, 0, 0),
(6, 1, 1, 1001, 1, '7', 100.00, 1, 1602493174071, 0, 0, 0, 0),
(7, 1, 2, 1002, 1, '2', 100.00, 1, 1602493682434, 0, 0, 0, 0),
(8, 1, 2, 1002, 1, '3', 100.00, 1, 1602493682434, 0, 0, 0, 0),
(9, 1, 2, 1002, 1, '4', 100.00, 1, 1602493682434, 0, 0, 0, 0),
(10, 1, 2, 1002, 1, '5', 100.00, 1, 1602493682434, 0, 0, 0, 0),
(11, 1, 2, 1002, 1, '6', 100.00, 1, 1602493682434, 0, 0, 0, 0),
(12, 1, 2, 1002, 1, '7', 100.00, 1, 1602493682434, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_opration_master`
--

CREATE TABLE `tr_opration_master` (
  `id` int(11) NOT NULL,
  `mainGroupCode` varchar(50) NOT NULL,
  `mainGroupName` varchar(500) NOT NULL,
  `groupCode` varchar(50) NOT NULL,
  `groupName` varchar(500) NOT NULL,
  `title` varchar(500) NOT NULL,
  `createdby` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_opration_master`
--

INSERT INTO `tr_opration_master` (`id`, `mainGroupCode`, `mainGroupName`, `groupCode`, `groupName`, `title`, `createdby`) VALUES
(1, 'MG', 'MAIN GROUP', 'MG', 'MAIN GROUP', 'MAIN GROUP', '1'),
(4, 'MG', 'MAIN GROUP', '2', 'CUTTING', 'CUTTING', '1'),
(5, '2', 'CUTTING', '3', 'CUTTING OPRATION', 'CUTTING OPRATION', '1'),
(6, 'MG', 'MAIN GROUP', '4', 'FRABIGATION', 'FRABIGATION', '1'),
(7, '4', 'FRABIGATION', '5', 'PUNCHING', 'PUNCHING', '1'),
(8, '4', 'FRABIGATION', '6', 'BENDING', 'BENDING', '1'),
(9, '4', 'FRABIGATION', '7', 'NOTCHING', 'NOTCHING', '1'),
(14, 'MG', 'MAIN GROUP', '10', 'ASSEMBLY', 'ASSEMBLY', '1'),
(17, '10', 'ASSEMBLY', '9', 'BENDING', 'BENDING', '1'),
(18, '10', 'ASSEMBLY', '10', 'WELDING', 'WELDING', '1'),
(19, 'MG', 'MAIN GROUP', '11', 'PACKING', 'PACKING', '1'),
(20, '11', 'PACKING', '12', 'BOX PACKING', 'BOX PACKING', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tr_order_account_tracking`
--

CREATE TABLE `tr_order_account_tracking` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `order_booking_id` int(11) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `invoice_date` date NOT NULL,
  `dispach_qty` double(11,2) NOT NULL,
  `proof_of_delivery` text NOT NULL,
  `payment_received_doc` varchar(200) NOT NULL,
  `payment_received_date` date NOT NULL,
  `payment_received_amount` double(11,2) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_order_account_tracking`
--

INSERT INTO `tr_order_account_tracking` (`id`, `branch_id`, `order_booking_id`, `invoice_no`, `invoice_date`, `dispach_qty`, `proof_of_delivery`, `payment_received_doc`, `payment_received_date`, `payment_received_amount`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`) VALUES
(1, 1, 1, '1', '2020-10-03', 300.00, 'proofofdelivery/0_1_1_1602143714441.png', '', '2020-10-20', 2000.00, 'No', 1, 1602141587992, 1, 1602143714441),
(2, 1, 1, '2', '2020-10-05', 90.00, 'proofofdelivery/1_2_1_1602141587992.png', '', '2020-10-21', 1000.00, 'Noa', 1, 1602141587992, 1, 1602143714441),
(3, 1, 1, '3', '2020-10-07', 7.00, 'proofofdelivery/2_3_1_1602141587992.png', '', '2020-10-22', 500.00, 'Nooo', 1, 1602141587992, 1, 1602143714441);

-- --------------------------------------------------------

--
-- Table structure for table `tr_order_booking`
--

CREATE TABLE `tr_order_booking` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `customer_name` varchar(500) NOT NULL,
  `po_no` varchar(500) NOT NULL,
  `project_no` varchar(500) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `item_code` varchar(500) NOT NULL,
  `booking_date` date NOT NULL,
  `target_date` date NOT NULL,
  `order_qty` double(11,2) NOT NULL,
  `order_unit` varchar(50) NOT NULL,
  `pc_shade` varchar(500) NOT NULL,
  `customer_remark` varchar(500) NOT NULL,
  `category` varchar(500) NOT NULL,
  `delivery_site` varchar(500) NOT NULL,
  `order_status` varchar(100) NOT NULL,
  `rawmaterial` varchar(500) NOT NULL,
  `hardware` varchar(500) NOT NULL,
  `powderName` varchar(500) NOT NULL,
  `powderRemark` varchar(200) NOT NULL,
  `powderInKg` double(11,2) NOT NULL,
  `packing_material` varchar(500) NOT NULL,
  `special_requirment` varchar(500) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `end_product` varchar(500) NOT NULL,
  `qa_check` varchar(500) NOT NULL,
  `pc` varchar(500) NOT NULL,
  `pck` varchar(500) NOT NULL,
  `order_tracking_no` varchar(200) NOT NULL,
  `tracking_date` date NOT NULL,
  `production_remark` varchar(500) NOT NULL,
  `first_dispach_qty` double(11,2) NOT NULL,
  `second_dispach_qty` double(11,2) NOT NULL,
  `third_dispach_qty` double(11,2) NOT NULL,
  `balance_qty` double(11,2) NOT NULL,
  `open_close_hold` varchar(500) NOT NULL,
  `invoice_no` varchar(500) NOT NULL,
  `invoice_date` date NOT NULL,
  `proof_received` text NOT NULL,
  `payment_received` varchar(500) NOT NULL,
  `account_remark` varchar(500) NOT NULL,
  `reason_for_failing` varchar(500) NOT NULL,
  `dsa_analysing_remark` varchar(500) NOT NULL,
  `completion` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `order_booking_by` int(11) NOT NULL,
  `order_booking_time` bigint(17) NOT NULL,
  `purchase_info_by` int(11) NOT NULL,
  `purchase_info_time` bigint(17) NOT NULL,
  `purchase_info_confirm_by` int(11) NOT NULL,
  `purchase_info_confirm_time` bigint(17) NOT NULL,
  `product_informaton_by` int(11) NOT NULL,
  `product_informaton_time` bigint(17) NOT NULL,
  `product_informaton_confirm_by` int(11) NOT NULL,
  `product_informaton_confirm_time` bigint(17) NOT NULL,
  `dispach_informaton_by` int(11) NOT NULL,
  `dispach_informaton_time` bigint(17) NOT NULL,
  `dispach_informaton_confirm_by` int(11) NOT NULL,
  `dispach_informaton_confirm_time` bigint(17) NOT NULL,
  `acccount_informaton_by` int(11) NOT NULL,
  `acccount_informaton_time` bigint(17) NOT NULL,
  `acccount_informaton_confirm_by` int(11) NOT NULL,
  `acccount_informaton_confirm_time` bigint(17) NOT NULL,
  `dsa_analysing_by` int(11) NOT NULL,
  `dsa_analysing_time` bigint(17) NOT NULL,
  `dsa_analysing_confirm_by` int(11) NOT NULL,
  `dsa_analysing_confirm_time` bigint(17) NOT NULL,
  `active` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_order_booking`
--

INSERT INTO `tr_order_booking` (`id`, `branch_id`, `customer_name`, `po_no`, `project_no`, `item_name`, `item_code`, `booking_date`, `target_date`, `order_qty`, `order_unit`, `pc_shade`, `customer_remark`, `category`, `delivery_site`, `order_status`, `rawmaterial`, `hardware`, `powderName`, `powderRemark`, `powderInKg`, `packing_material`, `special_requirment`, `remark`, `end_product`, `qa_check`, `pc`, `pck`, `order_tracking_no`, `tracking_date`, `production_remark`, `first_dispach_qty`, `second_dispach_qty`, `third_dispach_qty`, `balance_qty`, `open_close_hold`, `invoice_no`, `invoice_date`, `proof_received`, `payment_received`, `account_remark`, `reason_for_failing`, `dsa_analysing_remark`, `completion`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `order_booking_by`, `order_booking_time`, `purchase_info_by`, `purchase_info_time`, `purchase_info_confirm_by`, `purchase_info_confirm_time`, `product_informaton_by`, `product_informaton_time`, `product_informaton_confirm_by`, `product_informaton_confirm_time`, `dispach_informaton_by`, `dispach_informaton_time`, `dispach_informaton_confirm_by`, `dispach_informaton_confirm_time`, `acccount_informaton_by`, `acccount_informaton_time`, `acccount_informaton_confirm_by`, `acccount_informaton_confirm_time`, `dsa_analysing_by`, `dsa_analysing_time`, `dsa_analysing_confirm_by`, `dsa_analysing_confirm_time`, `active`) VALUES
(1, 1, '1', '17', '17', '17', '17', '2020-10-01', '2020-10-31', 100.00, 'BAG', '17', 'NA', '1', 'NA', 'Placed Order', '', '', '', '', 0.00, '', '', '', '', '', '', '', '', '0000-00-00', '', 0.00, 0.00, 0.00, 0.00, '', '', '0000-00-00', '', '', '', '', '', '', 1, 1602573069377, 0, 0, 0, 0, 1, 1602573265462, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_order_dispach_tracking`
--

CREATE TABLE `tr_order_dispach_tracking` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `order_booking_id` int(11) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `invoice_date` date NOT NULL,
  `dispach_qty` double(11,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_order_dispach_tracking`
--

INSERT INTO `tr_order_dispach_tracking` (`id`, `branch_id`, `order_booking_id`, `invoice_no`, `invoice_date`, `dispach_qty`, `created_by`, `created_time`, `updated_by`, `updated_time`) VALUES
(1, 1, 1, '1', '2020-10-03', 300.00, 1, 1602054881493, 1, 1602059575421),
(2, 1, 1, '2', '2020-10-05', 90.00, 1, 1602054881493, 1, 1602059575421),
(3, 1, 1, '3', '2020-10-07', 7.00, 1, 1602057762515, 1, 1602059575421);

-- --------------------------------------------------------

--
-- Table structure for table `tr_ppep_componet`
--

CREATE TABLE `tr_ppep_componet` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `process` varchar(500) NOT NULL,
  `sop` text NOT NULL,
  `check_list` text NOT NULL,
  `online_qa_graph` text NOT NULL,
  `list_pokayoke` text NOT NULL,
  `training_sheet` text NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_ppep_componet`
--

INSERT INTO `tr_ppep_componet` (`id`, `product_id`, `process`, `sop`, `check_list`, `online_qa_graph`, `list_pokayoke`, `training_sheet`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 'PUNCHING', 'ppepimage/sop_0_1_1601069092731.jpg', 'ppepimage/check_list_0_1_1601069092731.jpg', 'ppepimage/online_qa_graph_0_1_1601069092731.jpg', 'ppepimage/list_pokayoke_0_1_1601069092731.jpg', 'ppepimage/training_sheet_0_1_1601069092731.jpg', 'jk1', 1, 1601069092731, 1, 1601071892574, 0, 0),
(2, 1, 'PUNCHING', 'ppepimage/sop_0_1_1601069092731.jpg', 'ppepimage/check_list_0_1_1601069092731.jpg', 'ppepimage/online_qa_graph_0_1_1601069092731.jpg', 'ppepimage/list_pokayoke_0_1_1601069092731.jpg', 'ppepimage/training_sheet_0_1_1601069092731.jpg', 'jk1', 1, 1601071800219, 1, 1601071892574, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_ppep_master`
--

CREATE TABLE `tr_ppep_master` (
  `id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `control_chart` text NOT NULL,
  `pdir` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_ppep_master`
--

INSERT INTO `tr_ppep_master` (`id`, `product_name`, `control_chart`, `pdir`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 'KBPT', 'ppepimage/controlChart_1601069092731.jpg', 'ppepimage/pdir1601069092731.jpg', 1, 1601069092731, 1, 1601071892574, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_quality_instrument_master`
--

CREATE TABLE `tr_quality_instrument_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `instrument_name` varchar(500) NOT NULL,
  `capacity` varchar(500) NOT NULL,
  `accuracy` varchar(500) NOT NULL,
  `make` varchar(500) NOT NULL,
  `purchase_date` date NOT NULL,
  `last_calibration_date` date NOT NULL,
  `next_calibration_due_date` date NOT NULL,
  `current_condition` varchar(500) NOT NULL,
  `remark` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_quality_instrument_master`
--

INSERT INTO `tr_quality_instrument_master` (`id`, `branch_id`, `instrument_name`, `capacity`, `accuracy`, `make`, `purchase_date`, `last_calibration_date`, `next_calibration_due_date`, `current_condition`, `remark`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, 'ABc', '10', '10', 'xyz', '2020-09-01', '2020-09-01', '2020-09-30', 'good', 'ghj', 1, 1601013952008, 0, 0, 0, 0, ''),
(2, 1, 'Ravindra', '100', '100', 'Tata', '2020-09-01', '2020-09-18', '2020-09-26', 'poor', 'need', 1, 1601013952008, 1, 1601014242200, 0, 0, ''),
(3, 1, 'hjk', '85', '85', 'vhjkl', '2020-09-09', '2020-09-30', '2020-10-07', 'hjkl', 'guhjn', 1, 1601013952008, 0, 0, 0, 0, ''),
(4, 1, 'jkl', '4875', '85', 'hjkl', '2020-09-28', '2020-09-30', '2020-09-24', 'hjkm', 'hujn', 1, 1601013952008, 0, 0, 0, 0, ''),
(5, 1, 'hjik', '785', '485', 'hjk', '2020-09-07', '2020-09-30', '2020-09-30', 'ijkl', 'hujnm', 1, 1601013952008, 0, 0, 0, 0, ''),
(6, 1, 'hjk', '8956', '85', 'gyhj', '2020-09-01', '2020-09-30', '2020-09-30', 'jokl', 'hujn', 1, 1601013952008, 0, 0, 0, 0, ''),
(7, 1, 'jkl', '785', '85', 'hhj', '2020-09-01', '2020-09-15', '2020-09-30', 'jnkm', 'gyhb', 1, 1601013952008, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_states`
--

CREATE TABLE `tr_states` (
  `id` int(11) NOT NULL,
  `statename` varchar(500) NOT NULL,
  `statecode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_states`
--

INSERT INTO `tr_states` (`id`, `statename`, `statecode`) VALUES
(1, 'JAMMU AND KASHMIR', 1),
(2, 'HIMACHAL PRADESH', 2),
(3, 'PUNJAB', 3),
(4, 'CHANDIGARH', 4),
(5, 'UTTARAKHAND', 5),
(6, 'HARYANA', 6),
(7, 'DELHI', 7),
(8, 'RAJASTHAN', 8),
(9, 'UTTAR PRADESH', 9),
(10, 'BIHAR', 10),
(11, 'SIKKIM', 11),
(12, 'ARUNACHAL PRADESH', 12),
(13, 'NAGALAND', 13),
(14, 'MANIPUR', 14),
(15, 'MIZORAM', 15),
(16, 'TRIPURA', 16),
(17, 'MEGHLAYA', 17),
(18, 'ASSAM', 18),
(19, 'WEST BENGAL', 19),
(20, 'JHARKHAND', 20),
(21, 'ODISHA', 21),
(22, 'CHATTISGARH', 22),
(23, 'MADHYA PRADESH', 23),
(24, 'GUJARAT', 24),
(25, 'DAMAN AND DIU', 25),
(26, 'DADRA AND NAGAR HAVELI', 26),
(27, 'MAHARASHTRA', 27),
(28, 'ANDHRA PRADESH(BEFORE DIVISION)', 28),
(29, 'KARNATAKA', 29),
(30, 'GOA', 30),
(31, 'LAKSHWADEEP', 31),
(32, 'KERALA', 32),
(33, 'TAMIL NADU', 33),
(34, 'PUDUCHERRY', 34),
(35, 'ANDAMAN AND NICOBAR ISLANDS', 35),
(36, 'TELANGANA', 36),
(37, 'ANDHRA PRADESH (NEW)', 37);

-- --------------------------------------------------------

--
-- Table structure for table `tr_stock`
--

CREATE TABLE `tr_stock` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `finish_good_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `opening_balance` double(11,2) NOT NULL,
  `moq` double(11,2) NOT NULL,
  `msq` double(11,2) NOT NULL,
  `material_in` double(11,2) NOT NULL,
  `material_out` double(11,2) NOT NULL,
  `adjustment_qty_plus` double(11,2) NOT NULL,
  `adjustment_qty_minus` double(11,2) NOT NULL,
  `balance` double(11,2) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_stock`
--

INSERT INTO `tr_stock` (`id`, `branch_id`, `finish_good_id`, `item_id`, `item_name`, `opening_balance`, `moq`, `msq`, `material_in`, `material_out`, `adjustment_qty_plus`, `adjustment_qty_minus`, `balance`, `created_time`, `updated_time`) VALUES
(1, 1, 1, 2, 'ROD', 100.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 100.00, 1601490600000, 0),
(2, 1, 1, 3, 'TRAY', 200.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 200.00, 1601490600000, 0),
(3, 1, 1, 4, 'MOUCE TRAY', 300.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 300.00, 1601490600000, 0),
(4, 1, 1, 5, 'HARDWARE', 400.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 400.00, 1601490600000, 0),
(5, 1, 1, 6, 'PLASTIC BAG', 500.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 500.00, 1601490600000, 0),
(7, 1, 1, 2, 'ROD', 100.00, 0.00, 0.00, 100.00, 0.00, 0.00, 0.00, 200.00, 1602493174071, 0),
(8, 1, 1, 3, 'TRAY', 200.00, 0.00, 0.00, 100.00, 0.00, 0.00, 0.00, 300.00, 1602493174071, 0),
(9, 1, 1, 4, 'MOUCE TRAY', 300.00, 0.00, 0.00, 100.00, 0.00, 0.00, 0.00, 400.00, 1602493174071, 0),
(10, 1, 1, 5, 'HARDWARE', 400.00, 0.00, 0.00, 100.00, 0.00, 0.00, 0.00, 500.00, 1602493174071, 0),
(11, 1, 1, 6, 'PLASTIC BAG', 500.00, 0.00, 0.00, 100.00, 0.00, 0.00, 0.00, 600.00, 1602493174071, 0),
(12, 1, 1, 7, 'SLIDER', 0.00, 0.00, 0.00, 100.00, 0.00, 0.00, 0.00, 100.00, 1602493174071, 0),
(13, 1, 1, 2, 'ROD', 100.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 300.00, 1602493682434, 0),
(14, 1, 1, 3, 'TRAY', 200.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 400.00, 1602493682434, 0),
(15, 1, 1, 4, 'MOUCE TRAY', 300.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 500.00, 1602493682434, 0),
(16, 1, 1, 5, 'HARDWARE', 400.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 600.00, 1602493682434, 0),
(17, 1, 1, 6, 'PLASTIC BAG', 500.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 700.00, 1602493682434, 0),
(18, 1, 1, 7, 'SLIDER', 0.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 200.00, 1602493682434, 0),
(19, 1, 1, 2, 'ROD', 100.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 300.00, 1602503818087, 0),
(20, 1, 1, 2, 'ROD', 100.00, 0.00, 0.00, 200.00, 0.00, 200.00, 0.00, 500.00, 0, 0),
(21, 1, 1, 3, 'TRAY', 200.00, 0.00, 0.00, 200.00, 0.00, 100.00, 0.00, 500.00, 0, 0),
(22, 1, 1, 4, 'MOUCE TRAY', 300.00, 0.00, 0.00, 200.00, 0.00, 0.00, 0.00, 500.00, 0, 0),
(23, 1, 1, 5, 'HARDWARE', 400.00, 0.00, 0.00, 200.00, 0.00, 0.00, 100.00, 500.00, 0, 0),
(24, 1, 1, 6, 'PLASTIC BAG', 500.00, 0.00, 0.00, 200.00, 0.00, 0.00, 200.00, 500.00, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_stock_adjustment`
--

CREATE TABLE `tr_stock_adjustment` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `stock_adjustment_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_stock_adjustment`
--

INSERT INTO `tr_stock_adjustment` (`id`, `branch_id`, `item_id`, `stock_adjustment_date`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, '2020-10-01', 1, 1602505808277, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_stock_adjustment_component`
--

CREATE TABLE `tr_stock_adjustment_component` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `stock_adjustment_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `component_id` int(11) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `current_stock` double(11,2) NOT NULL,
  `stock_adjust_plus` double(11,2) NOT NULL,
  `stock_adjust_minus` double(11,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_stock_adjustment_component`
--

INSERT INTO `tr_stock_adjustment_component` (`id`, `branch_id`, `stock_adjustment_id`, `item_id`, `component_id`, `component_name`, `current_stock`, `stock_adjust_plus`, `stock_adjust_minus`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 1, 2, 'ROD', 300.00, 200.00, 0.00, 1, 1602505808277, 0, 0, 0, 0),
(2, 1, 1, 1, 3, 'TRAY', 400.00, 100.00, 0.00, 1, 1602505808277, 0, 0, 0, 0),
(3, 1, 1, 1, 4, 'MOUCE TRAY', 500.00, 0.00, 0.00, 1, 1602505808277, 0, 0, 0, 0),
(4, 1, 1, 1, 5, 'HARDWARE', 600.00, 0.00, 100.00, 1, 1602505808277, 0, 0, 0, 0),
(5, 1, 1, 1, 6, 'PLASTIC BAG', 700.00, 0.00, 200.00, 1, 1602505808277, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_tool_component_master`
--

CREATE TABLE `tr_tool_component_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `tool_id` int(11) NOT NULL,
  `process_name` varchar(500) NOT NULL,
  `tool_name` varchar(500) NOT NULL,
  `tool_number` varchar(500) NOT NULL,
  `photo` text NOT NULL,
  `check_list` text NOT NULL,
  `tool_grinding_frequency` varchar(500) NOT NULL,
  `last_grinding_date` date NOT NULL,
  `next_due_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_tool_component_master`
--

INSERT INTO `tr_tool_component_master` (`id`, `branch_id`, `tool_id`, `process_name`, `tool_name`, `tool_number`, `photo`, `check_list`, `tool_grinding_frequency`, `last_grinding_date`, `next_due_date`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`) VALUES
(1, 1, 1, 'SHEARING', 'NO TOOL REQD', '1', 'toolmasterimage/SHEARING_1_1601044757354.png', 'toolmasterimage/SHEARING_1_1601044757354.png', '1', '0000-00-00', '0000-00-00', 1, 1601044687430, 1, 1601624830079, 0, 0),
(2, 1, 1, 'CORNER PUNCHING', 'CRN PUNCHING TOOL', 'CRN-001', 'toolmasterimage/CORNER PUNCHING_1_1601044809156.png', 'toolmasterimage/CORNER PUNCHING_1_1601044809156.png', '2', '0000-00-00', '0000-00-00', 1, 1601044809156, 1, 1601624830079, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tr_tool_master`
--

CREATE TABLE `tr_tool_master` (
  `id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `product_name` varchar(500) NOT NULL,
  `sub_sr` varchar(100) NOT NULL,
  `component_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_time` bigint(17) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_time` bigint(17) NOT NULL,
  `deleted_by` int(11) NOT NULL,
  `deleted_time` bigint(17) NOT NULL,
  `active` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_tool_master`
--

INSERT INTO `tr_tool_master` (`id`, `branch_id`, `product_name`, `sub_sr`, `component_name`, `created_by`, `created_time`, `updated_by`, `updated_time`, `deleted_by`, `deleted_time`, `active`) VALUES
(1, 1, '1', '1', 'Tray', 1, 1601044687430, 1, 1601624830079, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_unit`
--

CREATE TABLE `tr_unit` (
  `id` int(11) NOT NULL,
  `unitcode` varchar(10) NOT NULL,
  `unitdesc` text NOT NULL,
  `uniteway` varchar(50) NOT NULL,
  `created_by` varchar(120) NOT NULL,
  `status` varchar(120) NOT NULL,
  `approved_by` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_unit`
--

INSERT INTO `tr_unit` (`id`, `unitcode`, `unitdesc`, `uniteway`, `created_by`, `status`, `approved_by`) VALUES
(3, 'BAG', 'BAG-BAGS', 'BAG', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(4, 'BAL', 'BAL-BALE', 'BALE', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(5, 'BDL', 'BDL-BUNDLES', 'BUNDLES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(6, 'BKL', 'BKL-BUCKLES', 'BUCKLES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(7, 'BOU', 'BOU-BILLION OF UNITS', 'BILLION OF UNITS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(8, 'BOX', 'BOX-BOX', 'BOX', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(9, 'BTL', 'BTL-BOTTLES', 'BOTTLES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(10, 'BUN', 'BUN-BUNCHES', 'BUNCHES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(11, 'CAN', 'CAN-CANS', 'CANS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(12, 'CBM', 'CBM-CUBIC METERS', 'CUBIC METERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(13, 'CCM', 'CCM-CUBIC CENTIMETERS', 'CUBIC CENTIMETERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(14, 'CMS', 'CMS-CENTIMETERS', 'CENTI METERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(15, 'CTN', 'CTN-CARTONS', 'CARTONS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(16, 'DOZ', 'DOZ-DOZENS', 'DOZENS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(17, 'DRM', 'DRM-DRUMS', 'DRUMS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(18, 'GGK', 'GGK-GREAT GROSS', 'GREAT GROSS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(19, 'GMS', 'GMS-GRAMMES', 'GRAMMES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(20, 'GRS', 'GRS-GROSS', 'GROSS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(21, 'GYD', 'GYD-GROSS YARDS', 'GROSS YARDS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(22, 'KGS', 'KGS-KILOGRAMS', 'KILOGRAMS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(23, 'KLR', 'KLR-KILOLITRE', 'KILOLITRE', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(24, 'KME', 'KME-KILOMETRE', 'KILOMETRE', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(25, 'MLT', 'MLT-MILILITRE', 'MILILITRE', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(26, 'MTR', 'MTR-METERS', 'METERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(27, 'MTS', 'MTS-METRIC TON', 'METRIC TON', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(28, 'NOS', 'NOS-NUMBERS', 'NUMBERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(29, 'OTH', 'OTH-OTHERS', 'OTHERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(30, 'PAC', 'PAC-PACKS', 'PACKS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(31, 'PCS', 'PCS-PIECES', 'PIECES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(32, 'PRS', 'PRS-PAIRS', 'PAIRS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(33, 'QTL', 'QTL-QUINTAL', 'QUINTAL', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(34, 'ROL', 'ROL-ROLLS', 'ROLLS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(35, 'SET', 'SET-SETS', 'SETS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(36, 'SQF', 'SQF-SQUARE FEET', 'SQUARE FEET', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(37, 'SQM', 'SQM-SQUARE METERS', 'SQUARE METERS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(38, 'SQY', 'SQY-SQUARE YARDS', 'SQUARE YARDS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(39, 'TBS', 'TBS-TABLETS', 'TABLETS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(40, 'TGM', 'TGM-TEN GROSS', 'TEN GROSS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(41, 'THD', 'THD-THOUSANDS', 'THOUSANDS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(42, 'TON', 'TON-TONNES', 'TONNES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(43, 'TUB', 'TUB-TUBES', 'TUBES', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(44, 'UGS', 'UGS-US GALLONS', 'US GALLONS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(45, 'UNT', 'UNT-UNITS', 'UNITS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(46, 'YDS', 'YDS-YARDS', 'YARDS', 'Indian Governmant', 'Approved', 'Indian Governmant'),
(47, 'LTR', 'LTR-LITRE', 'LITRE', 'Indian Governmant', 'Approved', 'Indian Governmant\r\n'),
(48, 'MM', 'MM-MILIMETRE', 'MILIMETRE', 'Indian Governmant', 'Approved', 'Indian Governmant\r\n'),
(49, 'PAIR', 'PAIR-PAIRS', 'PAIR', 'Indian Governmant', 'Approved', 'Indian Governmant');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tr_admin`
--
ALTER TABLE `tr_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_boq_component_master`
--
ALTER TABLE `tr_boq_component_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_boq_hardware_master`
--
ALTER TABLE `tr_boq_hardware_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_boq_master`
--
ALTER TABLE `tr_boq_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_boq_rawmaterial_master`
--
ALTER TABLE `tr_boq_rawmaterial_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_branch_master`
--
ALTER TABLE `tr_branch_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_contact_person_master`
--
ALTER TABLE `tr_contact_person_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_countrymaster`
--
ALTER TABLE `tr_countrymaster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_customer_master`
--
ALTER TABLE `tr_customer_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_drawing_component_master`
--
ALTER TABLE `tr_drawing_component_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_drawing_master`
--
ALTER TABLE `tr_drawing_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_fixture_component`
--
ALTER TABLE `tr_fixture_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_fixture_master`
--
ALTER TABLE `tr_fixture_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_inspection_gauges_component`
--
ALTER TABLE `tr_inspection_gauges_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_inspection_gauges_master`
--
ALTER TABLE `tr_inspection_gauges_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_item_component_opening_balance`
--
ALTER TABLE `tr_item_component_opening_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_item_group_master`
--
ALTER TABLE `tr_item_group_master`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tr_item_master`
--
ALTER TABLE `tr_item_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_item_opening_balance`
--
ALTER TABLE `tr_item_opening_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_ledger_master`
--
ALTER TABLE `tr_ledger_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_location_master`
--
ALTER TABLE `tr_location_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_machine_component`
--
ALTER TABLE `tr_machine_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_machine_master`
--
ALTER TABLE `tr_machine_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_man_master`
--
ALTER TABLE `tr_man_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_material_in`
--
ALTER TABLE `tr_material_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_material_in_component`
--
ALTER TABLE `tr_material_in_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_opration_master`
--
ALTER TABLE `tr_opration_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_order_account_tracking`
--
ALTER TABLE `tr_order_account_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_order_booking`
--
ALTER TABLE `tr_order_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_order_dispach_tracking`
--
ALTER TABLE `tr_order_dispach_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_ppep_componet`
--
ALTER TABLE `tr_ppep_componet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_ppep_master`
--
ALTER TABLE `tr_ppep_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_quality_instrument_master`
--
ALTER TABLE `tr_quality_instrument_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_states`
--
ALTER TABLE `tr_states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_stock`
--
ALTER TABLE `tr_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_stock_adjustment`
--
ALTER TABLE `tr_stock_adjustment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_stock_adjustment_component`
--
ALTER TABLE `tr_stock_adjustment_component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_tool_component_master`
--
ALTER TABLE `tr_tool_component_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_tool_master`
--
ALTER TABLE `tr_tool_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tr_unit`
--
ALTER TABLE `tr_unit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_admin`
--
ALTER TABLE `tr_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_boq_component_master`
--
ALTER TABLE `tr_boq_component_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tr_boq_hardware_master`
--
ALTER TABLE `tr_boq_hardware_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tr_boq_master`
--
ALTER TABLE `tr_boq_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tr_boq_rawmaterial_master`
--
ALTER TABLE `tr_boq_rawmaterial_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tr_branch_master`
--
ALTER TABLE `tr_branch_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_contact_person_master`
--
ALTER TABLE `tr_contact_person_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tr_countrymaster`
--
ALTER TABLE `tr_countrymaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=493;

--
-- AUTO_INCREMENT for table `tr_customer_master`
--
ALTER TABLE `tr_customer_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tr_drawing_component_master`
--
ALTER TABLE `tr_drawing_component_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tr_drawing_master`
--
ALTER TABLE `tr_drawing_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_fixture_component`
--
ALTER TABLE `tr_fixture_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tr_fixture_master`
--
ALTER TABLE `tr_fixture_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_inspection_gauges_component`
--
ALTER TABLE `tr_inspection_gauges_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tr_inspection_gauges_master`
--
ALTER TABLE `tr_inspection_gauges_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_item_component_opening_balance`
--
ALTER TABLE `tr_item_component_opening_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tr_item_group_master`
--
ALTER TABLE `tr_item_group_master`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tr_item_master`
--
ALTER TABLE `tr_item_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tr_item_opening_balance`
--
ALTER TABLE `tr_item_opening_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_ledger_master`
--
ALTER TABLE `tr_ledger_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tr_location_master`
--
ALTER TABLE `tr_location_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_machine_component`
--
ALTER TABLE `tr_machine_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tr_machine_master`
--
ALTER TABLE `tr_machine_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_man_master`
--
ALTER TABLE `tr_man_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_material_in`
--
ALTER TABLE `tr_material_in`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_material_in_component`
--
ALTER TABLE `tr_material_in_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tr_opration_master`
--
ALTER TABLE `tr_opration_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tr_order_account_tracking`
--
ALTER TABLE `tr_order_account_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tr_order_booking`
--
ALTER TABLE `tr_order_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_order_dispach_tracking`
--
ALTER TABLE `tr_order_dispach_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tr_ppep_componet`
--
ALTER TABLE `tr_ppep_componet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_ppep_master`
--
ALTER TABLE `tr_ppep_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_quality_instrument_master`
--
ALTER TABLE `tr_quality_instrument_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tr_states`
--
ALTER TABLE `tr_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `tr_stock`
--
ALTER TABLE `tr_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tr_stock_adjustment`
--
ALTER TABLE `tr_stock_adjustment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_stock_adjustment_component`
--
ALTER TABLE `tr_stock_adjustment_component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tr_tool_component_master`
--
ALTER TABLE `tr_tool_component_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tr_tool_master`
--
ALTER TABLE `tr_tool_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tr_unit`
--
ALTER TABLE `tr_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
