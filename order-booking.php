<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName  = "Order Booking";
$pageURL   = 'order-booking.php';
$deleteURL = 'order-booking.php';
$editURL   = 'order-book.php';
$navenq1   = 'background:#27ae60;';
$tableName = 'order_booking';


include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


$results = $admin->query("SELECT * FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time=0");

if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET deleted_by = '".$loggedInUserDetailsArr['id']."', deleted_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_GET['confirmId']) && !empty($_GET['confirmId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['confirmId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET order_booking_by = '".$loggedInUserDetailsArr['id']."', order_booking_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?confirmsuccess");
	exit();
}

if(isset($_GET['activate']) && !empty($_GET['activate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['activate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '1', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?activatesuccess");
	exit();
}

if(isset($_GET['deactivate']) && !empty($_GET['deactivate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['deactivate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '0', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?deactivatesuccess");
	exit();
}



?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.add-more {

			margin-top: 27px;

		}
		.form-control{
            border-bottom: 1px solid blue;
            height:25px!important;
        }
        label{
            font-size:11px;
        }
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

	</style>

</head>

<body>



    <div class='loading_wrapper' style="display: none;">

        <div class='loadertext1'>Please wait while we upload your files...</div>

    </div>

    <div class="main-wrapper">

        <!-- Header -->

        <?php include("include/header.php"); ?>

        <!-- /Header -->



        <!-- Sidebar -->

        <?php include("include/sidebar.php"); ?>

        <!-- /Sidebar -->



        <!-- Page Wrapper -->

        <div class="page-wrapper">



            <!-- Page Content -->

            <div class="content container-fluid">



                <!-- Page Header -->

                <div class="page-header">

                    <div class="row align-items-center">

                        <div class="col">

                            <h3 class="page-title"><?php echo $pageName; ?></h3>

                            <ul class="breadcrumb">

                                <li class="breadcrumb-item">Trasaction</li>


                                <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

                                <li class="breadcrumb-item"><?php echo $pageName; ?></li>

                                <li class="breadcrumb-item active">

                                    <?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

                                </li>

                                <?php } else { ?>

                                <li class="breadcrumb-item active"><?php echo $pageName; ?></li>

                                <?php } ?>

                            </ul>

                        </div>

                    </div>

                </div>

                <!-- /Page Header -->



                <?php if(isset($_GET['registersuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['registerfail'])){ ?>

                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['updatesuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['updatefail'])){ ?>

                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong>
                    <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

                </div>

                <?php } ?>



                <?php if(isset($_GET['deletesuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

                </div><br />

                <?php } ?>

				<?php if(isset($_GET['confirmsuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully Confirm.

                </div><br />

                <?php } ?>


                <?php include("include/tracking.php"); ?>


                <br>

                <div class="row">

                    <div class="col-md-12">

                        <table class="table table-striped custom-table mb-0 datatable datatable-selectable-data">
                            <thead>
                                <tr>
                                    <th width="20px">#</th>
                                    <th>Customer Name</th>
                                    <th>PO NO.</th>
                                    <th>Item Name</th>
                                    <th>Order Qty</th>
                                    <th>Category</th>
                                    <th width="10px">Actions</th>
                                </tr>
                            </thead>

                            <tbody>

								<?php

								$x = 1;

								while($row = $admin->fetch($results)){ 

                                    $customer_short_name = $admin->getUniqueCustomerById($row['customer_name']);
									$componentName = $admin->getUniqueBoqById($row['category']);
									
									$order_status = $row['order_status'];

									if ($order_status == "Urgent") {
										$backgroundcolor = "red";
										$color           = "#fff";
									}else {
										$backgroundcolor = "";
										$color           = "#000";
									}

                                    ?>
                                        
									<tr style="background-color:<?php echo $backgroundcolor;?>;color:<?php echo $color;?>">

										<td><?php echo $x++ ?></td>

                                        <td> <?php echo $customer_short_name['customer_short_name'];?> </td>
                                        <td> <?php echo $row['po_no']; ?> </td>
                                        <td> <?php echo $row['item_name']; ?> </td>
                                        <td> <?php echo $row['order_qty']; ?> </td>
                                        <td> <?php echo $componentName['item_name']; ?> </td>

										
										<td class="text-right">

											<div class="dropdown dropdown-action">

												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>

												<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(103px, 32px, 0px);">

													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $editURL; ?>?edit&id=<?php echo $row['id']; ?>" title="Edit">   <i class="fa fa-pencil"></i> Edit</a>

													<?php if($row['active'] == '0') { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?activate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to activate?');" title="Activate"> <i class="fa fa-thumbs-up"></i> Activate </a>

													<?php } else { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?deactivate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to deactivate?');" title="Deactivate"> <i class="fa fa-thumbs-down"></i> Deactivate </a>

													<?php } ?>

                                                    <a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?confirmId=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to Confirm?');" title="Confirm">   <i class="fa fa-check"></i> Confirm</a>

													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?delId=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to delete?');" title="Delete"> <i class="fa fa-trash-o"></i> Delete </a>


												</div>

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

                        </table>

                    </div>

                </div>

                <a class="btn" style="font-size:25px;border-radius:50px;width:60px;height:60px;position: fixed;bottom: 40px;
                right: 40px;background:#eb2f06;color:#fff;" href="order-book.php?add">+</a>

            </div>

            <!-- /Page Content -->

        </div>

        <!-- /Page Wrapper -->

    </div>

    <!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					man_name: {
						required: true,
					},
                    designation: {
						required: true,
					},

                    department: {
						required: true,
					},

					rate: {
						required: true,
						number:true,
					},

					hours: {
						required: true,
						number:true,

					},

					skill_matrix: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>
</body>

</html>