<?php
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$pageName = "Customer Master";
$parentPageURL = 'customer-master.php';
$pageURL = 'customer-master-add.php';

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}


$states  = $admin-> getAllStates();

include_once 'csrf.class.php';
$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addCustomerMaster($_POST, $loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$parentPageURL."?registersuccess");
	}
}



if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueCustomerById($id);
}

//   print_r($detailsData);
if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateCustomerMaster($_POST, $loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$parentPageURL."?updatesuccess&edit&id=".$id);
		exit();
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">

	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->
	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->
	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->
	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/bootstrap-datepicker.min.css" rel="stylesheet">
	
	<!-- Crop Image css -->
	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>
		em{
			color:red;
		}
		.add-more {
			margin-top: 27px;
		}
		
		.form-control{
            border-bottom: 1px solid blue;
            width:200px!important;
        }
        label{
            font-size:11px;
        }

		.tableFixHead          { overflow-y: auto; height: 600px; }
		.tableFixHead thead th { position: sticky; top: 0; }

		/* Just common table stuff. Really. */
		table  { border-collapse: collapse; width: 100%; }
		th, td { padding: 8px 16px; }
		th     { background:#2980b9; color:#fff; }
	</style>
</head>
<body>
	<!-- Main Wrapper -->
	<div class="main-wrapper">

		<!-- Header -->
		<?php include("include/header.php"); ?>
		<!-- /Header -->

		<!-- Sidebar -->
		<?php include("include/sidebar.php"); ?>
		<!-- /Sidebar -->

		<!-- Page Wrapper -->
		<div class="page-wrapper">

			<!-- Page Content -->
			<div class="content container-fluid">

				<!-- Page Header -->
				<div class="page-header">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="page-title"><?php echo $pageName; ?></h3>
							<ul class="breadcrumb">
								<li class="breadcrumb-item"><a href="javascript:;">Master</a></li>
								<li class="breadcrumb-item"><a href="<?php echo $parentPageURL; ?>"> Customer Master</a>
								</li>
								<li class="breadcrumb-item active">
									<?php if(isset($_GET['edit'])) {
										echo 'Edit '.$pageName;
									} else {
										echo 'Add New '.$pageName;
									}
									?>
								</li>
							</ul>
						</div>
						<div class="col-auto float-right ml-auto">
							<a href="<?php echo $parentPageURL; ?>" class="btn add-btn"><i class="fa fa-arrow-left"></i>
								Back to <?php echo $pageName; ?></a>
						</div>
					</div>
				</div>
				<!-- /Page Header -->

				<?php if(isset($_GET['registersuccess'])){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
							class="sr-only">Close</span></button>
					<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.
				</div><br />
				<?php } ?>

				<?php if(isset($_GET['registerfail'])){ ?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
							class="sr-only">Close</span></button>
					<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.
				</div><br />
				<?php } ?>

				<?php if(isset($_GET['updatesuccess'])){ ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
							class="sr-only">Close</span></button>
					<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.
				</div><br />
				<?php } ?>

				<?php if(isset($_GET['updatefail'])){ ?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
							class="sr-only">Close</span></button>
					<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong>
					<?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.
				</div>
				<?php } ?>

				<div class="row">
					<div class="col-lg-12">
						<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">
							<div class="card">
								<div class="card-header">
									<h4 class="card-title mb-0"><?php echo $pageName; ?> Info</h4>
								</div>
								<div class="card-body">

								<?php if(isset($_GET['id'])==''){ ?>

									<div class="form-actions text-left">

										<button type="button" id="add-more" class="btn btn-warning add-more"><i
												class="fa fa-plus"></i> Add More Component</button>

									</div><br>
								<?php } ?>
									<div class="table-responsive tableFixHead">

										<table class="table table-bordered" id="components">

											<thead>

												<tr>

													<th>#</th>
													<th>CUSTOMER</th>
													<th>SHORT FORM</th>
													<th>CUSTOMER CODE / VENDORE CODE</th>
													<th>ADDRESS</th>
													<th>Contact No</th>
													<th>CONTACT PERSON</th>
													<th>GST NO.</th>
													<th>TAX STRUCTURE</th>
													<th>Remark</th>
													<?php if(isset($_GET['id'])==''){ ?>

													<th>Action</th>
													<?php } ?>


												</tr>


											</thead>

											<tbody>


												<?php if(isset($_GET['edit'])) {

													$y = 1;
														
												?>
												<tr>
													<td>

														<?php echo $y;?>

													</td>

													<td>

														<input type="text" name="customer_name" id="customer_name" value="<?php if(isset($_GET['edit'])) { echo $data['customer_name']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="customer_short_name" id="customer_short_name" value="<?php if(isset($_GET['edit'])) { echo $data['customer_short_name']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="vendor_code" id="vendor_code" value="<?php if(isset($_GET['edit'])) { echo $data['vendor_code']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="company_address" id="company_address" value="<?php if(isset($_GET['edit'])) { echo $data['company_address']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="contact_no" id="contact_no" value="<?php if(isset($_GET['edit'])) { echo $data['contact_no']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="contact_person" id="contact_person" value="<?php if(isset($_GET['edit'])) { echo $data['contact_person']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="gst_no" id="gst_no" value="<?php if(isset($_GET['edit'])) { echo $data['gst_no']; } ?>" class="form-control form-control-sm" onchange="checkGstNo(this)">


													</td>

													<td>

														<input type="text" name="tax_structure" id="tax_structure" value="<?php if(isset($_GET['edit'])) { echo $data['tax_structure']; } ?>" class="form-control form-control-sm">


													</td>

													<td>

														<input type="text" name="remark" id="remark" value="<?php if(isset($_GET['edit'])) { echo $data['remark']; } ?>" class="form-control form-control-sm">


													</td>

												</tr>


											</tbody>


											<?php } else { ?>

											<tbody>



												<?php 

													$y=1;
												
													for ($x=0; $x < 10; $x++) { 
													
												?>


												<tr>

													<td>

														<?php echo $y;?>

													</td>

												

													<td>

														<input type="text" name="customer_name[<?php echo $x;?>]"
															class="form-control form-control-sm customer_name"
															required>

													</td>

													<td>

														<input type="text" name="customer_short_name[<?php echo $x;?>]"
															class="form-control form-control-sm customer_short_name" required>

													</td>


													<td>

														<input type="text" name="vendor_code[<?php echo $x;?>]"
															class="form-control form-control-sm vendor_code" required>

													</td>

													<td>

														<textarea type="text" name="company_address[<?php echo $x;?>]"
															class="form-control form-control-sm company_address" required></textarea>

													</td>

													<td>

														<input type="number" name="contact_no[<?php echo $x;?>]"
															class="form-control form-control-sm contact_no" required>

													</td>

													<td>

														<input type="text" name="contact_person[<?php echo $x;?>]"
															class="form-control form-control-sm contact_person" required>

													</td>

													<td>

														<input type="text" name="gst_no[<?php echo $x;?>]"
															class="form-control form-control-sm gst_no" onchange="checkGstNo(this)" required>

													</td>

													<td>

														<input type="text" name="tax_structure[<?php echo $x;?>]"
															class="form-control form-control-sm tax_structure" required>

													</td>


													<td>

														<input type="text" name="remark[<?php echo $x;?>]"
															class="form-control form-control-sm remark" required>

													</td>

													<td>

														<button class="btn btn-sm btn-danger remover"
															onclick="remove(this)">Remove</buuton>

													</td>

												</tr>


												<?php $y++;} ?>

											</tbody>

											<?php } ?>


										</table>

									</div>


									<div class="form-actions text-right">
										<input type="hidden" name="<?php echo $token_id; ?>"
											value="<?php echo $token_value; ?>" />
										<?php if(isset($_GET['edit'])){ ?>
										<input type="hidden" class="form-control" name="id" id="id"
											value="<?php echo $id ?>" />
										<button type="submit" name="update" value="update" id="update"
											class="btn btn-warning"><i class="icon-pencil"></i>Update
											<?php echo $pageName; ?></button>
										<?php } else { ?>
										<button type="submit" name="register" id="register" class="btn btn-danger"><i
												class="icon-signup"></i>Add <?php echo $pageName; ?></button>
										<?php } ?>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /Page Content -->

		</div>
		<!-- /Page Wrapper -->

	</div>
	<!-- /Main Wrapper -->

	<!-- jQuery -->
	<script src="assets/js/jquery-3.2.1.min.js"></script>

	<!-- Bootstrap Core JS -->
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>

	<!-- Slimscroll JS -->
	<script src="assets/js/jquery.slimscroll.min.js"></script>

	<!-- Custom JS -->
	<script src="assets/js/app.js"></script>

	<!-- Validate JS -->
	<script src="assets/js/jquery.validate.js"></script>
	<script src="assets/js/additional-methods.js"></script>

	<!-- Crop Image js -->
	<script src="assets/js/crop-image/cropper.min.js"></script>
	<script src="assets/js/crop-image/image-crop-app.js"></script>
	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>


	<!-- CK Editor -->
	<script type="text/javascript" src="assets/js/editor/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="assets/js/editor/ckeditor/adapters/jquery.js"></script>
	<script type="text/javascript" src="assets/js/editor/ckfinder/ckfinder.js"></script>

	<script type="text/javascript">
        $(document).ready(function() {
            $('.select2').select2();

        });

		$(document).ready(function() {

			$("#form").validate({
				ignore: [],
				debug: false,
				rules: {
					customer_name: {
						required: true,
					},
					customer_short_name:{
						required: true,
					},
					vendor_code: {
						required: true,
					},
					company_address:{
						required: true,
					},
					contact_person: {
						required: true,
					},
					credit_days:{
						required: true,
					},
					gst_no: {
						required: true,
					},
					tax_structure:{
						required: true,
					},
					remark:{
						required: true,
					},
					department: {
						number: true,
						remote:{
							url: "check_display_order.php",
							type: "post",
							data: {
								<?php if(isset($_GET['edit']) && !empty($data['display_order'])){ ?>
									id: function() {
										return "<?php echo $data['id']; ?>";
									},
								<?php } ?>
								table: "root_city_master"
							}
						},
					}
				},
				messages: {
					
				}
			});
			$.validator.addMethod('filesize', function (value, element, param) {
				return this.optional(element) || (element.files[0].size <= param)
			}, 'File size must be less than 2 MB');
			$.validator.addMethod("url", function(value, element) {
				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);
			}, "Please enter a valid link address.");
		});
	</script>
	<script>
		$(document).ready(function() {
			$('input[name=thumbnail_image]').change(function(){
				loadImagePreview(this, (542 / 384));
			});
		});

		$(function() {

			var callToEnhanceValidate=function(){
				$(".details_title").each(function(){
					$(this).rules('remove');
					$(this).rules('add', {
						required: true,
					});
				});
			}
			$(".add-more").on("click", function(){
                
				var count = $('#components > tbody > tr').length;
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxContactPerson.php',
                    success: function (services_clone) {

                        $("#components tr:last").after(services_clone);

                    }
                });

			});

			
			callToEnhanceValidate();
		});

		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));

				
			}
			
		}

		function remove(e) {

			$(e).parent().parent().remove();

		}
		
		function checkGstNo(e){

			gstNo = $(e).val();

			gstReg = /^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/;

			if (gstReg.test(gstNo)== false) {
				
				alert('invalid Gst No');
				$(e).focus();

			}
			
		}
	</script>
	</html>