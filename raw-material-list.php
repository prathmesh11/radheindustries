<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){

	header("location: admin-login.php");

	exit();

}

$pageName  = "Purchase Department";
$pageURL   = 'purchase-information.php';
$deleteURL = 'purchase-information.php';
$editURL   = 'purchase-info.php';
$navenq2   = 'background:#27ae60;';
$tableName = 'order_booking';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

 $boq_id    = $admin->escape_string($admin->strip_all($_GET['boq_id']));

 $branch_id = $admin->escape_string($admin->strip_all($_GET['branch_id']));

 $data  = $admin->getallItemBoqItemAndstockqtyId($boq_id,$branch_id);


?>

<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/radhelogo.jpeg">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- fancy CSS -->

	<link rel="stylesheet" href="assets/css/fancy.css">


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.form-control{
            border-bottom: 1px solid blue;
            height:25px!important;
			width:200px!important;

        }
        label{
            font-size:11px;
        }
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

		.tableFixHead          { overflow-y: auto; height: 600px; }
		.tableFixHead thead th { position: sticky; top: 0; }

		/* Just common table stuff. Really. */
		.tableFixHead table  { border-collapse: collapse; width: 100%; }
		.tableFixHead th, td { padding: 8px 16px; }
		.tableFixHead th     { background:#2980b9; color:#fff; }

        .page-wrapper {

            padding-top: 0px;
        }

	</style>

</head>

<body>

	<div class='loading_wrapper' style="display: none;">

	    <div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">


	    <!-- Page Wrapper -->

	    <div class="page-wrapper">



	        <!-- Page Content -->

	        <div class="content container-fluid">



	            <?php if(isset($_GET['registersuccess'])){ ?>

	            <div class="alert alert-success alert-dismissible" role="alert">

	                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
	                        class="sr-only">Close</span></button>

	                <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

	            </div><br />

	            <?php } ?>



	            <?php if(isset($_GET['registerfail'])){ ?>

	            <div class="alert alert-danger alert-dismissible" role="alert">

	                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
	                        class="sr-only">Close</span></button>

	                <i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

	            </div><br />

	            <?php } ?>



	            <?php if(isset($_GET['updatesuccess'])){ ?>

	            <div class="alert alert-success alert-dismissible" role="alert">

	                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
	                        class="sr-only">Close</span></button>

	                <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

	            </div><br />

	            <?php } ?>



	            <?php if(isset($_GET['updatefail'])){ ?>

	            <div class="alert alert-danger alert-dismissible" role="alert">

	                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
	                        class="sr-only">Close</span></button>

	                <i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong>
	                <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

	            </div>

	            <?php } ?>



	            <?php if(isset($_GET['deletesuccess'])){ ?>

	            <div class="alert alert-success alert-dismissible" role="alert">

	                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span
	                        class="sr-only">Close</span></button>

	                <i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

	            </div><br />

	            <?php } ?>

	            <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

                    <div class="table-responsive tableFixHead">

                        <table class="table table-bordered " id="components">

                            <thead>

                                <tr>

                                    <th>#</th>

                                    <th>Item Name</th>

                                    <th>Bom Qty</th>

                                    <th>Current Stock Qty</th>

                                </tr>

                            </thead>

                            <tbody> 

                                <?php 
                                    $count = 1;
                                    while($rows = $admin->fetch($data)){ 
                                    $currentQty = $admin->stockCurrentQty($rows['component_id'],$rows['branch_id'],$rows['item_name']);
                                ?>
                                    <tr>

                                        <td>
                                            
                                            <?php echo $count;?>

                                        </td>

                                        <td>
                                            
                                            <?php echo $rows['component'];?>

                                        </td>

                                        <td>
                                            
                                            <?php echo $rows['qty_per_piece'];?>

                                        </td>

                                        <td>
                                            
                                            <?php echo $currentQty;?>

                                        </td>
                                    
                                    </tr>

                                <?php 

                                    $count++;}
                                        
                                ?>
                            
                            </tbody>

                        </table>

                    </div>

	            </form>


	        </div>



	        <!-- /Page Content -->

	    </div>

	    <!-- /Page Wrapper -->


	</div>

	<!-- /Main Wrapper -->





	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>


	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>


	<!-- fancy js -->

	<script src="assets/js/fancy.js"></script>



		



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({
				rules: {
					maingroupname: {
						required: true,
					},

                    groupname: {
						required: true,
					},

					// title: {
					// 	required: true,
					// },

                    groupcode: {
						required: true,
					},

				},

				messages: {
					
				}
			});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});


		

		function change(e){

			var main=$(e).data('main');

			$('.'+main).toggle();

		}

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});


			$('#register').click(function(){

				if($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

		});


	</script>




</body>

</html>