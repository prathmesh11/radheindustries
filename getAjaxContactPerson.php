<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$count=$_POST['count'];
?>
    <tr>

        <td>

            <?php echo $count+1;?>

        </td>



        <td>

            <input type="text" name="customer_name[<?php echo $count;?>]" class="form-control form-control-sm customer_name"
                required>

        </td>

        <td>

            <input type="text" name="customer_short_name[<?php echo $count;?>]"
                class="form-control form-control-sm customer_short_name" required>

        </td>


        <td>

            <input type="text" name="vendor_code[<?php echo $count;?>]" class="form-control form-control-sm vendor_code"
                required>

        </td>

        <td>

            <textarea type="text" name="company_address[<?php echo $count;?>]" class="form-control form-control-sm company_address"
                required></textarea>

        </td>

         <td>

            <input type="text" name="contact_no[<?php echo $count;?>]" class="form-control form-control-sm contact_no"
                required>

        </td>

        <td>

            <input type="text" name="contact_person[<?php echo $count;?>]" class="form-control form-control-sm contact_person"
                required>

        </td>

        <td>

            <input type="text" name="gst_no[<?php echo $count;?>]" class="form-control form-control-sm gst_no" onchange="checkGstNo(this)" required>

        </td>

        <td>

            <input type="text" name="tax_structure[<?php echo $count;?>]" class="form-control form-control-sm tax_structure"
                required>

        </td>


        <td>

            <input type="text" name="remark[<?php echo $count;?>]" class="form-control form-control-sm remark" required>

        </td>

        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>

    </tr>
 <script>

function remove(e) {

    $(e).parent().parent().remove();

}

function checkGstNo(e) {

    gstNo = $(e).val();

    gstReg = /^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([a-zA-Z0-9]){1}?$/;

    if (gstReg.test(gstNo) == false) {

        alert('invalid Gst No');
        $(e).focus();

    }

}
 </script>