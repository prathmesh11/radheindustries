<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
// $locationResult = $admin-> getAllContactPersonDetails();


$unit  = $admin-> getAllUnit();

$rawMaterialItem1 = $admin-> getAllrawMaterialItem();

$count=$_POST['count'];
?>

<div class="form-group row" id="form_group<?php echo $count; ?>">

    <div class="col-md-3">
        <!-- <label>Select Item Name<em>*</em> </label> -->
        <select class="form-control select3" name="item_name[<?php echo $count; ?>]" data-count="<?php echo $count; ?>" onchange="rawMaterialItemDetails(this)" required>
            <option value="">Select Item Name</option>
            <?php while ($row = $admin->fetch($rawMaterialItem1)) { ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['item_name']; ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="col-md-1">
        <!-- <label>Qty Of Comp.</label> -->
        <input type="text" name="component_qty[<?php echo $count; ?>]"
            class="form-control form-control-sm component_qty" id="component_qty<?php echo $count; ?>"  required>
    </div>

    <div class="col-md-1">
        <!-- <label>size<em>*</em> </label> -->
        <input type="text" name="size[<?php echo $count; ?>]" id="size<?php echo $count; ?>" class="form-control form-control-sm size" required>

    </div>

    <div class="col-md-1">
        <!-- <label>Thickness</label> -->
        <input type="text" name="thickness[<?php echo $count; ?>]" id="thickness<?php echo $count; ?>" class="form-control form-control-sm thickness"
            required>
    </div>

    <div class="col-md-1">
        <!-- <label>Material Type</label> -->
        <input type="text" name="material_type[<?php echo $count; ?>]"
            class="form-control form-control-sm material_type" id="material_type<?php echo $count; ?>" required>
    </div>

    <div class="col-md-1">
        <!-- <label>Length<em>*</em> </label> -->
        <input type="text" name="length[<?php echo $count; ?>]" id="length<?php echo $count; ?>" class="form-control form-control-sm length" required>
    </div>

    <div class="col-md-1">
        <!-- <label>Prod. Qty R.M</label> -->
        <input type="text" name="produce_qty[<?php echo $count; ?>]" id="produce_qty<?php echo $count; ?>" data-count="<?php echo $count; ?>" onkeyup="rawMaterialReqQty(this)" class="form-control form-control-sm produce_qty" required>
    </div>

    <div class="col-md-1">
        <!-- <label>Req. Qty R.M<em>*</em> </label> -->
        <input type="text" name="required_qty[<?php echo $count; ?>]" id="required_qty<?php echo $count; ?>" class="form-control form-control-sm required_qty" required>

    </div>

    <div class="col-md-1">
        <!-- <label>Unit</label> -->
        <input type="text" name="unit[<?php echo $count; ?>]" id="unit<?php echo $count; ?>" class="form-control form-control-sm unit" required>
    </div>

    <div class="form-actions text-left"> <button type="button" class="btn btn-primary btn-sm remove-row"
            data-count="<?php echo $count; ?>"><strong>X</strong></button> </div>

</div>





 <script>

$(".remove-row").on("click", function () {

    let data_count = $(this).attr('data-count');

    $("#form_group" + data_count + "").remove();

});

$(document).ready(function () {
    $('.select3').select2();
});

function rawMaterialItemDetails(e) {

    let itemName = $(e).val();

    let dataCount = $(e).attr('data-count');

    if (itemName != '') {

        $.ajax({

            type: "POST",
            data: 'itemName=' + itemName,
            url: 'getAjaxrawMaterialItemDetails.php',
            cache: false,
            success: function (res) {

                if (res.status == 'success') {

                    $('#size' + dataCount + '').val(res.item_size);
                    $('#thickness' + dataCount + '').val(res.thickness);
                    $('#material_type' + dataCount + '').val(res.material_type);
                    $('#length' + dataCount + '').val(res.length);
                    $('#unit' + dataCount + '').val(res.unit);


                }

            }

        })

    } else {

        $('#size' + dataCount + '').val("");
        $('#thickness' + dataCount + '').val("");
        $('#material_type' + dataCount + '').val("");
        $('#length' + dataCount + '').val("");
        $('#unit' + dataCount + '').val("");

    }

}

function rawMaterialReqQty(e) {

    let produceQty = parseFloat($(e).val());
    let dataCount = $(e).attr('data-count');
    let itemQty = parseFloat($('#itemQty').val());

    let total = 0;

    if (!isNaN(produceQty) && !isNaN(itemQty)) {

        total = itemQty / produceQty;

        $('#required_qty' + dataCount + '').val(total.toFixed(2));
    }

}
 </script>