<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){

	header("location: admin-login.php");

	exit();

}

$pageName = "Opration Master";
$pageURL = 'opration-master.php';
$deleteURL = 'opration-master.php';
$tableName = 'opration_master';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$max = $admin->getOprationMaxId();
$max++;


if(isset($_GET['groupName']) && !empty($_GET['groupName'])){
	$groupName = $admin->escape_string($admin->strip_all($_GET['groupName']));
	
	$delete = $admin->query("DELETE FROM ".PREFIX.$tableName." WHERE  groupName='".$groupName."' ");

	header("location:".$pageURL."?deletesuccess");
	exit();
}




?>

<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- fancy CSS -->

	<link rel="stylesheet" href="assets/css/fancy.css">


	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.add-more {

			margin-top: 27px;

		}

	</style>

</head>

<body>

	<div class='loading_wrapper' style="display: none;">

		<div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">

		<!-- Header -->

		<?php include("include/header.php"); ?>

		<!-- /Header -->



		<!-- Sidebar -->

		<?php include("include/sidebar.php"); ?>

		<!-- /Sidebar -->



		<!-- Page Wrapper -->

		<div class="page-wrapper">



			<!-- Page Content -->

			<div class="content container-fluid">



				<!-- Page Header -->

				<div class="page-header">

					<div class="row align-items-center">

						<div class="col">

							<h3 class="page-title"><?php echo $pageName; ?></h3>

							<ul class="breadcrumb">

								<li class="breadcrumb-item">Master</li>


								<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

									<li class="breadcrumb-item"><?php echo $pageName; ?></li>

									<li class="breadcrumb-item active">

										<?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

									</li>

								<?php } else { ?>

									<li class="breadcrumb-item active"><?php echo $pageName; ?></li>

								<?php } ?>

							</ul>

						</div>

					</div>

				</div>

				<!-- /Page Header -->



				<?php if(isset($_GET['registersuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['registerfail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatefail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

					</div>

				<?php } ?>



				<?php if(isset($_GET['deletesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

					</div><br/>

				<?php } ?>



				
				<?php

					$results = $admin->query("SELECT groupName FROM ".PREFIX.$tableName."  WHERE mainGroupCode='MG'");

					$count  = 1;
					$count2 = 1001;
					$count3 = 2001;
					$count4 = 3001;
					$count5 = 4001;
					$count6 = 5001;
					$count7 = 6001;

					while($rows = $admin->fetch($results)){ 

						$groupName = $rows["groupName"];
						$style     = '';

						if($groupName=='MAIN GROUP') {

							$style = 'style="background:#000;float:right;"';

						} else {

							$style = 'style="width:95%;float:right;background-color: #007bff;"';

						}

						//echo '<div class="btn btn-sm btn-primary btn-block" data-main="main'.$count.'" onclick="change(this)" '.$style.'><button data-toggle="modal" data-target="#myModal" style="border-radius: 20Px;float: left;height: 30px;width: 29px;" data-group="'.$groupName.'" onclick="addgroup(this)" class="btn btn-sm btn-success">+</button> MAIN GROUP > '.$groupName.' <button style="border-radius: 20Px;float: right;height: 30px;width: 29px;" data-group="'.$groupName.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button></div>';
						?>
						
						<div class="btn btn-sm btn-primary btn-block" data-main="main<?php echo $count; ?>" onclick="change(this)" <?php echo $style; ?>>

							<a class="add-btn-entry btn-sm btn-success" style="border-radius: 20Px;float: left;height: 30px;width: 29px;" data-fancybox="newentry" data-type="iframe" data-src="iframe-opration.php?group=<?php echo $groupName; ?>&max=<?php echo $max; ?>" href="javascript:;"><i class="fa fa-plus"></i></a>

							MAIN GROUP > <?php echo $groupName; ?> 

							<a style="border-radius: 20Px;float: right;height: 30px;width: 29px;" href="<?php echo $pageURL; ?>?groupName=<?php echo $groupName; ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete?');" title="Delete">-</a>
							

						</div>
						
						<?php
							echo '<div class="main'.$count.'" style="display:none;">';
							
							$result1= $admin->query("SELECT groupName FROM ".PREFIX.$tableName."  WHERE mainGroupName='$groupName' AND mainGroupName<>'MAIN GROUP' ");

							while($rowss = $admin->fetch($result1)) {

								$groupName1 = $rowss["groupName"];

						?>

						<div class="btn btn-sm btn-warning btn-block" data-main="main<?php echo $count2; ?>" onclick="change(this)" style="width:90%;float:right;background:#8e44ad; color: #fff;" >


							MAIN GROUP > <?php echo $groupName; ?> > <?php echo $groupName1;?> 

							<a style="border-radius: 20Px;float: right;height: 30px;width: 29px;" href="<?php echo $pageURL; ?>?groupName=<?php echo $groupName1; ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete?');" title="Delete">-</a>
							

						</div>

						<?php
						
							

							// echo '<div class=" btn btn-sm btn-warning btn-block" data-main="main'.$count2.'" onclick="change(this)" style="width:90%;float:right;background:#8e44ad;"><button data-toggle="modal" data-target="#myModal" style="border-radius: 20Px;float: left;height: 30px;width: 29px;" data-group="'.$groupName1.'" onclick="addgroup(this)" class="btn btn-sm btn-success">+</button>MAIN GROUP > '.$groupName.' > '.$groupName1.'<button style="border-radius: 20Px;float: right;height: 30px;width: 29px;" data-group="'.$groupName1.'" onclick="removeitem(this)" class="btn btn-sm btn-danger">-</button> </div>';
							
							echo '<div class="main'.$count2.'" style="display:none;">';

							   $count2++;
							echo '</div>';
						}
						$count++;
					echo '</div>';
					}

				?>


			</div>



			<!-- /Page Content -->

		</div>

		<!-- /Page Wrapper -->


	</div>

	<!-- /Main Wrapper -->






	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>


	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>


	<!-- fancy js -->

	<script src="assets/js/fancy.js"></script>



		<!-- Modal -->

		<div class="modal fade" id="myModal">

			<div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">

					<div class="modal-header">

						<h4 class="modal-title w-100 text-center">Item Group Master</h4>

						<button type="button" class="close" data-dismiss="modal">&times;</button>

					</div>

					<div class="modal-body">

						<!-- <form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off"> -->

							<div class="form-group row">

								<label for="staticEmail" class="col-sm-4 col-form-label">Main Group Name</label>

								<div class="col-sm-8">

									<input type="text" name="maingroupname" id="maingroupname" class="form-control" readonly>

								</div>
								
							</div>

							<div class="form-group row">

								<label for="staticEmail" class="col-sm-4 col-form-label">Sub Group Name</label>

								<div class="col-sm-8">

									<input type="text" name="groupname" id="groupname" class="form-control">

								</div>
								
							</div>


							<div class="form-group row">

								<label for="staticEmail" class="col-sm-4 col-form-label">Title</label>

								<div class="col-sm-8">

									<input type="text" class="form-control input-sm" id="title" name="title">

								</div>
								
							</div>


							<div class="form-group row">

								<label for="staticEmail" class="col-sm-4 col-form-label">Sub Group code</label>

								<div class="col-sm-8">

									<input type="text" placeholder="Sub Item Group" name="groupcode" id="codenamer" value="<?php echo $max; ?>" class="form-control" readonly> 

								</div>
								
							</div>
							

						<!-- </form> -->

					</div>

					<div class="modal-footer">


						<button type="submit" id="register" onclick="submit()"  class="btn btn-danger btn-block"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

					</div>

				</div>

			</div>

		</div>

	<!-- /Modal -->



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({
				rules: {
					// maingroupname: {
					// 	required: true,
					// },

                    // groupname: {
					// 	required: true,
					// },

					// title: {
					// 	required: true,
					// },

                    // groupcode: {
					// 	required: true,
					// },

				},

				messages: {
					
				}
			});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});


		
		function addgroup(e){

			$('#maingroupname').val($(e).data('group'));

		}

		function change(e){

			var main=$(e).data('main');

			$('.'+main).toggle();

		}

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});


	
			$("[data-fancybox='newentry']").fancybox({
            smallBtn: true,
             toolbar: false,
             afterClose: function () {
               parent.location.reload(true); 
             },
               iframe : {
                 css : {
                   width : '560px',
				   height: '560px',
                 }
               }
            });

			// $('#register').click(function(){

			// 	if($("#form").valid()) {

			// 		$(".loading_wrapper").show();

			// 	}

			// })

		});




	</script>




</body>

</html>