<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$count=$_POST['count'];
?>

<tr>

    <td>

        <?php echo $count+1;?>

    </td>


    <td>

        <input type="text" name="process[<?php echo $count;?>]" class="form-control form-control-sm process" required>

    </td>

    <td>

        <input type="file" class="form-control-sm sop" onchange="photoupload(this)" name="sop[<?php echo $count;?>]" />

    </td>

    <td>

        <input type="file" class="form-control-sm check_list" onchange="photoupload(this)"
            name="check_list[<?php echo $count;?>]" />

    </td>

    <td>

        <input type="file" class="form-control-sm online_qa_graph" onchange="photoupload(this)"
            name="online_qa_graph[<?php echo $count;?>]" />

    </td>

    <td>

        <input type="file" class="form-control-sm list_pokayoke" onchange="photoupload(this)"
            name="list_pokayoke[<?php echo $count;?>]" />

    </td>

    <td>

        <input type="file" class="form-control-sm training_sheet" onchange="photoupload(this)"
            name="training_sheet[<?php echo $count;?>]" />

    </td>

    <td>

        <input type="text" name="remark[<?php echo $count;?>]" class="form-control form-control-sm remark">

    </td>

    <td>

        <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

    </td>

</tr>
 <script>

    function remove(e) {

        $(e).parent().parent().remove();

    }

    function photoupload(e) {

        let photoSize = e.files[0].size;
        if (photoSize > 5000000) {
            alert('Photo Size Not Greater Than 5 mb ');

            $(e).val("");

        }
    }
 </script>