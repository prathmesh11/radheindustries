<?php 

    include_once 'include/config.php';

    include_once 'include/admin-functions.php';

    $admin = new AdminFunctions();


    if(!$loggedInUserDetailsArr = $admin->sessionExists()){

        header("location: admin-login.php");

        exit();

    }

    if (isset($_POST['itemId'])) {

        $itemId = $_POST['itemId'];
    
        $allComponentName = $admin-> getAllcompoentName($itemId,$loggedInUserDetailsArr['branch_id']);

    }

?>
   
    <option value="">Select Component Name</option>

    <?php while ($row = $admin->fetch($allComponentName)) { ?>

        <option value="<?php echo $row['id']; ?>"><?php echo $row['item_name']; ?></option>

    <?php } ?>