<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$count=$_POST['count'];
?>
    <tr>

        <td>

            <?php echo $count+1;?>

        </td>

        <td>

            <input type="text" name="machine[<?php echo $count;?>]" class="form-control form-control-sm machine" required>

        </td>

        <td>

            <input type="text" name="size[<?php echo $count;?>]" class="form-control form-control-sm size" required>

        </td>

        <td>

            <input type="text" name="capacity[<?php echo $count;?>]" class="form-control form-control-sm capacity">

        </td>

        <td>

            <input type="text" name="machine_number[<?php echo $count;?>]"
                class="form-control form-control-sm machine_number">

        </td>

        <td>

            <input type="text" class="form-control-sm form-control sop" name="sop[<?php echo $count;?>]" />

        </td>

        <td>

            <input type="file" class="form-control-sm machine_photo" onchange="photoupload(this)"
                <?php if(isset($_GET['edit'])){if(empty($data['machine_photo'])){ echo "required"; } }?>
                name="machine_photo[<?php echo $count;?>]" data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="text" name="check_sheet_maintainance[<?php echo $count;?>]"
                class="form-control form-control-sm check_sheet_maintainance">

        </td>

        <td>

            <input type="text" name="maintenance_frequency[<?php echo $count;?>]"
                class="form-control form-control-sm maintenance_frequency">

        </td>

        <td>

            <input type="date" name="last_maintenance_date[<?php echo $count;?>]"
                class="form-control form-control-sm last_maintenance_date">

        </td>

        <td>

            <input type="date" name="next_maintenance_date[<?php echo $count;?>]"
                class="form-control form-control-sm next_maintenance_date">

        </td>

        <td>

            <input type="text" name="special_remark[<?php echo $count;?>]"
                class="form-control form-control-sm special_remark">

        </td>

        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>

    </tr>

 <script>

    function remove(e) {

        $(e).parent().parent().remove();

    }

    function photoupload(e) {

        let photoSize = e.files[0].size;
        if (photoSize > 5000000) {
            alert('Photo Size Not Greater Than 5 mb ');

            $(e).val("");

        }
    }
 </script>