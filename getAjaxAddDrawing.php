<?php 

    include_once 'include/config.php';

    include_once 'include/admin-functions.php';

    $admin = new AdminFunctions();

    $count=$_POST['count'];

    if(!$loggedInUserDetailsArr = $admin->sessionExists()){

        header("location: admin-login.php");

        exit();

    }

    if (isset($_POST['itemId'])) {
        
        $itemId = $_POST['itemId'];

        $allComponentName = $admin-> getAllcompoentName($itemId,$loggedInUserDetailsArr['branch_id']);

    }

?>
    <tr>

        <td>

            <?php echo $count+1;?>

        </td>

        <td>

            <select class="form-control form-control-sm select2" name="component_name[<?php echo $count;?>]" required>

                <option value="">Select Component Name</option>

                <?php while ($rows = $admin->fetch($allComponentName)) { ?>

                    <option value="<?php echo $rows['id']; ?>"><?php echo $rows['item_name']; ?></option>

                <?php } ?>

            </select>

        </td>


        <td>

            <input type="text" name="component_drawing_no[<?php echo $count;?>]"
                class="form-control form-control-sm component_drawing_no" required>

        </td>

        <td>

            <input type="file" class="form-control-sm drawing_photo" onchange="photoupload(this)"
                <?php if(isset($_GET['edit'])){if(empty($data['drawing_photo'])){ echo "required"; } }else{ echo "required"; }?>
                name="drawing_photo[<?php echo $count;?>]" id="drawing_photo<?php echo $count;?>"
                data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="text" name="rev_chk_frequency[<?php echo $count;?>]"
                class="form-control form-control-sm rev_chk_frequency" required>

        </td>

        <td>

            <input type="date" name="last_check_date[<?php echo $count;?>]"
                class="form-control form-control-sm last_check_date" required>

        </td>

        <td>

            <input type="date" name="next_due_date[<?php echo $count;?>]" class="form-control form-control-sm next_due_date"
                required>

        </td>


        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>

    </tr>

 <script>

    function remove(e) {

        $(e).parent().parent().remove();

    }

    $('.select2').select2();


    function photoupload(e) {

        let photoSize = e.files[0].size;
        if (photoSize > 5000000) {
            alert('Photo Size Not Greater Than 5 mb ');

            $(e).val("");

        }
    }
 </script>