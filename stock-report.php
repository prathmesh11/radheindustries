<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName  = "Stock Report";
$pageURL   = 'stock-report.php';
$navenq4   = 'background:#27ae60;';
$tableName = 'stock';


include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

// echo "SELECT * FROM ".PREFIX.$tableName."  WHERE  branch_id = '".$loggedInUserDetailsArr['branch_id']."' ORDER BY id DESC LIMIT 1 ";

$results = $admin->query("SELECT id,component_name,item_name FROM ".PREFIX."item_master  WHERE  branch_id = '".$loggedInUserDetailsArr['branch_id']."' AND main_group = 'Component' ");



?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

</head>

<body>



    <div class='loading_wrapper' style="display: none;">

        <div class='loadertext1'>Please wait while we upload your files...</div>

    </div>

    <div class="main-wrapper">

        <!-- Header -->

        <?php include("include/header.php"); ?>

        <!-- /Header -->



        <!-- Sidebar -->

        <?php include("include/sidebar.php"); ?>

        <!-- /Sidebar -->


        <!-- Page Wrapper -->

        <div class="page-wrapper">

            <!-- Page Content -->

            <div class="content container-fluid">

                <!-- Page Header -->

                <div class="page-header">

                    <div class="row align-items-center">

                        <div class="col">

                            <h3 class="page-title"><?php echo $pageName; ?></h3>

                            <ul class="breadcrumb">

                                <li class="breadcrumb-item">Trasaction</li>

                                <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

                                <li class="breadcrumb-item"><?php echo $pageName; ?></li>

                                <li class="breadcrumb-item active">

                                    <?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

                                </li>

                                <?php } else { ?>

                                <li class="breadcrumb-item active"><?php echo $pageName; ?></li>

                                <?php } ?>

                            </ul>

                        </div>

                    </div>

                </div>

                <!-- /Page Header -->


                <?php include("include/stock-tracking.php"); ?>


                <br>

                <div class="row">

                    <div class="col-md-12">

                        <table class="table table-striped custom-table mb-0 datatable datatable-selectable-data">

                            <thead>

                                <tr>

                                    <th width="20px">#</th>

                                    <th>Finish Good Item Name</th>

                                    <th>Component Name</th>

                                    <th>Opening Balance</th>

                                    <th>Material In</th>

                                    <th>Material Out</th>

                                    <th>Adjustment Qty Plus</th>

                                    <th>Adjustment Qty Minus</th>

                                    <th>Balance Qty</th>


                                </tr>

                            </thead>

                            <tbody>

								<?php

								$x = 1;

								while($row = $admin->fetch($results)){ 

                                    $id = $row['id'];
                                    $component_name = $row['component_name'];
                                    $item_name = $row['item_name'];

                                    $item     = $admin->fetch($admin->query("SELECT * FROM ".PREFIX."stock where item_id= '".$id."' AND finish_good_id = '".$component_name."' AND branch_id = '".$loggedInUserDetailsArr['branch_id']."'  ORDER BY id DESC LIMIT 1 "));
                                    if($item){
                                    
                                        $finishGoodName = $admin->getUniqueItemNameById($item['finish_good_id']);

                                        $componentName = $admin->getUniqueItemNameById($item['item_id']);


                                    ?>
                                        
									<tr>

										<td> <?php echo $x; ?> </td>

                                        <td> <?php echo $finishGoodName; ?> </td>

                                        <td> <?php echo $componentName; ?> </td>

                                        <td> <?php echo $item['opening_balance']; ?> </td>

                                        <td> <?php echo $item['material_in']; ?> </td>

                                        <td> <?php echo $item['material_out']; ?> </td>

                                        <td> <?php echo $item['adjustment_qty_plus']; ?> </td>

                                        <td> <?php echo $item['adjustment_qty_minus']; ?> </td>

                                        <td> <?php echo $item['balance']; ?> </td>										
										

									</tr>

								<?php $x++;} } ?>

							</tbody>

                        </table>

                    </div>

                </div>

            </div>

            <!-- /Page Content -->

        </div>

        <!-- /Page Wrapper -->

    </div>

    <!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					man_name: {
						required: true,
					},
                    designation: {
						required: true,
					},

                    department: {
						required: true,
					},

					rate: {
						required: true,
						number:true,
					},

					hours: {
						required: true,
						number:true,

					},

					skill_matrix: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>
</body>

</html>