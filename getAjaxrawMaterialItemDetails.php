<?php 

    header('content-type: application/json; charset=utf-8');
    header("access-control-allow-origin: *");

    include_once 'include/config.php';
    include_once 'include/admin-functions.php';
    $admin = new AdminFunctions();

    if (isset($_POST['itemName'])) {
        
        $itemName = $admin-> getRawmaterialItem($_POST);

    }

    echo '{"status":"success","item_size":"'.$itemName['item_size'].'","thickness":"'.$itemName['thickness'].'","material_type":"'.$itemName['material_type'].'","length":"'.$itemName['length'].'","unit":"'.$itemName['unit'].'"}'

?>



