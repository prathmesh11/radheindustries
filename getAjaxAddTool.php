<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$count=$_POST['count'];
?>
    <tr>

        <td>

            <?php echo $count+1;?>

        </td>

        <td>

            <input type="text" name="process_name[<?php echo $count;?>]" class="form-control form-control-sm process_name boxSize"
                required>

        </td>

        <td>

            <input type="text" name="tool_name[<?php echo $count;?>]" class="form-control form-control-sm tool_name boxSize"
                required>

        </td>

        <td>

            <input type="text" name="tool_number[<?php echo $count;?>]" class="form-control form-control-sm tool_number boxSize"
                required>

        </td>

        <td>

            <input type="file" class="form-control-sm photo" onchange="photoupload(this)"
                <?php if(isset($_GET['edit'])){if(empty($data['photo'])){ echo "required"; } }?>
                name="photo[<?php echo $count;?>]" id="<?php echo $count;?>" data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="file" class="form-control-sm check_list" onchange="photoupload(this)"
                <?php if(isset($_GET['edit'])){if(empty($data['check_list'])){ echo "required"; } }?>
                name="check_list[<?php echo $count;?>]" id="<?php echo $count;?>" data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="text" name="tool_grinding_frequency[<?php echo $count;?>]"
                class="form-control form-control-sm tool_grinding_frequency boxSize" >

        </td>

        <td>

            <input type="date" name="last_grinding_date[<?php echo $count;?>]"
                class="form-control form-control-sm last_grinding_date boxSize" >

        </td>

        <td>

            <input type="date" name="next_due_date[<?php echo $count;?>]" class="form-control form-control-sm next_due_date boxSize"
                >

        </td>

        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>

    </tr>

 <script>

    function remove(e) {

        $(e).parent().parent().remove();

    }

    function photoupload(e) {

        let photoSize = e.files[0].size;
        if (photoSize > 5000000) {
            alert('Photo Size Not Greater Than 5 mb ');

            $(e).val("");

        }
    }
 </script>