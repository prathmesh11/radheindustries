<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName = "Account Information";
$pageURL = 'account-information.php';
$deleteURL = 'account-information.php';
$editURL   = 'account-info.php';
$navenq5   = 'background:#27ae60;';
$tableName = 'order_booking';



include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


if(isset($_GET['edit']) && !empty($_GET['id'])  || isset($_GET['add']) && !empty($_GET['id'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$datas  = $admin->getUniqueOrderBookAccountId($id);
	$orderDispachTracking = $admin->getUniqueOrderDispachId($id);
	$data  = $admin->getUniqueOrderBookId($id);


}



if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addAccountInformation($_POST,$_FILES,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$editURL."?registersuccess");
		exit();

	}
}

//print_r($_FILES);
if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateAccountInformation($_POST,$_FILES,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$editURL."?updatesuccess");
		exit();
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.form-control{
            border-bottom: 1px solid blue;
            width:200px!important;
        }

	</style>

</head>

<body>



    <div class='loading_wrapper' style="display: none;">

        <div class='loadertext1'>Please wait while we upload your files...</div>

    </div>

    <div class="main-wrapper">

        <!-- Header -->

        <?php include("include/header.php"); ?>

        <!-- /Header -->



        <!-- Sidebar -->

        <?php include("include/sidebar.php"); ?>

        <!-- /Sidebar -->



        <!-- Page Wrapper -->

        <div class="page-wrapper">



            <!-- Page Content -->

            <div class="content container-fluid">



                <!-- Page Header -->

                <div class="page-header">

                    <div class="row align-items-center">

                        <div class="col">

                            <h3 class="page-title"><?php echo $pageName; ?></h3>

                            <ul class="breadcrumb">

                                <li class="breadcrumb-item">Trasaction</li>


                                <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

                                <li class="breadcrumb-item"><?php echo $pageName; ?></li>

                                <li class="breadcrumb-item active">

                                    <?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

                                </li>

                                <?php } else { ?>

                                <li class="breadcrumb-item active"><?php echo $pageName; ?></li>

                                <?php } ?>

                            </ul>

                        </div>

                    </div>

                </div>

                <!-- /Page Header -->



                <?php if(isset($_GET['registersuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['registerfail'])){ ?>

                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['updatesuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['updatefail'])){ ?>

                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong>
                    <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

                </div>

                <?php } ?>



                <?php if(isset($_GET['deletesuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

                </div><br />

                <?php } ?>


                <?php include("include/tracking.php"); ?>

                <br>
				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<h4 class="card-title mb-0"><?php if(isset($_GET['edit'])) {

										echo 'Edit '.$pageName;

									} else {

										echo 'Add New '.$pageName;

									}

									?></h4>

								</div>

								<div class="card-body">
								
									<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

									<table class="table table-bordered">
											<thead>
												<tr>
													<th width="20px">#</th>
													<th>Customer Name</th>
													<th>PO NO.</th>
													<th>Item Name</th>
													<th>Order Qty</th>
													<th>Balance Qty</th>
													<th>Category</th>
													<th>Order Status</th>
												</tr>
											</thead>

											<tbody>

											<?php

												$order_status = $data['order_status'];
												
												if ($order_status == "Urgent") {
													$backgroundcolor = "red";
													$color           = "#fff";
												}else {
													$backgroundcolor = "";
													$color           = "#000";
												}
											
											?>
												<tr style="background-color:<?php echo $backgroundcolor;?>;color:<?php echo $color;?>">
													<td>
														1
													</td>
													<td>
														<?php 
														$customer_short_name = $admin->getUniqueCustomerById($data['customer_name']);
														$componentName = $admin->getUniqueItemById($data['category']);

														echo $customer_short_name['customer_short_name'];?>
													</td>
													
													<td>
														<?php echo $data['po_no'];?>
													</td>
													<td>
														<?php echo $data['item_name'];?>
													</td>
													<td>
														<?php echo $data['order_qty'];?>
													</td>
													<td>
														<?php echo $data['balance_qty'];?>
													</td>
													<td>
														<?php echo $componentName['item_name'];?>
													</td>
													<td>
														<?php echo $data['order_status'];?>
													</td>
												</tr>
											</tbody>
										</table>

											<div class="table-responsive">

												<table class="table table-bordered" id="components">

													<thead style="background: #2980b9;color: #fff;">

														<tr>

															<th>#</th>
															<th>INVOICE NO.</th>
															<th>INVOICE DATE</th>
															<th>INVOICE Qty</th>
															<th>PROOF OF DELIVERY RECEIVED?</th>
															<th colSpan="3">PAYMENT RECEIVED</th>
															<th>ACCOUNT REMARK</th>
														</tr>

														<tr>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th></th>
															<th>Doc No</th>
															<th>Date</th>
															<th>Amount</th>
															<th></th>
														</tr>


													</thead>
													<?php
														
														if(isset($_GET['edit'])) { 
															$i=0;
															$y=1;
															while($row = $admin->fetch($datas)) {

													?>
														<tbody>

															<tr>

																<td>

																	<?php echo $y;?>

																	<input type="hidden" value="<?php echo $row['id']; ?>" name="account_id[<?php echo $i;?>]" class="form-control form-control-sm">

																</td>

																<td>

																	<input type="text" name="invoice_no[<?php echo $i;?>]" value="<?php echo $row['invoice_no']?>" class="form-control form-control-sm invoice_no boxSize" required readonly>

																</td>

																<td>

																	<input type="date" name="invoice_date[<?php echo $i;?>]" value="<?php echo $row['invoice_date']; ?>" class="form-control form-control-sm invoice_date boxSize" required readonly>

																</td>

																<td>

																	<input type="number" name="dispach_qty[<?php echo $i;?>]" value="<?php echo $row['dispach_qty']; ?>"  class="form-control form-control-sm dispach_qty boxSize" required readonly>

																</td>

																<td>

																	<input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['proof_of_delivery'])){ echo "required"; } }else{ echo "required"; }?> name="proof_of_delivery[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																	<?php
																		if(!empty($row['proof_of_delivery'])){ 
																	?>
																		<a target="_blank_" href="<?php echo $row['proof_of_delivery'];?>" class="btn btn-info btn-sm">View Photo</a>

																		<a target="_blank_" href="<?php echo $row['proof_of_delivery'];?>" class="btn btn-info btn-sm" download>Download Photo</a>

																	<?php
																		}
																	?>

																</td>

																<td>

																	<input type="text" name="payment_received_doc[<?php echo $i;?>]" value="<?php echo $row['payment_received_doc']; ?>" class="form-control form-control-sm payment_received_doc boxSize" required>

																</td>

																<td>

																	<input type="date" name="payment_received_date[<?php echo $i;?>]" value="<?php echo $row['payment_received_date']; ?>" class="form-control form-control-sm payment_received_date boxSize" required>

																</td>

																<td>

																	<input type="number" name="payment_received_amount[<?php echo $i;?>]" value="<?php echo $row['payment_received_amount']; ?>" class="form-control form-control-sm payment_received_amount boxSize" required>

																</td>

																<td>

																	<input type="text" name="remark[<?php echo $i;?>]" value="<?php echo $row['remark']; ?>" class="form-control form-control-sm remark boxSize" required>

																</td>

															</tr>

														</tbody>

													<?php
														$i++; $y++;} } else {
															$i=0;
															$y=1;
															while($row = $admin->fetch($orderDispachTracking)) {

													?>
														<tbody>

															<tr>
																<td>
																	<?php echo $y;?>
																</td>
																<td>
																	<input type="text" name="invoice_no[<?php echo $i;?>]" value="<?php echo $row['invoice_no']; ?>" class="form-control form-control-sm invoice_no boxSize" required readonly>
																</td>
																<td>
																	<input type="date" name="invoice_date[<?php echo $i;?>]" value="<?php echo $row['invoice_date']; ?>" class="form-control form-control-sm invoice_date boxSize" required readonly>
																</td>
																<td>
																	<input type="number" name="dispach_qty[<?php echo $i;?>]" value="<?php echo $row['dispach_qty']; ?>" class="form-control form-control-sm dispach_qty boxSize" required readonly>
																</td>
																<td>
																	<input type="file" class="form-control-sm photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['proof_of_delivery'])){ echo "required"; } }else{ echo "required"; }?> name="proof_of_delivery[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

																</td>
																<td>
																	<input type="text" name="payment_received_doc[<?php echo $i;?>]" class="form-control form-control-sm payment_received_doc boxSize" required>

																</td>
																<td>
																<input type="date" name="payment_received_date[<?php echo $i;?>]" class="form-control form-control-sm payment_received_date boxSize" required>

																</td>
																<td>
																<input type="number" name="payment_received_amount[<?php echo $i;?>]" class="form-control form-control-sm payment_received_amount boxSize" required>

																</td>
																<td>
																<input type="text" name="remark[<?php echo $i;?>]" class="form-control form-control-sm remark boxSize" required>

																</td>
															</tr>

														</tbody>

													<?php
														
														$i++; $y++;} }

													?>

												</table>

											</div>

										<br>
										<div class="form-actions text-left">

											<input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

											<?php if(isset($_GET['edit'])){ ?>

												<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

												<button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button>

											<?php } else { ?>

												<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

												<button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

											<?php } ?>

										</div>

									</form>

								</div>

							</div>

						</div>

					</div>

				<?php } ?>

            </div>

            <!-- /Page Content -->

        </div>

        <!-- /Page Wrapper -->

    </div>

    <!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					invoice_no: {
						required: true,
					},
                    invoice_date: {
						required: true,
					},

                    proof_received: {
						required: true,
					},

					payment_received: {
						required: true,
					},

					account_remark: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function(){

				if($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

			$('.select2').select2();


			$(".add-more").on("click", function(){
                
				var count = $('#components > tbody > tr').length;
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxAddMan.php',
                    success: function (services_clone) {

                        $("#components tr:last").after(services_clone);

                    }
                });

			});

		});


		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));
				
			}
			
		}
		

		function photoupload(e) {

			let photoSize = e.files[0].size;

			if (photoSize > 5000000) {

				alert('Photo Size Not Greater Than 5 mb ');

				$(e).val("");

			}
		}
				
	</script>

</body>

</html>