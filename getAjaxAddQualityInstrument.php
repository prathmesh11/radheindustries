<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$count=$_POST['count'];
?>
<tr>

    <td>

        <?php echo $count+1;?>

    </td>



    <td>

        <input type="text" name="instrument_name[<?php echo $count;?>]" class="form-control form-control-sm instrument_name"
            required>

    </td>

    <td>

        <input type="text" name="capacity[<?php echo $count;?>]" class="form-control form-control-sm capacity" required>

    </td>


    <td>

        <input type="text" name="accuracy[<?php echo $count;?>]" class="form-control form-control-sm accuracy" required>

    </td>

    <td>

        <input type="text" name="make[<?php echo $count;?>]" class="form-control form-control-sm make" required>

    </td>

    <td>

        <input type="date" name="purchase_date[<?php echo $count;?>]" class="form-control form-control-sm purchase_date"
            required>

    </td>

    <td>

        <input type="date" name="last_calibration_date[<?php echo $count;?>]"
            class="form-control form-control-sm last_calibration_date" required>

    </td>

    <td>

        <input type="date" name="next_calibration_due_date[<?php echo $count;?>]"
            class="form-control form-control-sm next_calibration_due_date" required>

    </td>

    <td>

        <input type="text" name="current_condition[<?php echo $count;?>]"
            class="form-control form-control-sm current_condition" required>

    </td>

    <td>

        <input type="text" name="remark[<?php echo $count;?>]" class="form-control form-control-sm remark" required>

    </td>

    <td>

        <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

    </td>

</tr>
 <script>

function remove(e) {

    $(e).parent().parent().remove();

}
 </script>