<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName = "Man Master";
$pageURL = 'man-master.php';
$deleteURL = 'man-master.php';
$tableName = 'man_master';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);

$itemGroupMaster   = $admin-> getAllItemGroupMaster();

$unit   = $admin-> getAllUnit();



$results = $admin->query("SELECT * FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND branch_id = '".$loggedInUserDetailsArr['branch_id']."' ");



if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET deleted_by = '".$loggedInUserDetailsArr['id']."', deleted_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_GET['activate']) && !empty($_GET['activate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['activate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '1', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?activatesuccess");
	exit();
}

if(isset($_GET['deactivate']) && !empty($_GET['deactivate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['deactivate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '0', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?deactivatesuccess");
	exit();
}

if(isset($_POST['register'])) {
	 if($csrf->check_valid('post')) {
		$result = $admin->addManMaster($_POST,$_FILES,$loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?registersuccess");
		exit;
	}
}

//print_r($_FILES);

if(isset($_GET['edit']) && !empty($_GET['id'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueManId($id);
}

//   print_r($data);

if(isset($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$result = $admin->updateManMaster($_POST,$_FILES,$loggedInUserDetailsArr['id']);
		header("location:".$pageURL."?updatesuccess");
		exit;
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.boxSize{
            border-bottom: 1px solid blue;
            height:25px!important;
			/* width:200px!important; */

        }

		.nav-tabs .nav-link.active {
			color: #2980b9;
			background-color: #fff;
			border-color: #2980b9 #2980b9 #fff;
			/* font-weight:bold; */
			/* font-size:18px; */
		}

		.nav-tabs {
			border-bottom: 1px solid #2980b9;
		}

	</style>

</head>

<body>



	<div class='loading_wrapper' style="display: none;">

		<div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">

		<!-- Header -->

		<?php include("include/header.php"); ?>

		<!-- /Header -->



		<!-- Sidebar -->

		<?php include("include/sidebar.php"); ?>

		<!-- /Sidebar -->



		<!-- Page Wrapper -->

		<div class="page-wrapper">



			<!-- Page Content -->

			<div class="content container-fluid">



				<!-- Page Header -->

				<div class="page-header">

					<div class="row align-items-center">

						<div class="col">

							<h3 class="page-title"><?php echo $pageName; ?></h3>

							<ul class="breadcrumb">

								<li class="breadcrumb-item">Master</li>


								<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

									<li class="breadcrumb-item"><?php echo $pageName; ?></li>

									<li class="breadcrumb-item active">

										<?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

									</li>

								<?php } else { ?>

									<li class="breadcrumb-item active"><?php echo $pageName; ?></li>

								<?php } ?>

							</ul>

						</div>

						<div class="col-auto float-right ml-auto">

							<a href="<?php echo $pageURL; ?>?add" class="btn add-btn"><i class="fa fa-plus"></i> Add <?php echo $pageName; ?></a>

						</div>

					</div>

				</div>

				<!-- /Page Header -->



				<?php if(isset($_GET['registersuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['registerfail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatefail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

					</div>

				<?php } ?>



				<?php if(isset($_GET['deletesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

								<div class="row">

									<div class="col-sm-4" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

										<div class="form-group row">

											<label for="finish_weight_net" class="col-sm-5 col-form-label">Select Branch<em>*</em></label>

											<div class="col-sm-7">

												<select class="form-control form-control-sm boxSize" name="branch">

													<option value="1" <?php if(isset($_GET['edit']) and $data['branch_id']=='1') { echo 'selected'; } ?>>Virar</option>

													<option value="2" <?php if(isset($_GET['edit']) and $data['branch_id']=='2') { echo 'selected'; } ?>>Pune</option>

												</select>
													
											</div>

										</div>

										<div class="form-group row">

											<label for="finish_weight_net" class="col-sm-5 col-form-label">Employee Name<em>*</em></label>

											<div class="col-sm-7">

												<input type="text" name="emp_name" id="empName" value="<?php if(isset($_GET['edit'])) { echo $data['emp_name']; } ?>" class="form-control form-control-sm boxSize">

											</div>

										</div>

										<div class="form-group row">

											<label for="finish_weight_net" class="col-sm-5 col-form-label">Employee Photo<em>*</em></label>

											<div class="col-sm-7">

												<input type="file" class="form-control-sm" onchange="photoemploayeeupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['emp_photo'])){ echo "required"; } }else{ echo "required"; }?> name="emp_photo" id="0" data-image-index="0" />

											</div>

										</div>

										<div class="form-group row">

											<div class="col-sm-3"></div>

											<div class="col-sm-9">

												<img id="img" class="img-responsive" style="width:170px;height:170px;border:1px solid #CCC;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAIAAAAiOjnJAAAACXBIWXMAAAsSAAALEgHS3X78AAAJ00lEQVR42u2dXXOTXBeGcScQSkqTZog1GbWOdvTI//83nPFQR20aQyMpLYHSncCmzwFvO33TJiUpJOzFfR046jiVwMW91v6AvLq9vVUAyBuGUwAgFoBYAGIBALEAxAIQCwCIBSAWgFgAQCxQduo4BQtEURRFkRCCc776XzabTUVRVFVVVRXnbYFXWITmnIdhyDmfzWbPyrQMVVV1XW80Gs1ms9Fo1Go1iFVRscIw9H3f9/0oinL/4YZhmKZpmmZlw6xyYgkhPM9zXbcIn540rN1ut1otiEVZKdd1XddNkmTL/7Wqqt1ut1J6VUUs3/dHo9H2lVrQq9/vG4YBsYgElW3bvu+X5Hg6nY5lWeS7e+Jicc6Hw+F22qns6Lre6/V0XYdYUuJ53vn5+W7L3zIYY8fHx4TdIiuW53mj0ajkB9nv96l29DSXdNJWvfzHORqNNp6ShVg76KuksCrl9PSUpFvUxBJCDIfDcvZVT5IkiW3bQgiIVWps2y7bGDBLxNq2DbHKi+u65ZmvWrcplPTI6YslhHAcR97jH4/HlAoiHbHG47FErdVjoihyXRdile6qeJ5HoJSTCS0iYkldBB+OEMmEFgWxaMQVsdCiIBaNuLoPLRo3ifRipTtCKQ3UaVRD6cUiZlVa2Qks8kAsfCiI9QjOOckVXAKz8AwXANUQYi0SBIFClDAMIRZua4QxIbFkv6eRWCXl+vpaIY3UbiGxSj3mhVg7aLCk2ykKsXDSy3LzQCyIhVpPQizyDZbsoYXEglgQ6w4hhNTb26tw/0gp1mw2U6qBvLtJGe5j3EIQS/r7GImF+xggsegi76wKeiwAse6oyFwDxAKo+/KLVZHFHNlHKkgsALEAxMKQEGKhmQUQC0AsALEAqLxY5B/OgVgQC6AUAogFIBYAEAtALHCPqqoQC0AsiAUgFoBYKA25Xh7GIBbEyh9d1yEWABAL8QyxFmg2mxALYoEXUavVIBZuYjTvEAsfFmJRuo8hFk437p/qidVoNBBXEAu3chXvH4hVXqSesZO1x5J3dRaJhbt5xzePvLOjEotlGAZtsWT/gBALkQyx/r9/pz2bhcTCqS+kwZL9tpFYLNM0qYpF4KPJLRbVSQcCYcxwZ5fukjCGxIJY+FBExaJXDdvtNsTCZch/PEhjtCu9WJ1Oh5JYrVaLSKdI4Ban1GmRCWAKDQqZ0Gq1WmSWEyiIZRgGjb6EUlknMqQiUEEMw6C0gZGIWASKSLfbpTQKoTMJJPWFabVaxNbU6Ygl9bUhFlcKsXc3SHp5Op0Ovb1lpMSScXjIGLMsSyEHtYW2fr8vXcpK/dBEVcRSVVWi2SBd14ktSZEVS1EUy7Jk2fLQ6/UUohAUq1arSVEQO50O4Ue6aW7tNU2z5CvTqqqS7NmJi5VWmTIXxLdv35Ls2emLVeaCaFkW+feaUH61hmmaJRxz7e3t0Ztnr5ZYiqIcHR1pmlae44njuCJvjaP/MqD3798nSVKGI0mSZDweX15eQiwKpN96Xwa3HMeZz+cQiwicc8YY53y3bjmOE4ahoijT6XQ+n0Ms6Ukvp2EYl5eXu3LLcZwgCNLf397eXlxcQCzpub6+vh8k2ra9ZbeSJHloVcpoNIJY0tfBe5M0TWs2m2dnZ1urREmS2La9YJWiKJPJRAgBsSTG87yHf2y3241G4+/fv9PpdAtOL5NYCPHv3z+IJTFXV1cLf2NZVq1Wu7i4sG07juOCgir9+SvK7nA4hFgSx9XjS1uv19PV3zRRLi4u8u26giA4Ozt7NhEnk8nNzQ3hk//q9vaW6mf7+fNnOon1mOl0ej80Y4wZhnF4eFiv11+SUkEQeJ6XPQU/ffr05csXqie/TvWDeZ63zCpFUQ4ODubzue/7904EQaBpmmmauq5nXwVKkiQMw5ubm8cd+rMMBoOTkxOqexxoJpYQ4vfv3yvEuq9HqVuL/QFjmqbt7e0pT329ShzHcRzPZrM4jl84wDw5Ofn8+TMSSxomk8mzVimK0ul05vP5bDZ7nEOcc8550cf558+fDx8+lGqZHM37UsIwdF0304dn7M2bNzt8VX8cxz9+/MCoUI4iuNa8NmPs9evXO9zHfHp6SnJZmppYg8EgSxFcwLKsw8PDXR3zt2/f6E3EkxJrNBpt3Bi12+2jo6OdjNFubm6+f/8Oscpr1cICzroYhtHv99PB4PYP/tevX5huKF1fNR6PX2jVQ6bT6dXV1fbL09evX9+9ewexymLVYDDIfWogSRLXdZ+c5SqCvb29drut63q/36fx4mS5xQrD8OzsrLgtVnEcX11dFadX+lb3g4ODh6tJNNySVSwhxGQyyThf9XK90jWfDcaby3zSdX3FS5darZZ0r82hIJbneefn59vfZzyfz4Mg4Jw/nqx/llqtpmla+gbbLFPtuq73ej15n2uVTCzP8xzHySs5XtKBzefzdHtqulwYx/H9UaUOKYpSr9fr9bqmaemvG/xHlmV1Oh0ZF6rlEEsI4Xme67o7V2r7pOtO+/v7culVarGEEEEQ+L6/tdFZmfVqt9sSva20jGJFURSGIXxa1nvt7++n+8YgVqZwCu/Ywn4VGhnWbDYbjUb6a9kK5S7Fgkw5kn7xvWEYmqapqrpz1bYtFmTaZqTpuq7rejpttmXVtiQW59z3/dQnXPIdplpqWLPZLPqF+MWK5d9RkhcJgYekb2otaCKjELE45+kKLnySomKaptntdvOdyMhZrHQaE82TjLRarRy3OuYmVkkWW8AL06vb7eby4tYcxOKc27aNlKLUe/V6vRdG10vFchxnMpngYhDj5XsrNhdLCDEcDjF9QLgsHh8fb+zWhmKh/MGt/MXinJ+enmIqoSJuffz4cYOZiLUf/xJCwKrqkCTJcDjc4IEltq5Vg8EAVlUKzvl4PC5WLPRV1cTzvHX3xq0h1jafswNlYzwer1UQs4oVRZHjODi/lSWKorUmLLOKNRqN0FpVnLUeZskkFvZRgZTsVSuTWFX4ig6QsYvPGFosx58FEFpriIWeHTzE9/0sw0P2bHeFuAIPSZIky6vI2LMDAZxKsIEVq8SKoggzouBJMZ5dgFkl1uOvzgLgfki3uVg5vtUT0GvhNxSLc462HWxcDRniChQRWmzjrAMVZ/U36bFlQYc6CFbDOV8xU8oQV6CI0HpaLOxlAFm4vr5GYoH8WRFADHEFNiaKomVtFlsr3wDIGENILLAtsfCAF8jOMlvY43+HhyZA/omFOghyCS2GOgiKCK1FsTb4wjSAxHpGLCEEEgusy5NhxBBXoPDEwtQoyKvNQmKBQkILiQUKFiuKIkyNgrz6d7aiTAKQQ2JhogHk2L8jsUAhofU/sTA1CgoRCxMNIN9S+B9fledcwXKJ+QAAAABJRU5ErkJggg==" accept="image/*" />

											</div>

										</div>

										<div class="col-sm-12">
											<br>
											<a style="border-radius:0px;" class="btn btn-primary btn-block" onclick="personalui()">PERSONAL DETAILS</a>

										</div>

										<div class="col-sm-12">
											<br>
											<a style="border-radius:0px;" class="btn btn-primary btn-block" onclick="official()">OFFICIAL DETAILS</a>

										</div>

										<div class="col-sm-12">
											<br>
											<a style="border-radius:0px;" class="btn btn-primary btn-block" onclick="attachment()">ATTACHED DOCUMENTS</a>
										
										</div>

										<div class="col-sm-12">
											<br>
											<!-- <button style="border-radius:0px;" class="btn btn-success btn-block" id="submit" onclick="submit()">SUBMIT</button> -->

												<input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

												<?php if(isset($_GET['edit'])){ ?>

													<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

													<button style="border-radius:0px;" type="submit" name="update" value="update" id="update" class="btn btn-warning btn-block"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button>

												<?php } else { ?>

													<button style="border-radius:0px;" type="submit" name="register" id="register" class="btn btn-success btn-block"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

												<?php } ?>
											<br>
										</div>

									</div>

									<div class="col-sm-8" id="section1">

										<div class="row">

											<div class="col-sm-12" align="center" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;"><h3 style="margin:5px; padding: 0px 0px 10px 0px;" id="details">Personal Details</h3></div>            
											
											<div style="clear:both;"></div>

											<div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Gender<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="gender">

															<option value="Male" <?php if(isset($_GET['edit']) and $data['gender']=='Male') { echo 'selected'; } ?>>Male</option>

															<option value="Female" <?php if(isset($_GET['edit']) and $data['gender']=='Female') { echo 'selected'; } ?>>Female</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">DOB<em>*</em></label>

													<div class="col-sm-7">

														<input type="date" name="dob" id="dob" value="<?php if(isset($_GET['edit'])) { echo $data['dob']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Contact No<em>*</em></label>

													<div class="col-sm-7">

														<input type="number" name="contact_no" id="contactNo" value="<?php if(isset($_GET['edit'])) { echo $data['contact_no']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Email Id<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="email_id" id="emailId" value="<?php if(isset($_GET['edit'])) { echo $data['email_id']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Experience<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="experience" id="experience" value="<?php if(isset($_GET['edit'])) { echo $data['experience']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Qualification<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="qualification" id="qualification" value="<?php if(isset($_GET['edit'])) { echo $data['qualification']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Address<em>*</em></label>

													<div class="col-sm-7">

														<textarea type="text" name="address" id="address"  class="form-control form-control-sm boxSize"><?php if(isset($_GET['edit'])) { echo $data['address']; } ?></textarea>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Permenant_Add<em>*</em></label>

													<div class="col-sm-7">

														<textarea type="text" name="pAddress" id="pAddress" class="form-control form-control-sm boxSize"><?php if(isset($_GET['edit'])) { echo $data['pAddress']; } ?></textarea>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Language<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="language" id="language" value="<?php if(isset($_GET['edit'])) { echo $data['language']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Blood Group<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="blood_group" id="bloodGroup" value="<?php if(isset($_GET['edit'])) { echo $data['blood_group']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Emergency Name<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="emergency_name" id="emergencyName" value="<?php if(isset($_GET['edit'])) { echo $data['emergency_name']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Emergency Contact<em>*</em></label>

													<div class="col-sm-7">

														<input type="number" name="emergency_contact" id="emergencyContact" value="<?php if(isset($_GET['edit'])) { echo $data['emergency_contact']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Relation<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="relation" id="relation" value="<?php if(isset($_GET['edit'])) { echo $data['relation']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Machine Emp. Id.<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="machine_id" id="machineId" value="<?php if(isset($_GET['edit'])) { echo $data['machine_id']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

											</div>

											<div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Reference By<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="reference_by" id="referenceBy" value="<?php if(isset($_GET['edit'])) { echo $data['reference_by']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Category<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="category">

															<option value="SKILLED" <?php if(isset($_GET['edit']) and $data['category']=='SKILLED') { echo 'selected'; } ?>>SKILLED</option>

															<option value="UNSKILLED" <?php if(isset($_GET['edit']) and $data['category']=='UNSKILLED') { echo 'selected'; } ?>>UNSKILLED</option>

															<option value="SEMI SKILLED" <?php if(isset($_GET['edit']) and $data['category']=='SEMI SKILLED') { echo 'selected'; } ?>>SEMI SKILLED</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Department<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="department">

															<option value="SALES" <?php if(isset($_GET['edit']) and $data['department']=='SALES') { echo 'selected'; } ?>>SALES</option>

															<option value="MARKETING" <?php if(isset($_GET['edit']) and $data['department']=='MARKETING') { echo 'selected'; } ?>>MARKETING</option>

															<option value="ACCOUNTS" <?php if(isset($_GET['edit']) and $data['department']=='ACCOUNTS') { echo 'selected'; } ?>>ACCOUNTS</option>
															
															<option value="QA" <?php if(isset($_GET['edit']) and $data['department']=='QA') { echo 'selected'; } ?>>QA</option>

															<option value="WORKER" <?php if(isset($_GET['edit']) and $data['department']=='WORKER') { echo 'selected'; } ?>>WORKER</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Designation<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="designation">

															<option value="MANAGER" <?php if(isset($_GET['edit']) and $data['designation']=='MANAGER') { echo 'selected'; } ?>>MANAGER</option>

															<option value="ACCOUNTS ASSISTANT" <?php if(isset($_GET['edit']) and $data['designation']=='ACCOUNTS ASSISTANT') { echo 'selected'; } ?>>ACCOUNTS ASSISTANT</option>

															<option value="DRIVER" <?php if(isset($_GET['edit']) and $data['designation']=='DRIVER') { echo 'selected'; } ?>>DRIVER</option>
															
															<option value="OFFICE BOY" <?php if(isset($_GET['edit']) and $data['designation']=='OFFICE BOY') { echo 'selected'; } ?>>OFFICE BOY</option>

															<option value="FIELD OPERATOR" <?php if(isset($_GET['edit']) and $data['designation']=='FIELD OPERATOR') { echo 'selected'; } ?>>FIELD OPERATOR</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Salary Per<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="salary_per">

															<option value="MONTHLY" <?php if(isset($_GET['edit']) and $data['salary_per']=='MONTHLY') { echo 'selected'; } ?>>MONTHLY</option>

															<option value="DAILY" <?php if(isset($_GET['edit']) and $data['salary_per']=='DAILY') { echo 'selected'; } ?>>DAILY</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Payment<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="payment">

															<option value="CASH" <?php if(isset($_GET['edit']) and $data['payment']=='CASH') { echo 'selected'; } ?>>CASH</option>

															<option value="CHEQUE" <?php if(isset($_GET['edit']) and $data['payment']=='CHEQUE') { echo 'selected'; } ?>>CHEQUE</option>

															<option value="BANK TRASFER" <?php if(isset($_GET['edit']) and $data['payment']=='BANK TRASFER') { echo 'selected'; } ?>>BANK TRASFER</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Bank Name<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="bank_name" id="bankName" value="<?php if(isset($_GET['edit'])) { echo $data['bank_name']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Branch Name<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="branch_name" id="branchName" value="<?php if(isset($_GET['edit'])) { echo $data['branch_name']; } ?>" class="form-control form-control-sm boxSize"></textarea>
													
													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">IFSC Code<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="ifsc_code" id="ifscCode" value="<?php if(isset($_GET['edit'])) { echo $data['ifsc_code']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Account Number<em>*</em></label>

													<div class="col-sm-7">

														<input type="text" name="account_number" id="accountNumber" value="<?php if(isset($_GET['edit'])) { echo $data['account_number']; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Smoking<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="smoking">

															<option value="No" <?php if(isset($_GET['edit']) and $data['smoking']=='No') { echo 'selected'; } ?>>NO</option>

															<option value="Yes" <?php if(isset($_GET['edit']) and $data['smoking']=='Yes') { echo 'selected'; } ?>>YES</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Tobacco<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="tobacco">

															<option value="No" <?php if(isset($_GET['edit']) and $data['tobacco']=='No') { echo 'selected'; } ?>>NO</option>

															<option value="Yes" <?php if(isset($_GET['edit']) and $data['tobacco']=='Yes') { echo 'selected'; } ?>>YES</option>

														</select>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-5 col-form-label">Liquor<em>*</em></label>

													<div class="col-sm-7">

														<select class="form-control form-control-sm boxSize" name="liquor">

															<option value="No" <?php if(isset($_GET['edit']) and $data['liquor']=='No') { echo 'selected'; } ?>>NO</option>

															<option value="Yes" <?php if(isset($_GET['edit']) and $data['liquor']=='Yes') { echo 'selected'; } ?>>YES</option>

														</select>

													</div>

												</div>

											</div>

										</div>

									</div>

									<div class="col-sm-8" id="section2" style="display:none;">

										<div class="row">

											<div class="col-sm-12" align="center" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;"><h3 style="margin:5px; padding: 0px 0px 10px 0px;" id="details">Official Details</h3></div> 
											
											<div style="clear:both;"></div>
											
											<div class="col-sm-4" id="section22" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Basic<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="basic" id="basic" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['basic']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">DA<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="da" id="da" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['da']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">HRA<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="hra" id="hra" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['hra']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Travelling<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="travelling" id="travelling" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['travelling']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Medical<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="medical" id="medical" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['medical']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Conveyance<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="conveyance" id="conveyance" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['conveyance']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Education<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="education" id="education" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['education']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">CCA<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="cca" id="cca" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['cca']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Total Salary<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="total_salary" id="totalSalary" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['total_salary']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize" readonly>

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Mobile<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="mobile" id="mobile" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['mobile']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Uniform<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="uniform" id="uniform" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['uniform']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Internet<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="internet" id="internet" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['internet']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Employee ESIC %<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="employee_esic" id="employeeEsic" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['employee_esic']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-4 col-form-label">Employer ESIC %<em>*</em></label>

													<div class="col-sm-8">

														<input type="number" name="employer_esic" id="employerEsic" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['employer_esic']; } else { echo 0; } ?>" class="form-control form-control-sm boxSize">

													</div>

												</div>

											</div>

											<div class="col-sm-8" style="padding:0;">

												<div class="col-sm-12" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Training Date<em>*</em></label>

														<div class="col-sm-8">

															<input type="date" name="training_date" id="trainingDate" value="<?php if(isset($_GET['edit'])) { echo $data['training_date']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Joining Date<em>*</em></label>

														<div class="col-sm-8">

															<input type="date" name="joining_date" id="joiningDate" value="<?php if(isset($_GET['edit'])) { echo $data['joining_date']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Probation Period<em>*</em></label>

														<div class="col-sm-8">

															<input type="text" name="probation_period" id="probationPeriod" value="<?php if(isset($_GET['edit'])) { echo $data['probation_period']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Permanent Date<em>*</em></label>

														<div class="col-sm-8">

															<input type="date" name="permanent_date" id="permanentDate" value="<?php if(isset($_GET['edit'])) { echo $data['permanent_date']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Leaving Date<em>*</em></label>

														<div class="col-sm-8">

															<input type="date" name="leaving_date" id="leavingDate" value="<?php if(isset($_GET['edit'])) { echo $data['leaving_date']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Additional Charges<em>*</em></label>

														<div class="col-sm-8">

															<input type="number" name="additional_charges" id="additionalCharges" value="<?php if(isset($_GET['edit'])) { echo $data['additional_charges']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

													<div class="form-group row">

														<label for="finish_weight_net" class="col-sm-4 col-form-label">Additional Deduction<em>*</em></label>

														<div class="col-sm-8">

															<input type="number" name="additional_deduction" id="additionalDeduction" value="<?php if(isset($_GET['edit'])) { echo $data['additional_deduction']; } ?>" class="form-control form-control-sm boxSize">

														</div>

													</div>

												</div>

												<div class="row" style="padding:0; margin-right: 0px; margin-left: 0px;">

													<div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">PF. Wages<em>*</em></label>

															<div class="col-sm-6">

																<input type="number" name="pf_wages" id="pfWages" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['pf_wages']; }else { echo 0; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Employee Pf %<em>*</em></label>

															<div class="col-sm-6">

																<input type="number" name="employee_pf" id="employeePf" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['employee_pf']; }else { echo 0; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Employer Pf %<em>*</em></label>

															<div class="col-sm-6">

																<input type="number" name="employer_pf" id="employerPf" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['employer_pf']; }else { echo 0; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Bonus<em>*</em></label>

															<div class="col-sm-6">

																<input type="number" name="bonus" id="bonus" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['bonus']; }else { echo 0; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Total_Ctc(Yearly)<em>*</em></label>

															<div class="col-sm-6">

																<input type="number" name="ctc" id="ctc" onkeyup="officialchange()" value="<?php if(isset($_GET['edit'])) { echo $data['ctc']; }else { echo 0; } ?>" class="form-control form-control-sm boxSize" readonly>

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Working Hours<em>*</em></label>

															<div class="col-sm-6">

																<input type="number" name="working_hours" id="workingHours" value="<?php if(isset($_GET['edit'])) { echo $data['working_hours']; }else { echo 9; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Professional_Tax<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="professional_tax">

																	<option value="NO" <?php if(isset($_GET['edit']) and $data['professional_tax']=='NO') { echo 'selected'; } ?>>NO</option>

																	<option value="YES" <?php if(isset($_GET['edit']) and $data['professional_tax']=='YES') { echo 'selected'; } ?>>YES</option>

																</select>

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Over_Time<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="over_time">

																	<option value="NO" <?php if(isset($_GET['edit']) and $data['over_time']=='NO') { echo 'selected'; } ?>>NO</option>

																	<option value="YES" <?php if(isset($_GET['edit']) and $data['over_time']=='YES') { echo 'selected'; } ?>>YES</option>

																</select>

															</div>

														</div>

													</div>

													<div class="col-sm-6" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Sunday_Payment<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="sunday_payment">

																	<option value="NO" <?php if(isset($_GET['edit']) and $data['sunday_payment']=='NO') { echo 'selected'; } ?>>NO</option>

																	<option value="YES" <?php if(isset($_GET['edit']) and $data['sunday_payment']=='YES') { echo 'selected'; } ?>>YES</option>

																</select>

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Dress_Allowed<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="dress_allowed">

																	<option value="NO" <?php if(isset($_GET['edit']) and $data['dress_allowed']=='NO') { echo 'selected'; } ?>>NO</option>

																	<option value="YES" <?php if(isset($_GET['edit']) and $data['dress_allowed']=='YES') { echo 'selected'; } ?>>YES</option>

																</select>

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Shoes_Allowed<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="shoes_allowed">

																	<option value="NO" <?php if(isset($_GET['edit']) and $data['shoes_allowed']=='NO') { echo 'selected'; } ?>>NO</option>

																	<option value="YES" <?php if(isset($_GET['edit']) and $data['shoes_allowed']=='YES') { echo 'selected'; } ?>>YES</option>

																</select>

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Lockers_Allowed<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="lockers_allowed">

																	<option value="NO" <?php if(isset($_GET['edit']) and $data['lockers_allowed']=='NO') { echo 'selected'; } ?>>NO</option>

																	<option value="YES" <?php if(isset($_GET['edit']) and $data['lockers_allowed']=='YES') { echo 'selected'; } ?>>YES</option>

																</select>

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">ESIC No.<em>*</em></label>

															<div class="col-sm-6">

																<input type="text" name="esic_no" id="esicNo" value="<?php if(isset($_GET['edit'])) { echo $data['esic_no']; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">PF No.<em>*</em></label>

															<div class="col-sm-6">

																<input type="text" name="pf_no" id="pf_no" value="<?php if(isset($_GET['edit'])) { echo $data['esic_no']; } ?>" class="form-control form-control-sm boxSize">

															</div>

														</div>


														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">IT-Deduction<em>*</em></label>

															<div class="col-sm-6">

																<input type="text" name="it_deduction" id="itDeduction" value="<?php if(isset($_GET['edit'])) { echo $data['it_deduction']; }?>" class="form-control form-control-sm boxSize">

															</div>

														</div>

														<div class="form-group row">

															<label for="finish_weight_net" class="col-sm-6 col-form-label">Weekly Off<em>*</em></label>

															<div class="col-sm-6">

																<select class="form-control form-control-sm boxSize" name="weekly_Off">

																	<option value="SUNDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='SUNDAY') { echo 'selected'; } ?>>SUNDAY</option>

																	<option value="MONDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='MONDAY') { echo 'selected'; } ?>>MONDAY</option>

																	<option value="TUESDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='TUESDAY') { echo 'selected'; } ?>>TUESDAY</option>

																	<option value="WEDNESDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='WEDNESDAY') { echo 'selected'; } ?>>WEDNESDAY</option>

																	<option value="THURSDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='THURSDAY') { echo 'selected'; } ?>>THURSDAY</option>

																	<option value="FRIDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='FRIDAY') { echo 'selected'; } ?>>FRIDAY</option>
																	
																	<option value="SATURDAY" <?php if(isset($_GET['edit']) and $data['weekly_Off']=='SATURDAY') { echo 'selected'; } ?>>SATURDAY</option>

																</select>

															</div>

														</div>

													</div>

												</div>

											</div>

										</div>

									</div>

									<div class="col-sm-8" id="section3" style="display:none;">

										<div class="row">

											<div class="col-sm-12" align="center" style="border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;"><h3 style="margin:5px;" id="details">Attachment Documents</h3></div>            
											
											<div style="clear:both;"></div>

											<div class="col-sm-12" style="padding-bottom:10px;border-radius:4px;border:1px solid #CCC;background:#FFF;padding-top:10px;-webkit-box-shadow: 2px 2px 3px 3px #ccc; -moz-box-shadow:2px 2px 3px 3px #ccc;box-shadow:2px 2px 3px 3px #ccc;">
												
												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-2 col-form-label">Aadhar_Card_No<em>*</em></label>

													<div class="col-sm-3">

														<input type="text" name="aadhar_no" id="aadhar_no" value="<?php if(isset($_GET['edit'])) { echo $data['aadhar_no']; } ?>" class="form-control form-control-sm boxSize">

													</div>

													<div class="col-sm-4">

														<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['aadhar_photo'])){ echo "required"; } }else{ echo "required"; }?> name="aadhar_photo" id="0" data-image-index="0" />

													</div>

													<div class="col-sm-1">

														<?php
															
															if(!empty($data['aadhar_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['aadhar_photo'];?>" class="btn btn-info btn-sm">View</a>
														<?php
															}
														?>

													</div>

													<div class="col-sm-2">

														<?php
															
															if(!empty($data['aadhar_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['aadhar_photo'];?>" class="btn btn-info btn-sm" download>Download</a>
														<?php
															}
														?>

													</div>

												</div>

												<div style="clear:both;"></div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-2 col-form-label">Pan_Card_No<em>*</em></label>

													<div class="col-sm-3">

														<input type="text" name="pan_no" id="pan_no" value="<?php if(isset($_GET['edit'])) { echo $data['pan_no']; } ?>" class="form-control form-control-sm boxSize">

													</div>

													<div class="col-sm-4">

														<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['pan_photo'])){ echo "required"; } }else{ echo "required"; }?> name="pan_photo" id="0" data-image-index="0" />

													</div>

													<div class="col-sm-1">

														<?php
															
															if(!empty($data['pan_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['pan_photo'];?>" class="btn btn-info btn-sm">View</a>
														<?php
															}
														?>

													</div>

													<div class="col-sm-2">

														<?php
															
															if(!empty($data['pan_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['pan_photo'];?>" class="btn btn-info btn-sm" download>Download</a>
														<?php
															}
														?>

													</div>

												</div>

												<div style="clear:both;"></div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-2 col-form-label">Driving_Licence<em>*</em></label>

													<div class="col-sm-3">

														<input type="text" name="driving_licence" id="drivingLicence" value="<?php if(isset($_GET['edit'])) { echo $data['driving_licence']; } ?>" class="form-control form-control-sm boxSize">

													</div>

													<div class="col-sm-4">

														<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['driving_licence_photo'])){ echo "required"; } }?> name="driving_licence_photo" id="0" data-image-index="0" />

													</div>

													<div class="col-sm-1">

														<?php
															
															if(!empty($data['driving_licence_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['driving_licence_photo'];?>" class="btn btn-info btn-sm">View</a>
														<?php
															}
														?>

													</div>

													<div class="col-sm-2">

														<?php
															
															if(!empty($data['driving_licence_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['driving_licence_photo'];?>" class="btn btn-info btn-sm" download>Download</a>
														<?php
															}
														?>

													</div>

												</div>

												<div style="clear:both;"></div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-2 col-form-label">Passport_No<em>*</em></label>

													<div class="col-sm-3">

														<input type="text" name="passport_no" id="passportno" value="<?php if(isset($_GET['edit'])) { echo $data['passport_no']; } ?>" class="form-control form-control-sm boxSize">

													</div>

													<div class="col-sm-4">

														<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['passport_photo'])){ echo "required"; } }?> name="passport_photo" id="0" data-image-index="0" />

													</div>

													
													<div class="col-sm-1">

														<?php
															
															if(!empty($data['passport_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['passport_photo'];?>" class="btn btn-info btn-sm">View</a>
														<?php
															}
														?>

													</div>

													<div class="col-sm-2">

														<?php
															
															if(!empty($data['passport_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['passport_photo'];?>" class="btn btn-info btn-sm" download>Download</a>
														<?php
															}
														?>

													</div>

												</div>

												<div style="clear:both;"></div>

												<div class="form-group row">

													<label for="finish_weight_net" class="col-sm-2 col-form-label">Other_document<em>*</em></label>

													<div class="col-sm-3">

														<input type="text" name="Other_document" id="OtherDocument" value="<?php if(isset($_GET['edit'])) { echo $data['Other_document']; } ?>" class="form-control form-control-sm boxSize">

													</div>

													<div class="col-sm-4">

														<input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['Other_document_photo'])){ echo "required"; } }?> name="Other_document_photo" id="0" data-image-index="0" />


													</div>

													<div class="col-sm-1">

														<?php
															
															if(!empty($data['Other_document_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['Other_document_photo'];?>" class="btn btn-info btn-sm">View</a>
														<?php
															}
														?>

													</div>

													<div class="col-sm-2">

														<?php
															
															if(!empty($data['Other_document_photo'])){ 

														?>
															<a target="_blank_" href="<?php echo $data['Other_document_photo'];?>" class="btn btn-info btn-sm" download>Download</a>
														<?php
															}
														?>

													</div>

												</div>
												<div style="clear:both;"></div>


											</div>
											

										</div>

									</div>


								</div>

							</form>

						</div>

					</div>

				<?php } else { ?>

				<div class="row" style="margin-top:10px;">

					<div class="col-md-12">

						<table class="table table-striped custom-table mb-0 datatable datatable-selectable-data">
							<thead>
								<tr>
									<th width="20px">#</th>
									<th>Name</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th>CTC</th>
									<th>Hours</th>
									<th width="100px">Active</th>
									<th width="10px">Actions</th>
								</tr>
							</thead>
							<tbody>

								<?php

								$x = 1;

								while($row = $admin->fetch($results)){ ?>

									<tr>

										<td><?php echo $x++ ?></td>

                                        <td> <?php echo $row['emp_name']; ?> </td>
                                        <td> <?php echo $row['designation']; ?> </td>
										<td> <?php echo $row['department']; ?> </td>
										<td> <?php echo $row['ctc']; ?> </td>
										<td> <?php echo $row['working_hours']; ?> </td>
										<td>
											<div class="badge badge-<?php echo $row['active'] == '1'?'success':'danger'; ?> ml-2"><?php echo $row['active'] == '1'?'Yes':'No'; ?></div>
										</td>
										<td class="text-right">

											<div class="dropdown dropdown-action">

												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>

												<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(103px, 32px, 0px);">

													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?edit&id=<?php echo $row['id']; ?>" title="Edit">   <i class="fa fa-pencil"></i> Edit</a>

													<?php if($row['active'] == '0') { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?activate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to activate?');" title="Activate"> <i class="fa fa-thumbs-up"></i> Activate </a>

													<?php } else { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?deactivate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to deactivate?');" title="Deactivate"> <i class="fa fa-thumbs-down"></i> Deactivate </a>

													<?php } ?>

											
													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?delId=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to delete?');" title="Delete"> <i class="fa fa-trash-o"></i> Delete </a>

												</div>

											</div>

										</td>

									</tr>

								<?php } } ?>

							</tbody>

						</table>

					</div>

				</div>

			</div>

			<!-- /Page Content -->

		</div>

		<!-- /Page Wrapper -->

	</div>

	<!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					man_name: {
						required: true,
					},
                    designation: {
						required: true,
					},

                    department: {
						required: true,
					},

					rate: {
						required: true,
						number:true,
					},

					hours: {
						required: true,
						number:true,

					},

					skill_matrix: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function(){

				if($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})
		

		});

		<?php if(isset($_GET['edit'])){ ?>
			$('#img').attr('src', '<?php echo $data['emp_photo'];?>');
                   
		<?php  }?>

		function photoemploayeeupload(e){

			let photoSize = e.files[0].size;

			if (photoSize > 5000000) {

				alert('Photo Size Not Greater Than 5 mb ');

				$(e).val("");

			} else {

				if (e.files && e.files[0]) {

					var reader = new FileReader();

					reader.onload = function (e) {

						$('#img').attr('src', e.target.result);

					}

					reader.readAsDataURL(e.files[0]);

				}


			}

		}

		function photoupload(e){

			let photoSize = e.files[0].size;

			if (photoSize > 5000000) {

				alert('Photo Size Not Greater Than 5 mb ');

				$(e).val("");

			} 

		}


		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));
				
			}
			
		}


		function personalui() {

			$('#section1').fadeIn(500);

			$('#section2').fadeOut(500);

			$('#section3').fadeOut(500);

		}

		function official() {

			$('#section1').fadeOut(500);

			$('#section2').fadeIn(500);

			$('#section3').fadeOut(500);

		}

		function attachment() {

			$('#section1').fadeOut(500);

			$('#section2').fadeOut(500);

			$('#section3').fadeIn(500);

		}

		function officialchange() {
			var inp1  = parseFloat($('#basic').val());
			var inp2  = parseFloat($('#da').val());
			var inp3  = parseFloat($('#hra').val());
			var inp4  = parseFloat($('#travelling').val());
			var inp5  = parseFloat($('#medical').val());
			var inp6  = parseFloat($('#conveyance').val());
			var inp7  = parseFloat($('#education').val());
			var inp8  = parseFloat($('#cca').val());
			var inp9  = parseFloat($('#totalSalary').val());
			var inp10 = parseFloat($('#mobile').val());
			var inp11 = parseFloat($('#uniform').val());
			var inp12 = parseFloat($('#internet').val());
			var inp13 = parseFloat($('#employeeEsic').val());
			var inp14 = parseFloat($('#employerEsic').val());
			var inp15 = parseFloat($('#pfWages').val());
			var inp16 = parseFloat($('#employeePf').val());
			var inp17 = parseFloat($('#employerPf').val());
			var inp18 = parseFloat($('#bonus').val());
			var inp19 = parseFloat($('#ctc').val());

			var a = (inp1 + inp2 + inp3 + inp4 + inp5 + inp6 + inp7 + inp8);
			var b = (inp10 + inp11 + inp12);
			var c = ((a + b) * inp14) / 100;
			var d = ((inp1 + inp6) * inp17) / 100;
			var ctc = ((a + b + c + d) * 12) + inp18;

			$('#totalSalary').val(a);
			$('#ctc').val(ctc);

		}
	</script>

</body>

</html>