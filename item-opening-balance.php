<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName      = "Opening Balance";
$parentPageURL = 'opening-balance.php';
$navenq1       = 'background:#27ae60;';
$tableName     = 'item_opening_balance';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);



$itemName = $admin->getAllItemFinishGood();

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addOpeingBalance($_POST,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
        header("location:".$parentPageURL."?registersuccess");
        exit();

	}
}



if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueItemOpeingBalanceId($id);
    $detailsData = $admin->getUniqueItemComponentOpeingBalanceId($id);
    
}


if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateOpeingBalance($_POST,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$parentPageURL."?updatesuccess");
		exit();
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.boxSize{
            border-bottom: 1px solid blue;
            height:25px!important;
			width:200px!important;

        }
        label{
            font-size:11px;
        }
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

		

		.tableFixHead          { overflow-y: auto; height: 500px; }
		.tableFixHead thead th { position: sticky; top: 0; }

		/* Just common table stuff. Really. */
		table  { border-collapse: collapse; width: 100%; }
		th, td { padding: 8px 16px; }
		th     { background:#2980b9; color:#fff; }
	</style>

</head>

<body>



	<div class='loading_wrapper' style="display: none;">

		<div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">

		<!-- Header -->

		<?php include("include/header.php"); ?>

		<!-- /Header -->



		<!-- Sidebar -->

		<?php include("include/sidebar.php"); ?>

		<!-- /Sidebar -->



		<!-- Page Wrapper -->

		<div class="page-wrapper">



			<!-- Page Content -->

			<div class="content container-fluid">



				<!-- Page Header -->

				<div class="page-header">

					<div class="row align-items-center">

						<div class="col">

							<h3 class="page-title"><?php echo $pageName; ?></h3>

							<ul class="breadcrumb">

								<li class="breadcrumb-item">Master</li>


								<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

									<li class="breadcrumb-item"><?php echo $pageName; ?></li>

									<li class="breadcrumb-item active">

										<?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

									</li>

								<?php } else { ?>

									<li class="breadcrumb-item active"><?php echo $pageName; ?></li>

								<?php } ?>

							</ul>

						</div>


					</div>

				</div>

				<!-- /Page Header -->



				<?php if(isset($_GET['registersuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['registerfail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatefail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

					</div>

				<?php } ?>



				<?php if(isset($_GET['deletesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

					</div><br/>

				<?php } ?>


                <?php include("include/stock-tracking.php"); ?>


                <br>
                
				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<h4 class="card-title mb-0"><?php if(isset($_GET['edit'])) {

										echo 'Edit '.$pageName;

									} else {

										echo 'Add New '.$pageName;

									}

									?></h4>

								</div>

								
								<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

                                    <div class="card-body" style="padding-top: 10px;padding-bottom: 0px;">

                                        <div class="row ">

                                            <div class="col-sm-5">

                                                <div class="form-group row">

                                                    <label for="product_name" class="col-sm-4 col-form-label">Item Name<em>*</em></label>

                                                    <div class="col-sm-8">

														<select class="form-control form-control-sm select2" name="finish_good_item_name" id="finishGoodItemName" onchange="componentName(this)">

															<option value="">Select Item Name</option>

															<?php while ($row = $admin->fetch($itemName)) { ?>

																<option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) && $data['finish_good_item_name'] == $row['id']) { echo 'selected'; } ?>><?php echo $row['item_name']; ?></option>

															<?php } ?>

														</select>

                                                    </div>

                                                </div>
                                            
                                            </div>


                                            <div class="col-sm-5">

                                                <div class="form-group row">

                                                    <label for="opening_balance_date"  class="col-sm-6 col-form-label">Opening Balance Date<em>*</em></label>

                                                    <div class="col-sm-6">

                                                        <input type="date" name="opening_balance_date" id="openingBalanceDate" value="<?php if(isset($_GET['edit'])) { echo $data['opening_balance_date']; } ?>" class="form-control form-control-sm">

                                                    </div>

                                                </div>
                                            
                                            </div>

											 <div class="col-sm-2">
												
											 <?php if(isset($_GET['add'])){ ?>

                                                <button type="button" id="add-more" class="btn btn-warning add-more"><i
                                                    class="fa fa-plus"></i> Add More</button>

											 <?php } ?>

                                            
                                            </div>
                                        
                                        </div>

                                    </div>


                                    <div class="card-body">

                                        <div class="table-responsive tableFixHead">

                                            <table class="table table-bordered" id="components">

                                                <thead>

                                                    <tr>

                                                        <th>#</th>
                                                        <th>Component Name</th>
                                                        <th>Opeing Balance</th>

                                                    </tr>

                                                </thead>

											
                                                <?php if(isset($_GET['edit'])) { $i = 0; $y = 1;

                                                     while($row = $admin->fetch($detailsData)) {

												?>

                                                    <tbody>
                                                        
                                                        <tr>
                                                        
                                                            <td>

                                                                <?php echo $y;?>

                                                                <input type="hidden" value="<?php echo $row['id']; ?>" name="component_opening_balance_id[<?php echo $i;?>]" class="form-control form-control-sm boxSize">

                                                            </td>

                                                            <td>

                                                                <input type="text" name="component_name[<?php echo $i;?>]" value="<?php echo $row['component_name']; ?>" class="form-control form-control-sm component_name boxSize" required readonly>

                                                            </td>

                                                            <td>

                                                                <input type="number" name="opening_balance[<?php echo $i;?>]" value="<?php echo $row['opening_balance']; ?>" class="form-control form-control-sm opening_balance boxSize" required readonly>

                                                            </td>
                                                        
                                                        </tr>
                                                        
                                                    </tbody>

                                                   

                                                <?php $y++; $i++; } }  else { ?>

                                                    <tbody>

														<tr>

															<td>
																1
															</td>

															<td>

																
																<select class="form-control form-control-sm select2 component_id"
																	name="component_id[0]" required>

																</select>

															</td>

															<td>

																<input type="number" name="opening_balance[0]"
																	class="form-control form-control-sm opening_balance boxSize" required>

															</td>

														</tr>

                                                    </tbody>


                                                <?php } ?>


                                            </table>

                                        </div>

                                        <div class="form-actions text-right">

                                            <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

                                            <?php if(isset($_GET['edit'])){ ?>

                                                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

                                                <!-- <button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button> -->

                                            <?php } else { ?>

                                                <button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

                                            <?php } ?>

                                        </div>

                                    </div>

								</form>

                            </div>

                        </div>

                    </div>

                <?php } ?>


			</div>

			<!-- /Page Content -->

		</div>

		<!-- /Page Wrapper -->

	</div>

	<!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					finish_good_item_name: {
						required: true,
					},
                    opening_balance_date: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function () {

			$('input[name="image"]').change(function () {

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function () {

				if ($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

			$('.select2').select2();




		});


		$(".add-more").on("click", function(){
                
			var count = $('#components > tbody > tr').length;
			
			$.ajax({
				type: 'POST',
				data: 'count='+count,
				url: 'getAjaxOpeningBalancetable.php',
				success: function (services_clone) {

					$("#components tr:last").after(services_clone);

				}
			});

		});


		function componentName(e) {

			let itemId = $(e).val();

			if (itemId != '') {

				$.ajax({

					type: 'POST',
					data: 'itemId=' + itemId,
					url: 'getAjaxComponentName.php',
					success: function (services_clone) {

						$(".component_id").html(services_clone);

					}

				})

			} else {

				$("#components > tbody").html(' ');

			}

		}

				
	</script>

</body>

</html>