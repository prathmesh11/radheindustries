<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();

if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName  = "Material In (Purchase)";
$pageURL   = 'material-in.php';
$deleteURL = 'material-in.php';
$editURL   = 'material-in.php';
$navenq2   = 'background:#27ae60;';
$tableName = 'material_in';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);



$max = $admin->getmaterialMaxId($loggedInUserDetailsArr['branch_id']);


if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addMaterialIn($_POST,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
        header("location:".$pageURL."?registersuccess");
        exit();

	}
}


if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueMaterialInId($id);
    $detailsData = $admin->getUniqueMaterialInComponentId($id);
    
}


if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updateMaterialIn($_POST,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$pageURL."?updatesuccess");
		exit();
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.boxSize{
            border-bottom: 1px solid blue;
            height:25px!important;
			width:200px!important;

        }
        
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

		

		.tableFixHead          { overflow-y: auto; height: 500px; }
		.tableFixHead thead th { position: sticky; top: 0;  z-index: 999;}

		/* Just common table stuff. Really. */
		table  { border-collapse: collapse; width: 100%; }
		th, td { padding: 8px 16px; }
		th     { background:#2980b9; color:#fff; }
	</style>

</head>

<body>



	<div class='loading_wrapper' style="display: none;">

		<div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">

		<!-- Header -->

		<?php include("include/header.php"); ?>

		<!-- /Header -->



		<!-- Sidebar -->

		<?php include("include/sidebar.php"); ?>

		<!-- /Sidebar -->



		<!-- Page Wrapper -->

		<div class="page-wrapper">



			<!-- Page Content -->

			<div class="content container-fluid">



				<!-- Page Header -->

				<div class="page-header">

					<div class="row align-items-center">

						<div class="col">

							<h3 class="page-title"><?php echo $pageName; ?></h3>

							<ul class="breadcrumb">

								<li class="breadcrumb-item">Master</li>


								<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

									<li class="breadcrumb-item"><?php echo $pageName; ?></li>

									<li class="breadcrumb-item active">

										<?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

									</li>

								<?php } else { ?>

									<li class="breadcrumb-item active"><?php echo $pageName; ?></li>

								<?php } ?>

							</ul>

						</div>


					</div>

				</div>

				<!-- /Page Header -->



				<?php if(isset($_GET['registersuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['registerfail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatefail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

					</div>

				<?php } ?>



				<?php if(isset($_GET['deletesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

					</div><br/>

				<?php } ?>


                <?php include("include/stock-tracking.php"); ?>


                <br>
                
				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<h4 class="card-title mb-0"><?php if(isset($_GET['edit'])) {

										echo 'Edit '.$pageName;

									} else {

										echo 'Add New '.$pageName;

									}

									?></h4>

								</div>

								
								<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

                                    <div class="card-body" style="padding-top: 10px;padding-bottom: 0px;">

                                        <div class="row form-group">

                                            <div class="col-sm-1">

                                                <label for="sr No">Sr No<em>*</em></label>

                                                <input type="number" name="sr_no" id="srno" value="<?php if(isset($_GET['edit'])) { echo $data['sr_no']; } else { echo $max; } ?>" class="form-control form-control-sm" readonly>

                                            </div>

                                            <div class="col-sm-2">

                                                <label for="entry_date">Date<em>*</em></label>

                                                <input type="date" name="entry_date" id="entryDate" value="<?php if(isset($_GET['edit'])) { echo $data['entry_date']; } ?>" class="form-control form-control-sm">

                                            </div>


                                            <div class="col-sm-3">

                                                <label for="product_name">Supplier Name<em>*</em></label>

                                                <input type="text" name="supplier_name" id="supplierName" value="<?php if(isset($_GET['edit'])) { echo $data['supplier_name']; } ?>" class="form-control form-control-sm">

                                            </div>

                                             <div class="col-sm-2">

                                                <label for="document no">Document No<em>*</em></label>

                                                <input type="text" name="document_no" id="documentNo" value="<?php if(isset($_GET['edit'])) { echo $data['document_no']; } ?>" class="form-control form-control-sm">

                                            </div>


                                            <div class="col-sm-2">

                                                <label for="document date">Document Date<em>*</em></label>

                                                <input type="date" name="document_date" id="document_date" value="<?php if(isset($_GET['edit'])) { echo $data['document_date']; } ?>" class="form-control form-control-sm">

                                            </div>

                                            <div class="col-sm-2">
                                            	<?php if(isset($_GET['add'])){ ?>

                                                <br>
                                                <button type="button" id="add-more" class="btn btn-warning add-more"><i
                                                    class="fa fa-plus"></i> Add More</button>

												<?php } ?>


                                            </div>
                                        
                                        </div>

                                    </div>


                                    <div class="card-body">

                                        <div class="table-responsive tableFixHead">

                                            <table class="table table-bordered" id="components">

                                                <thead>

                                                    <tr>

                                                        <th>#</th>
                                                        <th>Finish Good</th>
                                                        <th>Component Name</th>
                                                        <th>Material In Qty</th>

                                                    </tr>

                                                </thead>

											
                                                <?php if(isset($_GET['edit'])) { $i = 0;

                                                     while($row = $admin->fetch($detailsData)) {
                                                        


												?>

                                                    <tbody>
                                                        
                                                        <tr>
                                                        
                                                            <td>

                                                                <?php echo $i+1;?>

                                                                <input type="hidden" value="<?php echo $row['id']; ?>" name="material_in_id[<?php echo $i;?>]" class="form-control material_in_id form-control-sm boxSize">

                                                            </td>

                                                            <td>

                                                                <select class="form-control form-control-sm select2 item_name" name="item_name[<?php echo $i;?>]" onchange="componentName(this)">

                                                                    <option value="">Select Item Name</option>

                                                                    <?php $itemName = $admin->getAllItemFinishGood(); while ($rows = $admin->fetch($itemName)) { ?>

                                                                        <option value="<?php echo $row['item_name']; ?>"
                                                                            <?php if(isset($_GET['edit']) && $rows['id'] == $row['item_name']) { echo 'selected'; } ?>>
                                                                                <?php echo $rows['item_name']; ?></option>

                                                                     <?php } ?>

                                                                </select>

                                                            </td>

                                                            <td>

                                                                <select class="form-control form-control-sm select2 component_name" name="component_name[<?php echo $i;?>]">

                                                                    <option value="">Select Item Name</option>


                                                                </select>

                                                            </td>

                                                            <td>

                                                                <input type="number" name="material_in_qty[<?php echo $i;?>]" class="form-control form-control-sm material_in_qty boxSize" value="<?php echo $row['material_in_qty'];?>" required>

                                                            </td>
                                                        
                                                        </tr>
                                                        
                                                    </tbody>

                                                   

                                                <?php $i++; } }  else { ?>

                                                    <tbody>

                                                    </tbody>


                                                <?php } ?>


                                            </table>

                                        </div>

                                        <div class="form-actions text-right">

                                            <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

                                            <?php if(isset($_GET['edit'])){ ?>

                                                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

                                                <!-- <button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button> -->

                                            <?php } else { ?>

                                                <button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

                                            <?php } ?>

                                        </div>

                                    </div>

								</form>

                            </div>

                        </div>

                    </div>

                <?php } ?>


			</div>

			<!-- /Page Content -->

		</div>

		<!-- /Page Wrapper -->

	</div>

	<!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					product_name: {
						required: true,
					},
                    sub_sr: {
						required: true,
					},
                    component_name: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function () {

			$('input[name="image"]').change(function () {

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function () {

				if ($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

			$('.select2').select2();
            
            $(".add-more").on("click", function(){
                
				var count = $('#components > tbody > tr').length;

				console.log(count);
                
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxMaterialIntable.php',
                    success: function (services_clone) {

                        $("#components tbody ").append(services_clone);

                    }
                });

			});


             <?php if(isset($_GET['edit'])){ ?>
                $('#components > tbody > tr').each(function () {
                    componentName($(this).find('.item_name'));

			})

            <?php } ?>

		});



		function componentName(e) {

		    let itemId = $(e).val();

		    if (itemId != '') {

                <?php if(isset($_GET['edit'])){ ?>
                    var editId= $(e).parent().parent().find('.material_in_id').val();
                    <?php  }else{ ?>
                       var editId='';
                <?php } ?>

		        $.ajax({

		            type: 'POST',
		            data: 'itemId=' + itemId+'&editId='+editId,
		            url: 'getAjaxMaterialInComponentName.php',

		            success: function (services_clone) {

		                $(e).parent().parent().find('.component_name').html(services_clone);

		            }

		        })

		    } else {

				$(e).parent().parent().find('.component_name').html('');

		    }

		}


				
	</script>

</body>

</html>