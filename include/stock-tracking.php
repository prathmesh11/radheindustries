<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}




include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);
$tableName     = 'order_booking';

// $orderBooking  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time=0"))['x'];

// $purchaseInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time=0"))['x'];

// $productInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time=0"))['x'];

// $dispachInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time<>0 AND dispach_informaton_confirm_time=0"))['x'];

// $acccountInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time<>0 AND dispach_informaton_confirm_time<>0 AND acccount_informaton_confirm_time=0"))['x'];

// $dsaAnalysing  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time<>0 AND dispach_informaton_confirm_time<>0 AND acccount_informaton_confirm_time<>0 AND dsa_analysing_confirm_time=0"))['x'];



?>

<style>

.badge-pill {
        background-color: #f39c12;
}

</style>

<div id="div-content" class="content ">

        <ul class="list-group list-group-horizontal">

                <li class="list-group-item d-flex mx-auto justify-content-center"
                        style="border-radius:50px; background-color:#130f40; width:25%; <?php echo $navenq1;?>">
                        <a href="opening-balance.php" style="color:#fff;" >Opening Balance</a>
                </li>

                <li class="list-group-item d-flex mx-auto justify-content-center"
                        style="border-radius:50px; background-color:#130f40; width:25%; <?php echo $navenq2;?>">
                        <a href="material-in.php" style="color:#fff; ">Material In (Purchase)  </a>
                </li>

                <li class="list-group-item d-flex mx-auto justify-content-center"
                        style="border-radius:50px; background-color:#130f40; width:25%; <?php echo $navenq3;?>">
                        <a href="stock-adjust.php" style="color:#fff; "> Stock Adjustment  </a>
                </li>

                <li class="list-group-item d-flex mx-auto justify-content-center"
                        style="border-radius:50px; background-color:#130f40; width:25%; <?php echo $navenq4;?>">
                        <a href="stock-report.php" style="color:#fff; ">Stock Report  </a>
                </li>

               

        </ul>

</div>
                