<?php
	include('database.php');
	include('SaveImage.class.php');
	include('Email.class.php');


	class AdminFunctions extends Database {
		private $userType = 'admin';
		private $allowTags = "<strong><b><p><u><ul><li><ol><s><sub><sup><h1><img><h2><h3><h4><h5><h6><div><i><span><br><table><tr><th><td><thead><tbody><a>";

		function getAdminEmail() {
			$query = $this->query("SELECT * FROM ".PREFIX."admin");
			return $this->fetch($query)['username'];
		}

		/* Check login session */
		function loginSession($userId, $userName, $userType,$role) {
			$_SESSION[SITE_NAME][$this->userType."UserId"] = $userId;
			$_SESSION[SITE_NAME][$this->userType."UserName"] = $userName;
			$_SESSION[SITE_NAME][$this->userType."UserType"] = $this->userType;
			$_SESSION[SITE_NAME][$this->userType."role"] = $role;
		}

		/* Logout Function session */
		function logoutSession() {
			if(isset($_SESSION[SITE_NAME])){
				if(isset($_SESSION[SITE_NAME][$this->userType."UserId"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserId"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserName"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserName"]);
				}
				if(isset($_SESSION[SITE_NAME][$this->userType."UserType"])){
					unset($_SESSION[SITE_NAME][$this->userType."UserType"]);
				}
				return true;
			} else {
				return false;
			}
		}

		/* Admin login function */
		function adminLogin($data, $successURL, $failURL = "index.php?failed") {
			$email = $this->escape_string($this->strip_all($data['email']));
			$password = $this->escape_string($this->strip_all($data['password']));
			$query = "select * from ".PREFIX."admin where email='".$email."'";
			$result = $this->query($query);
			if($this->num_rows($result) == 1) { 
				$row = $this->fetch($result);
				if(password_verify($password, $row['password'])) {
					$this->loginSession($row['id'], $row['full_name'], $this->userType,$row['role']);
					$this->close_connection();
					header("location: ".$successURL);
					exit;
				} else {
					$this->close_connection();
					header("location: ".$failURL);
					exit;
				}
			} else {
				$this->close_connection();
				header("location: ".$failURL);
				exit;
			}
		}

		function sessionExists(){
			if($this->isUserLoggedIn()){
				return $loggedInUserDetailsArr = $this->getLoggedInUserDetails();
			} else {
				return false;
			}
		}

		function isUserLoggedIn(){
			if( isset($_SESSION[SITE_NAME]) && isset($_SESSION[SITE_NAME][$this->userType.'UserId']) && isset($_SESSION[SITE_NAME][$this->userType.'UserType']) && !empty($_SESSION[SITE_NAME][$this->userType.'UserId']) && $_SESSION[SITE_NAME][$this->userType.'UserType']==$this->userType){
				return true;
			} else {
				return false;
			}
		}

		function getLoggedInUserDetails(){
			$loggedInID = $this->escape_string($this->strip_all($_SESSION[SITE_NAME][$this->userType.'UserId']));
			$loggedInUserDetailsArr = $this->getUniqueUserById($loggedInID);
			return $loggedInUserDetailsArr;
		}

		function getUniqueUserById($userId) {
			$userId = $this->escape_string($this->strip_all($userId));
			$query = "select * from ".PREFIX."admin where id='".$userId."'";
			$sql = $this->query($query);
			return $this->fetch($sql);
		}

		function getDateFromDay($year, $month, $day) {
			/*
			0 for tuesday
			1 for monday
			2 for sunday
			3 for saturday
			4 for friday
			5 for thursday
			6 for wednesday
			*/
			$mondays = array();
			$firstDay = date('N', mktime(0, 0, 0, $month, $day, $year));
			/* Add 0 days if monday ... 6 days if tuesday, 1 day if sunday
			to get the first monday in month */
			$addDays = (8 - $firstDay);
			$mondays[] = date('Y-m-d', mktime(0, 0, 0, $month, 1 + $addDays, $year));

			$nextMonth = mktime(0, 0, 0, $month + 1, 1, $year);

			# Just add 7 days per iteration to get the date of the subsequent week
			for ($week = 1, $time = mktime(0, 0, 0, $month, 1 + $addDays + $week * 7, $year);
				$time < $nextMonth;
				++$week, $time = mktime(0, 0, 0, $month, 1 + $addDays + $week * 7, $year))
			{
				$mondays[] = date('Y-m-d', $time);
			}
			return $mondays;
		} 

		// === LOGIN ENDS ====

		// == EXTRA FUNCTIONS STARTS ==
		function getValidatedPermalink($permalink){ 
			$permalink = trim($permalink, '()');
			$replace_keywords = array("-:-", "-:", ":-", " : ", " :", ": ", ":",
				"-@-", "-@", "@-", " @ ", " @", "@ ", "@", 
				"-.-", "-.", ".-", " . ", " .", ". ", ".", 
				"-\\-", "-\\", "\\-", " \\ ", " \\", "\\ ", "\\",
				"-/-", "-/", "/-", " / ", " /", "/ ", "/", 
				"-&-", "-&", "&-", " & ", " &", "& ", "&", 
				"-,-", "-,", ",-", " , ", " ,", ", ", ",", 
				" ",
				"---", "--", " - ", " -", "- ",
				"-#-", "-#", "#-", " # ", " #", "# ", "#",
				"-$-", "-$", "$-", " $ ", " $", "$ ", "$",
				"-%-", "-%", "%-", " % ", " %", "% ", "%",
				"-^-", "-^", "^-", " ^ ", " ^", "^ ", "^",
				"-*-", "-*", "*-", " * ", " *", "* ", "*",
				"-(-", "-(", "(-", " ( ", " (", "( ", "(",
				"-)-", "-)", ")-", " ) ", " )", ") ", ")",
				"-;-", "-;", ";-", " ; ", " ;", "; ", ";",
				"-'-", "-'", "'-", " ' ", " '", "' ", "'",
				"-?-", "-?", "?-", " ? ", " ?", "? ", "?",
				"-!-", "-!", "!-", " ! ", " !", "! ", "!");
			$escapedPermalink = str_replace($replace_keywords, '-', $permalink); 
			return strtolower($escapedPermalink);
		}

		function getUniquePermalink($permalink,$tableName,$main_menu,$newPermalink='',$num=1) {
			if($newPermalink=='') {
				$checkPerma = $permalink;
			} else {
				$checkPerma = $newPermalink;
			}
			$sql = $this->query("select * from ".PREFIX.$tableName." where permalink='$checkPerma' and main_menu='$main_menu'");
			if($this->num_rows($sql)>0) {
				$count = $num+1;
				$newPermalink = $permalink.$count;
				return $this->getUniquePermalink($permalink,$tableName,$main_menu,$newPermalink,$count);
			} else {
				return $checkPerma;
			}
		}

		function getActiveLabel($isActive){
			if($isActive){
				return 'Yes';
			} else {
				return 'No';
			}
		}

		function getImageUrl($imageFor, $fileName, $imageSuffix){
			$image = strtolower(pathinfo($fileName, PATHINFO_FILENAME));
			$image_ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
			switch($imageFor){
				case "home-banner":
				$fileDir = "../images/home-banner/";
				break;
				
				default:
				return false;
				break;
			}
			$imageUrl = $fileDir.$image."_".$imageSuffix.".".$image_ext;
			if(file_exists($imageUrl)){
				return $imageUrl;
			} else {
				return false;
			}
		}

		function unlinkImage($imageFor, $fileName, $imageSuffix){
			$imagePath = $this->getImageUrl($imageFor, $fileName, $imageSuffix);
			$status = false;
			if($imagePath!==false){
				$status = unlink($imagePath);
			}
			return $status;
		}

		function checkUserPermissions($permission,$loggedInUserDetailsArr) {
			$userPermissionsArray = explode(',',$loggedInUserDetailsArr['permissions']);
			if(!in_array($permission,$userPermissionsArray) and $loggedInUserDetailsArr['role']!='super') {
				header("location: admin-login.php");
				exit;
			}
		}

		function generate_id($prefix, $randomNo, $tableName, $columnName){
			$chkprofile=$this->query("select ".$columnName." from ".PREFIX.$tableName." where ".$columnName." = '".$prefix.$randomNo."'");
			if($this->num_rows($chkprofile)>0){
				$randomNo = str_shuffle('1234567890123456789012345678901234567890');
				$randomNo = substr($randomNo,0,8);
				$this->generate_id($prefix, $randomNo, $tableName, $columnName);
			}else{
				return  $prefix.$randomNo;
			}
		}

		function getIndianCurrency($number) {
			$decimal = round($number - ($no = floor($number)), 2) * 100;
			$hundred = null;
			$digits_length = strlen($no);
			$i = 0;
			$str = array();
			$words = array(0 => '', 1 => 'one', 2 => 'two',
				3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
				7 => 'seven', 8 => 'eight', 9 => 'nine',
				10 => 'ten', 11 => 'eleven', 12 => 'twelve',
				13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
				16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
				19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
				40 => 'forty', 50 => 'fifty', 60 => 'sixty',
				70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
			$digits = array('', 'hundred','thousand','lakh', 'crore');
			while( $i < $digits_length ) {
				$divider = ($i == 2) ? 10 : 100;
				$number = floor($no % $divider);
				$no = floor($no / $divider);
				$i += $divider == 10 ? 1 : 2;
				if ($number) {
					$plural = (($counter = count($str)) && $number > 9) ? '' : null;
					//$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
					$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
					$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
				} else $str[] = null;
			}
			$Rupees = implode('', array_reverse($str));
			$paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
			//return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
			return ucfirst(($Rupees ? $Rupees . 'rupees ' : '') . $paise);
		}

		function ckeditorRefresh($data) {
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		} 

		// == EXTRA FUNCTIONS ENDS ==

		// Master Functions Start

	

		function getAllItemMaster() {
			$query = $this->query("SELECT * FROM ".PREFIX."item_master WHERE deleted_time=0");
			return $query;
		}

		function getAllItemFinishGood($branch_id) {
			$query = $this->query("SELECT * FROM ".PREFIX."item_master WHERE deleted_time=0 AND main_group = 'Finish Good' AND branch_id = '".$branch_id."'");
			return $query;
		}

		

		//Get Max Id from Item Group Master start
		function getItemGroupMaxId()
		{

			$max = $this->fetch($this->query("SELECT max(id) x FROM ".PREFIX."item_group_master"))['x'];
			return $max;

		}

		function addItemGroupMaster($data,$created_by){

			$maingroupname = $this->escape_string($this->strip_all($_POST["maingroupname"]));
			$groupname     = strtoupper($this->escape_string($this->strip_all($_POST["groupname"])));
			$title         = strtoupper($this->escape_string($this->strip_all($_POST["title"])));
			$groupcode     = $this->escape_string($this->strip_all($_POST["groupcode"]));

			$maingroupcode = $this->fetch($this->query("SELECT DISTINCTROW groupCode x FROM ".PREFIX."item_group_master WHERE groupName='$maingroupname'"))['x'];

			$results=$this->fetch($this->query("SELECT id FROM ".PREFIX."item_group_master WHERE mainGroupCode='$maingroupcode' AND groupCode='$groupcode'"));
			
			if(!$results){

				$query = $this->query("INSERT INTO ".PREFIX."item_group_master (mainGroupCode,mainGroupName,groupCode,groupName,title,createdby) values ('".$maingroupcode."', '".$maingroupname."', '".$groupcode."','".$groupname."', '".$groupname."','".$created_by."')");
					
				return $query;


			}

		}
		
		//Get Max Id from Item Group Master end


		//Opration Master start


		function getOprationMaxId()
		{

			$max = $this->fetch($this->query("SELECT count(id) x FROM ".PREFIX."opration_master"))['x'];
			return $max;

		}

		function addOprationMaster($data,$created_by){

			$maingroupname = $this->escape_string($this->strip_all($_POST["maingroupname"]));
			$groupname     = strtoupper($this->escape_string($this->strip_all($_POST["groupname"])));
			$title         = strtoupper($this->escape_string($this->strip_all($_POST["title"])));
			$groupcode     = $this->escape_string($this->strip_all($_POST["groupcode"]));

			$maingroupcode = $this->fetch($this->query("SELECT DISTINCTROW groupCode x FROM ".PREFIX."opration_master WHERE groupName='$maingroupname'"))['x'];

			$results=$this->fetch($this->query("SELECT id FROM ".PREFIX."opration_master WHERE mainGroupCode='$maingroupcode' AND groupCode='$groupcode'"));
			
			if(!$results){

				$query = $this->query("INSERT INTO ".PREFIX."opration_master (mainGroupCode,mainGroupName,groupCode,groupName,title,createdby) values ('".$maingroupcode."', '".$maingroupname."', '".$groupcode."','".$groupname."', '".$groupname."','".$created_by."')");
					
				return $query;


			}

		}


		//Opration Master end

		// Item Master Start

		function addItemMaster($data,$created_by,$branch_id)
		{
			$main_group     = $this->escape_string($this->strip_all($_POST["main_group"]));
			$component_name = $this->escape_string($this->strip_all($_POST["component_name"]));
			$item_name      = strtoupper($this->escape_string($this->strip_all($_POST["item_name"])));
			$description    = $this->escape_string($this->strip_all($_POST["description"]));
			$stock_ind      = $this->escape_string($this->strip_all($_POST["stock_ind"]));
			$unit           = $this->escape_string($this->strip_all($_POST["unit"]));
			$class          = $this->escape_string($this->strip_all($_POST["class"]));
			$max_level      = $this->escape_string($this->strip_all($_POST["max_level"]));
			$min_level      = $this->escape_string($this->strip_all($_POST["min_level"]));
			$reorder_level  = $this->escape_string($this->strip_all($_POST["reorder_level"]));
			$reorder_qty    = $this->escape_string($this->strip_all($_POST["reorder_qty"]));
			$hsn_code       = $this->escape_string($this->strip_all($_POST["hsn_code"]));
			$gst_rate       = $this->escape_string($this->strip_all($_POST["gst_rate"]));

			$query = $this->query("INSERT INTO ".PREFIX."item_master (branch_id,main_group,component_name,item_name,description,stock_ind,unit,class,max_level,min_level,reorder_level,reorder_qty,hsn_code,gst_rate,created_by,created_time) values ('".$branch_id."','".$main_group."','".$component_name."', '".$item_name."','".$description."','".$stock_ind."','".$unit."','".$class."','".$max_level."','".$min_level."','".$reorder_level."','".$reorder_qty."','".$hsn_code."','".$gst_rate."', '".$created_by."','".CURRENTMILLIS."')");
					
			return $query;

		}


		function getUniqueItemById($id)
		{
			 
			$query = $this->query("SELECT * FROM ".PREFIX."item_master WHERE id='".$id."'");

			return $this->fetch($query);
			
		}
		

		function getAllcompoentName($itemId, $branch_id)
		{
			$component_name = $itemId;
			 
			$query = $this->query("SELECT * FROM ".PREFIX."item_master WHERE component_name='".$component_name."' AND branch_id='".$branch_id."' ");

			return $query;
			
		}

		

		
		function updateItemMaster($data,$created_by,$branch_id)
		{
			$id             = $this->escape_string($this->strip_all($_POST["id"]));
			$main_group     = $this->escape_string($this->strip_all($_POST["main_group"]));
			$component_name = $this->escape_string($this->strip_all($_POST["component_name"]));
			$item_name      = strtoupper($this->escape_string($this->strip_all($_POST["item_name"])));
			$description    = $this->escape_string($this->strip_all($_POST["description"]));
			$stock_ind      = $this->escape_string($this->strip_all($_POST["stock_ind"]));
			$unit           = $this->escape_string($this->strip_all($_POST["unit"]));
			$class          = $this->escape_string($this->strip_all($_POST["class"]));
			$max_level      = $this->escape_string($this->strip_all($_POST["max_level"]));
			$min_level      = $this->escape_string($this->strip_all($_POST["min_level"]));
			$reorder_level  = $this->escape_string($this->strip_all($_POST["reorder_level"]));
			$reorder_qty    = $this->escape_string($this->strip_all($_POST["reorder_qty"]));
			$hsn_code       = $this->escape_string($this->strip_all($_POST["hsn_code"]));
			$gst_rate       = $this->escape_string($this->strip_all($_POST["gst_rate"]));

			$insertData = $this->query("UPDATE ".PREFIX."item_master SET main_group='".$main_group."',component_name='".$component_name."',item_name='".$item_name."',description = '".$description."',stock_ind = '".$stock_ind."',unit = '".$unit."',class = '".$class."',max_level = '".$max_level."',min_level = '".$min_level."',reorder_level = '".$reorder_level."',reorder_qty = '".$reorder_qty."',hsn_code = '".$hsn_code."',gst_rate = '".$gst_rate."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."' WHERE branch_id='".$branch_id."' AND id='".$id."' ");
					
			return $query;

		}


		//Item Master End


		//Machine Master Start

		function addMachineMaster($data,$file,$created_by,$branch_id)
		{
			$department = $this->escape_string($this->strip_all($data['department']));

			$insertData = $this->query("INSERT INTO ".PREFIX."machine_master (branch_id,department,created_by,created_time) VALUES ('".$branch_id."','".$department."','".$created_by."','".CURRENTMILLIS."') ");

			$last_id = $this->last_insert_id();

			$target='machinemasterimage/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['machine'])) {

				$machine                  = $data['machine'];
				$size                     = $data['size'];
				$capacity                 = $data['capacity'];
				$machine_number           = $data['machine_number'];
				$sop                      = $data['sop'];
				$check_sheet_maintainance = $data['check_sheet_maintainance'];
				$maintenance_frequency    = $data['maintenance_frequency'];
				$last_maintenance_date    = $data['last_maintenance_date'];
				$next_maintenance_date    = $data['next_maintenance_date'];
				$special_remark           = $data['special_remark'];
				$storableLocation         = "";

				for ($i = 0; $i < count($data['machine']); $i++) {

					$machine_value                  = $this->escape_string($this->strip_all($machine[$i]));
					$size_value                     = $this->escape_string($this->strip_all($size[$i]));
					$capacity_value                 = $this->escape_string($this->strip_all($capacity[$i]));
					$machine_number_value           = $this->escape_string($this->strip_all($machine_number[$i]));
					$sop_value                      = $this->escape_string($this->strip_all($sop[$i]));
					$check_sheet_maintainance_value = $this->escape_string($this->strip_all($check_sheet_maintainance[$i]));
					$maintenance_frequency_value    = $this->escape_string($this->strip_all($maintenance_frequency[$i]));
					$last_maintenance_date_value    = $this->escape_string($this->strip_all($last_maintenance_date[$i]));
					$next_maintenance_date_value    = $this->escape_string($this->strip_all($next_maintenance_date[$i]));
					$special_remark_value           = $this->escape_string($this->strip_all($special_remark[$i]));
					
					if(isset($file['machine_photo']['name'][$i]) && !empty($file['machine_photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['machine_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'machinemasterimage/'.'Machinephoto_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'machinemasterimage/'.'Machinephoto_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['machine_photo']['tmp_name'][$i], $fileLocation);

						$info         = pathinfo($file['machine_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = 'Machinephoto_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['machine_photo']['tmp_name'][$i], $target.'/'.$storableLocation);
			
					}

					$insertData = $this->query("INSERT INTO ".PREFIX."machine_component (branch_id,machine_id,machine,size,capacity,machine_number,sop,machine_photo,check_sheet_maintainance,maintenance_frequency,last_maintenance_date,next_maintenance_date,special_remark,created_by,created_time) values ('".$branch_id."','".$last_id."','".$machine_value."','".$size_value."','".$capacity_value."','".$machine_number_value."','".$sop_value."','".$storableLocation."','".$check_sheet_maintainance_value."','".$maintenance_frequency_value."','".$last_maintenance_date_value."','".$next_maintenance_date_value."','".$special_remark_value."','".$created_by."', '".CURRENTMILLIS."')");
				}

			}

			return $last_id;

		}

		function getUniqueMachineById($id)
		{
			 
			$query = $this->query("SELECT * FROM ".PREFIX."machine_master WHERE id='".$id."'");

			return $this->fetch($query);
			
		}	

		function getUniqueMachineComponentById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."machine_component WHERE machine_id='".$id."'");
			
			return $query;
		}

		function updateMachineMaster($data,$file,$created_by,$branch_id)
		{
			$id         = $this->escape_string($this->strip_all($data['id']));
			$department = $this->escape_string($this->strip_all($data['department']));

			$insertData = $this->query("UPDATE ".PREFIX."machine_master SET department='".$department."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");

			$target='machinemasterimage/'.$id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['machine'])) {

				$machine                  = $data['machine'];
				$machine_component_id     = $data['machine_component_id'];
				$size                     = $data['size'];
				$capacity                 = $data['capacity'];
				$machine_number           = $data['machine_number'];
				$sop                      = $data['sop'];
				$check_sheet_maintainance = $data['check_sheet_maintainance'];
				$maintenance_frequency    = $data['maintenance_frequency'];
				$last_maintenance_date    = $data['last_maintenance_date'];
				$next_maintenance_date    = $data['next_maintenance_date'];
				$special_remark           = $data['special_remark'];
				$storableLocation         = "";

				for ($i = 0; $i < count($data['machine']); $i++) {

					$machine_component_id_value     = $this->escape_string($this->strip_all($machine_component_id[$i]));
					$machine_value                  = $this->escape_string($this->strip_all($machine[$i]));
					$size_value                     = $this->escape_string($this->strip_all($size[$i]));
					$capacity_value                 = $this->escape_string($this->strip_all($capacity[$i]));
					$machine_number_value           = $this->escape_string($this->strip_all($machine_number[$i]));
					$sop_value                      = $this->escape_string($this->strip_all($sop[$i]));
					$check_sheet_maintainance_value = $this->escape_string($this->strip_all($check_sheet_maintainance[$i]));
					$maintenance_frequency_value    = $this->escape_string($this->strip_all($maintenance_frequency[$i]));
					$last_maintenance_date_value    = $this->escape_string($this->strip_all($last_maintenance_date[$i]));
					$next_maintenance_date_value    = $this->escape_string($this->strip_all($next_maintenance_date[$i]));
					$special_remark_value           = $this->escape_string($this->strip_all($special_remark[$i]));
					
					if(isset($file['machine_photo']['name'][$i]) && !empty($file['machine_photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['machine_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'machinemasterimage/'.'Machinephoto_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'machinemasterimage/'.'Machinephoto_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['machine_photo']['tmp_name'][$i], $fileLocation);

						$storableLocations = $this->fetch($this->query("SELECT machine_photo x FROM ".PREFIX."machine_component WHERE id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."'"))['x'];

						if ($storableLocations) {
							
							unlink($target.'/'.$storableLocations);

						}

						$info         = pathinfo($file['machine_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = 'Machinephoto_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['machine_photo']['tmp_name'][$i], $target.'/'.$storableLocation);
						
					} else {

						$storableLocation = $this->fetch($this->query("SELECT machine_photo x FROM ".PREFIX."machine_component WHERE id='".$machine_component_id_value."' AND branch_id='".$branch_id."' "))['x'];

					}


					if ($machine_component_id_value != '') {

						$updateData = $this->query("UPDATE ".PREFIX."machine_component SET machine = '".$machine_value."',size = '".$size_value."',capacity = '".$capacity_value."',machine_number = '".$machine_number_value."',sop = '".$sop_value."',machine_photo = '".$storableLocation."',check_sheet_maintainance = '".$check_sheet_maintainance_value."',maintenance_frequency = '".$maintenance_frequency_value."',last_maintenance_date = '".$last_maintenance_date_value."',next_maintenance_date = '".$next_maintenance_date_value."',special_remark = '".$special_remark_value."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."' WHERE machine_id='".$id."' AND id='".$machine_component_id."' AND branch_id='".$branch_id."' ");

					} else {

						$insertData = $this->query("INSERT INTO ".PREFIX."machine_component (branch_id,machine_id,machine,size,capacity,machine_number,sop,machine_photo,check_sheet_maintainance,maintenance_frequency,last_maintenance_date,next_maintenance_date,special_remark,created_by,created_time) values ('".$branch_id."','".$id."','".$machine_value."','".$size_value."','".$capacity_value."','".$machine_number_value."','".$sop_value."','".$storableLocation."','".$check_sheet_maintainance_value."','".$maintenance_frequency_value."','".$last_maintenance_date_value."','".$next_maintenance_date_value."','".$special_remark_value."','".$created_by."', '".CURRENTMILLIS."')");

					}
					
				}

			}


			return $id;
		}

		function getAllMachineName()
		{
			$query = $this->query("SELECT * FROM ".PREFIX."machine_component");
			
			return $query;
		}


		//Machine Master End


		//custome Master Start

		function getAllStates() {
			$query = $this->query("SELECT * FROM ".PREFIX."states");
			return $query;
		}

		function getAllContactPersonDetails() {
			$query = $this->query("SELECT * FROM ".PREFIX."contact_person_master WHERE deleted_time=0");
			return $query;
		}

		function addCustomerMaster($data,$created_by,$branch_id){
		
			if(isset($data['customer_name'])) {
				$customer_name       = $data['customer_name'];
				$customer_short_name = $data['customer_short_name'];
				$vendor_code         = $data['vendor_code'];
				$company_address     = $data['company_address'];
				$contact_person      = $data['contact_person'];
				$contact_no          = $data['contact_no'];
				$gst_no              = $data['gst_no'];
				$tax_structure       = $data['tax_structure'];
				$remark              = $data['remark'];

				for ($i = 0; $i < count($data['customer_name']); $i++) {

					$customer_name_value       = $this->escape_string($this->strip_all($customer_name[$i]));
					$customer_short_name_value = $this->escape_string($this->strip_all($customer_short_name[$i]));
					$vendor_code_value         = $this->escape_string($this->strip_all($vendor_code[$i]));
					$company_address_value     = $this->escape_string($this->strip_all($company_address[$i]));
					$contact_person_value      = $this->escape_string($this->strip_all($contact_person[$i]));
					$contact_no_value     = $this->escape_string($this->strip_all($contact_no[$i]));
					$gst_no_value              = $this->escape_string($this->strip_all($gst_no[$i]));
					$tax_structure_value       = $this->escape_string($this->strip_all($tax_structure[$i]));
					$remark_value              = $this->escape_string($this->strip_all($remark[$i]));

					$insertData = $this->query("INSERT INTO ".PREFIX."customer_master (branch_id,customer_name,customer_short_name,vendor_code,company_address,contact_person,contact_no,gst_no,tax_structure,remark,created_by,created_time) values ('".$branch_id."','".$customer_name_value."','".$customer_short_name_value."','".$vendor_code_value."','".$company_address_value."','".$contact_person_value."','".$contact_no_value."','".$gst_no_value."','".$tax_structure_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");
				}
			}
			return $insertData;
		}

		function getUniqueCustomerById($id) {
			$query = $this->query("SELECT * FROM ".PREFIX."customer_master where id = '".$id."'");
			return $this->fetch($query);
		}

		function getUniqueContactPersonById($id){
			$query = $this->query("SELECT * FROM ".PREFIX."contact_person_master where customer_id = '".$id."' ORDER BY person_name ASC");
			return $query;
		}

		function updateCustomerMaster($data,$updated_by,$branch_id) {
			$id                  = $this->escape_string($this->strip_all($data['id']));
			$customer_name       = $this->escape_string($this->strip_all($data['customer_name']));
			$customer_short_name = $this->escape_string($this->strip_all($data['customer_short_name']));
			$vendor_code         = $this->escape_string($this->strip_all($data['vendor_code']));
			$company_address     = $this->escape_string($this->strip_all($data['company_address']));
			$contact_person      = $this->escape_string($this->strip_all($data['contact_person']));
			$contact_no          = $this->escape_string($this->strip_all($data['contact_no']));
			$gst_no              = $this->escape_string($this->strip_all($data['gst_no']));
			$tax_structure       = $this->escape_string($this->strip_all($data['tax_structure']));
			$remark              = $this->escape_string($this->strip_all($data['remark']));


			$updateData = $this->query("UPDATE ".PREFIX."customer_master set customer_name='".$customer_name."', customer_short_name = '".$customer_short_name."',vendor_code='".$vendor_code."',company_address='".$company_address."',contact_person='".$contact_person."',contact_no='".$contact_no."',gst_no='".$gst_no."',tax_structure='".$tax_structure."',remark='".$remark."', updated_by='".$updated_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id ='".$branch_id."' ");
			
			return $id;
		}

		//custome Master end

		//Man Master Start

		function getAllItemGroupMaster() {
			$query = $this->query("SELECT * FROM ".PREFIX."item_group_master WHERE mainGroupCode = 'MG' ");
			return $query;
		}

		function getAllSubItemGroupMaster($data) {

			$groupCode = $this->escape_string($this->strip_all($data['groupCode']));

			$query = $this->query("SELECT * FROM ".PREFIX."item_group_master WHERE mainGroupCode = '".$groupCode."' ");
			return $query;
		}

		function getAllUnit() {
			$query = $this->query("SELECT * FROM ".PREFIX."unit");
			return $query;
		}


		function addManMaster($data,$file,$created_by){

			$branch_id            = $this->escape_string($this->strip_all($data['branch']));
			$emp_name             = $this->escape_string($this->strip_all($data['emp_name']));
			$gender               = $this->escape_string($this->strip_all($data['gender']));
			$dob                  = $this->escape_string($this->strip_all($data['dob']));
			$contact_no           = $this->escape_string($this->strip_all($data['contact_no']));
			$email_id             = $this->escape_string($this->strip_all($data['email_id']));
			$experience           = $this->escape_string($this->strip_all($data['experience']));
			$qualification        = $this->escape_string($this->strip_all($data['qualification']));
			$address              = $this->escape_string($this->strip_all($data['address']));
			$pAddress             = $this->escape_string($this->strip_all($data['pAddress']));
			$language             = $this->escape_string($this->strip_all($data['language']));
			$blood_group          = $this->escape_string($this->strip_all($data['blood_group']));
			$emergency_name       = $this->escape_string($this->strip_all($data['emergency_name']));
			$emergency_contact    = $this->escape_string($this->strip_all($data['emergency_contact']));
			$relation             = $this->escape_string($this->strip_all($data['relation']));
			$machine_id           = $this->escape_string($this->strip_all($data['machine_id']));
			$reference_by         = $this->escape_string($this->strip_all($data['reference_by']));
			$category             = $this->escape_string($this->strip_all($data['category']));
			$department           = $this->escape_string($this->strip_all($data['department']));
			$designation          = $this->escape_string($this->strip_all($data['designation']));
			$salary_per           = $this->escape_string($this->strip_all($data['salary_per']));
			$payment              = $this->escape_string($this->strip_all($data['payment']));
			$bank_name            = $this->escape_string($this->strip_all($data['bank_name']));
			$branch_name          = $this->escape_string($this->strip_all($data['branch_name']));
			$ifsc_code            = $this->escape_string($this->strip_all($data['ifsc_code']));
			$account_number       = $this->escape_string($this->strip_all($data['account_number']));
			$smoking              = $this->escape_string($this->strip_all($data['smoking']));
			$tobacco              = $this->escape_string($this->strip_all($data['tobacco']));
			$liquor               = $this->escape_string($this->strip_all($data['liquor']));
			$basic                = $this->escape_string($this->strip_all($data['basic']));
			$da                   = $this->escape_string($this->strip_all($data['da']));
			$hra                  = $this->escape_string($this->strip_all($data['hra']));
			$travelling           = $this->escape_string($this->strip_all($data['travelling']));
			$medical              = $this->escape_string($this->strip_all($data['medical']));
			$conveyance           = $this->escape_string($this->strip_all($data['conveyance']));
			$education            = $this->escape_string($this->strip_all($data['education']));
			$cca                  = $this->escape_string($this->strip_all($data['cca']));
			$total_salary         = $this->escape_string($this->strip_all($data['total_salary']));
			$mobile               = $this->escape_string($this->strip_all($data['mobile']));
			$uniform              = $this->escape_string($this->strip_all($data['uniform']));
			$internet             = $this->escape_string($this->strip_all($data['internet']));
			$employee_esic        = $this->escape_string($this->strip_all($data['employee_esic']));
			$employer_esic        = $this->escape_string($this->strip_all($data['employer_esic']));
			$training_date        = $this->escape_string($this->strip_all($data['training_date']));
			$joining_date         = $this->escape_string($this->strip_all($data['joining_date']));
			$probation_period     = $this->escape_string($this->strip_all($data['probation_period']));
			$permanent_date       = $this->escape_string($this->strip_all($data['permanent_date']));
			$leaving_date         = $this->escape_string($this->strip_all($data['leaving_date']));
			$additional_charges   = $this->escape_string($this->strip_all($data['additional_charges']));
			$additional_deduction = $this->escape_string($this->strip_all($data['additional_deduction']));
			$pf_wages             = $this->escape_string($this->strip_all($data['pf_wages']));
			$employee_pf          = $this->escape_string($this->strip_all($data['employee_pf']));
			$employer_pf          = $this->escape_string($this->strip_all($data['employer_pf']));
			$bonus                = $this->escape_string($this->strip_all($data['bonus']));
			$ctc                  = $this->escape_string($this->strip_all($data['ctc']));
			$working_hours        = $this->escape_string($this->strip_all($data['working_hours']));
			$professional_tax     = $this->escape_string($this->strip_all($data['professional_tax']));
			$over_time            = $this->escape_string($this->strip_all($data['over_time']));
			$sunday_payment       = $this->escape_string($this->strip_all($data['sunday_payment']));
			$dress_allowed        = $this->escape_string($this->strip_all($data['dress_allowed']));
			$shoes_allowed        = $this->escape_string($this->strip_all($data['shoes_allowed']));
			$lockers_allowed      = $this->escape_string($this->strip_all($data['lockers_allowed']));
			$esic_no              = $this->escape_string($this->strip_all($data['esic_no']));
			$pf_no                = $this->escape_string($this->strip_all($data['pf_no']));
			$it_deduction         = $this->escape_string($this->strip_all($data['it_deduction']));
			$weekly_Off           = $this->escape_string($this->strip_all($data['weekly_Off']));
			$aadhar_no            = $this->escape_string($this->strip_all($data['aadhar_no']));
			$pan_no               = $this->escape_string($this->strip_all($data['pan_no']));
			$driving_licence      = $this->escape_string($this->strip_all($data['driving_licence']));
			$passport_no          = $this->escape_string($this->strip_all($data['passport_no']));
			$Other_document       = $this->escape_string($this->strip_all($data['Other_document']));

			$emp_photo             = "";
			$aadhar_photo          = "";
			$pan_photo             = "";
			$driving_licence_photo = "";
			$passport_photo        = "";
			$Other_document_photo  = "";


			if(isset($file['emp_photo']['name']) && !empty($file['emp_photo']['name'])) {
				
				$fileInfo  = pathinfo($file['emp_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.CURRENTMILLIS.'.'.$extension;

				$emp_photo = 'manimage/'.$emp_name.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['emp_photo']['tmp_name'], $fileLocation);

			} 

			if(isset($file['aadhar_photo']['name']) && !empty($file['aadhar_photo']['name'])) {
				
				$fileInfo  = pathinfo($file['aadhar_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$aadhar_no.'_'.CURRENTMILLIS.'.'.$extension;

				$aadhar_photo = 'manimage/'.$emp_name.'_'.$aadhar_no.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['aadhar_photo']['tmp_name'], $fileLocation);

			} 

			if(isset($file['pan_photo']['name']) && !empty($file['pan_photo']['name'])) {
				
				$fileInfo  = pathinfo($file['pan_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$pan_no.'_'.CURRENTMILLIS.'.'.$extension;

				$pan_photo = 'manimage/'.$emp_name.'_'.$pan_no.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['pan_photo']['tmp_name'], $fileLocation);

			}

			if(isset($file['driving_licence_photo']['name']) && !empty($file['driving_licence_photo']['name'])) {
				
				$fileInfo  = pathinfo($file['driving_licence_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$driving_licence.'_'.CURRENTMILLIS.'.'.$extension;

				$driving_licence_photo = 'manimage/'.$emp_name.'_'.$driving_licence.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['driving_licence_photo']['tmp_name'], $fileLocation);

			}

			if(isset($file['passport_photo']['name']) && !empty($file['passport_photo']['name'])) {
				
				$fileInfo  = pathinfo($file['passport_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$passport_no.'_'.CURRENTMILLIS.'.'.$extension;

				$passport_photo = 'manimage/'.$emp_name.'_'.$passport_no.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['passport_photo']['tmp_name'], $fileLocation);

			}

			if(isset($file['Other_document_photo']['name']) && !empty($file['Other_document_photo']['name'])) {
				
				$fileInfo  = pathinfo($file['Other_document_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$Other_document.'_'.CURRENTMILLIS.'.'.$extension;

				$Other_document_photo = 'manimage/'.$emp_name.'_'.$Other_document.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['Other_document_photo']['tmp_name'], $fileLocation);

			}


			$insertData = $this->query("INSERT INTO ".PREFIX."man_master (`branch_id`, `emp_name`, `emp_photo`, `gender`, `dob`, `contact_no`, `email_id`, `experience`, `qualification`, `address`, `pAddress`, `language`, `blood_group`, `emergency_name`, `emergency_contact`, `relation`, `machine_id`, `reference_by`, `category`, `department`, `designation`, `salary_per`, `payment`, `bank_name`, `branch_name`, `ifsc_code`, `account_number`, `smoking`, `tobacco`, `liquor`, `basic`, `da`, `hra`, `travelling`, `medical`, `conveyance`, `education`, `cca`, `total_salary`, `mobile`, `uniform`, `internet`, `employee_esic`, `employer_esic`, `training_date`, `joining_date`, `probation_period`, `permanent_date`, `leaving_date`, `additional_charges`, `additional_deduction`, `pf_wages`, `employee_pf`, `employer_pf`, `bonus`, `ctc`, `working_hours`, `professional_tax`, `over_time`, `sunday_payment`, `dress_allowed`, `shoes_allowed`, `lockers_allowed`, `esic_no`, `pf_no`, `it_deduction`, `weekly_Off`, `aadhar_no`, `aadhar_photo`, `pan_no`, `pan_photo`, `driving_licence`, `driving_licence_photo`, `passport_no`, `passport_photo`, `Other_document`, `Other_document_photo`, `created_by`, `created_time`) values ('".$branch_id."','".$emp_name."','".$emp_photo."','".$gender."','".$dob."','".$contact_no."','".$email_id."','".$experience."','".$qualification."','".$address."','".$pAddress."','".$language."','".$blood_group."','".$emergency_name."','".$emergency_contact."','".$relation."','".$machine_id."','".$reference_by."','".$category."','".$department."','".$designation."','".$salary_per."','".$payment."','".$bank_name."','".$branch_name."','".$ifsc_code."','".$account_number."','".$smoking."','".$tobacco."','".$liquor."','".$basic."','".$da."','".$hra."','".$travelling."','".$medical."','".$conveyance."','".$education."','".$cca."','".$total_salary."','".$mobile."','".$uniform."','".$internet."','".$employee_esic."','".$employer_esic."','".$training_date."','".$joining_date."','".$probation_period."','".$permanent_date."','".$leaving_date."','".$additional_charges."','".$additional_deduction."','".$pf_wages."','".$employee_pf."','".$employer_pf."','".$bonus."','".$ctc."','".$working_hours."','".$professional_tax."','".$over_time."','".$sunday_payment."','".$dress_allowed."','".$shoes_allowed."','".$lockers_allowed."','".$esic_no."','".$pf_no."','".$it_deduction."','".$weekly_Off."','".$aadhar_no."','".$aadhar_photo."','".$pan_no."','".$pan_photo."','".$driving_licence."','".$driving_licence_photo."','".$passport_no."','".$passport_photo."','".$Other_document."','".$Other_document_photo."','".$created_by."', '".CURRENTMILLIS."')");
				
			return $insertData;

		}

		function getUniqueManId($id) {
			$query = $this->query("SELECT * FROM ".PREFIX."man_master where id = '".$id."'");
			return $this->fetch($query);
		}

		function updateManMaster($data,$file,$updated_by) {

			$id                   = $this->escape_string($this->strip_all($data['id']));
			$branch_id            = $this->escape_string($this->strip_all($data['branch']));
			$emp_name             = $this->escape_string($this->strip_all($data['emp_name']));
			$gender               = $this->escape_string($this->strip_all($data['gender']));
			$dob                  = $this->escape_string($this->strip_all($data['dob']));
			$contact_no           = $this->escape_string($this->strip_all($data['contact_no']));
			$email_id             = $this->escape_string($this->strip_all($data['email_id']));
			$experience           = $this->escape_string($this->strip_all($data['experience']));
			$qualification        = $this->escape_string($this->strip_all($data['qualification']));
			$address              = $this->escape_string($this->strip_all($data['address']));
			$pAddress             = $this->escape_string($this->strip_all($data['pAddress']));
			$language             = $this->escape_string($this->strip_all($data['language']));
			$blood_group          = $this->escape_string($this->strip_all($data['blood_group']));
			$emergency_name       = $this->escape_string($this->strip_all($data['emergency_name']));
			$emergency_contact    = $this->escape_string($this->strip_all($data['emergency_contact']));
			$relation             = $this->escape_string($this->strip_all($data['relation']));
			$machine_id           = $this->escape_string($this->strip_all($data['machine_id']));
			$reference_by         = $this->escape_string($this->strip_all($data['reference_by']));
			$category             = $this->escape_string($this->strip_all($data['category']));
			$department           = $this->escape_string($this->strip_all($data['department']));
			$designation          = $this->escape_string($this->strip_all($data['designation']));
			$salary_per           = $this->escape_string($this->strip_all($data['salary_per']));
			$payment              = $this->escape_string($this->strip_all($data['payment']));
			$bank_name            = $this->escape_string($this->strip_all($data['bank_name']));
			$branch_name          = $this->escape_string($this->strip_all($data['branch_name']));
			$ifsc_code            = $this->escape_string($this->strip_all($data['ifsc_code']));
			$account_number       = $this->escape_string($this->strip_all($data['account_number']));
			$smoking              = $this->escape_string($this->strip_all($data['smoking']));
			$tobacco              = $this->escape_string($this->strip_all($data['tobacco']));
			$liquor               = $this->escape_string($this->strip_all($data['liquor']));
			$basic                = $this->escape_string($this->strip_all($data['basic']));
			$da                   = $this->escape_string($this->strip_all($data['da']));
			$hra                  = $this->escape_string($this->strip_all($data['hra']));
			$travelling           = $this->escape_string($this->strip_all($data['travelling']));
			$medical              = $this->escape_string($this->strip_all($data['medical']));
			$conveyance           = $this->escape_string($this->strip_all($data['conveyance']));
			$education            = $this->escape_string($this->strip_all($data['education']));
			$cca                  = $this->escape_string($this->strip_all($data['cca']));
			$total_salary         = $this->escape_string($this->strip_all($data['total_salary']));
			$mobile               = $this->escape_string($this->strip_all($data['mobile']));
			$uniform              = $this->escape_string($this->strip_all($data['uniform']));
			$internet             = $this->escape_string($this->strip_all($data['internet']));
			$employee_esic        = $this->escape_string($this->strip_all($data['employee_esic']));
			$employer_esic        = $this->escape_string($this->strip_all($data['employer_esic']));
			$training_date        = $this->escape_string($this->strip_all($data['training_date']));
			$joining_date         = $this->escape_string($this->strip_all($data['joining_date']));
			$probation_period     = $this->escape_string($this->strip_all($data['probation_period']));
			$permanent_date       = $this->escape_string($this->strip_all($data['permanent_date']));
			$leaving_date         = $this->escape_string($this->strip_all($data['leaving_date']));
			$additional_charges   = $this->escape_string($this->strip_all($data['additional_charges']));
			$additional_deduction = $this->escape_string($this->strip_all($data['additional_deduction']));
			$pf_wages             = $this->escape_string($this->strip_all($data['pf_wages']));
			$employee_pf          = $this->escape_string($this->strip_all($data['employee_pf']));
			$employer_pf          = $this->escape_string($this->strip_all($data['employer_pf']));
			$bonus                = $this->escape_string($this->strip_all($data['bonus']));
			$ctc                  = $this->escape_string($this->strip_all($data['ctc']));
			$working_hours        = $this->escape_string($this->strip_all($data['working_hours']));
			$professional_tax     = $this->escape_string($this->strip_all($data['professional_tax']));
			$over_time            = $this->escape_string($this->strip_all($data['over_time']));
			$sunday_payment       = $this->escape_string($this->strip_all($data['sunday_payment']));
			$dress_allowed        = $this->escape_string($this->strip_all($data['dress_allowed']));
			$shoes_allowed        = $this->escape_string($this->strip_all($data['shoes_allowed']));
			$lockers_allowed      = $this->escape_string($this->strip_all($data['lockers_allowed']));
			$esic_no              = $this->escape_string($this->strip_all($data['esic_no']));
			$pf_no                = $this->escape_string($this->strip_all($data['pf_no']));
			$it_deduction         = $this->escape_string($this->strip_all($data['it_deduction']));
			$weekly_Off           = $this->escape_string($this->strip_all($data['weekly_Off']));
			$aadhar_no            = $this->escape_string($this->strip_all($data['aadhar_no']));
			$pan_no               = $this->escape_string($this->strip_all($data['pan_no']));
			$driving_licence      = $this->escape_string($this->strip_all($data['driving_licence']));
			$passport_no          = $this->escape_string($this->strip_all($data['passport_no']));
			$Other_document       = $this->escape_string($this->strip_all($data['Other_document']));

			$emp_photo             = "";
			$aadhar_photo          = "";
			$pan_photo             = "";
			$driving_licence_photo = "";
			$passport_photo        = "";
			$Other_document_photo  = "";


			if(isset($file['emp_photo']['name']) && !empty($file['emp_photo']['name'])) {

				$emp_photo = $this->fetch($this->query("SELECT emp_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];
				
				unlink($emp_photo);
				
				$fileInfo  = pathinfo($file['emp_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.CURRENTMILLIS.'.'.$extension;

				$emp_photo = 'manimage/'.$emp_name.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['emp_photo']['tmp_name'], $fileLocation);

			} else {
				
				$emp_photo = $this->fetch($this->query("SELECT emp_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

			}

			if(isset($file['aadhar_photo']['name']) && !empty($file['aadhar_photo']['name'])) {

				$aadhar_photo = $this->fetch($this->query("SELECT aadhar_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

				unlink($aadhar_photo);

				$fileInfo  = pathinfo($file['aadhar_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$aadhar_no.'_'.CURRENTMILLIS.'.'.$extension;

				$aadhar_photo = 'manimage/'.$emp_name.'_'.$aadhar_no.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['aadhar_photo']['tmp_name'], $fileLocation);

			} else {
				
				$aadhar_photo = $this->fetch($this->query("SELECT aadhar_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

			}

			if(isset($file['pan_photo']['name']) && !empty($file['pan_photo']['name'])) {

				$pan_photo = $this->fetch($this->query("SELECT pan_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

				unlink($pan_photo);
				
				$fileInfo  = pathinfo($file['pan_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$pan_no.'_'.CURRENTMILLIS.'.'.$extension;

				$pan_photo = 'manimage/'.$emp_name.'_'.$pan_no.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['pan_photo']['tmp_name'], $fileLocation);

			} else {
				
				$pan_photo = $this->fetch($this->query("SELECT pan_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

			}

			if(isset($file['driving_licence_photo']['name']) && !empty($file['driving_licence_photo']['name'])) {

				$driving_licence_photo = $this->fetch($this->query("SELECT driving_licence_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

				unlink($driving_licence_photo);
				
				$fileInfo  = pathinfo($file['driving_licence_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$driving_licence.'_'.CURRENTMILLIS.'.'.$extension;

				$driving_licence_photo = 'manimage/'.$emp_name.'_'.$driving_licence.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['driving_licence_photo']['tmp_name'], $fileLocation);

			} else {
				
				$driving_licence_photo = $this->fetch($this->query("SELECT driving_licence_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

			}

			if(isset($file['passport_photo']['name']) && !empty($file['passport_photo']['name'])) {

				$passport_photo = $this->fetch($this->query("SELECT passport_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

				unlink($passport_photo);
				
				$fileInfo  = pathinfo($file['passport_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$passport_no.'_'.CURRENTMILLIS.'.'.$extension;

				$passport_photo = 'manimage/'.$emp_name.'_'.$passport_no.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['passport_photo']['tmp_name'], $fileLocation);

			} else {
				
				$passport_photo = $this->fetch($this->query("SELECT passport_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

			}

			if(isset($file['Other_document_photo']['name']) && !empty($file['Other_document_photo']['name'])) {

				$Other_document_photo = $this->fetch($this->query("SELECT Other_document_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

				unlink($Other_document_photo);
				
				$fileInfo  = pathinfo($file['Other_document_photo']['name']);

				$extension = $fileInfo['extension'];

				$fileLocation = 'manimage/'.$emp_name.'_'.$Other_document.'_'.CURRENTMILLIS.'.'.$extension;

				$Other_document_photo = 'manimage/'.$emp_name.'_'.$Other_document.'_'.CURRENTMILLIS.'.'.$extension;

				move_uploaded_file($file['Other_document_photo']['tmp_name'], $fileLocation);

			} else {
				
				$Other_document_photo = $this->fetch($this->query("SELECT Other_document_photo x FROM ".PREFIX."man_master WHERE id = '".$id."' "))['x'];

			}
			
		

			$query = $this->query("UPDATE ".PREFIX."man_master set branch_id = '".$branch_id."',emp_name = '".$emp_name."',emp_photo = '".$emp_photo."',gender = '".$gender."',dob = '".$dob."',contact_no = '".$contact_no."',email_id = '".$email_id."',experience = '".$experience."',qualification = '".$qualification."',address = '".$address."',pAddress = '".$pAddress."',language = '".$language."',blood_group = '".$blood_group."',emergency_name = '".$emergency_name."',emergency_contact = '".$emergency_contact."',relation = '".$relation."',machine_id = '".$machine_id."',reference_by = '".$reference_by."',category = '".$category."',department = '".$department."',designation = '".$designation."',salary_per = '".$salary_per."',payment = '".$payment."',bank_name = '".$bank_name."',branch_name = '".$branch_name."',ifsc_code = '".$ifsc_code."',account_number = '".$account_number."',smoking = '".$smoking."',tobacco = '".$tobacco."',liquor = '".$liquor."',basic = '".$basic."',da = '".$da."',hra = '".$hra."',travelling = '".$travelling."',medical = '".$medical."',conveyance = '".$conveyance."',education = '".$education."',cca = '".$cca."',total_salary = '".$total_salary."',mobile = '".$mobile."',uniform = '".$uniform."',internet = '".$internet."',employee_esic = '".$employee_esic."',employer_esic = '".$employer_esic."',training_date = '".$training_date."',joining_date = '".$joining_date."',probation_period = '".$probation_period."',permanent_date = '".$permanent_date."',leaving_date = '".$leaving_date."',additional_charges = '".$additional_charges."',additional_deduction = '".$additional_deduction."',pf_wages = '".$pf_wages."',employee_pf = '".$employee_pf."',employer_pf = '".$employer_pf."',bonus = '".$bonus."',ctc = '".$ctc."',working_hours = '".$working_hours."',professional_tax = '".$professional_tax."',over_time = '".$over_time."',sunday_payment = '".$sunday_payment."',dress_allowed = '".$dress_allowed."',shoes_allowed = '".$shoes_allowed."',lockers_allowed = '".$lockers_allowed."',esic_no = '".$esic_no."',pf_no = '".$pf_no."',it_deduction = '".$it_deduction."',weekly_Off = '".$weekly_Off."',aadhar_no = '".$aadhar_no."',aadhar_photo = '".$aadhar_photo."',pan_no = '".$pan_no."',pan_photo = '".$pan_photo."',driving_licence = '".$driving_licence."',driving_licence_photo = '".$driving_licence_photo."',passport_no = '".$passport_no."',passport_photo = '".$passport_photo."',Other_document = '".$Other_document."',Other_document_photo = '".$Other_document_photo."',updated_by='".$updated_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."'");

			return $query;
			
		}

		//Man  Master end


		//Quality Instrument Master Start


		function addQualityInstrumentMaster($data,$created_by,$branch_id){

			if(isset($data['instrument_name'])) {
				$instrument_name           = $data['instrument_name'];
				$capacity                  = $data['capacity'];
				$accuracy                  = $data['accuracy'];
				$make                      = $data['make'];
				$purchase_date             = $data['purchase_date'];
				$last_calibration_date     = $data['last_calibration_date'];
				$next_calibration_due_date = $data['next_calibration_due_date'];
				$current_condition         = $data['current_condition'];
				$remark                    = $data['remark'];

				for ($i = 0; $i < count($data['instrument_name']); $i++) {

					$instrument_name_value           = $this->escape_string($this->strip_all($instrument_name[$i]));
					$capacity_value                  = $this->escape_string($this->strip_all($capacity[$i]));
					$accuracy_value                  = $this->escape_string($this->strip_all($accuracy[$i]));
					$make_value                      = $this->escape_string($this->strip_all($make[$i]));
					$purchase_date_value             = $this->escape_string($this->strip_all($purchase_date[$i]));
					$last_calibration_date_value     = $this->escape_string($this->strip_all($last_calibration_date[$i]));
					$next_calibration_due_date_value = $this->escape_string($this->strip_all($next_calibration_due_date[$i]));
					$current_condition_value         = $this->escape_string($this->strip_all($current_condition[$i]));
					$remark_value                    = $this->escape_string($this->strip_all($remark[$i]));

					$insertData = $this->query("INSERT INTO ".PREFIX."quality_instrument_master (branch_id,instrument_name,capacity,accuracy,make,purchase_date,last_calibration_date,next_calibration_due_date,current_condition,remark,created_by,created_time) values ('".$branch_id."','".$instrument_name_value."','".$capacity_value."','".$accuracy_value."','".$make_value."','".$purchase_date_value."','".$last_calibration_date_value."','".$next_calibration_due_date_value."','".$current_condition_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");
				}
			}
			return $insertData;
		}

		function getUniqueaQualityInstrumentId($id) {
			$query = $this->query("SELECT * FROM ".PREFIX."quality_instrument_master where id = '".$id."'");
			return $this->fetch($query);
		}

		function updateQualityInstrumentMaster($data,$updated_by,$branch_id) {
			$id                        = $this->escape_string($this->strip_all($data['id']));
			$instrument_name           = $this->escape_string($this->strip_all($data['instrument_name']));
			$capacity                  = $this->escape_string($this->strip_all($data['capacity']));
			$accuracy                  = $this->escape_string($this->strip_all($data['accuracy']));
			$make                      = $this->escape_string($this->strip_all($data['make']));
			$purchase_date             = $this->escape_string($this->strip_all($data['purchase_date']));
			$last_calibration_date     = $this->escape_string($this->strip_all($data['last_calibration_date']));
			$next_calibration_due_date = $this->escape_string($this->strip_all($data['next_calibration_due_date']));
			$current_condition         = $this->escape_string($this->strip_all($data['current_condition']));
			$remark                    = $this->escape_string($this->strip_all($data['remark']));

			$query = $this->query("UPDATE ".PREFIX."quality_instrument_master set instrument_name='".$instrument_name."',capacity='".$capacity."',accuracy='".$accuracy."',make='".$make."',purchase_date='".$purchase_date."',last_calibration_date='".$last_calibration_date."',next_calibration_due_date='".$next_calibration_due_date."',current_condition='".$current_condition."',remark='".$remark."',updated_by='".$updated_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");
			return $query;
		}


		//Quality Instrument Master end


		// Drawing master start

		function addDrawingMaster($data,$file,$created_by,$branch_id)
		{

			$product_name = $this->escape_string($this->strip_all($data['product_name']));
			$drawing_no   = $this->escape_string($this->strip_all($data['drawing_no']));

			$insertData = $this->query("INSERT INTO ".PREFIX."drawing_master (branch_id,product_name, drawing_no, created_by, created_time) VALUES  ('".$branch_id."','".$product_name."','".$drawing_no."','".$created_by."', '".CURRENTMILLIS."')");

			$last_id = $this->last_insert_id();

			$target='drawingmasterimage/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['component_name'])) {

				$component_name       = $data['component_name'];
				$component_drawing_no = $data['component_drawing_no'];
				$rev_chk_frequency    = $data['rev_chk_frequency'];
				$last_check_date      = $data['last_check_date'];
				$next_due_date        = $data['next_due_date'];
				$storableLocation     = "";

				for ($i = 0; $i < count($data['component_name']); $i++) {
					$component_name_value       = $this->escape_string($this->strip_all($component_name[$i]));
					$component_drawing_no_value = $this->escape_string($this->strip_all($component_drawing_no[$i]));
					$rev_chk_frequency_value    = $this->escape_string($this->strip_all($rev_chk_frequency[$i]));
					$last_check_date_value      = $this->escape_string($this->strip_all($last_check_date[$i]));
					$next_due_date_value        = $this->escape_string($this->strip_all($next_due_date[$i]));

					if(isset($file['drawing_photo']['name'][$i]) && !empty($file['drawing_photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['drawing_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'drawingmasterimage/'.$component_name_value.'_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'drawingmasterimage/'.$component_name_value.'_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['drawing_photo']['tmp_name'][$i], $fileLocation);

						$info         = pathinfo($file['drawing_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = $component_name_value.'_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['drawing_photo']['tmp_name'][$i], $target.'/'.$storableLocation);

			
					} 

					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."drawing_component_master (branch_id,drawing_id, product_name, component_name, component_drawing_no, drawing_photo, rev_chk_frequency, last_check_date, next_due_date, created_by, created_time) VALUES ('".$branch_id."','".$last_id."', '".$product_name."', '".$component_name_value."','".$component_drawing_no_value."','".$storableLocation."','".$rev_chk_frequency_value."','".$last_check_date_value."','".$next_due_date_value."','".$created_by."', '".CURRENTMILLIS."')");
				}
			}

			return $last_id;
			
		}


		function getUniqueDrawingById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."drawing_master where id = '".$id."'");
			return $this->fetch($query);
		}

		function getUniqueDrawingComponentById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."drawing_component_master where drawing_id = '".$id."' ORDER BY id ASC");
			return $query;
		}

		function getAllDrawingComponentMaster($itemId,$branch_id)
		{

			$query = $this->query("SELECT * FROM ".PREFIX."drawing_component_master where product_name = '".$itemId."' and branch_id = '".$branch_id."' ORDER BY id ASC");
			
			return $query;

		}

		function getUniqueComponentPhotoById($data){
			$id = $this->escape_string($this->strip_all($data['componentphoto']));
			$check_list = $this->fetch($this->query("SELECT drawing_photo x FROM ".PREFIX."drawing_component_master where id = '".$id."'"))['x'];
			return $check_list;
		}

		function updateDrawingMaster($data,$file,$created_by,$branch_id)
		{
			$id           = $this->escape_string($this->strip_all($data['id']));
			$product_name = $this->escape_string($this->strip_all($data['product_name']));
			$drawing_no   = $this->escape_string($this->strip_all($data['drawing_no']));

			$updateData = $this->query("UPDATE ".PREFIX."drawing_master set product_name='".$product_name."', drawing_no = '".$drawing_no."', updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");

			$target='drawingmasterimage/'.$id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['component_name'])) {
				$drawing_component_id = $data['drawing_component_id'];
				$component_name       = $data['component_name'];
				$component_drawing_no = $data['component_drawing_no'];
				$rev_chk_frequency    = $data['rev_chk_frequency'];
				$last_check_date      = $data['last_check_date'];
				$next_due_date        = $data['next_due_date'];
				$storableLocation     = "";

				for ($i = 0; $i < count($data['component_name']); $i++) {

					$drawing_component_id_value = $this->escape_string($this->strip_all($drawing_component_id[$i]));
					$component_name_value       = $this->escape_string($this->strip_all($component_name[$i]));
					$component_drawing_no_value = $this->escape_string($this->strip_all($component_drawing_no[$i]));
					$rev_chk_frequency_value    = $this->escape_string($this->strip_all($rev_chk_frequency[$i]));
					$last_check_date_value      = $this->escape_string($this->strip_all($last_check_date[$i]));
					$next_due_date_value        = $this->escape_string($this->strip_all($next_due_date[$i]));

					if(isset($file['drawing_photo']['name'][$i]) && !empty($file['drawing_photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['drawing_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'drawingmasterimage/'.$component_name_value.'_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'drawingmasterimage/'.$component_name_value.'_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['drawing_photo']['tmp_name'][$i], $fileLocation);

						$storableLocations = $this->fetch($this->query("SELECT drawing_photo x FROM ".PREFIX."drawing_component_master WHERE id = '".$drawing_component_id_value."' AND branch_id='".$branch_id."' "))['x'];

						if ($storableLocations) {

							unlink($target.'/'.$storableLocation);
							
						}

						$info         = pathinfo($file['drawing_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = $component_name_value.'_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['drawing_photo']['tmp_name'][$i], $storableLocation);

			
					} else {
						
						$storableLocation = $this->fetch($this->query("SELECT drawing_photo x FROM ".PREFIX."drawing_component_master WHERE id = '".$drawing_component_id_value."' AND branch_id='".$branch_id."' "))['x'];

					}
					
					if ($drawing_component_id_value != '') {
						
						$updateIntorawMaterial = $this->query("UPDATE ".PREFIX."drawing_component_master SET product_name = '".$product_name."', component_name='".$component_name_value."',component_drawing_no='".$component_drawing_no_value."',drawing_photo='".$storableLocation."',rev_chk_frequency='".$rev_chk_frequency_value."',last_check_date='".$last_check_date_value."',next_due_date='".$next_due_date_value."',updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  drawing_id = '".$id."' AND id = '".$drawing_component_id_value."' AND branch_id='".$branch_id."' ");

					} else {

						$insertIntorawMaterial = $this->query("INSERT INTO ".PREFIX."drawing_component_master (branch_id, drawing_id, product_name, component_name, component_drawing_no, drawing_photo, rev_chk_frequency, last_check_date, next_due_date, created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$product_name."', '".$component_name_value."','".$component_drawing_no_value."','".$storableLocation."','".$rev_chk_frequency_value."','".$last_check_date_value."','".$next_due_date_value."','".$created_by."', '".CURRENTMILLIS."')");

					}
					
				}

			}

			return $id;

		}


		// Drawing master end


		// Tool Master Start

		function addToolMaster($data,$file,$created_by,$branch_id)
		{

			$product_name   = $this->escape_string($this->strip_all($data['product_name']));
			
			$sub_sr         = $this->escape_string($this->strip_all($data['sub_sr']));
			
			$component_name = $this->escape_string($this->strip_all($data['component_name']));

			$insertData     = $this->query("INSERT INTO ".PREFIX."tool_master (branch_id,product_name, sub_sr,component_name, created_by, created_time) VALUES  ('".$branch_id."','".$product_name."','".$sub_sr."','".$component_name."','".$created_by."', '".CURRENTMILLIS."')");

			$last_id        = $this->last_insert_id();

			$target='toolmasterimage/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['process_name'])) {

				$process_name            = $data['process_name'];
				
				$tool_name               = $data['tool_name'];
				
				$tool_number             = $data['tool_number'];
				
				$tool_grinding_frequency = $data['tool_grinding_frequency'];
				
				$last_grinding_date      = $data['last_grinding_date'];
				
				$next_due_date           = $data['next_due_date'];
				
				$storableLocation        = "";
				
				$storableLocation1       = "";


				for ($i = 0; $i < count($data['process_name']); $i++) {
				
					$process_name_value            = $this->escape_string($this->strip_all($process_name[$i]));
				
					$tool_name_value               = $this->escape_string($this->strip_all($tool_name[$i]));
				
					$tool_number_value             = $this->escape_string($this->strip_all($tool_number[$i]));
				
					$tool_grinding_frequency_value = $this->escape_string($this->strip_all($tool_grinding_frequency[$i]));
				
					$last_grinding_date_value      = $this->escape_string($this->strip_all($last_grinding_date[$i]));
				
					$next_due_date_value           = $this->escape_string($this->strip_all($next_due_date[$i]));

					if(isset($file['photo']['name'][$i]) && !empty($file['photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'toolmasterimage/'.'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'toolmasterimage/'.'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['photo']['tmp_name'][$i], $fileLocation);

						$info         = pathinfo($file['photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = 'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						//move_uploaded_file($file['pan_photo']['tmp_name'],$target.'/'.$pan_photo);
						move_uploaded_file($file['photo']['tmp_name'][$i], $target.'/'.$storableLocation);
			
					}
					
					if(isset($file['check_list']['name'][$i]) && !empty($file['check_list']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['check_list']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'toolmasterimage/'.'check_list_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation1 = 'toolmasterimage/'.'check_list_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['check_list']['tmp_name'][$i], $fileLocation);

						$info         = pathinfo($file['check_list']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation1 = 'check_list_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						//move_uploaded_file($file['pan_photo']['tmp_name'],$target.'/'.$pan_photo);
						move_uploaded_file($file['check_list']['tmp_name'][$i], $target.'/'.$storableLocation1);
			
					}  

					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."tool_component_master (branch_id,tool_id, process_name, product_name, component_name, tool_name, tool_number, photo, check_list, tool_grinding_frequency,last_grinding_date,next_due_date, created_by, created_time) VALUES ('".$branch_id."','".$last_id."', '".$product_name."', '".$component_name."','".$process_name_value."','".$tool_name_value."','".$tool_number_value."','".$storableLocation."','".$storableLocation1."','".$tool_grinding_frequency_value."','".$last_grinding_date_value."','".$next_due_date_value."','".$created_by."', '".CURRENTMILLIS."')");
				}
			}

			return $last_id;
		}

		function getUniqueToolById($id)
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."tool_master where id = '".$id."'");
			return $this->fetch($query);
			
		}

		function getAllTool()
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."tool_component_master");
			return $query;
			
		}

		function getAllFromBomTool($component_name,$product_name,$branch_id)
		{

			//echo "SELECT * FROM ".PREFIX."tool_component_master where branch_id = '".$branch_id."' AND product_name = '".$product_name."' AND component_name = '".$component_name."' ";
			
			$query = $this->query("SELECT * FROM ".PREFIX."tool_component_master where branch_id = '".$branch_id."' AND product_name = '".$product_name."' AND component_name = '".$component_name."' ");
			return $query;
			
		}

		function getUniqueToolComponentById($id)
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."tool_component_master where tool_id = '".$id."'");
			return $query;

		}

		function getUniqueCheckSheetToolPhotoById($data){

			$id = $this->escape_string($this->strip_all($data['checkSheetToolPhoto']));

			$check_list = $this->fetch($this->query("SELECT * FROM ".PREFIX."tool_component_master where id = '".$id."'"));

			return $check_list;

		}

		function getAllToolComponentMaster($data)
		{
			$id = $this->escape_string($this->strip_all($data['itemName']));
			
			$tool_id = $this->fetch($this->query("SELECT id x FROM ".PREFIX."tool_master WHERE product_name = '".$id."'"))['x'];

			$query = $this->query("SELECT * FROM ".PREFIX."tool_component_master where tool_id = '".$tool_id."' ");
			return $query;
		}


		function updateToolMaster($data,$file,$created_by,$branch_id)
		{
			$id           = $this->escape_string($this->strip_all($data['id']));
			$product_name   = $this->escape_string($this->strip_all($data['product_name']));
			$sub_sr         = $this->escape_string($this->strip_all($data['sub_sr']));
			$component_name = $this->escape_string($this->strip_all($data['component_name']));

			$updateData = $this->query("UPDATE ".PREFIX."tool_master set product_name='".$product_name."', sub_sr = '".$sub_sr."', component_name = '".$component_name."' , updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");

			$target='toolmasterimage/'.$id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}
			if(isset($data['process_name'])) {

				$process_name            = $data['process_name'];
				$tool_component_id       = $data['tool_component_id'];
				$tool_name               = $data['tool_name'];
				$tool_number             = $data['tool_number'];
				$tool_grinding_frequency = $data['tool_grinding_frequency'];
				$last_grinding_date      = $data['last_grinding_date'];
				$next_due_date           = $data['next_due_date'];
				$storableLocation        = "";
				$storableLocation1      = "";


				for ($i = 0; $i < count($data['process_name']); $i++) {
					$tool_component_id_value       = $this->escape_string($this->strip_all($tool_component_id[$i]));
					$process_name_value            = $this->escape_string($this->strip_all($process_name[$i]));
					$tool_name_value               = $this->escape_string($this->strip_all($tool_name[$i]));
					$tool_number_value             = $this->escape_string($this->strip_all($tool_number[$i]));
					$tool_grinding_frequency_value = $this->escape_string($this->strip_all($tool_grinding_frequency[$i]));
					$last_grinding_date_value      = $this->escape_string($this->strip_all($last_grinding_date[$i]));
					$next_due_date_value           = $this->escape_string($this->strip_all($next_due_date[$i]));

					if(isset($file['photo']['name'][$i]) && !empty($file['photo']['name'][$i])) {

						// $storableLocation = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."tool_component_master WHERE id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."'"))['x'];
						// unlink($storableLocation);
			
						// $fileInfo  = pathinfo($file['photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'toolmasterimage/'.'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'toolmasterimage/'.'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['photo']['tmp_name'][$i], $fileLocation);

						$storableLocations = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."tool_component_master WHERE id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."'"))['x'];

						if ($storableLocations) {
							
							unlink($target.'/'.$storableLocations);

						}

						$info         = pathinfo($file['photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = 'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;

						move_uploaded_file($file['photo']['tmp_name'][$i],$target.'/'.$storableLocation);

			
					} else {
						
						$storableLocation = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."tool_component_master WHERE id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."'"))['x'];

					}

					if(isset($file['check_list']['name'][$i]) && !empty($file['check_list']['name'][$i])) {

						$storableLocations1 = $this->fetch($this->query("SELECT check_list x FROM ".PREFIX."tool_component_master WHERE id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."' "))['x'];

						if ($storableLocations1) {
							
							unlink($target.'/'.$storableLocations1);

						}
						$info         = pathinfo($file['check_list']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation1 = 'check_list_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['check_list']['tmp_name'][$i], $target.'/'.$storableLocation1);
			
					} else {
						
						$storableLocation1 = $this->fetch($this->query("SELECT check_list x FROM ".PREFIX."tool_component_master WHERE id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."' "))['x'];

					}
				
					if ($tool_component_id_value != '') {
						
						$updateIntorawMaterial = $this->query("UPDATE ".PREFIX."tool_component_master SET product_name = '".$product_name."', component_name = '".$component_name."', process_name='".$process_name_value."',tool_name='".$tool_name_value."',tool_number='".$tool_number_value."',photo='".$storableLocation."',check_list='".$storableLocation1."',tool_grinding_frequency='".$tool_grinding_frequency_value."',last_grinding_date='".$last_grinding_date_value."',next_due_date='".$next_due_date_value."',updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  tool_id = '".$id."' AND id = '".$tool_component_id_value."' AND branch_id = '".$branch_id."' ");

					} else {

						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."tool_component_master (branch_id,tool_id, product_name, component_name,process_name, tool_name, tool_number, photo, check_list, tool_grinding_frequency,last_grinding_date,next_due_date, created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$product_name."', '".$component_name."', '".$process_name_value."','".$tool_name_value."','".$tool_number_value."','".$storableLocation."','".$storableLocation1."','".$tool_grinding_frequency_value."','".$last_grinding_date_value."','".$next_due_date_value."','".$created_by."', '".CURRENTMILLIS."')");

					}
					
				}

			}

			return $id;

		}

		// Tool Master End


		// Inspect Gauges Master Start

		function addInspectGaugesMaster($data,$file,$created_by,$branch_id)
		{

			$product_name   = $this->escape_string($this->strip_all($data['product_name']));
			$sub_sr         = $this->escape_string($this->strip_all($data['sub_sr']));
			$component_name = $this->escape_string($this->strip_all($data['component_name']));

			$insertData = $this->query("INSERT INTO ".PREFIX."inspection_gauges_master (branch_id,product_name, sub_sr,component_name, created_by, created_time) VALUES  ('".$branch_id."','".$product_name."','".$sub_sr."','".$component_name."','".$created_by."', '".CURRENTMILLIS."')");

			$last_id = $this->last_insert_id();

			$target='inspectmasterimage/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['process_name'])) {

				$process_name          = $data['process_name'];
				$inspect_gauges        = $data['inspect_gauges'];
				$gauges_number         = $data['gauges_number'];
				$calibration_frequency = $data['calibration_frequency'];
				$last_calibration_date = $data['last_calibration_date'];
				$next_calibration_date = $data['next_calibration_date'];
				$remark                = $data['remark'];
				$storableLocation      = "";
				$storableLocation1     = "";

				for ($i = 0; $i < count($data['process_name']); $i++) {
					$process_name_value          = $this->escape_string($this->strip_all($process_name[$i]));
					$inspect_gauges_value        = $this->escape_string($this->strip_all($inspect_gauges[$i]));
					$gauges_number_value         = $this->escape_string($this->strip_all($gauges_number[$i]));
					$calibration_frequency_value = $this->escape_string($this->strip_all($calibration_frequency[$i]));
					$last_calibration_date_value = $this->escape_string($this->strip_all($last_calibration_date[$i]));
					$next_calibration_date_value = $this->escape_string($this->strip_all($next_calibration_date[$i]));
					$remark_value                = $this->escape_string($this->strip_all($remark[$i]));


					// if(isset($file['gauges_photo']['name'][$i]) && !empty($file['gauges_photo']['name'][$i])) {
			
					// 	$fileInfo  = pathinfo($file['gauges_photo']['name'][$i]);
			
					// 	$extension = $fileInfo['extension'];
			
					// 	$fileLocation = 'inspectmasterimage/'.'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
					// 	$storableLocation = 'inspectmasterimage/'.'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
					// 	move_uploaded_file($file['gauges_photo']['tmp_name'][$i], $fileLocation);
			
					// }

					if(isset($file['gauges_photo']['name'][$i]) && !empty($file['gauges_photo']['name'][$i])) {

						$info         = pathinfo($file['gauges_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = 'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['gauges_photo']['tmp_name'][$i], $target.'/'.$storableLocation);
			
					}

					if(isset($file['gauges_check_list']['name'][$i]) && !empty($file['gauges_check_list']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['gauges_check_list']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation1 = 'inspectmasterimage/'.'Checklist_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation1 = 'inspectmasterimage/'.'Checklist_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['gauges_check_list']['tmp_name'][$i], $fileLocation1);

						$info         = pathinfo($file['gauges_check_list']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation1 = 'Checklist_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['gauges_check_list']['tmp_name'][$i], $target.'/'.$storableLocation1);

					}  

					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."inspection_gauges_component (branch_id,inspect_gauges_id, product_name, component_name, process_name, inspect_gauges, gauges_number, photo, gauges_check_list, calibration_frequency,last_calibration_date,next_calibration_date, remark,created_by, created_time) VALUES ('".$branch_id."','".$last_id."', '".$product_name."', '".$component_name."', '".$process_name_value."','".$inspect_gauges_value."','".$gauges_number_value."','".$storableLocation."','".$storableLocation1."','".$calibration_frequency_value."','".$last_calibration_date_value."','".$next_calibration_date_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");

				}
			}

			return $last_id;
		}

		function getUniqueInspectGaugesById($id)
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."inspection_gauges_master where id = '".$id."'");
			return $this->fetch($query);
			
		}

		function getUniqueInspectGaugesComponentById($id)
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."inspection_gauges_component where inspect_gauges_id = '".$id."'");
			return $query;

		}

		function getAllInspectionGaugesComponent()
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."inspection_gauges_component");
			return $query;

		}

		function getAllFromBomInspectionGaugesComponent($component_name,$product_name,$branch_id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."inspection_gauges_component where branch_id = '".$branch_id."' AND product_name = '".$product_name."' AND component_name = '".$component_name."' ");
			return $query;
		}
		

		function getAllGaugesComponentMaster($data)
		{
			$id = $this->escape_string($this->strip_all($data['itemName']));
			
			$tool_id = $this->fetch($this->query("SELECT id x FROM ".PREFIX."inspection_gauges_master WHERE product_name = '".$id."'"))['x'];

			$query = $this->query("SELECT * FROM ".PREFIX."inspection_gauges_component where inspect_gauges_id = '".$tool_id."' ");
			return $query;
		}

		
		function getUniqueCheckSheetJigsPhotoById($data){

			$id = $this->escape_string($this->strip_all($data['checkSheetJigsPhoto']));

			$check_list = $this->fetch($this->query("SELECT * FROM ".PREFIX."inspection_gauges_component where id = '".$id."'"));
			return $check_list;
		}


		function updateInspectGaugesMaster($data,$file,$created_by,$branch_id)
		{
			$id             = $this->escape_string($this->strip_all($data['id']));
			$product_name   = $this->escape_string($this->strip_all($data['product_name']));
			$sub_sr         = $this->escape_string($this->strip_all($data['sub_sr']));
			$component_name = $this->escape_string($this->strip_all($data['component_name']));

			$updateData = $this->query("UPDATE ".PREFIX."inspection_gauges_master set product_name='".$product_name."', sub_sr = '".$sub_sr."', component_name = '".$component_name."' , updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");

			$target='inspectmasterimage/'.$id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['process_name'])) {

				$process_name                = $data['process_name'];
				$inspect_gauges_component_id = $data['inspect_gauges_component_id'];
				$inspect_gauges              = $data['inspect_gauges'];
				$gauges_number               = $data['gauges_number'];
				$calibration_frequency       = $data['calibration_frequency'];
				$last_calibration_date       = $data['last_calibration_date'];
				$next_calibration_date       = $data['next_calibration_date'];
				$remark                      = $data['remark'];
				$storableLocations            = "";
				$storableLocations1           = "";


				for ($i = 0; $i < count($data['process_name']); $i++) {
					$inspect_gauges_component_id_value = $this->escape_string($this->strip_all($inspect_gauges_component_id[$i]));
					$process_name_value                = $this->escape_string($this->strip_all($process_name[$i]));
					$inspect_gauges_value              = $this->escape_string($this->strip_all($inspect_gauges[$i]));
					$gauges_number_value               = $this->escape_string($this->strip_all($gauges_number[$i]));
					$calibration_frequency_value       = $this->escape_string($this->strip_all($calibration_frequency[$i]));
					$last_calibration_date_value       = $this->escape_string($this->strip_all($last_calibration_date[$i]));
					$next_calibration_date_value       = $this->escape_string($this->strip_all($next_calibration_date[$i]));
					$remark_value                      = $this->escape_string($this->strip_all($remark[$i]));


					if(isset($file['gauges_photo']['name'][$i]) && !empty($file['gauges_photo']['name'][$i])) {

						$storableLocation = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."inspection_gauges_component WHERE id = '".$inspect_gauges_component_id_value."' AND branch_id='".$branch_id."' "))['x'];
						// unlink($storableLocation);
			
						// $fileInfo  = pathinfo($file['gauges_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'inspectmasterimage/'.'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'inspectmasterimage/'.'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['gauges_photo']['tmp_name'][$i], $fileLocation);

						
						unlink($target.'/'.$storableLocation);

						$info         = pathinfo($file['gauges_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocations = 'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['gauges_photo']['tmp_name'][$i], $storableLocations);
			
					} else {
						
						$storableLocations = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."inspection_gauges_component WHERE id = '".$inspect_gauges_component_id_value."' AND branch_id='".$branch_id."' "))['x'];

					}

					if(isset($file['gauges_check_list']['name'][$i]) && !empty($file['gauges_check_list']['name'][$i])) {

						$storableLocation1 = $this->fetch($this->query("SELECT gauges_check_list x FROM ".PREFIX."inspection_gauges_component WHERE id = '".$inspect_gauges_component_id_value."' AND branch_id='".$branch_id."' "))['x'];
						// unlink($storableLocation1);
			
						// $fileInfo  = pathinfo($file['gauges_check_list']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation1 = 'inspectmasterimage/'.'Checklist_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation1 = 'inspectmasterimage/'.'Checklist_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['gauges_check_list']['tmp_name'][$i], $fileLocation1);

						unlink($target.'/'.$storableLocation1);

						$info         = pathinfo($file['gauges_check_list']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocations1 = 'Checklist_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['gauges_check_list']['tmp_name'][$i], $storableLocations1);

					} else {
						
						$storableLocations1 = $this->fetch($this->query("SELECT gauges_check_list x FROM ".PREFIX."inspection_gauges_component WHERE id = '".$inspect_gauges_component_id_value."' AND branch_id='".$branch_id."' "))['x'];

					}
				
					if ($inspect_gauges_component_id_value != '') {

						
						$updateIntorawMaterial = $this->query("UPDATE ".PREFIX."inspection_gauges_component SET product_name = '".$product_name."', component_name = '".$component_name."', process_name='".$process_name_value."',inspect_gauges='".$inspect_gauges_value."',gauges_number='".$gauges_number_value."',photo='".$storableLocations."',gauges_check_list='".$storableLocations1."',calibration_frequency='".$calibration_frequency_value."',last_calibration_date='".$last_calibration_date_value."',next_calibration_date='".$next_calibration_date_value."',remark='".$remark_value."',updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  inspect_gauges_id = '".$id."' AND id = '".$inspect_gauges_component_id_value."' AND branch_id='".$branch_id."' ");

					} else {


						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."inspection_gauges_component (branch_id,inspect_gauges_id, product_name, component_name, process_name, inspect_gauges, gauges_number, photo, gauges_check_list, calibration_frequency,last_calibration_date,next_calibration_date, remark,created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$product_name."', '".$component_name."' '".$process_name_value."','".$inspect_gauges_value."','".$gauges_number_value."','".$storableLocations."','".$storableLocations1."','".$calibration_frequency_value."','".$last_calibration_date_value."','".$next_calibration_date_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");

					}
					
				}

			}

			return $id;

		}


		//Inspect Gauges Master End


		// Fixture Master Start

		function addFixtureMaster($data,$file,$created_by,$branch_id)
		{

			$product_name   = $this->escape_string($this->strip_all($data['product_name']));
			$sub_sr         = $this->escape_string($this->strip_all($data['sub_sr']));
			$component_name = $this->escape_string($this->strip_all($data['component_name']));

			$insertData = $this->query("INSERT INTO ".PREFIX."fixture_master (branch_id,product_name, sub_sr,component_name, created_by, created_time) VALUES  ('".$branch_id."','".$product_name."','".$sub_sr."','".$component_name."','".$created_by."', '".CURRENTMILLIS."')");

			$last_id = $this->last_insert_id();

			$target='fixturemasterimage/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($data['process_name'])) {

				$process_name          = $data['process_name'];
				$fixture               = $data['fixture'];
				$fixture_number        = $data['fixture_number'];
				$calibration_frequency = $data['calibration_frequency'];
				$last_calibration_date = $data['last_calibration_date'];
				$next_calibration_date = $data['next_calibration_date'];
				$remark                = $data['remark'];
				$storableLocation      = "";
				$storableLocation1     = "";

				for ($i = 0; $i < count($data['process_name']); $i++) {
					$process_name_value          = $this->escape_string($this->strip_all($process_name[$i]));
					$fixture_value               = $this->escape_string($this->strip_all($fixture[$i]));
					$fixture_number_value        = $this->escape_string($this->strip_all($fixture_number[$i]));
					$calibration_frequency_value = $this->escape_string($this->strip_all($calibration_frequency[$i]));
					$last_calibration_date_value = $this->escape_string($this->strip_all($last_calibration_date[$i]));
					$next_calibration_date_value = $this->escape_string($this->strip_all($next_calibration_date[$i]));
					$remark_value                = $this->escape_string($this->strip_all($remark[$i]));


					if(isset($file['fixture_photo']['name'][$i]) && !empty($file['fixture_photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['fixture_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'fixturemasterimage/'.'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'fixturemasterimage/'.'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['fixture_photo']['tmp_name'][$i], $fileLocation);

						$info         = pathinfo($file['fixture_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation = 'photo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['fixture_photo']['tmp_name'][$i], $target.'/'.$storableLocation);
			
					}

					if(isset($file['fixture_check_list']['name'][$i]) && !empty($file['fixture_check_list']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['fixture_check_list']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation1 = 'fixturemasterimage/'.'FixtureList_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation1 = 'fixturemasterimage/'.'FixtureList_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['fixture_check_list']['tmp_name'][$i], $fileLocation1);

						$info         = pathinfo($file['fixture_check_list']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocation1 = 'FixtureList_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['fixture_check_list']['tmp_name'][$i], $target.'/'.$storableLocation);

					}  

					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."fixture_component (branch_id,fixture_id, process_name, fixture, fixture_number, photo, fixture_check_list, calibration_frequency,last_calibration_date,next_calibration_date, remark,created_by, created_time) VALUES ('".$branch_id."','".$last_id."', '".$process_name_value."','".$fixture_value."','".$fixture_number_value."','".$storableLocation."','".$storableLocation1."','".$calibration_frequency_value."','".$last_calibration_date_value."','".$next_calibration_date_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");

				}
			}
			
			return $last_id;
		}

		function getUniqueFixtureById($id)
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."fixture_master where id = '".$id."'");
			return $this->fetch($query);
			
		}

		function getUniqueFixtureComponentById($id)
		{
			
			$query = $this->query("SELECT * FROM ".PREFIX."fixture_component where fixture_id = '".$id."'");
			return $query;

		}


		function updateFixtureMaster($data,$file,$created_by,$branch_id)
		{
			$id             = $this->escape_string($this->strip_all($data['id']));
			$product_name   = $this->escape_string($this->strip_all($data['product_name']));
			$sub_sr         = $this->escape_string($this->strip_all($data['sub_sr']));
			$component_name = $this->escape_string($this->strip_all($data['component_name']));

			$updateData = $this->query("UPDATE ".PREFIX."fixture_master set product_name='".$product_name."', sub_sr = '".$sub_sr."', component_name = '".$component_name."' , updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");

			if(isset($data['process_name'])) {

				$fixture_component_id  = $data['fixture_component_id'];
				$process_name          = $data['process_name'];
				$fixture               = $data['fixture'];
				$fixture_number        = $data['fixture_number'];
				$calibration_frequency = $data['calibration_frequency'];
				$last_calibration_date = $data['last_calibration_date'];
				$next_calibration_date = $data['next_calibration_date'];
				$remark                = $data['remark'];
				$storableLocations      = "";
				$storableLocations1     = "";


				for ($i = 0; $i < count($data['process_name']); $i++) {
					$fixture_component_id_value  = $this->escape_string($this->strip_all($fixture_component_id[$i]));
					$process_name_value          = $this->escape_string($this->strip_all($process_name[$i]));
					$fixture_value               = $this->escape_string($this->strip_all($fixture[$i]));
					$fixture_number_value        = $this->escape_string($this->strip_all($fixture_number[$i]));
					$calibration_frequency_value = $this->escape_string($this->strip_all($calibration_frequency[$i]));
					$last_calibration_date_value = $this->escape_string($this->strip_all($last_calibration_date[$i]));
					$next_calibration_date_value = $this->escape_string($this->strip_all($next_calibration_date[$i]));
					$remark_value                = $this->escape_string($this->strip_all($remark[$i]));


					if(isset($file['fixture_photo']['name'][$i]) && !empty($file['fixture_photo']['name'][$i])) {

						$storableLocation = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."fixture_component WHERE id = '".$fixture_component_id_value."' AND branch_id='".$branch_id."' "))['x'];
						// unlink($storableLocation);
						// $fileInfo  = pathinfo($file['fixture_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'fixturemasterimage/'.'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'fixturemasterimage/'.'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['fixture_photo']['tmp_name'][$i], $fileLocation);

						unlink($target.'/'.$storableLocation);

						$info         = pathinfo($file['fixture_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocations = 'photo_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['fixture_photo']['tmp_name'][$i], $storableLocations);

			
					} else {
						
						$storableLocations = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."fixture_component WHERE id = '".$fixture_component_id_value."' AND branch_id='".$branch_id."' "))['x'];

					}

					if(isset($file['fixture_check_list']['name'][$i]) && !empty($file['fixture_check_list']['name'][$i])) {

						$storableLocation1 = $this->fetch($this->query("SELECT fixture_check_list x FROM ".PREFIX."fixture_component WHERE id = '".$fixture_component_id_value."' AND branch_id='".$branch_id."'"))['x'];
						// unlink($storableLocation1);

						// $fileInfo  = pathinfo($file['fixture_check_list']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation1 = 'fixturemasterimage/'.'FixtureList'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation1 = 'fixturemasterimage/'.'FixtureList'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['fixture_check_list']['tmp_name'][$i], $fileLocation1);

						unlink($target.'/'.$storableLocation1);

						$info         = pathinfo($file['fixture_check_list']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$storableLocations1 = 'FixtureList'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['fixture_check_list']['tmp_name'][$i], $storableLocations);

					}  else {
						
						$storableLocations1 = $this->fetch($this->query("SELECT fixture_check_list x FROM ".PREFIX."fixture_component WHERE id = '".$fixture_component_id_value."' AND branch_id='".$branch_id."'"))['x'];

					}
				
					if ($fixture_component_id_value != '') {

						$updateIntorawMaterial = $this->query("UPDATE ".PREFIX."fixture_component SET process_name='".$process_name_value."',fixture='".$fixture_value."',fixture_number='".$fixture_number_value."',photo='".$storableLocations."',fixture_check_list='".$storableLocations1."',calibration_frequency='".$calibration_frequency_value."',last_calibration_date='".$last_calibration_date_value."',next_calibration_date='".$next_calibration_date_value."',remark='".$remark_value."',updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  fixture_id = '".$id."' AND id = '".$fixture_component_id_value."' AND branch_id='".$branch_id."' ");

					} else {

						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."fixture_component (branch_id,fixture_id, process_name, fixture, fixture_number, photo, fixture_check_list, calibration_frequency,last_calibration_date,next_calibration_date, remark,created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$process_name_value."','".$fixture_value."','".$fixture_number_value."','".$storableLocations."','".$storableLocations1."','".$calibration_frequency_value."','".$last_calibration_date_value."','".$next_calibration_date_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");

					}
					
				}

			}

			return $id;

		}


		//Fixture Master End




		//Bom master start

		function getAllItem($data)
		{
			$itemGroup = $this->escape_string($this->strip_all($data['itemGroup']));

			$query = $this->query("SELECT * FROM ".PREFIX."item_master WHERE main_group_name = '".$itemGroup."' ");
			return $query;
		}

		function getItemFullName($data)
		{
			$itemShortName = $this->escape_string($this->strip_all($data['itemShortName']));

			$item_name = $this->fetch($this->query("SELECT item_name x FROM ".PREFIX."item_master WHERE id = '".$itemShortName."'"))['x'];
			return $item_name;
		}

		function getItemShortName($data)
		{
			$item_short_name = $this->escape_string($this->strip_all($data));

			$item_short_name = $this->fetch($this->query("SELECT item_short_name x FROM ".PREFIX."item_master WHERE id = '".$item_short_name."'"))['x'];
			return $item_short_name;
		}

		function getAllrawMaterialItem($data)
		{
			$boq_id = $this->escape_string($this->strip_all($data));
			$query = $this->query("SELECT DISTINCTROW(component) FROM ".PREFIX."boq_hardware_master WHERE boq_id  = '".$boq_id."' ");
			return $query;
		}

		function getAllHardwareItem()
		{
			$query = $this->query("SELECT * FROM ".PREFIX."item_master WHERE main_group_name = 'HARDWARE' ");
			return $query;
		}

		function getRawmaterialItem($data)
		{
			$itemName = $this->escape_string($this->strip_all($data['itemName']));

			$item_name = $this->fetch($this->query("SELECT * FROM ".PREFIX."item_master WHERE id = '".$itemName."'"));
			return $item_name;;
		}


		function addBoqMaster($data,$file,$created_by,$branch_id){

			$item_name         = $this->escape_string($this->strip_all($data['item_name']));
			$unq_com_code      = $this->escape_string($this->strip_all($data['unq_com_code']));
			$coated            = $this->escape_string($this->strip_all($data['coated']));
			$drawing_no        = $this->escape_string($this->strip_all($data['drawing_no']));
			$stock_norms_qty   = $this->escape_string($this->strip_all($data['stock_norms_qty']));
			$finish_weight_net = $this->escape_string($this->strip_all($data['finish_weight_net']));
			$product_sale_rate = $this->escape_string($this->strip_all($data['product_sale_rate']));
			
			$insertData = $this->query("INSERT INTO ".PREFIX."boq_master (branch_id,item_name, unq_com_code, coated, drawing_no, drawing_photo, product_photo, stock_norms_qty, finish_weight_net, product_sale_rate, created_by, created_time) VALUES  ('".$branch_id."','".$item_name."','".$unq_com_code."','".$coated."','".$drawing_no."','".$drawing_photo."','".$product_photo."','".$stock_norms_qty."','".$finish_weight_net."','".$product_sale_rate."','".$created_by."', '".CURRENTMILLIS."')");
			$last_id = $this->last_insert_id();

			$target='boqimage/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			$targets='boqvideo/'.$last_id;

			if (!file_exists($target)) {

				mkdir($target, 0777, true);

			}

			if(isset($file['drawing_photo']['name']) && !empty($file['drawing_photo']['name'])) {
				// $SaveImage = new SaveImage();
				// $imgDir = 'boqimage/';
				// if(isset($file['drawing_photo']['name']) && !empty($file['drawing_photo']['name'])){
				// 	$file_name = strtolower( pathinfo($file['drawing_photo']['name'], PATHINFO_FILENAME));
				// 	$cropData = $this->strip_all($data['cropData1']);
					
				// 	$drawing_photo = $SaveImage->uploadCroppedImageFileFromForm($file['drawing_photo'], 1366, $cropData, $imgDir, $file_name.'-'.time().'-1');
				// } else {
				// 	$drawing_photo = '';
				// }

				
				$info         = pathinfo($file['drawing_photo']['name'][$i]);
			
				$ext          = $info['extension'];
				
				$storableLocation = 'drawing_photo'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
				
				move_uploaded_file($file['drawing_photo']['tmp_name'], $target.'/'.$storableLocation);

				$query = "UPDATE ".PREFIX."boq_master SET drawing_photo = '".$storableLocation1."' WHERE id = '".$last_id."' ";

				$sql = $this->query($query);

			}

			if(isset($file['product_photo']['name']) && !empty($file['product_photo']['name'])) {
				// $SaveImage = new SaveImage();
				// $imgDir = 'boqimage/';
				// if(isset($file['product_photo']['name']) && !empty($file['product_photo']['name'])){
				// 	$file_name = strtolower( pathinfo($file['product_photo']['name'], PATHINFO_FILENAME));
				// 	$cropData = $this->strip_all($data['cropData1']);
					
				// 	$product_photo = $SaveImage->uploadCroppedImageFileFromForm($file['product_photo'], 1366, $cropData, $imgDir, $file_name.'-'.time().'-1');
				// } else {
				// 	$product_photo = '';
				// }

				$info         = pathinfo($file['product_photo']['name'][$i]);
			
				$ext          = $info['extension'];
				
				$storableLocation1 = 'product_photo'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
				
				move_uploaded_file($file['product_photo']['tmp_name'], $target.'/'.$storableLocation1);

				$query = "UPDATE ".PREFIX."boq_master SET product_photo = '".$storableLocation1."' WHERE id = '".$last_id."' ";

				$sql = $this->query($query);

			}


			if(isset($data['components'])) {

				$component            = $data['components'];
				$component_drawing_no = $data['component_drawing_no'];
				$qty_per_piece        = $data['qty_per_piece'];
				$net_weight           = $data['net_weight'];
				$category             = $data['category'];
				$component_remarks    = $data['component_remarks'];

				for ($i = 0; $i < count($data['components']); $i++) {
					$component_value            = $this->escape_string($this->strip_all($component[$i]));
					$component_drawing_no_value = $this->escape_string($this->strip_all($component_drawing_no[$i]));
					$qty_per_piece_value        = $this->escape_string($this->strip_all($qty_per_piece[$i]));
					$net_weight_value           = $this->escape_string($this->strip_all($net_weight[$i]));
					$category_value             = $this->escape_string($this->strip_all($category[$i]));
					$component_remarks_value    = $this->escape_string($this->strip_all($component_remarks[$i]));

					 $insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."boq_component_master (branch_id,boq_id,item_name,component_id, component, component_drawing_no, qty_per_piece, net_weight, category, component_remarks, created_by, created_time) VALUES 
					 ('".$branch_id."','".$last_id."','".$item_name."', '".$item_last_id."' ,'".$component_value."','".$component_drawing_no_value."','".$qty_per_piece_value."','".$net_weight_value."','".$category_value."','".$component_remarks_value."','".$created_by."', '".CURRENTMILLIS."')");

				}


			}


			if(isset($data['component_name'])) {
				$component_name = $data['component_name'];
				// $qty            = $data['qty'];
				$size           = $data['size'];
				$thikness       = $data['thikness'];
				$mate_type      = $data['mate_type'];
				$lenght         = $data['lenght'];
				$produce_qty    = $data['produce_qty'];
				$reqd_qty       = $data['reqd_qty'];
				$unit           = $data['unit'];
				$Purchase_from  = $data['Purchase_from'];
				$Purchase_rate  = $data['Purchase_rate'];
				$remark         = $data['remark'];
				$storableLocations  = "";

				for ($i = 0; $i < count($data['component_name']); $i++) {
					$component_name_value = $this->escape_string($this->strip_all($component_name[$i]));
					// $qty_value            = $this->escape_string($this->strip_all($qty[$i]));
					$size_value           = $this->escape_string($this->strip_all($size[$i]));
					$thikness_value       = $this->escape_string($this->strip_all($thikness[$i]));
					$mate_type_value      = $this->escape_string($this->strip_all($mate_type[$i]));
					$lenght_value         = $this->escape_string($this->strip_all($lenght[$i]));
					$produce_qty_value    = $this->escape_string($this->strip_all($produce_qty[$i]));
					$reqd_qty_value       = $this->escape_string($this->strip_all($reqd_qty[$i]));
					$unit_value           = $this->escape_string($this->strip_all($unit[$i]));
					$Purchase_from_value  = $this->escape_string($this->strip_all($Purchase_from[$i]));
					$Purchase_rate_value  = $this->escape_string($this->strip_all($Purchase_rate[$i]));
					$remark_value         = $this->escape_string($this->strip_all($remark[$i]));



					if(isset($file['photo']['name'][$i]) && !empty($file['photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'boqimage/'.$i.'_'.$component_name_value.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $storableLocation = 'boqimage/'.$i.'_'.$component_name_value.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['photo']['tmp_name'][$i], $fileLocation);
						
						$info         = pathinfo($file['photo']['name'][$i]);
					
						$ext          = $info['extension'];
						
						$storableLocations = $component_name_value.'_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['photo']['tmp_name'], $target.'/'.$storableLocations);
			
					} 

					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."boq_rawmaterial_master (branch_id,boq_id,item_name, component_name, photo, size, thikness, mate_type, lenght, produce_qty, reqd_qty, unit, Purchase_from, Purchase_rate,remark, created_by, created_time) VALUES ('".$branch_id."','".$last_id."', '".$item_name."','".$component_name_value."','".$storableLocations."','".$size_value."','".$thikness_value."','".$mate_type_value."','".$lenght_value."','".$produce_qty_value."','".$reqd_qty_value."','".$unit_value."','".$Purchase_from_value."','".$Purchase_rate_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");
				}
			}


			if(isset($data['component'])) {
				$component       = $data['component'];
				$open_no         = $data['open_no'];
				$open            = $data['open'];
				$tool_setup_time = $data['tool_setup_time'];
				$cycle_time      = $data['cycle_time'];
				$mc_req          = $data['mc_req'];
				$man_req         = $data['man_req'];
				$tool            = $data['tool'];
				$quality_gauge   = $data['quality_gauge'];
				$pokayoke        = $data['pokayoke'];
				$last_done_date  = $data['last_done_date'];
				$next_due_date   = $data['next_due_date'];
				$remark          = $data['remark'];

				
				$videoLocation  = "";
				$checkSheetLocation  = "";
				$checkSheetToolLocation = "";
				$checkSheetJigsLocation = "";
				$pokayokePhotoLocation = "";

				for ($i = 0; $i < count($data['component']); $i++) {
					$component_values = $this->escape_string($this->strip_all($component[$i]));
					$open_no_values = $this->escape_string($this->strip_all($open_no[$i]));
					$open_values = $this->escape_string($this->strip_all($open[$i]));
					$tool_setup_time_values = $this->escape_string($this->strip_all($tool_setup_time[$i]));
					$cycle_time_values = $this->escape_string($this->strip_all($cycle_time[$i]));
					$mc_req_values = $this->escape_string($this->strip_all($mc_req[$i]));
					$man_req_values = $this->escape_string($this->strip_all($man_req[$i]));
					$tool_values = $this->escape_string($this->strip_all($tool[$i]));
					$quality_gauge_values = $this->escape_string($this->strip_all($quality_gauge[$i]));
					$pokayoke_values = $this->escape_string($this->strip_all($pokayoke[$i]));
					$last_done_date_values = $this->escape_string($this->strip_all($last_done_date[$i]));
					$next_due_date_values = $this->escape_string($this->strip_all($next_due_date[$i]));
					$remark_values = $this->escape_string($this->strip_all($remark[$i]));

					if(isset($file['video']['name'][$i]) && !empty($file['video']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['video']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation = 'boqvideo/'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $videoLocation = 'boqvideo/'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['video']['tmp_name'][$i], $fileLocation);

						$info         = pathinfo($file['video']['name'][$i]);
					
						$ext          = $info['extension'];
						
						$videoLocation = 'boqvideo_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['video']['tmp_name'], $targets.'/'.$videoLocation);
			
					} 


					if(isset($file['check_sheet']['name'][$i]) && !empty($file['check_sheet']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['check_sheet']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation1 = 'boqimage/'.$i.'_'.'check_sheet_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $checkSheetLocation = 'boqimage/'.$i.'_'.'check_sheet_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['check_sheet']['tmp_name'][$i], $fileLocation1);

						$info         = pathinfo($file['check_sheet']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$checkSheetLocation = 'check_sheet'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['check_sheet']['tmp_name'], $target.'/'.$checkSheetLocation);
			
					} 

					if(isset($file['pokayoke_photo']['name'][$i]) && !empty($file['pokayoke_photo']['name'][$i])) {
			
						// $fileInfo  = pathinfo($file['pokayoke_photo']['name'][$i]);
			
						// $extension = $fileInfo['extension'];
			
						// $fileLocation4 = 'boqimage/'.$i.'_'.'pokayoke_photo_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// $pokayokePhotoLocation = 'boqimage/'.$i.'_'.'pokayoke_photo_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						// move_uploaded_file($file['pokayoke_photo']['tmp_name'][$i], $fileLocation4);

						$info         = pathinfo($file['pokayoke_photo']['name'][$i]);
			
						$ext          = $info['extension'];
						
						$pokayokePhotoLocation = 'pokayoke_photo'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$ext;
						
						move_uploaded_file($file['pokayoke_photo']['tmp_name'], $target.'/'.$pokayokePhotoLocation);
			
					}
					
					$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."boq_hardware_master (branch_id,boq_id, item_name,component, open_no, open, video, check_sheet,tool_setup_time, cycle_time, mc_req, man_req, tool, quality_gauge, pokayoke, pokayoke_photo, last_done_date, next_due_date, remark, created_by, created_time) VALUES ('".$branch_id."','".$last_id."', '".$item_name."','".$component_values."', '".$open_no_values."', '".$open_values."','".$videoLocation."', '".$checkSheetLocation."','".$tool_setup_time_values."', '".$cycle_time_values."','".$mc_req_values."', '".$man_req_values."','".$tool_values."','".$quality_gauge_values."','".$pokayoke_values."','".$pokayokePhotoLocation."','".$last_done_date_values."','".$next_due_date_values."','".$remark_values."','".$created_by."', '".CURRENTMILLIS."')");
					
				}
			}

			return $last_id;
		}
		

		function getUniqueBoqById($id) {
			$query = $this->query("SELECT * FROM ".PREFIX."boq_master where id = '".$id."'");
			return $this->fetch($query);
		}

		function getUniqueBoqRawmaterialById($id){
			$query = $this->query("SELECT * FROM ".PREFIX."boq_rawmaterial_master where boq_id = '".$id."' ORDER BY id ASC");
			return $query;
		}

		function getUniqueBoqComponentById($id){
			$query = $this->query("SELECT * FROM ".PREFIX."boq_component_master where boq_id = '".$id."' ORDER BY id ASC");
			return $query;
		}

		function getUniqueBoqHardwareById($id){
			$query = $this->query("SELECT * FROM ".PREFIX."boq_hardware_master where boq_id = '".$id."' ORDER BY id ASC");
			return $query;
		}

		function getComponentHardwareName($id){
			$query = $this->query("SELECT component FROM ".PREFIX."boq_hardware_master where boq_id = '".$id."' ORDER BY id ASC");
			return $query;
		}

		function getcomponentName($id){
			$query = $this->query("SELECT component_name FROM ".PREFIX."boq_rawmaterial_master where boq_id = '".$id."' ORDER BY id ASC");
			return $query;
		}

		function getUniqueBoqHardwaretoolById($id)
		{
			$tool = $this->fetch($this->query("SELECT tool x FROM ".PREFIX."boq_hardware_master where boq_id = '".$id."' "))['x'];
			return $tool;
		}

		function updateBoqMaster($data,$file,$created_by,$branch_id){
			$id                = $this->escape_string($this->strip_all($data['id']));
			$item_name         = $this->escape_string($this->strip_all($data['item_name']));
			$unq_com_code      = $this->escape_string($this->strip_all($data['unq_com_code']));
			$coated            = $this->escape_string($this->strip_all($data['coated']));
			$drawing_no        = $this->escape_string($this->strip_all($data['drawing_no']));
			$stock_norms_qty   = $this->escape_string($this->strip_all($data['stock_norms_qty']));
			$finish_weight_net = $this->escape_string($this->strip_all($data['finish_weight_net']));
			$product_sale_rate = $this->escape_string($this->strip_all($data['product_sale_rate']));

			

			if(isset($file['drawing_photo']['name']) && !empty($file['drawing_photo']['name'])) {
				$SaveImage = new SaveImage();
				$imgDir = 'boqimage/';
				$Detail = $this->getUniqueSliderBannerById($id);
				$cropData = $this->strip_all($data['cropData1']);
				$file_name = strtolower( pathinfo($file['drawing_photo']['name'], PATHINFO_FILENAME));
				$this->unlinkImage($Detail['drawing_photo'], "crop");
				$image_name = $SaveImage->uploadCroppedImageFileFromForm($file['drawing_photo'], 1366, $cropData, $imgDir, $file_name.'-'.time().'-1');
				$sql="update ".PREFIX."boq_master set drawing_photo='$image_name' where id='$id'";
				// //echo $sql; 
				$this->query($sql);
				
			}

			if(isset($file['product_photo']['name']) && !empty($file['product_photo']['name'])) {
				$SaveImage = new SaveImage();
				$imgDir = 'boqimage/';
				$Detail = $this->getUniqueSliderBannerById($id);
				$cropData = $this->strip_all($data['cropData1']);
				$file_name = strtolower( pathinfo($file['product_photo']['name'], PATHINFO_FILENAME));
				$this->unlinkImage($Detail['product_photo'], "crop");
				$image_name = $SaveImage->uploadCroppedImageFileFromForm($file['product_photo'], 1366, $cropData, $imgDir, $file_name.'-'.time().'-1');
				$sql="update ".PREFIX."boq_master set product_photo='$image_name' where id='$id'";
				// //echo $sql; 
				$this->query($sql);
				
			}
			
	
			$updateData = $this->query("UPDATE ".PREFIX."boq_master set item_name='".$item_name."', unq_com_code = '".$unq_com_code."',coated='".$coated."',drawing_no='".$drawing_no."',stock_norms_qty='".$stock_norms_qty."',finish_weight_net='".$finish_weight_net."',product_sale_rate='".$product_sale_rate."' , updated_by='".$created_by."', updated_time='".CURRENTMILLIS."' WHERE id='".$id."' AND branch_id='".$branch_id."' ");


			if(isset($data['components'])) {

				$component            = $data['components'];
				$componentNameid      = $data['componentNameid'];
				$component_drawing_no = $data['component_drawing_no'];
				$qty_per_piece        = $data['qty_per_piece'];
				$net_weight           = $data['net_weight'];
				$category             = $data['category'];
				$component_remarks    = $data['component_remarks'];
				$storableLocation  = "";

				for ($i = 0; $i < count($data['components']); $i++) {
					$component_value            = $this->escape_string($this->strip_all($component[$i]));
					$componentNameid_value      = $this->escape_string($this->strip_all($componentNameid[$i]));
					$component_drawing_no_value = $this->escape_string($this->strip_all($component_drawing_no[$i]));
					$qty_per_piece_value        = $this->escape_string($this->strip_all($qty_per_piece[$i]));
					$net_weight_value           = $this->escape_string($this->strip_all($net_weight[$i]));
					$category_value             = $this->escape_string($this->strip_all($category[$i]));
					$component_remarks_value    = $this->escape_string($this->strip_all($component_remarks[$i]));

					$componentNames = $this->fetch($this->query("SELECT component x FROM ".PREFIX."boq_component_master WHERE  boq_id = '".$id."' AND id = '".$componentNameid_value."' AND branch_id='".$branch_id."' AND item_name='".$item_name."' "))['x'];

					if ($componentNames != '') {
						
						$item_names = $this->fetch($this->query("SELECT item_name  x FROM ".PREFIX."item_master WHERE component_name='".$item_name."' AND branch_id='".$branch_id."' AND item_name ='".$componentNames."'"))['x'];


						if ($item_names == '') {
	
							$insertItemMaster = $this->query("INSERT INTO ".PREFIX."item_master (branch_id,main_group, component_name, item_name, description, stock_ind, class, created_by, created_time) VALUES ('".$branch_id."','Component','".$item_name."','".$component_value."','".$component_value."','stock','C','".$created_by."', '".CURRENTMILLIS."')");
							$item_last_id = $this->last_insert_id();

						} else {
	
							$updateItemMaster = $this->query("UPDATE ".PREFIX."item_master SET   item_name = '".$component_value."', updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE branch_id='".$branch_id."' AND component_name='".$item_name."' AND main_group='Component' AND item_name ='".$componentNames."' ");
							$item_last_id = $this->fetch($this->query("SELECT id  x FROM ".PREFIX."item_master WHERE component_name='".$item_name."' AND branch_id='".$branch_id."' AND item_name ='".$componentNames."'"))['x'];
						}

					} else {

						$item_names = $this->fetch($this->query("SELECT item_name  x FROM ".PREFIX."item_master WHERE component_name='".$item_name."' AND branch_id='".$branch_id."' AND item_name ='".$component_value."'"))['x'];

						if ($item_names == '') {
	
							$insertItemMaster = $this->query("INSERT INTO ".PREFIX."item_master (branch_id,main_group, component_name, item_name, description, stock_ind, class, created_by, created_time) VALUES ('".$branch_id."','Component','".$item_name."','".$component_value."','".$component_value."','stock','C','".$created_by."', '".CURRENTMILLIS."')");
							$item_last_id = $this->last_insert_id();

						} else {
	
							$updateItemMaster = $this->query("UPDATE ".PREFIX."item_master SET   item_name = '".$component_value."', updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE branch_id='".$branch_id."' AND component_name='".$item_name."' AND main_group='Component' AND item_name ='".$componentNames."' ");
							$item_last_id = $this->fetch($this->query("SELECT id  x FROM ".PREFIX."item_master WHERE component_name='".$item_name."' AND branch_id='".$branch_id."' AND item_name ='".$componentNames."'"))['x'];
						}

					}


					if ($componentNameid_value != '') {
						
						$UpdateComponentMaster = $this->query("UPDATE ".PREFIX."boq_component_master SET item_name='".$item_name."',component_id = '".$item_last_id."' , component='".$component_value."',component_drawing_no='".$component_drawing_no_value."',qty_per_piece='".$qty_per_piece_value."',net_weight='".$net_weight_value."',category='".$category_value."',component_remarks='".$component_remarks_value."',updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  boq_id = '".$id."' AND id = '".$componentNameid_value."' AND branch_id='".$branch_id."' ");

					} else {

						$insertComponentMaster = $this->query("INSERT INTO ".PREFIX."boq_component_master (branch_id,boq_id,item_name, component_id , component, component_drawing_no, qty_per_piece, net_weight, category, component_remarks, created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$item_name."','".$item_last_id."','".$component_value."','".$component_drawing_no_value."','".$qty_per_piece_value."','".$net_weight_value."','".$category_value."','".$component_remarks_value."','".$created_by."', '".CURRENTMILLIS."')");

					}

				}

				// die();

			}


			if(isset($data['component_name'])) {
				$component_id   = $data['component_id'];
				$component_name = $data['component_name'];
				// $qty            = $data['qty'];
				$size           = $data['size'];
				$thikness       = $data['thikness'];
				$mate_type      = $data['mate_type'];
				$lenght         = $data['lenght'];
				$produce_qty    = $data['produce_qty'];
				$reqd_qty       = $data['reqd_qty'];
				$unit           = $data['unit'];
				$Purchase_from  = $data['Purchase_from'];
				$Purchase_rate  = $data['Purchase_rate'];
				$remark         = $data['remark'];
				$storableLocation  = "";

				for ($i = 0; $i < count($data['component_name']); $i++) {
					$component_id_value   = $this->escape_string($this->strip_all($component_id[$i]));
					$component_name_value = $this->escape_string($this->strip_all($component_name[$i]));
					// $qty_value            = $this->escape_string($this->strip_all($qty[$i]));
					$size_value           = $this->escape_string($this->strip_all($size[$i]));
					$thikness_value       = $this->escape_string($this->strip_all($thikness[$i]));
					$mate_type_value      = $this->escape_string($this->strip_all($mate_type[$i]));
					$lenght_value         = $this->escape_string($this->strip_all($lenght[$i]));
					$produce_qty_value    = $this->escape_string($this->strip_all($produce_qty[$i]));
					$reqd_qty_value       = $this->escape_string($this->strip_all($reqd_qty[$i]));
					$unit_value           = $this->escape_string($this->strip_all($unit[$i]));
					$Purchase_from_value  = $this->escape_string($this->strip_all($Purchase_from[$i]));
					$Purchase_rate_value  = $this->escape_string($this->strip_all($Purchase_rate[$i]));
					$remark_value  = $this->escape_string($this->strip_all($remark[$i]));



					if(isset($file['photo']['name'][$i]) && !empty($file['photo']['name'][$i])) {

						$storableLocations = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."boq_rawmaterial_master WHERE id = '".$component_id_value."' AND branch_id ='".$branch_id."' "))['x'];
						unlink($storableLocations);
			
						$fileInfo  = pathinfo($file['photo']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation = 'boqimage/'.$i.'_'.$component_name_value.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$storableLocation = 'boqimage/'.$i.'_'.$component_name_value.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['photo']['tmp_name'][$i], $fileLocation);
			
					} else {
						
						$storableLocation = $this->fetch($this->query("SELECT photo x FROM ".PREFIX."boq_rawmaterial_master WHERE id = '".$component_id_value."' AND branch_id ='".$branch_id."' "))['x'];

					}
					
					if ($component_id_value != '') {
						
						$updateIntorawMaterial = $this->query("UPDATE ".PREFIX."boq_rawmaterial_master SET component_name='".$component_name_value."',photo='".$storableLocation."',size='".$size_value."',thikness='".$thikness_value."',mate_type='".$mate_type_value."',lenght='".$lenght_value."',produce_qty='".$produce_qty_value."',reqd_qty='".$reqd_qty_value."',unit='".$unit_value."',Purchase_from = '".$Purchase_from_value."', Purchase_rate = '".$Purchase_rate_value."',remark= '".$remark_value."',updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  boq_id = '".$id."' AND id = '".$component_id_value."'");

					} else {

						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."boq_rawmaterial_master (branch_id,boq_id, component_name, photo, size, thikness, mate_type, lenght, produce_qty, reqd_qty, unit, Purchase_from, Purchase_rate,remark, created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$component_name_value."','".$storableLocation."','".$size_value."','".$thikness_value."','".$mate_type_value."','".$lenght_value."','".$produce_qty_value."','".$reqd_qty_value."','".$unit_value."','".$Purchase_from_value."','".$Purchase_rate_value."','".$remark_value."','".$created_by."', '".CURRENTMILLIS."')");

					}
					
				}
			}


			if(isset($data['component'])) {
				$components            = $data['component'];
				$hardware_component_id = $data['hardware_component_id'];
				$open_no               = $data['open_no'];
				$open                  = $data['open'];
				$tool_setup_time       = $data['tool_setup_time'];
				$cycle_time            = $data['cycle_time'];
				$mc_req                = $data['mc_req'];
				$man_req               = $data['man_req'];
				$tool                  = $data['tool'];
				$quality_gauge         = $data['quality_gauge'];
				$pokayoke              = $data['pokayoke'];
				$last_done_date        = $data['last_done_date'];
				$next_due_date         = $data['next_due_date'];
				$remark                = $data['remark'];

				$videoLocation  = "";
				$checkSheetLocation  = "";
				$checkSheetToolLocation = "";
				$checkSheetJigsLocation = "";
				$pokayokePhotoLocation = "";

				for ($i = 0; $i < count($data['component']); $i++) {
					$hardware_component_id_values = $this->escape_string($this->strip_all($hardware_component_id[$i]));
					$component_values = $this->escape_string($this->strip_all($components[$i]));
					$open_no_values = $this->escape_string($this->strip_all($open_no[$i]));
					$open_values = $this->escape_string($this->strip_all($open[$i]));
					$tool_setup_time_values = $this->escape_string($this->strip_all($tool_setup_time[$i]));
					$cycle_time_values = $this->escape_string($this->strip_all($cycle_time[$i]));
					$mc_req_values = $this->escape_string($this->strip_all($mc_req[$i]));
					$man_req_values = $this->escape_string($this->strip_all($man_req[$i]));
					$tool_values = $this->escape_string($this->strip_all($tool[$i]));
					$quality_gauge_values = $this->escape_string($this->strip_all($quality_gauge[$i]));
					$pokayoke_values = $this->escape_string($this->strip_all($pokayoke[$i]));
					$last_done_date_values = $this->escape_string($this->strip_all($last_done_date[$i]));
					$next_due_date_values = $this->escape_string($this->strip_all($next_due_date[$i]));
					$remark_values = $this->escape_string($this->strip_all($remark[$i]));

					if(isset($file['video']['name'][$i]) && !empty($file['video']['name'][$i])) {

						$videoLocations = $this->fetch($this->query("SELECT video x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."' AND branch_id ='".$branch_id."' "))['x'];
						
						unlink($videoLocations);
			
						$fileInfo  = pathinfo($file['video']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation = 'boqvideo/'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$videoLocation = 'boqvideo/'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['video']['tmp_name'][$i], $fileLocation);
			
					}  else {
						
						$videoLocation = $this->fetch($this->query("SELECT video x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."' AND branch_id ='".$branch_id."' "))['x'];

					}

					

					if(isset($file['check_sheet']['name'][$i]) && !empty($file['check_sheet']['name'][$i])) {

						$checkSheetLocations = $this->fetch($this->query("SELECT check_sheet x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."' AND branch_id ='".$branch_id."' "))['x'];

						unlink($checkSheetLocations);
			
						$fileInfo  = pathinfo($file['check_sheet']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation1 = 'boqimage/'.$i.'_'.'check_sheet_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$checkSheetLocation = 'boqimage/'.$i.'_'.'check_sheet_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['check_sheet']['tmp_name'][$i], $fileLocation1);
			
					} else {
						
						$checkSheetLocation = $this->fetch($this->query("SELECT check_sheet x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."' AND branch_id ='".$branch_id."' "))['x'];

					}

					// if(isset($file['check_sheet_tool']['name'][$i]) && !empty($file['check_sheet_tool']['name'][$i])) {
			
					// 	$fileInfo  = pathinfo($file['check_sheet_tool']['name'][$i]);
			
					// 	$extension = $fileInfo['extension'];
			
					// 	$fileLocation2 = 'boqimage/'.$i.'_'.'check_sheet_tool_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
					// 	$checkSheetToolLocation = 'boqimage/'.$i.'_'.'check_sheet_tool_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
					// 	move_uploaded_file($file['check_sheet_tool']['tmp_name'][$i], $fileLocation2);
			
					// } else {
						
					// 	$checkSheetToolLocation = $this->fetch($this->query("SELECT check_sheet_tool x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."'"))['x'];

					// }

					// if(isset($file['check_sheet_jigs']['name'][$i]) && !empty($file['check_sheet_jigs']['name'][$i])) {
			
					// 	$fileInfo  = pathinfo($file['check_sheet_jigs']['name'][$i]);
			
					// 	$extension = $fileInfo['extension'];
			
					// 	$fileLocation3 = 'boqimage/'.$i.'_'.'check_sheet_jigs_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
					// 	$checkSheetJigsLocation = 'boqimage/'.$i.'_'.'check_sheet_jigs_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
					// 	move_uploaded_file($file['check_sheet_jigs']['tmp_name'][$i], $fileLocation3);
			
					// } else {
						
					// 	$checkSheetJigsLocation = $this->fetch($this->query("SELECT check_sheet_jigs x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."'"))['x'];

					// }


					if(isset($file['pokayoke_photo']['name'][$i]) && !empty($file['pokayoke_photo']['name'][$i])) {

						$pokayokePhotoLocations = $this->fetch($this->query("SELECT pokayoke_photo x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."' AND branch_id ='".$branch_id."' "))['x'];
						unlink($pokayokePhotoLocations);
			
						$fileInfo  = pathinfo($file['pokayoke_photo']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation4 = 'boqimage/'.$i.'_'.'pokayoke_photo_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$pokayokePhotoLocation = 'boqimage/'.$i.'_'.'pokayoke_photo_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['pokayoke_photo']['tmp_name'][$i], $fileLocation4);
			
					} else {
						
						$pokayokePhotoLocation = $this->fetch($this->query("SELECT pokayoke_photo x FROM ".PREFIX."boq_hardware_master WHERE id = '".$hardware_component_id_values."' AND branch_id ='".$branch_id."' "))['x'];

					}


					if ($hardware_component_id_values != '') {
						
						$updateIntohardware = $this->query("UPDATE ".PREFIX."boq_hardware_master SET component='".$component_values."',open_no='".$open_no_values."',open='".$open_values."',video='".$videoLocation."',check_sheet='".$checkSheetLocation."',tool_setup_time='".$tool_setup_time_values."',cycle_time='".$cycle_time_values."',mc_req='".$mc_req_values."',man_req='".$man_req_values."',tool='".$tool_values."',quality_gauge = '".$quality_gauge_values."',pokayoke='".$pokayoke_values."',pokayoke_photo='".$pokayokePhotoLocation."',last_done_date ='".$last_done_date_values."',next_due_date='".$next_due_date_values."',remark='".$remark_values."', updated_by='".$created_by."',updated_time= '".CURRENTMILLIS."' WHERE  boq_id = '".$id."' AND id = '".$hardware_component_id_values."' AND branch_id='".$branch_id."' ");

					} else {

						$insertIntoDetailsTable = $this->query("INSERT INTO ".PREFIX."boq_hardware_master (branch_id,boq_id, component, open_no, open, video, check_sheet,tool_setup_time, cycle_time, mc_req, man_req, tool, quality_gauge, pokayoke, pokayoke_photo, last_done_date, next_due_date, remark, created_by, created_time) VALUES ('".$branch_id."','".$id."', '".$component_values."', '".$open_no_values."', '".$open_values."','".$videoLocation."', '".$checkSheetLocation."','".$tool_setup_time_values."', '".$cycle_time_values."','".$mc_req_values."', '".$man_req_values."','".$tool_values."','".$quality_gauge_values."','".$pokayoke_values."','".$pokayokePhotoLocation."','".$last_done_date_values."','".$next_due_date_values."','".$remark_values."','".$created_by."', '".CURRENTMILLIS."')");
					}
					
					
					
				}

			}

			return $id;

		}

		//Bom Master End 

		// PPEP Master Start

		function addPpepMaster($data,$file,$created_by)
		{

			$product_name  = $this->escape_string($this->strip_all($data['product_name']));
			$control_chart = "";
			$pdir          = "";

			if(isset($file['control_chart']['name']) && !empty($file['control_chart']['name'])) {
			
				$fileInfo  = pathinfo($file['control_chart']['name']);
	
				$extension = $fileInfo['extension'];
	
				$fileLocation = 'ppepimage/'.'controlChart_'.CURRENTMILLIS.'.'.$extension;
	
				$control_chart = 'ppepimage/'.'controlChart_'.CURRENTMILLIS.'.'.$extension;
	
				move_uploaded_file($file['control_chart']['tmp_name'], $fileLocation);
	
			}

			if(isset($file['pdir']['name']) && !empty($file['pdir']['name'])) {
			
				$fileInfo  = pathinfo($file['pdir']['name']);
	
				$extension = $fileInfo['extension'];
	
				$fileLocation1 = 'ppepimage/'.'pdir'.CURRENTMILLIS.'.'.$extension;
	
				$pdir = 'ppepimage/'.'pdir'.CURRENTMILLIS.'.'.$extension;
	
				move_uploaded_file($file['pdir']['tmp_name'], $fileLocation1);
	
			}


			$insertData = $this->query("INSERT INTO ".PREFIX."ppep_master (product_name,control_chart,pdir,created_by,created_time) VALUES ('".$product_name."','".$control_chart."','".$pdir."','".$created_by."','".CURRENTMILLIS."')");

			$last_id = $this->last_insert_id();

			if(isset($data['process'])) {

				$process = $data['process'];
				$remark  = $data['remark'];

				$sop = "";
				$check_list = "";
				$online_qa_graph = "";
				$list_pokayoke = "";
				$training_sheet = "";


				for ($i = 0; $i < count($data['process']); $i++) {

					$process_values = $this->escape_string($this->strip_all($process[$i]));
					$remark_values = $this->escape_string($this->strip_all($remark[$i]));

					if(isset($file['sop']['name'][$i]) && !empty($file['sop']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['sop']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation2 = 'ppepimage/'.'sop_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$sop = 'ppepimage/'.'sop_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['sop']['tmp_name'][$i], $fileLocation2);
			
					}

					if(isset($file['check_list']['name'][$i]) && !empty($file['check_list']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['check_list']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation3 = 'ppepimage/'.'check_list_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$check_list = 'ppepimage/'.'check_list_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['check_list']['tmp_name'][$i], $fileLocation3);
			
					}

					if(isset($file['online_qa_graph']['name'][$i]) && !empty($file['online_qa_graph']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['online_qa_graph']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation4 = 'ppepimage/'.'online_qa_graph_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$online_qa_graph = 'ppepimage/'.'online_qa_graph_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['online_qa_graph']['tmp_name'][$i], $fileLocation4);
			
					}

					if(isset($file['list_pokayoke']['name'][$i]) && !empty($file['list_pokayoke']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['list_pokayoke']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation5 = 'ppepimage/'.'list_pokayoke_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$list_pokayoke = 'ppepimage/'.'list_pokayoke_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['list_pokayoke']['tmp_name'][$i], $fileLocation5);
			
					}

					if(isset($file['training_sheet']['name'][$i]) && !empty($file['training_sheet']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['training_sheet']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation6 = 'ppepimage/'.'training_sheet_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$training_sheet = 'ppepimage/'.'training_sheet_'.$i.'_'.$last_id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['training_sheet']['tmp_name'][$i], $fileLocation6);
			
					}

					$insertData = $this->query("INSERT INTO ".PREFIX."ppep_componet (product_id,process,sop,check_list,online_qa_graph,list_pokayoke,training_sheet,remark,created_by,created_time) VALUES ('".$last_id."','".$process_values."','".$sop."','".$check_list."','".$online_qa_graph."','".$list_pokayoke."','".$training_sheet."','".$remark_values."','".$created_by."','".CURRENTMILLIS."') ");

				}


			}


			return $last_id;

		}

		// PPEP Master End


		function getUniquePpepById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."ppep_master WHERE id = '".$id."'");
			return $this->fetch($query);
		}

		function getUniquePpepComponentById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."ppep_componet WHERE product_id = '".$id."'");
			return $query;
		}

		function updatePpepMaster($data,$file,$created_by)
		{
			$id  = $this->escape_string($this->strip_all($data['id']));
			$product_name  = $this->escape_string($this->strip_all($data['product_name']));
			$control_chart = "";
			$pdir          = "";

			if(isset($file['control_chart']['name']) && !empty($file['control_chart']['name'])) {
			
				$fileInfo  = pathinfo($file['control_chart']['name']);
	
				$extension = $fileInfo['extension'];
	
				$fileLocation = 'ppepimage/'.'controlChart_'.CURRENTMILLIS.'.'.$extension;
	
				$control_chart = 'ppepimage/'.'controlChart_'.CURRENTMILLIS.'.'.$extension;
	
				move_uploaded_file($file['control_chart']['tmp_name'], $fileLocation);
	
			} else {
				
				$control_chart = $this->fetch($this->query("SELECT control_chart x FROM ".PREFIX."ppep_master WHERE id = '".$id."'"))['x'];

			}

			if(isset($file['pdir']['name']) && !empty($file['pdir']['name'])) {
			
				$fileInfo  = pathinfo($file['pdir']['name']);
	
				$extension = $fileInfo['extension'];
	
				$fileLocation1 = 'ppepimage/'.'pdir'.CURRENTMILLIS.'.'.$extension;
	
				$pdir = 'ppepimage/'.'pdir'.CURRENTMILLIS.'.'.$extension;
	
				move_uploaded_file($file['pdir']['tmp_name'], $fileLocation1);
	
			} else {
				
				$pdir = $this->fetch($this->query("SELECT pdir x FROM ".PREFIX."ppep_master WHERE id = '".$id."'"))['x'];

			}


			$UpdateData = $this->query("UPDATE ".PREFIX."ppep_master SET product_name='".$product_name."',control_chart='".$control_chart."',pdir='".$pdir."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."' WHERE  id = '".$id."'");

			if(isset($data['process'])) {

				$process           = $data['process'];
				$ppep_component_id = $data['ppep_component_id'];
				$remark            = $data['remark'];

				$sop = "";
				$check_list = "";
				$online_qa_graph = "";
				$list_pokayoke = "";
				$training_sheet = "";


				for ($i = 0; $i < count($data['process']); $i++) {

					$ppep_component_id_values = $this->escape_string($this->strip_all($ppep_component_id[$i]));
					$process_values = $this->escape_string($this->strip_all($process[$i]));
					$remark_values = $this->escape_string($this->strip_all($remark[$i]));

					if(isset($file['sop']['name'][$i]) && !empty($file['sop']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['sop']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation2 = 'ppepimage/'.'sop_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$sop = 'ppepimage/'.'sop_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['sop']['tmp_name'][$i], $fileLocation2);
			
					} else {
						
						$sop = $this->fetch($this->query("SELECT sop x FROM ".PREFIX."ppep_componet WHERE id = '".$ppep_component_id_values."'"))['x'];
	
					}

					if(isset($file['check_list']['name'][$i]) && !empty($file['check_list']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['check_list']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation3 = 'ppepimage/'.'check_list_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$check_list = 'ppepimage/'.'check_list_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['check_list']['tmp_name'][$i], $fileLocation3);
			
					} else {
						
						$check_list = $this->fetch($this->query("SELECT check_list x FROM ".PREFIX."ppep_componet WHERE id = '".$ppep_component_id_values."'"))['x'];
	
					}

					if(isset($file['online_qa_graph']['name'][$i]) && !empty($file['online_qa_graph']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['online_qa_graph']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation4 = 'ppepimage/'.'online_qa_graph_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$online_qa_graph = 'ppepimage/'.'online_qa_graph_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['online_qa_graph']['tmp_name'][$i], $fileLocation4);
			
					} else {
						
						$online_qa_graph = $this->fetch($this->query("SELECT online_qa_graph x FROM ".PREFIX."ppep_componet WHERE id = '".$ppep_component_id_values."'"))['x'];
	
					}

					if(isset($file['list_pokayoke']['name'][$i]) && !empty($file['list_pokayoke']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['list_pokayoke']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation5 = 'ppepimage/'.'list_pokayoke_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$list_pokayoke = 'ppepimage/'.'list_pokayoke_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['list_pokayoke']['tmp_name'][$i], $fileLocation5);
			
					} else {
						
						$list_pokayoke = $this->fetch($this->query("SELECT list_pokayoke x FROM ".PREFIX."ppep_componet WHERE id = '".$ppep_component_id_values."'"))['x'];
	
					}

					if(isset($file['training_sheet']['name'][$i]) && !empty($file['training_sheet']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['training_sheet']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation6 = 'ppepimage/'.'training_sheet_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$training_sheet = 'ppepimage/'.'training_sheet_'.$i.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['training_sheet']['tmp_name'][$i], $fileLocation6);
			
					} else {
						
						$training_sheet = $this->fetch($this->query("SELECT training_sheet x FROM ".PREFIX."ppep_componet WHERE id = '".$ppep_component_id_values."'"))['x'];
	
					}

					if ($ppep_component_id_values != '') {

						$updateData = $this->query("UPDATE ".PREFIX."ppep_componet SET process = '".$process_values."', sop='".$sop."',check_list='".$check_list."',online_qa_graph='".$online_qa_graph."',list_pokayoke='".$list_pokayoke."',training_sheet='".$training_sheet."',remark='".$remark_values."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."'");
						
					} else {

						$insertData = $this->query("INSERT INTO ".PREFIX."ppep_componet (product_id,process,sop,check_list,online_qa_graph,list_pokayoke,training_sheet,remark,created_by,created_time) VALUES ('".$id."','".$process_values."','".$sop."','".$check_list."','".$online_qa_graph."','".$list_pokayoke."','".$training_sheet."','".$remark_values."','".$created_by."','".CURRENTMILLIS."') ");
						
					}

				}


			}

			return $id;


		}


		//---------------------------Order Tracking Start----------------------------------------//

		//Order Booking Start

		function getAllCustomerName($branch_id)
		{
			
			$query = $this->query("SELECT id,customer_short_name FROM  ".PREFIX."customer_master WHERE branch_id = '".$branch_id."' ");

			return $query;

		}

		function getAllboq($branch_id)
		{
			
			$query = $this->query("SELECT id,item_name FROM  ".PREFIX."boq_master WHERE branch_id = '".$branch_id."'");

			return $query;

		}

		function addOrderBook($data,$created_by,$branch_id)
		{
			$customer_name   = $this->escape_string($this->strip_all($data['customer_name']));
			$po_no           = $this->escape_string($this->strip_all($data['po_no']));
			$project_no      = $this->escape_string($this->strip_all($data['project_no']));
			$item_name       = $this->escape_string($this->strip_all($data['item_name']));
			$item_code       = $this->escape_string($this->strip_all($data['item_code']));
			$booking_date    = $this->escape_string($this->strip_all($data['booking_date']));
			$target_date     = $this->escape_string($this->strip_all($data['target_date']));
			$order_qty       = $this->escape_string($this->strip_all($data['order_qty']));
			$order_unit      = $this->escape_string($this->strip_all($data['order_unit']));
			$pc_shade        = $this->escape_string($this->strip_all($data['pc_shade']));
			$customer_remark = $this->escape_string($this->strip_all($data['customer_remark']));
			$category        = $this->escape_string($this->strip_all($data['category']));
			$delivery_site   = $this->escape_string($this->strip_all($data['delivery_site']));
			$order_status    = $this->escape_string($this->strip_all($data['order_status']));

			
			$query = $this->query("INSERT INTO ".PREFIX."order_booking (branch_id,customer_name, po_no, project_no, item_name, item_code, booking_date, target_date, order_qty,order_unit, pc_shade, customer_remark, category,delivery_site,order_status, created_by,created_time) values ('".$branch_id."','".$customer_name."','".$po_no."','".$project_no."','".$item_name."','".$item_code."','".$booking_date."','".$target_date."','".$order_qty."','".$order_unit."','".$pc_shade."','".$customer_remark."','".$category."','".$delivery_site."','".$order_status."','".$created_by."', '".CURRENTMILLIS."')");

			return $query;
			
		}

		function getUniqueOrderBookId($id){
			$query = $this->query("SELECT * FROM ".PREFIX."order_booking where id = '".$id."' ORDER BY id ASC");
			return $this->fetch($query);
		}

		function updateOrderBook($data,$created_by,$branch_id)
		{
			
			$id              = $this->escape_string($this->strip_all($data['id']));
			$customer_name   = $this->escape_string($this->strip_all($data['customer_name']));
			$po_no           = $this->escape_string($this->strip_all($data['po_no']));
			$project_no      = $this->escape_string($this->strip_all($data['project_no']));
			$item_name       = $this->escape_string($this->strip_all($data['item_name']));
			$item_code       = $this->escape_string($this->strip_all($data['item_code']));
			$booking_date    = $this->escape_string($this->strip_all($data['booking_date']));
			$target_date     = $this->escape_string($this->strip_all($data['target_date']));
			$order_qty       = $this->escape_string($this->strip_all($data['order_qty']));
			$order_unit      = $this->escape_string($this->strip_all($data['order_unit']));
			$pc_shade        = $this->escape_string($this->strip_all($data['pc_shade']));
			$customer_remark = $this->escape_string($this->strip_all($data['customer_remark']));
			$category        = $this->escape_string($this->strip_all($data['category']));
			$delivery_site   = $this->escape_string($this->strip_all($data['delivery_site']));
			$order_status    = $this->escape_string($this->strip_all($data['order_status']));

			$query = $this->query("UPDATE ".PREFIX."order_booking SET customer_name = '".$customer_name."',po_no = '".$po_no."',project_no = '".$project_no."',item_name = '".$item_name."',item_code = '".$item_code."',booking_date = '".$booking_date."',target_date = '".$target_date."',order_qty = '".$order_qty."',order_unit = '".$order_unit."',pc_shade = '".$pc_shade."',customer_remark = '".$customer_remark."',category = '".$category."',delivery_site = '".$delivery_site."',order_status = '".$order_status."', updated_by = '".$created_by."', updated_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id = '".$branch_id."'");

			return $query;

		}

		//Order Booking End

		// Purchase Information Start


		function getallItemBoqItemAndstockqtyId($boq_id,$branch_id)
		{
			$boq_id    = $this->escape_string($this->strip_all($boq_id));

			$branch_id = $this->escape_string($this->strip_all($branch_id));

			$query = $this->query("SELECT * FROM ".PREFIX."boq_component_master where boq_id = '".$boq_id."' AND branch_id = '".$branch_id."' ");
			return $query;

		}

		function stockCurrentQty($component_id,$branch_id,$item_name)
		{
			$item_name = $this->escape_string($this->strip_all($item_name));

			$component_id = $this->escape_string($this->strip_all($component_id));

			$branch_id = $this->escape_string($this->strip_all($branch_id));

			$balanceQty = $this->fetch($this->query("SELECT balance x FROM ".PREFIX."stock where finish_good_id = '".$item_name."' AND branch_id = '".$branch_id."' AND  item_id = '".$component_id."' ORDER BY id DESC LIMIT 1 "))['x'];
			
			return $balanceQty;

		}

		
		function updatePurchaseInformation($data,$created_by,$branch_id)
		{
			
			$id                 = $this->escape_string($this->strip_all($data['id']));
			$rawmaterial        = $this->escape_string($this->strip_all($data['rawmaterial']));
			$powderName         = $this->escape_string($this->strip_all($data['powderName']));
			$powderRemark       = $this->escape_string($this->strip_all($data['powderRemark']));
			$powderInKg         = $this->escape_string($this->strip_all($data['powderInKg']));
			$special_requirment = $this->escape_string($this->strip_all($data['special_requirment']));
			$remark             = $this->escape_string($this->strip_all($data['remark']));


			$query = $this->query("UPDATE ".PREFIX."order_booking SET rawmaterial = '".$rawmaterial."',powderName = '".$powderName."',powderRemark = '".$powderRemark."',powderInKg = '".$powderInKg."',special_requirment = '".$special_requirment."',remark = '".$remark."', purchase_info_by = '".$created_by."', purchase_info_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id = '".$branch_id."' ");

			return $query;

		}

		// Purchase Information End

		// Production Information Start

		function updateProductionInformation($data,$created_by,$branch_id)
		{
			
			$id                = $this->escape_string($this->strip_all($data['id']));
			$end_product       = $this->escape_string($this->strip_all($data['end_product']));
			$qa_check          = $this->escape_string($this->strip_all($data['qa_check']));
			$pc                = $this->escape_string($this->strip_all($data['pc']));
			$pck               = $this->escape_string($this->strip_all($data['pck']));
			// $dispach_plan_date = $this->escape_string($this->strip_all($data['dispach_plan_date']));
			$production_remark = $this->escape_string($this->strip_all($data['production_remark']));


			$query = $this->query("UPDATE ".PREFIX."order_booking SET end_product = '".$end_product."',qa_check = '".$qa_check."',pc = '".$pc."',pck = '".$pck."',production_remark = '".$production_remark."', product_informaton_by = '".$created_by."', product_informaton_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id = '".$branch_id."' ");

			return $query;

		}

		// Production Information End


		//Dispach Information Start


		function addDispachInformation($data,$created_by,$branch_id)
		{
			
			$id                = $this->escape_string($this->strip_all($data['id']));
			$order_tracking_no = $this->escape_string($this->strip_all($data['order_tracking_no']));
			$tracking_date     = $this->escape_string($this->strip_all($data['tracking_date']));
			$balance_qty       = $this->escape_string($this->strip_all($data['balance_qty']));
			$open_close_hold   = $this->escape_string($this->strip_all($data['open_close_hold']));

			$query = $this->query("UPDATE ".PREFIX."order_booking SET order_tracking_no = '".$order_tracking_no."',tracking_date = '".$tracking_date."',balance_qty = '".$balance_qty."',open_close_hold = '".$open_close_hold."', dispach_informaton_by = '".$created_by."', dispach_informaton_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id='".$branch_id."' ");
			
			if(isset($data['invoice_no'])) {

				$invoice_no   = $data['invoice_no'];
				$invoice_date = $data['invoice_date'];
				$dispach_qty  = $data['dispach_qty'];

				for ($i = 0; $i < count($data['invoice_no']); $i++) {

					$invoice_no_values   = $this->escape_string($this->strip_all($invoice_no[$i]));
					$invoice_date_values = $this->escape_string($this->strip_all($invoice_date[$i]));
					$dispach_qty_values  = $this->escape_string($this->strip_all($dispach_qty[$i]));

					$query = $this->query("INSERT INTO ".PREFIX."order_dispach_tracking (branch_id,order_booking_id,invoice_no,invoice_date,dispach_qty,created_by,created_time) VALUES ('".$branch_id."','".$id."','".$invoice_no_values."','".$invoice_date_values."','".$dispach_qty_values."','".$created_by."', '".CURRENTMILLIS."') ");
					
				}
			}

			return $id;

		}

		function getUniqueOrderDispachTrackingById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."order_dispach_tracking WHERE order_booking_id = '".$id."' LIMIT 99999999 OFFSET 1 ");
			return $query;
		}

		function getUniqueOrderDispachTrackingFristRowById($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."order_dispach_tracking WHERE order_booking_id = '".$id."' LIMIT 1 ");
			return $this->fetch($query);
		}

		function updateDispachInformation($data,$created_by,$branch_id)
		{
			$id                = $this->escape_string($this->strip_all($data['id']));
			$order_tracking_no = $this->escape_string($this->strip_all($data['order_tracking_no']));
			$tracking_date     = $this->escape_string($this->strip_all($data['tracking_date']));
			$balance_qty       = $this->escape_string($this->strip_all($data['balance_qty']));
			$open_close_hold   = $this->escape_string($this->strip_all($data['open_close_hold']));

			$query = $this->query("UPDATE ".PREFIX."order_booking SET order_tracking_no = '".$order_tracking_no."',tracking_date = '".$tracking_date."',balance_qty = '".$balance_qty."',open_close_hold = '".$open_close_hold."', dispach_informaton_by = '".$created_by."', dispach_informaton_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id='".$branch_id."' ");
			
			if(isset($data['invoice_no'])) {

				$invoice_no   = $data['invoice_no'];
				$dispach_id   = $data['dispach_id'];
				$invoice_date = $data['invoice_date'];
				$dispach_qty  = $data['dispach_qty'];

				for ($i = 0; $i < count($data['invoice_no']); $i++) {

					$invoice_no_values   = $this->escape_string($this->strip_all($invoice_no[$i]));
					$dispach_id_values   = $this->escape_string($this->strip_all($dispach_id[$i]));
					$invoice_date_values = $this->escape_string($this->strip_all($invoice_date[$i]));
					$dispach_qty_values  = $this->escape_string($this->strip_all($dispach_qty[$i]));

					if ($dispach_id_values != '') {

						$query = $this->query("UPDATE ".PREFIX."order_dispach_tracking SET invoice_no = '".$invoice_no_values."', invoice_date='".$invoice_date_values."',dispach_qty='".$dispach_qty_values."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."' WHERE branch_id = '".$branch_id."' AND id = '".$dispach_id_values."' AND order_booking_id = '".$id."' ");
						
					} else {

						$query = $this->query("INSERT INTO ".PREFIX."order_dispach_tracking (branch_id,order_booking_id,invoice_no,invoice_date,dispach_qty,created_by,created_time) VALUES ('".$branch_id."','".$id."','".$invoice_no_values."','".$invoice_date_values."','".$dispach_qty_values."','".$created_by."', '".CURRENTMILLIS."') ");
						
					}

					
				}
			}

			return $id;
		}

		//Dispach Information End

		//Account Information Start

		function getUniqueOrderDispachId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."order_dispach_tracking WHERE order_booking_id = '".$id."' ");
			return $query;
		}


		function addAccountInformation($data,$file,$created_by,$branch_id)
		{
			$id = $this->escape_string($this->strip_all($data['id']));

			$query = $this->query("UPDATE ".PREFIX."order_booking SET acccount_informaton_by = '".$created_by."', acccount_informaton_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id = '".$branch_id."' ");

			if(isset($data['invoice_no'])) {

				$invoice_no              = $data['invoice_no'];
				$invoice_date            = $data['invoice_date'];
				$dispach_qty             = $data['dispach_qty'];
				$payment_received_doc    = $data['payment_received_doc'];
				$payment_received_date   = $data['payment_received_date'];
				$payment_received_amount = $data['payment_received_amount'];
				$remark                  = $data['remark'];
				$storableLocation        = "";


				for ($i = 0; $i < count($data['invoice_no']); $i++) {

					$invoice_no_values              = $this->escape_string($this->strip_all($invoice_no[$i]));
					$invoice_date_values            = $this->escape_string($this->strip_all($invoice_date[$i]));
					$dispach_qty_values             = $this->escape_string($this->strip_all($dispach_qty[$i]));
					$payment_received_doc_values    = $this->escape_string($this->strip_all($payment_received_doc[$i]));
					$payment_received_date_values   = $this->escape_string($this->strip_all($payment_received_date[$i]));
					$payment_received_amount_values = $this->escape_string($this->strip_all($payment_received_amount[$i]));
					$remark_values                  = $this->escape_string($this->strip_all($remark[$i]));

					if(isset($file['proof_of_delivery']['name'][$i]) && !empty($file['proof_of_delivery']['name'][$i])) {
			
						$fileInfo  = pathinfo($file['proof_of_delivery']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation = 'proofofdelivery/'.$i.'_'.$invoice_no_values.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$storableLocation = 'proofofdelivery/'.$i.'_'.$invoice_no_values.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['proof_of_delivery']['tmp_name'][$i], $fileLocation);
			
					} 

					$query = $this->query("INSERT INTO ".PREFIX."order_account_tracking (branch_id,order_booking_id,invoice_no,invoice_date,dispach_qty,proof_of_delivery,payment_received_doc,payment_received_date,payment_received_amount,remark,created_by,created_time) VALUES ('".$branch_id."','".$id."','".$invoice_no_values."','".$invoice_date_values."','".$dispach_qty_values."','".$storableLocation."','".$payment_received_doc_values."','".$payment_received_date_values."','".$payment_received_amount_values."','".$remark_values."','".$created_by."', '".CURRENTMILLIS."') ");
						
					
				}

			}

			return $id;

		}

		function getUniqueOrderBookAccountId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."order_account_tracking WHERE order_booking_id = '".$id."' ");
			return $query;
		}


		function updateAccountInformation($data,$file,$created_by,$branch_id)
		{
			$id = $this->escape_string($this->strip_all($data['id']));

			$query = $this->query("UPDATE ".PREFIX."order_booking SET acccount_informaton_by = '".$created_by."', acccount_informaton_time = '".CURRENTMILLIS."' WHERE id = '".$id."' AND branch_id = '".$branch_id."' ");

			if(isset($data['invoice_no'])) {

				$invoice_no              = $data['invoice_no'];
				$account_id              = $data['account_id'];
				$invoice_date            = $data['invoice_date'];
				$dispach_qty             = $data['dispach_qty'];
				$payment_received_doc    = $data['payment_received_doc'];
				$payment_received_date   = $data['payment_received_date'];
				$payment_received_amount = $data['payment_received_amount'];
				$remark                  = $data['remark'];
				$storableLocation        = "";


				for ($i = 0; $i < count($data['invoice_no']); $i++) {

					$invoice_no_values              = $this->escape_string($this->strip_all($invoice_no[$i]));
					$account_id_values              = $this->escape_string($this->strip_all($account_id[$i]));
					$invoice_date_values            = $this->escape_string($this->strip_all($invoice_date[$i]));
					$dispach_qty_values             = $this->escape_string($this->strip_all($dispach_qty[$i]));
					$payment_received_doc_values    = $this->escape_string($this->strip_all($payment_received_doc[$i]));
					$payment_received_date_values   = $this->escape_string($this->strip_all($payment_received_date[$i]));
					$payment_received_amount_values = $this->escape_string($this->strip_all($payment_received_amount[$i]));
					$remark_values                  = $this->escape_string($this->strip_all($remark[$i]));

					if(isset($file['proof_of_delivery']['name'][$i]) && !empty($file['proof_of_delivery']['name'][$i])) {

						$proofOfDelivery = $this->fetch($this->query("SELECT proof_of_delivery x FROM ".PREFIX."order_account_tracking WHERE id = '".$account_id_values."' AND branch_id = '".$branch_id."' AND order_booking_id = '".$id."'  "))['x'];

						unlink($proofOfDelivery);

						$fileInfo  = pathinfo($file['proof_of_delivery']['name'][$i]);
			
						$extension = $fileInfo['extension'];
			
						$fileLocation = 'proofofdelivery/'.$i.'_'.$invoice_no_values.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						$storableLocation = 'proofofdelivery/'.$i.'_'.$invoice_no_values.'_'.$id.'_'.CURRENTMILLIS.'.'.$extension;
			
						move_uploaded_file($file['proof_of_delivery']['tmp_name'][$i], $fileLocation);
			
					} else {
				
						$storableLocation = $this->fetch($this->query("SELECT proof_of_delivery x FROM ".PREFIX."order_account_tracking  WHERE id = '".$account_id_values."' AND branch_id = '".$branch_id."' AND order_booking_id = '".$id."'  "))['x'];
		
					} 

					if ($account_id_values != '') {

						$query = $this->query("UPDATE ".PREFIX."order_account_tracking SET invoice_no = '".$invoice_no_values."', invoice_date='".$invoice_date_values."',dispach_qty='".$dispach_qty_values."',proof_of_delivery = '".$storableLocation."',payment_received_doc = '".$$payment_received_doc_values."',payment_received_date = '".$payment_received_date_values."',payment_received_amount = '".$payment_received_amount_values."',remark = '".$remark_values."',updated_by='".$created_by."',updated_time='".CURRENTMILLIS."' WHERE branch_id = '".$branch_id."' AND id = '".$account_id_values."' AND order_booking_id = '".$id."' ");
						
					} else {

						$query = $this->query("INSERT INTO ".PREFIX."order_account_tracking (branch_id,order_booking_id,invoice_no,invoice_date,dispach_qty,proof_of_delivery,payment_received_doc,payment_received_date,payment_received_amount,remark,created_by,created_time) VALUES ('".$branch_id."','".$id."','".$invoice_no_values."','".$invoice_date_values."','".$dispach_qty_values."','".$storableLocation."','".$payment_received_doc_values."','".$payment_received_date_values."','".$payment_received_amount_values."','".$remark_values."','".$created_by."', '".CURRENTMILLIS."') ");

					}
					

				}

			}

			return $id;

		}


		//Account Information End

		//Dsa Analysing Start

		function updateDsaAnalysing($data,$created_by)
		{
			$id                   = $this->escape_string($this->strip_all($data['id']));
			$reason_for_failing   = $this->escape_string($this->strip_all($data['reason_for_failing']));
			$dsa_analysing_remark = $this->escape_string($this->strip_all($data['dsa_analysing_remark']));
			$completion           = $this->escape_string($this->strip_all($data['completion']));

			$query = $this->query("UPDATE ".PREFIX."order_booking SET reason_for_failing = '".$reason_for_failing."',dsa_analysing_remark = '".$dsa_analysing_remark."',completion = '".$completion."', dsa_analysing_by = '".$created_by."', dsa_analysing_time = '".CURRENTMILLIS."' WHERE id = '".$id."' ");

			return $query;

		}


		//Dsa Analysing End


		//---------------------------Order Tracking end----------------------------------------//



		//---------------------------Stock Start------------------------------------------------//

		//Opeing Balance start

		function getUniqueItemNameById($id,$branch_id) {
			$itemName = $this->fetch($this->query("SELECT item_name x FROM ".PREFIX."item_master where id = '".$id."' AND branch_id = '".$branch_id."' "))['x'];
			return $itemName;
		}

		function addOpeingBalance($data,$created_by,$branch_id)
		{

			$finish_good_item_name = $this->escape_string($this->strip_all($data['finish_good_item_name']));

			$opening_balance_date  = $this->escape_string($this->strip_all($data['opening_balance_date']));

			$opening_balance_timestamp_date = (strtotime($opening_balance_date))*1000;

			$query = $this->query("INSERT INTO ".PREFIX."item_opening_balance (branch_id,finish_good_item_name,opening_balance_date,opening_balance_timestamp_date,created_by,created_time) VALUES ('".$branch_id."','".$finish_good_item_name."','".$opening_balance_date."','".$opening_balance_timestamp_date."','".$created_by."', '".CURRENTMILLIS."') ");
			
			$last_id = $this->last_insert_id();


			if(isset($data['component_id'])) {

				$component_id  = $data['component_id'];

				$opening_balance = $data['opening_balance'];

				for ($i = 0; $i < count($data['component_id']); $i++) {

					$component_id_values    = $this->escape_string($this->strip_all($component_id[$i]));

					$component_name_values  = $this->fetch($this->query("SELECT item_name x FROM ".PREFIX."item_master WHERE id = '".$component_id_values."'"))['x'];

					$opening_balance_values = $this->escape_string($this->strip_all($opening_balance[$i]));

					$stock_finish_good_id       = 0;
					$stock_item_id              = 0;
					$stock_opening_balance      = 0;
					$stock_material_in          = 0;
					$stock_material_out         = 0;
					$stock_adjustment_qty_plus  = 0;
					$stock_adjustment_qty_minus = 0;
					$balance                    = 0;

					$query = $this->query("INSERT INTO ".PREFIX."item_component_opening_balance (branch_id,opening_balace_id,item_name,component_id,component_name,opening_balance,created_by,created_time) VALUES  ('".$branch_id."','".$last_id."','".$finish_good_item_name."','".$component_id_values."','".$component_name_values."','".$opening_balance_values."','".$created_by."', '".CURRENTMILLIS."') ");

					$item = $this->fetch($this->query("SELECT * FROM ".PREFIX."stock where finish_good_id = '".$finish_good_item_name."' AND item_id = '".$component_id_values."' AND branch_id = '".$branch_id."' ORDER BY id DESC LIMIT 1 "));

					$stock_finish_good_id       = $item['finish_good_id'];
					$stock_item_id              = $item['item_id'];
					$stock_opening_balance      = $item['opening_balance'];
					$stock_material_in          = $item['material_in'];
					$stock_material_out         = $item['material_out'];
					$stock_adjustment_qty_plus  = $item['adjustment_qty_plus'];
					$stock_adjustment_qty_minus = $item['adjustment_qty_minus'];
					$balance         = ($stock_opening_balance+$stock_material_in+$opening_balance_values+$stock_adjustment_qty_plus)-($stock_material_out+$stock_adjustment_qty_minus);

                    $add_opening_balance = $stock_opening_balance+$opening_balance_values;

					$query = $this->query("INSERT INTO ".PREFIX."stock (branch_id,finish_good_id,item_id,item_name,opening_balance,material_in,material_out,adjustment_qty_plus,adjustment_qty_minus,balance,created_time) VALUES ('".$branch_id."','".$finish_good_item_name."','".$component_id_values."','".$component_name_values."','".$add_opening_balance."','".$stock_material_in."','".$stock_material_out."','".$stock_adjustment_qty_plus."','".$stock_adjustment_qty_minus."','".$balance."','".CURRENTMILLIS."') ");

					
				}

			}

			return $last_id;
		}

		function getUniqueItemOpeingBalanceId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."item_opening_balance WHERE id = '".$id."' ");
			return $this->fetch($query);
		}

		function getUniqueItemComponentOpeingBalanceId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."item_component_opening_balance WHERE opening_balace_id = '".$id."' ");
			return $query;
		}

		
		function updateOpeingBalance($data,$created_by,$branch_id)
		{

			$id = $this->escape_string($this->strip_all($data['id']));

			if(isset($data['component_name'])) {

				$component_name               = $data['component_name'];

				$component_opening_balance_id = $data['component_opening_balance_id'];

				$opening_balance              = $data['opening_balance'];

				for ($i = 0; $i < count($data['component_name']); $i++) {

					$component_name_values               = $this->escape_string($this->strip_all($component_name[$i]));

					$component_opening_balance_id_values = $this->escape_string($this->strip_all($component_opening_balance_id[$i]));

					$opening_balance_values              = $this->escape_string($this->strip_all($opening_balance[$i]));

					$query = $this->query("UPDATE ".PREFIX."item_component_opening_balance SET opening_balance = '".$opening_balance_values."' ,updated_by = '".$created_by."' ,updated_time = '".CURRENTMILLIS."' WHERE branch_id='".$branch_id."' AND id = '".$component_opening_balance_id_values."' ");
					
				}
			}

			return $id;
			
		}

		//Opeing Balance End

		// Material In start

		function getmaterialMaxId($branch_id)
		{
		
			$maxId = $this->fetch($this->query("SELECT max(id) x FROM ".PREFIX."material_in WHERE branch_id='".$branch_id."' "))['x'];

			if ($maxId) {

				$max = 1001+$maxId;

			} else {

				$max = 1001;
				
			}

			return $max;

		}

		function addMaterialIn($data,$created_by,$branch_id)
		{

			$sr_no         = $this->escape_string($this->strip_all($data['sr_no']));

			$entry_date    = $this->escape_string($this->strip_all($data['entry_date']));

			$supplier_name = $this->escape_string($this->strip_all($data['supplier_name']));

			$document_no   = $this->escape_string($this->strip_all($data['document_no']));

			$document_date = $this->escape_string($this->strip_all($data['document_date']));

			$query = $this->query("INSERT INTO ".PREFIX."material_in (branch_id,sr_no,entry_date,supplier_name,document_no,document_date,created_by,created_time) VALUES ('".$branch_id."','".$sr_no."','".$entry_date."','".$supplier_name."','".$document_no."','".$document_date."','".$created_by."', '".CURRENTMILLIS."') ");
			
			$last_id = $this->last_insert_id();


			if(isset($data['item_name'])) {

				$item_name       = $data['item_name'];

				$component_name  = $data['component_name'];

				$material_in_qty = $data['material_in_qty'];

				for ($i = 0; $i < count($data['item_name']); $i++) {

					$item_name_values       = $this->escape_string($this->strip_all($item_name[$i]));

					$component_name_values  = $this->escape_string($this->strip_all($component_name[$i]));

					$component_item_name  = $this->fetch($this->query("SELECT item_name x FROM ".PREFIX."item_master WHERE id = '".$component_name_values."'"))['x'];

					$material_in_qty_values = $this->escape_string($this->strip_all($material_in_qty[$i]));

					$stock_finish_good_id       = 0;
					$stock_item_id              = 0;
					$stock_opening_balance      = 0;
					$stock_material_in          = 0;
					$stock_material_out         = 0;
					$stock_adjustment_qty_plus  = 0;
					$stock_adjustment_qty_minus = 0;
					$balance                    = 0;

					$query = $this->query("INSERT INTO ".PREFIX."material_in_component (branch_id,material_in_id,material_in_sr_no,item_name,component_name,material_in_qty,created_by,created_time) VALUES ('".$branch_id."','".$last_id."','".$sr_no."','".$item_name_values."','".$component_name_values."','".$material_in_qty_values."','".$created_by."', '".CURRENTMILLIS."') ");

					$item = $this->fetch($this->query("SELECT * FROM ".PREFIX."stock where finish_good_id = '".$item_name_values."' AND item_id = '".$component_name_values."' AND branch_id = '".$branch_id."' ORDER BY id DESC LIMIT 1 "));

					$stock_finish_good_id       = $item['finish_good_id'];
					$stock_item_id              = $item['item_id'];
					$stock_opening_balance      = $item['opening_balance'];
					$stock_material_in          = $item['material_in'];
					$stock_material_out         = $item['material_out'];
					$stock_adjustment_qty_plus  = $item['adjustment_qty_plus'];
					$stock_adjustment_qty_minus = $item['adjustment_qty_minus'];
					$balance         = ($stock_opening_balance+$stock_material_in+$opening_balance_values+$material_in_qty_values)-($stock_material_out+$stock_adjustment_qty_minus);

                    $add_material_in = $stock_material_in+$material_in_qty_values;

					$query = $this->query("INSERT INTO ".PREFIX."stock (branch_id,finish_good_id,item_id,item_name,opening_balance,material_in,material_out,adjustment_qty_plus,adjustment_qty_minus,balance,created_time) VALUES ('".$branch_id."','".$item_name_values."','".$component_name_values."','".$component_item_name."','".$stock_opening_balance."','".$add_material_in."','".$stock_material_out."','".$stock_adjustment_qty_plus."','".$stock_adjustment_qty_minus."','".$balance."','".CURRENTMILLIS."') ");

				}
			}

			return $last_id;

		}


		function getUniqueMaterialInId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."material_in WHERE id = '".$id."' ");
			return $this->fetch($query);
		}

		function getUniqueMaterialInComponentId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."material_in_component WHERE material_in_id = '".$id."' ");
			return $query;
		}

		function getUniqueMaterialInComponentIds($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."material_in_component WHERE id = '".$id."' ");
			return $this->fetch($query);
		}

		function updateMaterialIn($data,$created_by,$branch_id)
		{

			$id            = $this->escape_string($this->strip_all($data['id']));

			$sr_no         = $this->escape_string($this->strip_all($data['sr_no']));

			$entry_date    = $this->escape_string($this->strip_all($data['entry_date']));

			$supplier_name = $this->escape_string($this->strip_all($data['supplier_name']));

			$document_no   = $this->escape_string($this->strip_all($data['document_no']));

			$document_date = $this->escape_string($this->strip_all($data['document_date']));

			$query = $this->query("UPDATE ".PREFIX."material_in SET entry_date = '".$entry_date."',supplier_name = '".$supplier_name."',document_no = '".$document_no."' , document_date = '".$document_date."' ,updated_by = '".$created_by."' ,updated_time =  '".CURRENTMILLIS."' WHERE  branch_id = '".$branch_id."' AND sr_no = '".$sr_no."' AND id = '".$id."' ");
			
			$last_id = $this->last_insert_id();


			if(isset($data['item_name'])) {

				$material_in_id  = $data['material_in_id'];

				$item_name       = $data['item_name'];

				$component_name  = $data['component_name'];

				$material_in_qty = $data['material_in_qty'];

				for ($i = 0; $i < count($data['item_name']); $i++) {

					$material_in_id_values  = $this->escape_string($this->strip_all($material_in_id[$i]));

					$item_name_values       = $this->escape_string($this->strip_all($item_name[$i]));

					$component_name_values  = $this->escape_string($this->strip_all($component_name[$i]));

					$material_in_qty_values = $this->escape_string($this->strip_all($material_in_qty[$i]));

					if ($material_in_id_values !='') {

						$query = $this->query("UPDATE ".PREFIX."material_in_component SET item_name = '".$item_name_values."',component_name = '".$component_name_values."', material_in_qty = '".$material_in_qty_values."',updated_by = '".$created_by."' ,updated_time =  '".CURRENTMILLIS."' WHERE  branch_id = '".$branch_id."'  AND id = '".$material_in_id_values."' AND material_in_sr_no = '".$sr_no."'  AND material_in_id = '".$id."' ");

					} else {

						$query = $this->query("INSERT INTO ".PREFIX."material_in_component (branch_id,material_in_id,material_in_sr_no,item_name,component_name,material_in_qty,created_by,created_time) VALUES ('".$branch_id."','".$id."','".$sr_no."','".$item_name_values."','".$component_name_values."','".$material_in_qty_values."','".$created_by."', '".CURRENTMILLIS."') ");

					}

					
				}
			}

			return $id;

		}
		// Material In End


		// Stock Adjustment Start

		function getUniqueCurrentStock($data,$branch_id)
		{
			$finish_good_id = $this->escape_string($this->strip_all($data['finishGoodId']));

			$item_id        = $this->escape_string($this->strip_all($data['componentId']));

			$balance        = $this->fetch($this->query("SELECT balance x FROM ".PREFIX."stock where finish_good_id = '".$finish_good_id."' AND item_id = '".$item_id."' AND branch_id = '".$branch_id."' ORDER BY id DESC LIMIT 1"))['x'];
			return $balance;

		}


		function addStockAdjustMent($data,$created_by,$branch_id)
		{

			$item_id               = $this->escape_string($this->strip_all($data['item_id']));

			$stock_adjustment_date = $this->escape_string($this->strip_all($data['stock_adjustment_date']));

			$stock_adjustment_timestamp_date = (strtotime($stock_adjustment_date))*1000;


			$query = $this->query("INSERT INTO ".PREFIX."stock_adjustment (branch_id,item_id,stock_adjustment_date,created_by,created_time) VALUES ('".$branch_id."','".$item_id."','".$stock_adjustment_date."','".$created_by."', '".CURRENTMILLIS."') ");
			
			$last_id = $this->last_insert_id();

			if(isset($data['component_id'])) {

				$component_id       = $data['component_id'];

				$current_stock      = $data['current_stock'];

				$stock_adjust_plus  = $data['stock_adjust_plus'];

				$stock_adjust_minus = $data['stock_adjust_minus'];


				for ($i = 0; $i < count($data['component_id']); $i++) {

					$component_id_values       = $this->escape_string($this->strip_all($component_id[$i]));

					$component_item_name       = $this->fetch($this->query("SELECT item_name x FROM ".PREFIX."item_master WHERE id = '".$component_id_values."'"))['x'];

					$current_stock_values      = $this->escape_string($this->strip_all($current_stock[$i]));

					$stock_adjust_plus_values  = $this->escape_string($this->strip_all($stock_adjust_plus[$i]));

					$stock_adjust_minus_values = $this->escape_string($this->strip_all($stock_adjust_minus[$i]));


					$stock_finish_good_id       = 0;
					$stock_item_id              = 0;
					$stock_opening_balance      = 0;
					$stock_material_in          = 0;
					$stock_material_out         = 0;
					$stock_adjustment_qty_plus  = 0;
					$stock_adjustment_qty_minus = 0;
					$balance                    = 0;

					$query = $this->query("INSERT INTO ".PREFIX."stock_adjustment_component (branch_id,stock_adjustment_id,item_id,component_id,component_name,current_stock,stock_adjust_plus,stock_adjust_minus,created_by,created_time) VALUES ('".$branch_id."','".$last_id."','".$item_id."','".$component_id_values."','".$component_item_name."','".$current_stock_values."','".$stock_adjust_plus_values."','".$stock_adjust_minus_values."','".$created_by."', '".CURRENTMILLIS."') ");

					$item = $this->fetch($this->query("SELECT * FROM ".PREFIX."stock where finish_good_id = '".$item_id."' AND item_id = '".$component_id_values."' AND branch_id = '".$branch_id."' ORDER BY id DESC LIMIT 1 "));

					$stock_finish_good_id       = $item['finish_good_id'];
					$stock_item_id              = $item['item_id'];
					$stock_opening_balance      = $item['opening_balance'];
					$stock_material_in          = $item['material_in'];
					$stock_material_out         = $item['material_out'];
					$stock_adjustment_qty_plus  = $item['adjustment_qty_plus'];
					$stock_adjustment_qty_minus = $item['adjustment_qty_minus'];
					$balance         = ($stock_opening_balance+$stock_material_in+$stock_adjust_plus_values+$stock_adjustment_qty_plus)-($stock_material_out+$stock_adjustment_qty_minus+$stock_adjust_minus_values);

					$add_adjustment_qty_plus = $stock_adjustment_qty_plus+$stock_adjust_plus_values;
					
					$add_adjustment_qty_minus = $stock_adjustment_qty_minus+$stock_adjust_minus_values;

					$query = $this->query("INSERT INTO ".PREFIX."stock (branch_id,finish_good_id,item_id,item_name,opening_balance,material_in,material_out,adjustment_qty_plus,adjustment_qty_minus,balance,created_time) VALUES ('".$branch_id."','".$item_id."','".$component_id_values."','".$component_item_name."','".$stock_opening_balance."','".$stock_material_in."','".$stock_material_out."','".$add_adjustment_qty_plus."','".$add_adjustment_qty_minus."','".$balance."','".$stock_adjustment_timestamp_date."') ");

				}
			}

			return $last_id;

		}

		function getUniqueItemStockAdjustMentId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."stock_adjustment WHERE id = '".$id."' ");
			return $this->fetch($query);
		}

		function getUniqueItemComponentStockAdjustMentId($id)
		{
			$query = $this->query("SELECT * FROM ".PREFIX."stock_adjustment_component WHERE stock_adjustment_id = '".$id."' ");
			return $query;
		}


		// Stock Adjustment End

		//----------------------------Stock Start-----------------------------------------------//



	} 
?>