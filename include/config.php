<?php

	//DELUXORA DB CRED.
	/* incinson_deluxus	USER
	incinson_deluxora	DB
	auBTfWqxccQM		PASS */

	/*
	* CONFIG
	* - v1 - 
	* - v2 - updated BASE CONFIG, error_reporting based on PROJECTSTATUS
	* - v3 - added staging option
	* - v3.1 - BUGFIX in staging option
	*/

	/* DEVELOPMENT CONFIG */
	 //DEFINE('PROJECTSTATUS','LIVE');
	// DEFINE('PROJECTSTATUS','STAGING');
	DEFINE('PROJECTSTATUS','DEV');
	/* DEVELOPMENT CONFIG */

	/* TIMEZONE CONFIG */
	$timezone = "Asia/Calcutta";
	if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);
	/* TIMEZONE CONFIG */

	if(PROJECTSTATUS=="LIVE"){
		error_reporting(0);
		DEFINE('BASE_URL','https://radheindustries.in');
		DEFINE('ADMIN_EMAIL','');

		// PAYMENT GATEWAY
		DEFINE('RAZORPAY_API_KEY_ID','rzp_test_NcgmZkpI1odSfw');
		DEFINE('RAZORPAY_API_KEY_SECRET','9bLIowAJei9cpPWI55NYYUlq');
		DEFINE('WEBHOOK_SECRET','');
		// PAYMENT GATEWAY

	} else if(PROJECTSTATUS=="STAGING"){
		error_reporting(0);
		DEFINE('BASE_URL','http://shareittofriends.com/demo/paperpanda/php');
		DEFINE('ADMIN_EMAIL','info@paperpanda.com');

		// PAYMENT GATEWAY
		DEFINE('RAZORPAY_API_KEY_ID','rzp_test_NcgmZkpI1odSfw');
		DEFINE('RAZORPAY_API_KEY_SECRET','9bLIowAJei9cpPWI55NYYUlq');
		DEFINE('WEBHOOK_SECRET','');
		// PAYMENT GATEWAY

	} else { 
		// DEFAULT TO DEV
		error_reporting(E_ALL);
		DEFINE('BASE_URL','http://crm.local');
		DEFINE('ADMIN_EMAIL','radhakishan@innovins.com');

		// PAYMENT GATEWAY
		DEFINE('RAZORPAY_API_KEY_ID','rzp_test_NcgmZkpI1odSfw');
		DEFINE('RAZORPAY_API_KEY_SECRET','9bLIowAJei9cpPWI55NYYUlq');
		DEFINE('WEBHOOK_SECRET','');
		// PAYMENT GATEWAY

	}

	/* BASE CONFIG */
	DEFINE('SITE_NAME','CRM');
	DEFINE('ADMIN_TITLE','Administrator Panel | '.SITE_NAME);
	DEFINE('TITLE', SITE_NAME);
	DEFINE('PREFIX','tr_');
	DEFINE('COPYRIGHT','2020');
	DEFINE('currentdate',date('Y-m-d H:i:s'));
	DEFINE('CURRENTMILLIS',round(microtime(true) * 1000));
	DEFINE('CURRENTDATETIME',date('Y-m-d H:i:s'));
	DEFINE('LOGO', BASE_URL.'/image/radhelogo.jpeg');
	DEFINE('FAVICON', BASE_URL.'/image/radhelogo.jpeg');
	DEFINE('CAPTCHA_PUBLIC_KEY','');
	DEFINE('CAPTCHA_PRIVATE_KEY','');


	DEFINE('ADMIN_PANEL', 'transport-dashboard');
	/* BASE CONFIG */
?>