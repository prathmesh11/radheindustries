<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}




include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);
$tableName     = 'order_booking';

$orderBooking  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time=0"))['x'];

$purchaseInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time=0"))['x'];

$productInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time=0"))['x'];

$dispachInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time<>0 AND dispach_informaton_confirm_time=0"))['x'];

$acccountInformation  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time<>0 AND dispach_informaton_confirm_time<>0 AND acccount_informaton_confirm_time=0"))['x'];

$dsaAnalysing  = $admin->fetch($admin->query("SELECT count(*) x FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND order_booking_time<>0 AND purchase_info_confirm_time<>0 AND product_informaton_confirm_time<>0 AND dispach_informaton_confirm_time<>0 AND acccount_informaton_confirm_time<>0 AND dsa_analysing_confirm_time=0"))['x'];



?>

<style>

.badge-pill {
        background-color: #f39c12;
}

</style>

<div id="div-content" class="content ">

        <ul class="list-group list-group-horizontal">

                <li class="list-group-item d-flex justify-content-between align-items-center"
                        style="border-radius:50px; background-color:#130f40; <?php echo $navenq1;?>">
                        <a href="order-booking.php" style="color:#fff; ">Order Booking <span class="badge badge-pill " style="border-radius:50px;">
                                        <?php echo $orderBooking;?> </span> </a>
                </li>

                <li class="list-group-item d-flex justify-content-between align-items-center"
                        style="border-radius:50px; background-color:#130f40; <?php echo $navenq2;?>">
                        <a href="purchase-info.php" style="color:#fff; ">Purchase Department <span
                                        class="badge  badge-pill" style="border-radius:50px;"> <?php echo $purchaseInformation;?> </span> </a>
                </li>

                <li class="list-group-item d-flex justify-content-between align-items-center"
                        style="border-radius:50px; background-color:#130f40; <?php echo $navenq3;?>">
                        <a href="production-info.php" style="color:#fff; ">Production Information <span
                                        class="badge  badge-pill" style="border-radius:50px;"> <?php echo $productInformation;?> </span> </a>
                </li>

                <li class="list-group-item d-flex justify-content-between align-items-center"
                        style="border-radius:50px; background-color:#130f40; <?php echo $navenq4;?>">
                        <a href="dispach-info.php" style="color:#fff; ">Dispach Information <span
                                        class="badge badge-pill" style="border-radius:50px;"> <?php echo $dispachInformation;?> </span> </a>
                </li>

                <li class="list-group-item d-flex justify-content-between align-items-center"
                        style="border-radius:50px; background-color:#130f40; <?php echo $navenq5;?>">
                        <a href="account-info.php" style="color:#fff;">Account Information <span
                                        class="badge badge-pill" style="border-radius:50px;"> <?php echo $acccountInformation;?> </span> </a>
                </li>

                <li class="list-group-item d-flex justify-content-between align-items-center"
                        style="border-radius:50px; background-color:#130f40; <?php echo $navenq6;?>">
                        <a href="dsa-analys.php" style="color:#fff; "> Dsa Analysing <span class="badge badge-pill" style="border-radius:50px;"> <?php echo $dsaAnalysing;?>
                                </span> </a>
                </li>

        </ul>

</div>
                