<?php 
	$basename = basename($_SERVER['REQUEST_URI']);	
	$currentPage = pathinfo($_SERVER['PHP_SELF'], PATHINFO_BASENAME);
?>
<div class="sidebar" id="sidebar">
	<div class="sidebar-inner slimscroll">
		<div id="sidebar-menu" class="sidebar-menu">
			<ul>
				<?php
					$customerPages = array(
						'customer-master.php',
						'customer-add.php',
						'order.php',
						'order-add.php',
						'cancel-requests.php',
						'testimonials.php',
						'testimonial-add.php',
						'newsletter.php',
					);
				?>
				<li style="display:none;" class="has-ul class <?php if(in_array($currentPage, $customerPages)){ echo 'active'; } ?>">
					<a href="#" class="<?php if(in_array($currentPage, $customerPages)){ echo 'subdrop'; } ?>"><i class="fa fa-bars"></i><span>Customers</span></a>
					<ul class="hidden-ul" style="<?php if(in_array($currentPage, $customerPages)){ echo 'display:block;'; } ?>">
						<li><a href="order.php" class="<?php if($currentPage == 'order-add.php' || $currentPage=='banner.php') { echo 'active'; } ?>">Customer Orders</a></li>
						<?php /* <li><a href="cancel-requests.php" class="<?php if($currentPage == 'cancel-requests.php') { echo 'active'; } ?>">Cancel Requests</a></li> */ ?>
						<li><a href="customer-master.php" class="<?php if($currentPage == 'customer-master.php' || $currentPage=='customer-add.php') { echo 'active'; } ?>">Customer Login Master</a></li>
						<li><a href="testimonials.php" class="<?php if($currentPage == 'testimonials.php' || $currentPage=='testimonial-add.php') { echo 'active'; } ?>">Testimonials</a></li>
						<li><a href="newsletter.php" class="<?php if($currentPage == 'newsletter.php') { echo 'active'; } ?>">Newsletter Subscription</a></li>
					</ul>
				</li>

				<?php
					$productPages = array(
						'index.php',
						'product-add.php',
						'reviews.php',
						'review-add.php',
					);
				?>
				<li style="display:none;" class="has-ul class <?php if(in_array($currentPage, $productPages)){ echo 'active'; } ?>">
					<a href="#" class="<?php if(in_array($currentPage, $productPages)){ echo 'subdrop'; } ?>"><i class="fa fa-bars"></i><span>Products</span></a>
					<ul class="hidden-ul" style="<?php if(in_array($currentPage, $productPages)){ echo 'display:block;'; } ?>">
						<li><a href="index.php" class="<?php if($currentPage == 'product-add.php' || $currentPage=='index.php') { echo 'active'; } ?>">Product Master</a></li>
						<li><a href="reviews.php" class="<?php if($currentPage == 'reviews.php') { echo 'active'; } ?>">Reviews</a></li>
					</ul>
				</li>

				
				
				<?php
					$cmsPages = array(
						'item-group-master.php',
						'item-master.php',
						'machine-master.php',
						'opration-master.php',
						'customer-master.php',
						'quality-instrument-master.php',
						'drawing-master.php',
						'tool-master.php',
						'inspection-gauges-master.php',
						'fixture-master.php',
						'bom-master.php',
						'ppep-master.php',
					);
				?>
				<li class="has-ul class <?php if(in_array($currentPage, $cmsPages)){ echo 'active'; } ?>">
					<a href="#" class="<?php if(in_array($currentPage, $cmsPages)){ echo 'subdrop'; } ?>"><i class="fa fa-bars"></i><span>Master</span></a>
					<ul class="hidden-ul" style="<?php if(in_array($currentPage, $cmsPages)){ echo 'display:block;'; } ?>">
						<!-- <li><a href="item-group-master.php" class="<?php if($currentPage == 'item-group-master.php') { echo 'active'; } ?>">Item Group Master</a></li> -->
						<!-- <li><a href="opration-master.php" class="<?php if($currentPage == 'opration-master.php') { echo 'active'; } ?>">Opration Master</a></li> -->
						<li><a href="item-master.php" class="<?php if($currentPage == 'item-master.php') { echo 'active'; } ?>">Item Master</a></li>
						<li><a href="machine-master.php" class="<?php if($currentPage == 'machine-master.php') { echo 'active'; } ?>">Machine Master</a></li>
						<li><a href="tool-master.php" class="<?php if($currentPage == 'tool-master.php') { echo 'active'; } ?>">Tool Master</a></li>
						<li><a href="inspection-gauges-master.php" class="<?php if($currentPage == 'inspection-gauges-master.php') { echo 'active'; } ?>">Inspection Gauges Master</a></li>
						<li><a href="customer-master.php" class="<?php if($currentPage == 'customer-master.php' || $currentPage=='customer-master-add.php') { echo 'active'; } ?>">Customer Master</a></li>
						<li><a href="quality-instrument-master.php" class="<?php if($currentPage == 'quality-instrument-master.php') { echo 'active'; } ?>">Quality Instrument Master</a></li>
						<li><a href="drawing-master.php" class="<?php if($currentPage == 'drawing-master.php') { echo 'active'; } ?>">Drawing Master</a></li>
						<li><a href="fixture-master.php" class="<?php if($currentPage == 'fixture-master.php') { echo 'active'; } ?>">Fixture Master</a></li>
						<li><a href="bom-master.php" class="<?php if($currentPage == 'bom-master.php' || $currentPage=='bom-master-add.php') { echo 'active'; } ?>">BOM Master</a></li>
						<li><a href="man-master.php" class="<?php if($currentPage == 'man-master.php') { echo 'active'; } ?>">Man Master</a></li>

						<!-- <li><a href="ppep-master.php" class="<?php if($currentPage == 'ppep-master.php') { echo 'active'; } ?>">PPEP Master</a></li> -->

					</ul>
				</li>

				<?php
					$masterPages = array(
						'order-tracking.php',
						'stock.php',
					);
				?>
				<li class="has-ul class <?php if(in_array($currentPage, $masterPages)){ echo 'active'; } ?>">
					<a href="#" class="<?php if(in_array($currentPage, $masterPages)){ echo 'subdrop'; } ?>"><i class="fa fa-bars"></i><span>Trasaction</span></a>
					<ul class="hidden-ul" style="<?php if(in_array($currentPage, $masterPages)){ echo 'display:block;'; } ?>">
						<li><a href="order-tracking.php" class="<?php if($currentPage == 'order-tracking.php') { echo 'active'; } ?>">Order Tracking</a></li>
						<li><a href="stock.php" class="<?php if($currentPage == 'stock.php') { echo 'active'; } ?>">Stock</a></li>

					</ul>
				</li>

				<!-- <?php
					$reportPages = array(
						'most-sold-product.php',
						'least-sold-product.php',
						'sales-report.php',
						'most-viewed-product.php'
					);
				?>
				<li class="has-ul class <?php if(in_array($currentPage, $reportPages)){ echo 'active'; } ?>">
					<a href="#" class="<?php if(in_array($currentPage, $reportPages)){ echo 'subdrop'; } ?>"><i class="fa fa-bars"></i><span>Reports</span></a>
					<ul class="hidden-ul" style="<?php if(in_array($currentPage, $reportPages)){ echo 'display:block;'; } ?>">
						<li><a href="most-sold-product.php" class="<?php if($currentPage == 'most-sold-product.php') { echo 'active'; } ?>">Most Sold Products</a></li>
						<li><a href="least-sold-product.php" class="<?php if($currentPage == 'least-sold-product.php') { echo 'active'; } ?>">Least Sold Products</a></li>
						<li><a href="sales-report.php" class="<?php if($currentPage == 'sales-report.php') { echo 'active'; } ?>">Sales Report</a></li>
						<li><a href="most-viewed-product.php" class="<?php if($currentPage == 'most-viewed-product.php') { echo 'active'; } ?>">Most Viewed Product</a></li>
					</ul>
				</li> -->

			</ul>
		</div>
	</div>
</div>