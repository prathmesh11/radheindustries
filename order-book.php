<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName = "Order Booking";
$pageURL = 'order-booking.php';
$deleteURL = 'order-book.php';
$navenq1 = 'background:#27ae60;';
$tableName = 'order_booking';


include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


$results = $admin->query("SELECT * FROM ".PREFIX.$tableName."  WHERE deleted_time=0 AND branch_id = '".$loggedInUserDetailsArr['branch_id']."' ");


$customer = $admin-> getAllCustomerName($loggedInUserDetailsArr['branch_id']);
$boq      = $admin->getAllboq($loggedInUserDetailsArr['branch_id']);

$allUnit  = $admin->getAllUnit();




if(isset($_POST['register'])) {
	 if($csrf->check_valid('post')) {
		$result = $admin->addOrderBook($_POST,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$pageURL."?registersuccess");
		exit;
	}
}



if(isset($_GET['edit']) && !empty($_GET['id'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniqueOrderBookId($id);
}



if(isset($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$result = $admin->updateOrderBook($_POST,$loggedInUserDetailsArr['id'],$loggedInUserDetailsArr['branch_id']);
		header("location:".$pageURL."?updatesuccess");
		exit;
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		
		.form-control{
            border-bottom: 1px solid blue;
            width:200px!important;
        }
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

	</style>

</head>

<body>



    <div class='loading_wrapper' style="display: none;">

        <div class='loadertext1'>Please wait while we upload your files...</div>

    </div>

    <div class="main-wrapper">

        <!-- Header -->

        <?php include("include/header.php"); ?>

        <!-- /Header -->



        <!-- Sidebar -->

        <?php include("include/sidebar.php"); ?>

        <!-- /Sidebar -->



        <!-- Page Wrapper -->

        <div class="page-wrapper">



            <!-- Page Content -->

            <div class="content container-fluid">



                <!-- Page Header -->

                <div class="page-header">

                    <div class="row align-items-center">

                        <div class="col">

                            <h3 class="page-title"><?php echo $pageName; ?></h3>

                            <ul class="breadcrumb">

                                <li class="breadcrumb-item">Trasaction</li>


                                <?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

                                <li class="breadcrumb-item"><?php echo $pageName; ?></li>

                                <li class="breadcrumb-item active">

                                    <?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

                                </li>

                                <?php } else { ?>

                                <li class="breadcrumb-item active"><?php echo $pageName; ?></li>

                                <?php } ?>

                            </ul>

                        </div>

                    </div>

                </div>

                <!-- /Page Header -->



                <?php if(isset($_GET['registersuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['registerfail'])){ ?>

                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['updatesuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

                </div><br />

                <?php } ?>



                <?php if(isset($_GET['updatefail'])){ ?>

                <div class="alert alert-danger alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong>
                    <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

                </div>

                <?php } ?>



                <?php if(isset($_GET['deletesuccess'])){ ?>

                <div class="alert alert-success alert-dismissible" role="alert">

                    <button type="button" class="close" data-dismiss="alert"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

                </div><br />

                <?php } ?>


				<?php include("include/tracking.php"); ?>


                <br>
				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<h4 class="card-title mb-0"><?php if(isset($_GET['edit'])) {

										echo 'Edit '.$pageName;

									} else {

										echo 'Add New '.$pageName;

									}

									?></h4>

								</div>

								<div class="card-body">
								
									<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

											<div class="table-responsive">

												<table class="table table-bordered" id="components">

													<thead style="background: #2980b9;color: #fff;">

														<tr>

															<th>#</th>
															<th>CUSTOMER NAME</th>
															<th>P.O. NO.</th>
															<th>PROJECT NO.</th>
															<th>PRODUCT / TITLE / ITEM NAME</th>
															<th>ITEM CODE</th>
															<th>BOOKING DATE</th>
															<th>TARGET DATE</th>
															<th colSpan='2' style="text-align:center">ORDER QTY</th>
															<th>PC SHADE</th>
															<th>REMARK FROM CUSTOMER</th>
															<th>CATEGORY (CODE NAME)</th>
															<th>Delivery Site</th>
															<th>Order Status</th>

														</tr>


													</thead>

													<tbody>


														<tr>

															<td>

																1

															</td>

														

															<td>

																<select name="customer_name" class="form-control form-control-sm select2 customer_name">

																	<option value="">Select Customer Name</option>

																	<?php while ($row = $admin->fetch($customer)) {?>

																		<option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) && $data['customer_name'] == $row['id']) { echo 'selected'; } ?>><?php echo $row['customer_short_name']; ?></option>

																	<?php } ?>
																
																</select>

															</td>

															<td>

																<input type="text" name="po_no" id="po_no" value="<?php if(isset($_GET['edit'])) { echo $data['po_no']; } ?>" class="form-control form-control-sm">

															</td>


															<td>

																<input type="text" name="project_no" id="project_no" value="<?php if(isset($_GET['edit'])) { echo $data['project_no']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<input type="text" name="item_name" id="item_name" value="<?php if(isset($_GET['edit'])) { echo $data['item_name']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<input type="text" name="item_code" id="item_code" value="<?php if(isset($_GET['edit'])) { echo $data['item_code']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<input type="date" name="booking_date" id="booking_date" value="<?php if(isset($_GET['edit'])) { echo $data['booking_date']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<input type="date" name="target_date" id="target_date" value="<?php if(isset($_GET['edit'])) { echo $data['target_date']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<input type="number" name="order_qty" id="order_qty" value="<?php if(isset($_GET['edit'])) { echo $data['order_qty']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<select class="form-control form-control-sm select2" name="order_unit">
																	<option value="">Select Order Unit</option>
																	<?php while ($row = $admin->fetch($allUnit)) { ?>
																		<option value="<?php echo $row['unitcode']; ?>" <?php if(isset($_GET['edit']) && $data['order_unit'] == $row['unitcode']) { echo 'selected'; } ?>><?php echo $row['unitcode']; ?></option>
																	<?php } ?>
																</select>

															</td>

															<td>

																<input type="text" name="pc_shade" id="pc_shade" value="<?php if(isset($_GET['edit'])) { echo $data['pc_shade']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<input type="text" name="customer_remark" id="customer_remark" value="<?php if(isset($_GET['edit'])) { echo $data['customer_remark']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<select name="category" class="form-control form-control-sm select2 category">

																	<option value="">Select Category</option>

																	<?php while ($row = $admin->fetch($boq)) {

																		$itemName = $admin->getUniqueItemById($row['item_name']);
																		
																	?>

																		<option value="<?php echo $row['id']; ?>" <?php if(isset($_GET['edit']) && $data['category'] == $row['id']) { echo 'selected'; } ?>><?php echo $itemName['item_name']; ?></option>

																	<?php } ?>

																</select>

															</td>


															<td>

																<input type="text" name="delivery_site" id="delivery_site" value="<?php if(isset($_GET['edit'])) { echo $data['delivery_site']; } ?>" class="form-control form-control-sm">

															</td>

															<td>

																<select class="form-control form-control-sm " name="order_status">
																	<option value="">Select Order Status</option>
																	<option value="Urgent" <?php if(isset($_GET['edit']) and $data['order_status']=='Urgent') { echo 'selected'; } ?>>Urgent</option>
																	<option value="Placed Order" <?php if(isset($_GET['edit']) and $data['order_status']=='Placed Order') { echo 'selected'; } ?>>Placed Order</option>
																</select>

															</td>


														</tr>

													</tbody>

												</table>

											</div>

										<br>
										<div class="form-actions text-left">

											<input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

											<?php if(isset($_GET['edit'])){ ?>

												<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

												<button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button>

											<?php } else { ?>

												<button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

											<?php } ?>

										</div>

									</form>

								</div>

							</div>

						</div>

					</div>

				<?php } ?>

            </div>

            <!-- /Page Content -->

        </div>

        <!-- /Page Wrapper -->

    </div>

    <!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					customer_name: {
						required: true,
					},
                    po_no: {
						required: true,
					},

                    project_no: {
						required: true,
					},

					item_name: {
						required: true,
					},

					booking_date: {
						required: true,
					},

					target_date: {
						required: true,
					},
					order_qty:{
						required: true,
						number: true,
					},
					pc_shade: {
						required: true,
					},
					customer_remark: {
						required: true,
					},
					category: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function(){

				if($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

			$('.select2').select2();


			$(".add-more").on("click", function(){
                
				var count = $('#components > tbody > tr').length;
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxAddMan.php',
                    success: function (services_clone) {

                        $("#components tr:last").after(services_clone);

                    }
                });

			});

		});


		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));
				
			}
			
		}
		// maingroupcode($('#inp-text-1'));
		// roupcode1($('#inp-text-2'));

		function remove(e) {

			$(e).parent().parent().remove();

		}
				
	</script>

</body>

</html>