<?php 
   
   include_once 'include/config.php';
   
   include_once 'include/admin-functions.php';
   
   $admin = new AdminFunctions();

// if (isset($_POST['itemName'])) {
    
//     $gaugesNo = $admin-> getAllGaugesComponentMaster($_POST);
    
// }



    if(!$loggedInUserDetailsArr = $admin->sessionExists()){

        header("location: admin-login.php");

        exit();

    }

    if (isset($_POST['itemName'])) {

        $itemId = $_POST['itemName'];

        $drawingNo = $admin-> getAllDrawingComponentMaster($itemId,$loggedInUserDetailsArr['branch_id']);

    }

?>

<?php 

    $x = 0;

    $i = 1;


    while ($row = $admin->fetch($drawingNo)) { 

        $itemName = $admin-> getUniqueItemNameById($row['component_name'],$loggedInUserDetailsArr['branch_id']);

        $toolName    = $admin->getAllFromBomTool($row['component_name'],$itemId,$loggedInUserDetailsArr['branch_id']); 

        $InspectionGaugesComponent = $admin->getAllFromBomInspectionGaugesComponent($row['component_name'],$itemId,$loggedInUserDetailsArr['branch_id']); 


?>

    <tr>

        <td>

            <?php echo $i;?>

        </td>

        <td>

            <input type="hidden" name="component[<?php echo $x;?>]" value ="<?php echo $row['component_name'];?>" class="form-control form-control-sm component boxSize" required readonly>

            <input type="text" class="form-control form-control-sm component boxSize" value ="<?php echo $itemName;?>" required readonly>

        </td>

        <td>

            <input type="text" name="open_no[<?php echo $x;?>]" class="form-control form-control-sm open_no boxSize" onkeyup="openNo(this)" required>

        </td>

        <td>

            <input type="text" name="open[<?php echo $x;?>]" class="form-control form-control-sm open boxSize" required>

        </td>

        <td>

            <input type="file" onchange="videoupload(this)" name="video[<?php echo $x;?>]"  class="form-control-sm" required>

        </td>

        <td>

            <input type="file" class="form-control-sm check_sheet" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['check_sheet'])){ echo "required"; } }else{ echo "required"; }?> name="check_sheet[<?php echo $x;?>]" id="check_sheet<?php echo $x;?>" data-image-index="<?php echo $x;?>" />

        </td>

        <td>

            <input type="text" name="tool_setup_time[<?php echo $x;?>]" class="form-control form-control-sm tool_setup_time boxSize" required>

        </td>

        <td>

            <input type="text" name="cycle_time[<?php echo $x;?>]" class="form-control form-control-sm cycle_time boxSize" required>

        </td>

        <td>

            <select class="form-control form-control-sm select2 boxSize" name="mc_req[<?php echo $x;?>]">

                <option value="">Select Machine Name</option>
                <?php $machineName = $admin->getAllMachineName(); while ($row = $admin->fetch($machineName)) { ?>
                    <option value="<?php echo $row['id']; ?>" ><?php echo $row['machine']; ?></option>
                <?php } ?>

            </select>

        </td>

        <td>

            <input type="text" name="man_req[<?php echo $x;?>]" class="form-control form-control-sm man_req boxSize" required>

        </td>

        <td>

            <select class="form-control form-control-sm select2 boxSize tool" name="tool[<?php echo $x;?>]"  data-no="<?php echo $x;?>" onchange="checkSheetToolPhoto(this)" required>
                
                <option value="">Select Tool No</option>

                <?php 
                    
                    while ($row2 = $admin->fetch($toolName)) { 
                
                ?>
                    
                    <option value="<?php echo $row2['id']; ?>"><?php echo $row2['tool_number']; ?></option>
                
                <?php } ?>

            </select>

        </td>	

        <td class="check_sheet_tool<?php echo $x;?>">


        </td>

        <td>

            <select class="form-control form-control-sm select2 boxSize quality_gauge" name="quality_gauge[<?php echo $x;?>]" data-no="<?php echo $x;?>" onchange="checkSheetJigsPhoto(this)" required>
            
            <option value="">Select Quality Gauge</option>

            <?php 

                while ($row2 = $admin->fetch($InspectionGaugesComponent)) { 

            ?>
				
                <option value="<?php echo $row2['id']; ?>"><?php echo $row2['gauges_number']; ?></option>
																	
            <?php } ?>

            </select>

        </td>	

        <td class="check_sheet_jigs<?php echo $x;?>">


        </td>

        <td>

            <input type="text" name="pokayoke[<?php echo $x;?>]" class="form-control form-control-sm pokayoke boxSize" required>

        </td>	

        <td>


            <input type="file" class="form-control-sm pokayoke_photo" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['pokayoke_photo'])){ echo "required"; } }else{ echo "required"; }?> name="pokayoke_photo[<?php echo $x;?>]" id="pokayoke_photo<?php echo $x;?>" data-image-index="<?php echo $x;?>" />


        </td>

        <td>

            <input type="date" name="last_done_date[<?php echo $x;?>]" class="form-control form-control-sm last_done_date boxSize">

        </td>	

        <td>

            <input type="date" name="next_due_date[<?php echo $x;?>]" class="form-control form-control-sm next_due_date boxSize" >

        </td>

        <td>

            <input type="text" name="remark[<?php echo $x;?>]" class="form-control form-control-sm remark boxSize" required>

        </td>	

        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>	

    </tr>

<?php $x++;$i++; } ?>


<script>
			$('.select2').select2();

            function remove(e) {

$(e).parent().parent().remove();

}

function photoupload(e){

let photoSize = e.files[0].size;
if (photoSize > 5000000) {
    alert('Photo Size Not Greater Than 5 mb ');

    $(e).val("");

}
}

function videoupload(e){

let videoSize = e.files[0].size;
if (videoSize > 5000000) {
    
    alert('Video Size Not Greater Than 5 MB ');

    $(e).val("");

}
}

</script>