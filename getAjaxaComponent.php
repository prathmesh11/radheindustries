<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();
// $locationResult = $admin-> getAllContactPersonDetails();


$count=$_POST['count'];
// $itemName=$_POST['itemName'];

?>



<?php 

$y=$count+1;

for ($x=$count; $x < $count+6; $x++) { 

?>


    <tr>

        <td>

            <?php echo $y;?>

        </td>

        <td>

            <select name="component[<?php echo $count;?>]"
                class="form-control form-control-sm select2 component boxSize" required>

            </select>

        </td>

        <td>

            <input type="text" name="open_no[<?php echo $count;?>]" class="form-control form-control-sm open_no boxSize"
                onkeyup="openNo(this)" required>

        </td>

        <td>

            <input type="text" name="open[<?php echo $count;?>]" class="form-control form-control-sm open boxSize"
                required>

        </td>

        <td>

            <input type="file" onchange="videoupload(this)" name="video[<?php echo $count;?>]" class="form-control-sm"
                required>

        </td>

        <td>

            <input type="file" class="form-control-sm check_sheet" onchange="photoupload(this)"
                <?php if(isset($_GET['edit'])){if(empty($data['check_sheet'])){ echo "required"; } }else{ echo "required"; }?>
                name="check_sheet[<?php echo $count;?>]" id="check_sheet<?php echo $count;?>"
                data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="text" name="tool_setup_time[<?php echo $count;?>]"
                class="form-control form-control-sm tool_setup_time boxSize" required>

        </td>

        <td>

            <input type="text" name="cycle_time[<?php echo $count;?>]"
                class="form-control form-control-sm cycle_time boxSize" required>

        </td>

        <td>

            <select class="form-control form-control-sm select2 boxSize" name="mc_req[<?php echo $count;?>]">

                <option value="">Select Machine Name</option>
                <?php $machineName = $admin->getAllMachineName(); while ($row = $admin->fetch($machineName)) { ?>
                <option value="<?php echo $row['id']; ?>"><?php echo $row['machine']; ?></option>
                <?php } ?>

            </select>

        </td>

        <td>

            <input type="text" name="man_req[<?php echo $count;?>]" class="form-control form-control-sm man_req boxSize"
                required>

        </td>

        <td>

            <select class="form-control form-control-sm select2 boxSize tool" name="tool[<?php echo $count;?>]"
                onchange="checkSheetToolPhoto(this)" required>

            </select>

        </td>

        <td class="check_sheet_tool">


        </td>

        <td>

            <select class="form-control form-control-sm select2 boxSize quality_gauge"
                name="quality_gauge[<?php echo $count;?>]" onchange="checkSheetJigsPhoto(this)" required>

            </select>

        </td>

        <td class="check_sheet_jigs">


        </td>

        <td>

            <input type="text" name="pokayoke[<?php echo $count;?>]"
                class="form-control form-control-sm pokayoke boxSize" required>

        </td>

        <td>


            <input type="file" class="form-control-sm pokayoke_photo" onchange="photoupload(this)"
                <?php if(isset($_GET['edit'])){if(empty($data['pokayoke_photo'])){ echo "required"; } }else{ echo "required"; }?>
                name="pokayoke_photo[<?php echo $count;?>]" id="pokayoke_photo<?php echo $count;?>"
                data-image-index="<?php echo $count;?>" />


        </td>

        <td>

            <input type="date" name="last_done_date[<?php echo $count;?>]"
                class="form-control form-control-sm last_done_date boxSize">

        </td>

        <td>

            <input type="date" name="next_due_date[<?php echo $count;?>]"
                class="form-control form-control-sm next_due_date boxSize">

        </td>

        <td>

            <input type="text" name="remark[<?php echo $count;?>]" class="form-control form-control-sm remark boxSize"
                required>

        </td>

        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>

    </tr>


<?php $y++;} ?>														



 <script>

$(document).ready(function () {
    $('.select2').select2();
    // componentName($('.component_name'));
    // openNo($('.open_no'));
    itemName($('#itemNames'));


});

function remove(e) {

    $(e).parent().parent().remove();

}

function photoupload(e) {

    let photoSize = e.files[0].size;
    if (photoSize > 5000000) {
        alert('Photo Size Not Greater Than 5 mb ');

        $(e).val("");

    }
}

function videoupload(e) {

    let videoSize = e.files[0].size;
    if (videoSize > 5000000) {

        alert('Video Size Not Greater Than 5 MB ');

        $(e).val("");

    }
}

function openNo(e) {

    let openNo = $(e).val();
    let counter = 0;
    let component1 = $(e).parent().parent().find('.component').val();

    $('#components > tbody > tr').each(function () {

        let component = $(this).find('.component').val();

        let open_no = $(this).find('.open_no').val();

        if (component == component1 && openNo == open_no && counter == 1) {

            alert('Componet And Open No Are Same Pls Change');
            $(e).val("");

        } else {

            if (component == component1 && openNo == open_no && counter == 0) {

                counter++;

            }

        }

    });

}



function itemName(e) {

    let itemName = $(e).val();


    if (itemName != '') {

        $.ajax({

            type: "POST",
            data: 'itemName=' + itemName,
            url: 'getAjaxDrawingNoBom.php',
            cache: false,
            success: function (res) {

                $('#components > tbody > tr').each(function () {
                    $(this).find('.component_drawing_no').html(res);

                });

                $.ajax({

                    type: "POST",
                    data: 'itemName=' + itemName,
                    url: 'getAjaxToolBom.php',
                    cache: false,
                    success: function (res1) {
                        $('#processFlow > tbody > tr').each(function () {
                            $(this).find('.tool').html(res1);

                        });

                        $.ajax({

                            type: "POST",
                            data: 'itemName=' + itemName,
                            url: 'getAjaxGaugesBom.php',
                            cache: false,
                            success: function (res2) {
                                $('#processFlow > tbody > tr').each(function () {
                                    $(this).find('.quality_gauge ').html(res2);

                                });
                            }

                        });

                    }

                });

            }

        })

    }
}

function checkSheetToolPhoto(e) {

    let checkSheetToolPhoto = $(e).val();

    if (checkSheetToolPhoto != '') {

        $.ajax({

            type: "POST",
            data: 'checkSheetToolPhoto=' + checkSheetToolPhoto,
            url: 'getAjaxCheckSheetToolPhotoBom.php',
            cache: false,
            success: function (res) {

                $(e).parent().parent().find('.check_sheet_tool').html(res);


            }

        });

    }

}

function checkSheetJigsPhoto(e) {

    let checkSheetJigsPhoto = $(e).val();

    if (checkSheetJigsPhoto != '') {

        $.ajax({

            type: "POST",
            data: 'checkSheetJigsPhoto=' + checkSheetJigsPhoto,
            url: 'getAjaxCheckSheetJigsPhotoBom.php',
            cache: false,
            success: function (res) {

                $(e).parent().parent().find('.check_sheet_jigs').html(res);

            }

        });

    }

}

    // function hardwareReqQty(e) {

    //     let produceQty = parseFloat($(e).val());
    //     let dataCount = $(e).attr('data-count');
    //     let itemQty = parseFloat($('#itemQty').val());

    //     let total = 0;

    //     if (!isNaN(produceQty) && !isNaN(itemQty)) {

    //         total = itemQty / produceQty;

    //         $('#required_qtys' + dataCount + '').val(total.toFixed(2));
    //     }

    // }
 </script>