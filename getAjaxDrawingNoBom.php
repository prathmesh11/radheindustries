<?php 
    
    include_once 'include/config.php';
    
    include_once 'include/admin-functions.php';
    
    $admin = new AdminFunctions();

    if(!$loggedInUserDetailsArr = $admin->sessionExists()){

        header("location: admin-login.php");

        exit();

    }

    if (isset($_POST['itemName'])) {

        $itemId = $_POST['itemName'];
        
        $drawingNo = $admin-> getAllDrawingComponentMaster($itemId,$loggedInUserDetailsArr['branch_id']);
        
    }


?>

<?php 

    $x = 0;

    $i = 1;


    while ($row = $admin->fetch($drawingNo)) { 

        $itemName = $admin-> getUniqueItemNameById($row['component_name'],$loggedInUserDetailsArr['branch_id']);


?>

    <tr>

        <td>

            <?php echo $i;?>

        </td>

        <td>

            <input type="hidden" name="components[<?php echo $x;?>]" value ="<?php echo $row['component_name'];?>" class="form-control form-control-sm component boxSize" required readonly>

            <input type="text" class="form-control form-control-sm component boxSize" value ="<?php echo $itemName;?>" required readonly>

        </td>

        <td>        

            <input type="text" name="component_drawing_no[<?php echo $x;?>]" value ="<?php echo $row['component_drawing_no'];?>" class="form-control form-control-sm component boxSize" required readonly>

        </td>

        <td class="componentPhoto<?php echo $x;?>">

        <?php
            if(!empty($row['drawing_photo'])){ 
        ?>

            <a target="_blank_" href="/drawingmasterimage/<?php echo $row['drawing_id'].'/'.$row['drawing_photo'];?>"  class="btn btn-info btn-sm">Check Sheet Photo</a>

        <?php
            }
        ?>

        </td>

        <td>

            <input type="text" name="qty_per_piece[<?php echo $x;?>]" class="form-control form-control-sm qty_per_piece boxSize" required>

        </td>

        <td>

            <input type="text" name="net_weight[<?php echo $x;?>]" class="form-control form-control-sm net_weight boxSize" required>

        </td>

        <td>

            <input type="text" name="category[<?php echo $x;?>]" class="form-control form-control-sm category boxSize" required>

        </td>	

        <td>

            <input type="text" name="component_remarks[<?php echo $x;?>]" class="form-control form-control-sm component_remarks boxSize" required>

        </td>


        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove1(this)">Remove</buuton>

        </td>	

    </tr>

    
<?php $x++;$i++; } ?>

<script>
			$('.select2').select2();

            function remove(e) {

$(e).parent().parent().remove();

}

function photoupload(e) {

let photoSize = e.files[0].size;
if (photoSize > 5000000) {
    alert('Photo Size Not Greater Than 5 mb ');

    $(e).val("");

}
}

</script>