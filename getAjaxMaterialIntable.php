<?php 

    include_once 'include/config.php';

    include_once 'include/admin-functions.php';

    $admin = new AdminFunctions();


    $itemName = $admin->getAllItemFinishGood();


    $count = $_POST['count'];

?>
<tr>

    <td>

        <?php echo $count+1;?>

    </td>

    <td>


        <select class="form-control form-control-sm select2 item_name" name="item_name[<?php echo $count;?>]"
            onchange="componentName(this)">

            <option value="">Select Item Name</option>

            <?php while ($row = $admin->fetch($itemName)) { ?>

            <option value="<?php echo $row['id']; ?>"
                <?php if(isset($_GET['edit']) && $data['item_name'] == $row['id']) { echo 'selected'; } ?>>
                <?php echo $row['item_name']; ?></option>

            <?php } ?>

        </select>

    </td>

    <td>

        <select class="form-control form-control-sm select2 component_name"
            name="component_name[<?php echo $count; ?>]">

        </select>

    </td>

    <td>

        <input type="number" name="material_in_qty[<?php echo $count;?>]"
            class="form-control form-control-sm material_in_qty boxSize" required>

    </td>

</tr>

            
 <script>

    $(document).ready(function () {

        $('.select2').select2();
        //componentName('.item_name');


    });

//     function componentName(e) {

// let itemId = $(e).val();

// if (itemId != '') {

//     $.ajax({

//         type: 'POST',
//         data: 'itemId=' + itemId,
//         url: 'getAjaxMaterialInComponentName.php',

//         success: function (services_clone) {

//             $(e).parent().parent().find('.component_name').html(services_clone);

//         }

//     })

// } else {

//     $("#components > tbody").html(' ');

// }

// }
   
 </script>