<?php 
include_once 'include/config.php';
include_once 'include/admin-functions.php';
$admin = new AdminFunctions();

$count=$_POST['count'];
?>
    <tr>

        <td>

            <?php echo $count+1;?>

        </td>

        <td>

            <input type="text" name="process_name[<?php echo $count;?>]" class="form-control form-control-sm process_name boxSize"
                required>

        </td>

        <td>

            <input type="text" name="inspect_gauges[<?php echo $count;?>]"
                class="form-control form-control-sm inspect_gauges boxSize" required>

        </td>

        <td>

            <input type="text" name="gauges_number[<?php echo $count;?>]" class="form-control form-control-sm gauges_number boxSize"
                >

        </td>

        <td>

            <input type="file" class="form-control-sm gauges_photo" onchange="photoupload(this)" name="gauges_photo[<?php echo $count;?>]" data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="file" class="form-control-sm gauges_check_list" onchange="photoupload(this)"
                name="gauges_check_list[<?php echo $count;?>]" id="<?php echo $count;?>" data-image-index="<?php echo $count;?>" />

        </td>

        <td>

            <input type="text" name="calibration_frequency[<?php echo $count;?>]"
                class="form-control form-control-sm calibration_frequency boxSize">

        </td>

        <td>

            <input type="date" name="last_calibration_date[<?php echo $count;?>]"
                class="form-control form-control-sm last_calibration_date boxSize">

        </td>

        <td>

            <input type="date" name="next_calibration_date[<?php echo $count;?>]"
                class="form-control form-control-sm next_calibration_date boxSize">

        </td>

        <td>

            <input type="text" name="remark[<?php echo $count;?>]" class="form-control form-control-sm remark boxSize">

        </td>

        <td>

            <button class="btn btn-sm btn-danger remover" onclick="remove(this)">Remove</buuton>

        </td>

    </tr>

 <script>

    function remove(e) {

        $(e).parent().parent().remove();

    }

    function photoupload(e) {

        let photoSize = e.files[0].size;
        if (photoSize > 5000000) {
            alert('Photo Size Not Greater Than 5 mb ');

            $(e).val("");

        }
    }
 </script>