<?php

include_once 'include/config.php';

include_once 'include/admin-functions.php';

$admin = new AdminFunctions();



if(!$loggedInUserDetailsArr = $admin->sessionExists()){
	header("location: admin-login.php");
	exit();
}

$pageName = "PPEP Master";
$pageURL = 'ppep-master.php';
$deleteURL = 'ppep-master.php';
$tableName = 'ppep_master';

include_once 'csrf.class.php';

$csrf = new csrf();
$token_id = $csrf->get_token_id();
$token_value = $csrf->get_token($token_id);


$results = $admin->query("SELECT * FROM ".PREFIX.$tableName."  WHERE deleted_time=0");



if(isset($_GET['delId']) && !empty($_GET['delId'])){
	$id = $admin->escape_string($admin->strip_all($_GET['delId']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET deleted_by = '".$loggedInUserDetailsArr['id']."', deleted_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?deletesuccess");
	exit();
}

if(isset($_GET['activate']) && !empty($_GET['activate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['activate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '1', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?activatesuccess");
	exit();
}

if(isset($_GET['deactivate']) && !empty($_GET['deactivate'])) {
	$id = $admin->escape_string($admin->strip_all($_GET['deactivate']));
	$delete = $admin->query("UPDATE ".PREFIX.$tableName." SET active = '0', updated_by = '".$loggedInUserDetailsArr['id']."', updated_time='".CURRENTMILLIS."' where id = '".$id."'");
	header("location:".$pageURL."?deactivatesuccess");
	exit();
}

if(isset($_POST['register'])){
	if($csrf->check_valid('post')) {
		$result = $admin->addPpepMaster($_POST,$_FILES,$loggedInUserDetailsArr['id']);
        header("location:".$parentPageURL."?registersuccess");
        exit();

	}
}


if(isset($_GET['edit'])){
	$id = $admin->escape_string($admin->strip_all($_GET['id']));
	$data = $admin->getUniquePpepById($id);
    $detailsData = $admin->getUniquePpepComponentById($id);
}



if(isset($_POST['id']) && !empty($_POST['id'])) {
	if($csrf->check_valid('post')) {
		$id = trim($admin->escape_string($admin->strip_all($_POST['id'])));
		$result = $admin->updatePpepMaster($_POST,$_FILES,$loggedInUserDetailsArr['id']);
		header("location:".$parentPageURL."?updatesuccess");
		exit();
	}
}

  //print_r($_POST);

?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Smarthr - Bootstrap Admin Template">
	<meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, management, minimal, modern, accounts, invoice, html5, responsive, CRM, Projects">
	<meta name="author" content="Dreamguys - Bootstrap Admin Template">
	<meta name="robots" content="noindex, nofollow">
	<title><?php echo ADMIN_TITLE ?></title>

	<!-- Favicon -->

	<link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
	<!-- Bootstrap CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Fontawesome CSS -->

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">

	<!-- Lineawesome CSS -->

	<link rel="stylesheet" href="assets/css/line-awesome.min.css">

	<!-- Datatable CSS -->

	<link rel="stylesheet" href="assets/css/dataTables.bootstrap4.min.css">

	<!-- Select2 CSS -->

	<link rel="stylesheet" href="assets/css/select2.min.css">

	<!-- Datetimepicker CSS -->

	<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">

	<!-- Main CSS -->

	<link rel="stylesheet" href="assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

	<!--[if lt IE 9]>

		<script src="assets/js/html5shiv.min.js"></script>

		<script src="assets/js/respond.min.js"></script>

	<![endif]-->

	<!-- Crop Image css -->

	<link href="assets/css/crop-image/cropper.min.css" rel="stylesheet">

	<style>

		.form-control{
            border-bottom: 1px solid blue;
            height:25px!important;
        }
        label{
            font-size:11px;
        }
		.select2-container .select2-selection--single {
            height: 30px;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            top: 31%;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 27px;
        }

	</style>

</head>

<body>



	<div class='loading_wrapper' style="display: none;">

		<div class='loadertext1'>Please wait while we upload your files...</div>

	</div>

	<div class="main-wrapper">

		<!-- Header -->

		<?php include("include/header.php"); ?>

		<!-- /Header -->



		<!-- Sidebar -->

		<?php include("include/sidebar.php"); ?>

		<!-- /Sidebar -->



		<!-- Page Wrapper -->

		<div class="page-wrapper">



			<!-- Page Content -->

			<div class="content container-fluid">



				<!-- Page Header -->

				<div class="page-header">

					<div class="row align-items-center">

						<div class="col">

							<h3 class="page-title"><?php echo $pageName; ?></h3>

							<ul class="breadcrumb">

								<li class="breadcrumb-item">Master</li>


								<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

									<li class="breadcrumb-item"><?php echo $pageName; ?></li>

									<li class="breadcrumb-item active">

										<?php if(isset($_GET['edit'])) {

											echo 'Edit '.$pageName;

										} else {

											echo 'Add New '.$pageName;

										}

										?>

									</li>

								<?php } else { ?>

									<li class="breadcrumb-item active"><?php echo $pageName; ?></li>

								<?php } ?>

							</ul>

						</div>

						<div class="col-auto float-right ml-auto">

							<a href="<?php echo $pageURL; ?>?add" class="btn add-btn"><i class="fa fa-plus"></i> Add <?php echo $pageName; ?></a>

						</div>

					</div>

				</div>

				<!-- /Page Header -->



				<?php if(isset($_GET['registersuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['registerfail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> not added.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark3"></i> <?php echo $pageName; ?> successfully updated.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['updatefail'])){ ?>

					<div class="alert alert-danger alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-close"></i> <strong><?php echo $pageName; ?> not updated.</strong> <?php echo $admin->escape_string($admin->strip_all($_GET['msg'])); ?>.

					</div>

				<?php } ?>



				<?php if(isset($_GET['deletesuccess'])){ ?>

					<div class="alert alert-success alert-dismissible" role="alert">

						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

						<i class="icon-checkmark"></i> <?php echo $pageName; ?> successfully deleted.

					</div><br/>

				<?php } ?>



				<?php if(isset($_GET['add']) || isset($_GET['edit'])) { ?>

					<div class="row">

						<div class="col-md-12">

							<div class="card">

								<div class="card-header">

									<h4 class="card-title mb-0"><?php if(isset($_GET['edit'])) {

										echo 'Edit '.$pageName;

									} else {

										echo 'Add New '.$pageName;

									}

									?></h4>

								</div>

								
								<form action="" id="form" method="post" enctype="multipart/form-data" autocomplete="off">

                            
                                    <div class="card-body" style="padding-top: 10px;padding-bottom: 0px;">

                                        <div class="row ">

                                            <div class="col-sm-4">

                                                <div class="form-group row">

                                                    <label for="product_name" class="col-sm-4 col-form-label">Product Name<em>*</em></label>

                                                    <div class="col-sm-8">

                                                        <input type="text" name="product_name" id="product_name" value="<?php if(isset($_GET['edit'])) { echo $data['product_name']; } ?>" class="form-control form-control-sm">

                                                    </div>

                                                </div>
                                            
                                            </div>

                                            <div class="col-sm-3">

                                                <input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['control_chart'])){ echo "required"; } }else{ echo "required"; }?> name="control_chart" />

                                            
                                            </div>

                                            <div class="col-sm-3">

                                                <input type="file" class="form-control-sm" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($data['pdir'])){ echo "required"; } }else{ echo "required"; }?> name="pdir" />

                                            
                                            </div>


                                            <div class="col-sm-2">
    	
											    <button type="button" id="add-more" class="btn btn-warning add-more btn-sm"><i class="fa fa-plus"></i> Add More </button>
                                            
                                            </div>

                                            <div class="col-sm-4">


												<?php if(isset($_GET['edit'])){ 
													
												?>


													<a class="btn btn-info btn-sm" target="_blank_" href="<?php echo $data['control_chart'];?>">Open Control Chart</a>

													<a class="btn btn-success btn-sm" target="_blank_" href="<?php echo $data['pdir'];?>">Open Pdir</a>


												<?php }  ?>

											</div>
                                        
                                        </div>

                                    </div>


                                    <div class="card-body">

                                        <div class="table-responsive">

                                            <table class="table table-bordered" id="components">

                                                <thead style="background: #2980b9;color: #fff;">

                                                    <tr>

                                                        <th>#</th>
                                                        <th>PROCESS</th>
                                                        <th>SOP</th>
                                                        <th>CHK LIST</th>
                                                        <th>ONLINE QA GRAPH SHEET</th>
                                                        <th>LIST OF POKAYOKE</th>
                                                        <th>TRAININGH SHEET</th>
                                                        <th>REMARK</th>
                                                        <th>Action</th>

                                                    </tr>

                                                </thead>

                                                <?php if(isset($_GET['edit'])) { $i = 0; $y = 1;

													while($row = $admin->fetch($detailsData)) {

												?>

                                                    <tbody>

                                                        <tr>

                                                            <td>

                                                                <?php echo $y;?>

                                                                <input type="hidden" value="<?php echo $row['id']; ?>" name="ppep_component_id[<?php echo $i;?>]" class="form-control form-control-sm">

                                                            </td>

                                                    
                                                            <td>

                                                                <input type="text" name="process[<?php echo $i;?>]" value="<?php echo $row['process']; ?>" class="form-control form-control-sm process" required>

                                                            </td>

                                                            <td>

                                                                <input type="file" class="form-control-sm sop" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['sop'])){ echo "required"; } }else{ echo "required"; }?> name="sop[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

                                                                <?php
                                                                    if(!empty($row['sop'])){ 
                                                                ?>
                                                                    <a target="_blank_" href="<?php echo $row['sop'];?>" class="btn btn-info btn-sm">View Photo</a>
                                                                <?php
                                                                    }
                                                                ?>

                                                            </td>

                                                            <td>

                                                                <input type="file" class="form-control-sm check_list" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['check_list'])){ echo "required"; } }else{ echo "required"; }?> name="check_list[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

                                                                <?php
                                                                    if(!empty($row['check_list'])){ 
                                                                ?>
                                                                    <a target="_blank_" href="<?php echo $row['check_list'];?>" class="btn btn-info btn-sm">View Photo</a>
                                                                <?php
                                                                    }
                                                                ?>

                                                            </td>

                                                            <td>

                                                                <input type="file" class="form-control-sm online_qa_graph" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['online_qa_graph'])){ echo "required"; } }else{ echo "required"; }?> name="online_qa_graph[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

                                                                <?php
                                                                    if(!empty($row['online_qa_graph'])){ 
                                                                ?>
                                                                    <a target="_blank_" href="<?php echo $row['online_qa_graph'];?>" class="btn btn-info btn-sm">View Photo</a>
                                                                <?php
                                                                    }
                                                                ?>

                                                            </td>

                                                            <td>

                                                                <input type="file" class="form-control-sm list_pokayoke" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['list_pokayoke'])){ echo "required"; } }else{ echo "required"; }?> name="list_pokayoke[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

                                                                <?php
                                                                    if(!empty($row['list_pokayoke'])){ 
                                                                ?>
                                                                    <a target="_blank_" href="<?php echo $row['list_pokayoke'];?>" class="btn btn-info btn-sm">View Photo</a>
                                                                <?php
                                                                    }
                                                                ?>

                                                            </td>

                                                            <td>

                                                                
                                                                <input type="file" class="form-control-sm training_sheet" onchange="photoupload(this)" <?php if(isset($_GET['edit'])){if(empty($row['training_sheet'])){ echo "required"; } }else{ echo "required"; }?> name="training_sheet[<?php echo $i;?>]" id="<?php echo $i;?>" data-image-index="<?php echo $i;?>" />

                                                                <?php
                                                                    if(!empty($row['training_sheet'])){ 
                                                                ?>
                                                                    <a target="_blank_" href="<?php echo $row['training_sheet'];?>" class="btn btn-info btn-sm">View Photo</a>
                                                                <?php
                                                                    }
                                                                ?>

                                                            </td>

                                                            <td>

                                                                <input type="text" name="remark[<?php echo $i;?>]" value="<?php echo $row['remark']; ?>" class="form-control form-control-sm remark">

                                                            </td>

                                                            <td>

                                                                <button class="btn btn-sm btn-danger remover" onclick="remove(this)">R</buuton>

                                                            </td>
                                                            
                                                        </tr>

                                                    </tbody>

                                                <?php $y++; $i++; }  } else { ?>

                                                    <?php $y=1;for ($x=0; $x < 10; $x++) { ?>

                                                        <tbody>

                                                            <tr>

                                                                <td>

                                                                    <?php echo $y;?>

                                                                </td>

                                                                <td>

                                                                    <input type="text" name="process[<?php echo $x;?>]" class="form-control form-control-sm process" required>

                                                                </td>

                                                                <td>

                                                                    <input type="file" class="form-control-sm sop" onchange="photoupload(this)" name="sop[<?php echo $x;?>]" />

                                                                </td>

                                                                <td>

                                                                    <input type="file" class="form-control-sm check_list" onchange="photoupload(this)" name="check_list[<?php echo $x;?>]" />

                                                                </td>

                                                                <td>

                                                                    <input type="file" class="form-control-sm online_qa_graph" onchange="photoupload(this)" name="online_qa_graph[<?php echo $x;?>]" />

                                                                </td>

                                                               <td>

                                                                    <input type="file" class="form-control-sm list_pokayoke" onchange="photoupload(this)" name="list_pokayoke[<?php echo $x;?>]" />

                                                                </td>

                                                                <td>

                                                                    <input type="file" class="form-control-sm training_sheet" onchange="photoupload(this)" name="training_sheet[<?php echo $x;?>]" />

                                                                </td>

                                                                <td>

                                                                    <input type="text" name="remark[<?php echo $x;?>]" class="form-control form-control-sm remark" >

                                                                </td>

                                                                <td>

																	<button class="btn btn-sm btn-danger remover" onclick="remove(this)">R</buuton>

																</td>

                                                            </tr>

                                                        </tbody>    


                                                    <?php $y++;} ?>


                                                <?php } ?>


                                            </table>

                                        </div>

                                        <div class="form-actions text-right">

                                            <input type="hidden" name="<?php echo $token_id; ?>" value="<?php echo $token_value; ?>" />

                                            <?php if(isset($_GET['edit'])){ ?>

                                                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id ?>"/>

                                                <button type="submit" name="update" value="update" id="update" class="btn btn-warning"><i class="icon-pencil"></i>Update <?php echo $pageName; ?></button>

                                            <?php } else { ?>

                                                <button type="submit" name="register" id="register" class="btn btn-danger"><i class="icon-signup"></i>Add <?php echo $pageName; ?></button>

                                            <?php } ?>

                                        </div>

                                    </div>

								</form>

                            </div>

                        </div>

                    </div>

                <?php } ?>



				<div class="row">

					<div class="col-md-12">

						<table class="table table-striped custom-table mb-0 datatable datatable-selectable-data">
							<thead>
								<tr>
									<th width="20px">#</th>
									<th>Product Name</th>
                                    <th>Process Name</th>
									<th width="100px">Active</th>
									<th width="10px">Actions</th>
								</tr>
							</thead>
							<tbody>

								<?php

                                    $x = 1;

                                    while($row = $admin->fetch($results)){ 
                                        
                                        $detailsDatas = $admin->getUniquePpepComponentById($row['id']);

                                    
                                ?>

                                        
									<tr>

										<td><?php echo $x++ ?></td>

                                        <td> <?php echo $row['product_name']; ?> </td>

                                        
                                        <td>
                                            <?php while($rows = $admin->fetch($detailsDatas)){ ?>
                                                <?php echo $rows['process'].'<br>'; ?> 
                                            <?php }?>
                                         
                                        </td>
                                        
										<td>
											<div class="badge badge-<?php echo $row['active'] == '1'?'success':'danger'; ?> ml-2"><?php echo $row['active'] == '1'?'Yes':'No'; ?></div>
										</td>
										<td class="text-right">

											<div class="dropdown dropdown-action">

												<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="material-icons">more_vert</i></a>

												<div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(103px, 32px, 0px);">

													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?edit&id=<?php echo $row['id']; ?>" title="Edit">   <i class="fa fa-pencil"></i> Edit</a>

													<?php if($row['active'] == '0') { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?activate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to activate?');" title="Activate"> <i class="fa fa-thumbs-up"></i> Activate </a>

													<?php } else { ?>

														<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?deactivate=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to deactivate?');" title="Deactivate"> <i class="fa fa-thumbs-down"></i> Deactivate </a>

													<?php } ?>

											
													<a class="dropdown-item border-0 btn-transition btn passData" href="<?php echo $pageURL; ?>?delId=<?php echo $row['id']; ?>" onclick="return confirm('Are you sure you want to delete?');" title="Delete"> <i class="fa fa-trash-o"></i> Delete </a>

												</div>

											</div>

										</td>

									</tr>

								<?php } ?>

							</tbody>

						</table>

					</div>

				</div>



			</div>

			<!-- /Page Content -->

		</div>

		<!-- /Page Wrapper -->

	</div>

	<!-- /Main Wrapper -->



	<!-- jQuery -->

	<script src="assets/js/jquery-3.2.1.min.js"></script>



	<!-- Bootstrap Core JS -->

	<script src="assets/js/popper.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>



	<!-- Slimscroll JS -->

	<script src="assets/js/jquery.slimscroll.min.js"></script>



	<!-- Select2 JS -->

	<script src="assets/js/select2.min.js"></script>



	<!-- Datetimepicker JS -->

	<script src="assets/js/moment.min.js"></script>

	<script src="assets/js/bootstrap-datetimepicker.min.js"></script>



	<!-- Datatable JS -->

	<script src="assets/js/jquery.dataTables.min.js"></script>

	<script src="assets/js/dataTables.bootstrap4.min.js"></script>



	<!-- Custom JS -->

	<script src="assets/js/app.js"></script>



	<!-- Validate JS -->

	<script src="assets/js/jquery.validate.js"></script>

	<script src="assets/js/additional-methods.js"></script>



	<!-- Crop Image js -->

	<script src="assets/js/crop-image/cropper.min.js"></script>

	<script src="assets/js/crop-image/image-crop-app.js"></script>



	<script type="text/javascript">

		$(document).ready(function() {

			$("#form").validate({

				rules: {

					product_name: {
						required: true,
					},

                },
					
					
				messages: {

				
				}

		});

			$.validator.addMethod('filesize', function (value, element, param) {

				return this.optional(element) || (element.files[0].size <= param)

			}, 'File size must be less than 2 MB');

			$.validator.addMethod("url", function(value, element) {

				return this.optional(element) || /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(value);

			}, "Please enter a valid link address.");

			$.validator.addMethod("youtube", function(value, element) {

				return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);

			}, "Please enter a valid youtube link address.");



		});

	</script>

	<script>

		$(document).ready(function() {

			$('input[name="image"]').change(function(){

				loadImagePreview(this, (606 / 351));

			});



			$('#register').click(function(){

				if($("#form").valid()) {

					$(".loading_wrapper").show();

				}

			})

			$('.select2').select2();


			$(".add-more").on("click", function(){
                
				var count = $('#components > tbody > tr').length;
                $.ajax({
                    type: 'POST',
                    data: 'count='+count,
                    url: 'getAjaxAddppep.php',
                    success: function (services_clone) {

                        $("#components tr:last").after(services_clone);

                    }
                });

			});

		});


		function removeSpecialChar(e) {

			let removeChar= $(e).val();

			let name = $(e).attr('name');

			let regExpr = /[^a-zA-Z0-9-. ]/g;

			if (/^[a-zA-Z0-9- ]*$/.test(removeChar) == false ) {

				alert('Special Characters Not Allow');
				
				$( "input[name='"+name+"']" ).val(removeChar.replace(regExpr, ""));
				
			}
			
		}

        function photoupload(e) {

            let photoSize = e.files[0].size;
            if (photoSize > 5000000) {
                alert('Photo Size Not Greater Than 5 mb ');

                $(e).val("");

            }
        }


		function remove(e) {

			$(e).parent().parent().remove();

		}
				
	</script>

</body>

</html>